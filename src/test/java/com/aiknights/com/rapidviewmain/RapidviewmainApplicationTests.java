package com.aiknights.com.rapidviewmain;

import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import com.aiknights.com.rapidviewmain.dto.ReportTabDTO;
import com.aiknights.com.rapidviewmain.service.ReportGenerationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
class RapidviewmainApplicationTests {
	
	@Autowired
	private ReportGenerationService reportGenerationService;

	@Test
	void contextLoads() {
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testReportDetails() throws JsonProcessingException
	{
		Map<String,ReportTabDTO> map= reportGenerationService.getReportTabInfoUsingClaimId("126AB127");
		
		
		
        ObjectMapper Obj = new ObjectMapper(); 
        String jsonStr = Obj.writeValueAsString(map); 		
        System.out.println(jsonStr);
		/*
		 * JSONParser parser = new JSONParser(); JSONObject json = (JSONObject)
		 * parser.parse(jsonStr);
		 * 
		 * System.out.println("---"+json.toString());
		 */
 
		
		
		
		Assert.notNull(map);
		System.out.println();
	}
}
