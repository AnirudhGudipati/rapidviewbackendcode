var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#userinfo").html("");
}

function connect() {
//	var socket = new SockJS('/mywebsockets');
    var socket = new SockJS('http://173.208.242.26:8080/rapidviewmain/mywebsockets');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/9', function (greeting) {
        	console.log(greeting);
            showGreeting("MessageID : "+JSON.parse(greeting.body).messageid);
            showGreeting("MessageStatus : "+JSON.parse(greeting.body).messagestatus);
            showGreeting("ForUser : "+JSON.parse(greeting.body).messageforuser);
            showGreeting("MessageTimeStamp : "+JSON.parse(greeting.body).messagetimestamp);
            showGreeting("MessageType : "+JSON.parse(greeting.body).messagetype);
            showGreeting("MessageSubject : "+JSON.parse(greeting.body).messagesubject);
            showGreeting("MessageContent : "+JSON.parse(greeting.body).messagecontent);
        });
        stompClient.subscribe('/topic/broadcast', function (greeting) {
        	console.log(greeting);
            showGreeting("MessageID : "+JSON.parse(greeting.body).messageid);
            showGreeting("MessageStatus : "+JSON.parse(greeting.body).messagestatus);
            showGreeting("ForUser : "+JSON.parse(greeting.body).messageforuser);
            showGreeting("MessageTimeStamp : "+JSON.parse(greeting.body).messagetimestamp);
            showGreeting("MessageType : "+JSON.parse(greeting.body).messagetype);
            showGreeting("MessageSubject : "+JSON.parse(greeting.body).messagesubject);
            showGreeting("MessageContent : "+JSON.parse(greeting.body).messagecontent);
        });
        stompClient.subscribe('/topic/ADMIN', function (greeting) {
        	console.log(greeting);
            showGreeting("MessageID : "+JSON.parse(greeting.body).messageid);
            showGreeting("MessageStatus : "+JSON.parse(greeting.body).messagestatus);
            showGreeting("ForUser : "+JSON.parse(greeting.body).messageforuser);
            showGreeting("MessageTimeStamp : "+JSON.parse(greeting.body).messagetimestamp);
            showGreeting("MessageType : "+JSON.parse(greeting.body).messagetype);
            showGreeting("MessageSubject : "+JSON.parse(greeting.body).messagesubject);
            showGreeting("MessageContent : "+JSON.parse(greeting.body).messagecontent);
        });
        stompClient.subscribe('/topic/3/NotificationRefresh', function (greeting) {
        	console.log(greeting);
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    stompClient.send("/app/uimsg", {}, $("#name").val());
}

//function sendName() {
//stompClient.send("/app/notificationRead", {}, JSON.stringify({"userId": "1", "notification_Object_ID": "1462737477"}));
//}

function showGreeting(message) {
    $("#userinfo").append("<tr><td>" + message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
});