<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content='text/html' charset="utf-8">
<title>Weather Assessment Report</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/static/css/style.css"
	media="all" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/static/canvasjs.min.js"></script>
<script type="text/javascript">

	window.onload = function() {
		function Median(data) {
			return Quartile_50(data);
		}

		function Quartile_25(data) {
			return Quartile(data, 0.25);
		}

		function Quartile_50(data) {
			return Quartile(data, 0.5);
		}

		function Quartile_75(data) {
			return Quartile(data, 0.75);
		}

		function Quartile(data, q) {
			data = Array_Sort_Numbers(data);
			var pos = ((data.length) - 1) * q;
			var base = Math.floor(pos);
			var rest = pos - base;
			if ((data[base + 1] !== undefined)) {
				return data[base] + rest * (data[base + 1] - data[base]);
			} else {
				return data[base];
			}
		}

		function Array_Sort_Numbers(inputarray) {
			return inputarray.sort(function(a, b) {
				return a - b;
			});
		}

		function Array_Sum(t) {
			return t.reduce(function(a, b) {
				return a + b;
			}, 0);
		}

		function Array_Average(data) {
			return Array_Sum(data) / data.length;
		}

		function Array_Stdev(tab) {
			var i, j, total = 0, mean = 0, diffSqredArr = [];
			for (i = 0; i < tab.length; i += 1) {
				total += tab[i];
			}
			mean = total / tab.length;
			for (j = 0; j < tab.length; j += 1) {
				diffSqredArr.push(Math.pow((tab[j] - mean), 2));
			}
			return (Math.sqrt(diffSqredArr.reduce(function(firstEl, nextEl) {
				return firstEl + nextEl;
			}) / tab.length));
		}

		var boxplotdata = ${boxplotdata};
		var labels = [];
		boxplotdata.forEach(function(item) {
			/* console.log("AAA", item); */
			var date = item.beginDate;
			if (labels.indexOf(date) === -1) {
				labels.push(date);
			}
			/* console.log("labels", labels); */

		});
		var findata = [];
		var finsizedata=[];
		for (var i = 0; i < labels.length; i++) {
			/* console.log("inside For", labels[i]); */
			var yaxis = [];
			var ysize =[];
			var ytest = [];
			var ysizetest =[];
			var test = {};
			var sizetest ={};
			var mind, maxd, q1d, q2d, q3d,mins, maxs, q1s, q2s, q3s;
			boxplotdata.forEach(function(obj) {
				if (obj.beginDate === labels[i]) {
					var distance = obj.distance;
					var mag =obj.magnitude;
					yaxis.push(distance);
					ysize.push(mag);
					mind = Math.min.apply(null, yaxis);
					maxd = Math.max.apply(null, yaxis);
					q1d = Quartile_25(yaxis);
					q3d = Quartile_75(yaxis);
					q2d = Quartile_50(yaxis);
					mins = Math.min.apply(null, ysize);
					maxs = Math.max.apply(null, ysize);
					q1s = Quartile_25(ysize);
					q3s = Quartile_75(ysize);
					q2s = Quartile_50(ysize);
				}
			});
			ytest.push(mind);
			ytest.push(q1d);
			ytest.push(q3d);
			ytest.push(maxd);
			ytest.push(q2d);
			ysizetest.push(mins);
			ysizetest.push(q1s);
			ysizetest.push(q3s);
			ysizetest.push(maxs);
			ysizetest.push(q2s);
			test = {
				label : labels[i],
				y : ytest
			};
			sizetest = {
					label : labels[i],
					y : ysizetest
					};
			findata.push(test);
			finsizedata.push(sizetest);
		}
		;
		var chart = new CanvasJS.Chart("chartContainer1", {
			title : {
				text : "Distance from Claim (Miles)"
			},
			axisY : {
				title : "Distance"
			},
			axisX : {
				title : "Event Dates"
			},
			data : [ {
				type : "boxAndWhisker",
				dataPoints : findata
			} ]
		});
		/* console.log("CHART", chart); */
		chart.render();
		var chart = new CanvasJS.Chart("chartContainer2", {
			title : {
				text : "Wind Speed (Mph)"
			},
			axisY : {
				title : "Wind Speed"
			},
			axisX : {
				title : "Event Dates"
			},
			data : [ {
				type : "boxAndWhisker",
				dataPoints : finsizedata
			} ]
		});
		/* console.log("CHART", chart); */
		chart.render();
		setTimeout(getPDF, 1000);
	}
</script>
<style>
body
{
    overflow: hidden;
}
#canvas {
	/* background: #ffffff;
	box-shadow: 5px 5px 5px #ccc;
	border: 5px solid #eee;  */
	margin-bottom: 20px;
	padding-bottom: 20px;
}
</style>
</head>
<body>
	<div class="canvas_div_pdf">
	<div id="weatherReport">
		<header id="header" class="clearfix">
			<div id="logo">
				<img
					src="${pageContext.request.contextPath}/resources/static/images/logo1.png"
					width="193px" height="47px">
			</div>
			<div id="company">
				<h2 class="name">Weather Assessment Report</h2>
				<div></div>
				<div>${ReportGenDateTime} - ${UID}</div>
			</div>
		</header>
		<c:set var="count" value="${weathereventscount *20.8}"></c:set>
			<div>
				<div id="wData" class="claim-info">
					<h1 style="text-align: center">WEATHER ASSESSMENT REPORT</h1>
					<br />
					<div style="float: left; width: 100%;">
						<table style="width: 100%;">
						<tr>
							<td style="width: 20%;"><b>Claim ID</b></td>
							<td style="width: 4%;">:</td>
							<td>${claimid}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Owner Name</b></td>
							<td style="width: 4%;">:</td>
							<td>${ownername}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Insurance Carrier Name</b></td>
							<td style="width: 4%;">:</td>
							<td>${carrier}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Address</b></td>
							<td style="width: 4%;">:</td>
							<td>${address}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Coordinates</b></td>
							<td style="width: 4%;">:</td>
							<td>Latitude : ${latitude}, Longitude : ${longitude}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Date Range</b></td>
							<td style="width: 4%;">:</td>
							<td>${policystartDate}  to ${claimDate}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Report Generated</b></td>
							<td style="width: 4%;">:</td>
							<td>${ReportGenDateTime}</td>
						</tr>
					</table>
					</div>
					
					<div id="hailStormEvent" class='hello'
						style="float: left; width: 100%; height: ${count}%">

						<br />
						<h3 style="text-align: center">WIND STORM EVENTS</h3>
						<table style="width: 100%;">
							<col></col>
							<tr>
								<th style="border-right: 1px solid #5d5b6a; width: 25%;">Date</th>
								<th colspan="4" style="width: 75%; align: center">Estimated
									Max Wind Speed(Mph))</th>
							</tr>
							<tr>
								<th style="border-bottom: 1px solid #5d5b6a;"></th>
								<th style="border: 1px solid #5d5b6a;">0 - 1 mi</th>
								<th style="border: 1px solid #5d5b6a;">1 - 3 mi</th>
								<th style="border: 1px solid #5d5b6a;">3 - 5 mi</th>
								<th style="border: 1px solid #5d5b6a;">5 - 10 mi</th>
							</tr>

							<c:forEach var="wd" items="${wd}">
								<tr>
									<td align="center" style="border-right: 1px solid #5d5b6a;">${wd.eventdate}</td>
									<td align="center" style="border-right: 1px solid #5d5b6a;">${wd.zeroonemag}</td>
									<td align="center" style="border-right: 1px solid #5d5b6a;">${wd.onethreemag}</td>
									<td align="center" style="border-right: 1px solid #5d5b6a;">${wd.threefivemag}</td>
									<td align="center">${wd.fivetenmag}</td>
								</tr>
							</c:forEach>
						</table>
					</div>
					<br /> <br />
				</div>
				
				</div>
				</div>
				<div style="clear:both;"></div>
				<!-- Weather Report Ends here -->
			<div id="afterWeatherReport"></div>
			
			
			<!-- Hail Verification Map Starts from here -->
				<div id="hailVerificationMap">
				<header class="clearfix">
			<div id="logo">
				<img
					src="${pageContext.request.contextPath}/resources/static/images/logo1.png"
					width="193px" height="47px">
			</div>
			<div id="company">
				<h2 class="name">Weather Assessment Report</h2>
				<div></div>
				<div>${ReportGenDateTime} - ${UID}</div>
			</div>
		</header>
		<div>
				<div class="mapdiv" align="center">
				<h2 style="text-align: center">Wind Verification Map</h2>
				<div id="map"
					style="height: 300px; width: 100%; border-radius: 10pt;"></div>
			</div>
			<br />
			<br/>
			<div class="claim-info">
			<br/>
			<br/>	
			<div id="chartContainer1" style="height: 300px; width: 100%;"></div>
			<br/>
			<br/>
			<div id="chartContainer2" style="height: 300px; width: 100%;"></div>
			</div>
			</div>
			</div>
			
			
			<div id="disclaimer"  style="height:1266px;">
			<header class="clearfix">
			<div id="logo">
				<img
					src="${pageContext.request.contextPath}/resources/static/images/logo1.png"
					width="193px" height="47px">
			</div>
			<div id="company">
				<h2 class="name">Weather Assessment Report</h2>
				<div></div>
				<div>${ReportGenDateTime} - ${UID}</div>
			</div>
		</header>
			<div class="claim-info" align="left">
			<div style="margin-left: 1%">
			<p style="font-size: 30px;"><b>Legal Disclaimer</b></p>
			<p style="font-size: 12px;">The AIKnights makes GIS and additional data available as a service on the express condition that users who view, download, transfer or otherwise access or use the GIS and data expressly agree to these Terms of Use. If you do not accept these terms, you may not view or otherwise use the GIS or associated data for any purpose. These restrictions on use of the GIS or other data apply to you and to any transferee, or other end user of yours (collectively, "users").</p>
			<p style="font-size: 12px;">The AIKnights' offered data is expressly provided AS IS from open sources and assumes no responsibility or legal liability concerning the GIS or reported weather data's guarantees of completeness, accuracy, or timeliness. It further assumes no responsibility or liability for any errors or omissions in the weather content of this site. We strive to make the information on this website as timely and accurate as possible, making no claims, promises, or guarantees about the accuracy, completeness, or adequacy of the public data used, and expressly disclaims liability for errors and omissions in the contents of the report.</p> 
			<p style="font-size: 12px;">The information provided using this Web site is only intended to advance the knowledge and awareness of historic weather information related to the identified location which is available to the public.  We believe that this information is provided in good faith.</p>
			<p style="font-size: 12px;">The images of buildings are provided from other sources and used in good faith and 'fair use' to represent the address in the claim per section 107 of the US Copyright Law.</p>
			<p style="font-size: 12px;">This application and its data may be updated at irregular intervals. All users should check for updates regularly and ensure the most current version of the application or its data is being used</p>
			</div>
			</div>
			</div>
			
			<script>
			function initMap() {
				var gmapdata = ${gmapdata};
				var clong = parseFloat(gmapdata[0].HomeLongitude);
				var clat = parseFloat(gmapdata[0].HomeLatitude);
				/* console.log("longlat", clong, clat); */
				var map = new google.maps.Map(document
						.getElementById('map'), {
					zoom : 11,
					center : {
						lat : clat,
						lng : clong
					}
				});
				var myLatLng = {
					lat : clat,
					lng : clong
				};
				var marker = new google.maps.Marker({
					position : myLatLng,
					map : map,
					animation: google.maps.Animation.DROP,
					icon : defaultmarker
				});
				var centerString="<b>Home Location</b><br/>"+"<b>Latitude : </b>"+clat+"<br/>"+"<b>Longitude : </b>"+ clong;
				var infowindow = new google.maps.InfoWindow({
			          content: centerString
			        });
				marker.addListener('click', function() {
			          infowindow.open(map, marker);
			        });
				var defaultmarker ={
						scaledSize: new google.maps.Size(25,25)};
				var image = {
					url : "${pageContext.request.contextPath}/resources/static/images/orangemapmarker.png",
					scaledSize : new google.maps.Size(22, 25)
				};
				gmapdata.forEach(function(markerdata) {
					var yellowmarker = {
						lat : parseFloat(markerdata.Latitude),
						lng : parseFloat(markerdata.Longitude)
					};
					var contentString="<b>Distance from Home : </b>"+markerdata.Distance+"<br/>"+"<b>WindSpeed : </b>"+markerdata.Hailsize+"<br/>"+"<b>Event Date : </b>"+markerdata.eventDate;
					var infowindow = new google.maps.InfoWindow({
				          content: contentString
				        });
					var marker = new google.maps.Marker({
						position : yellowmarker,
						map : map,
						animation: google.maps.Animation.DROP,
						icon : image
					});
					marker.addListener('click', function() {
				          infowindow.open(map, marker);
				        });
				});
			}
			</script>
			<script async defer
				src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8y69GuUs-jLmtu5JWHN0DuyNfnyLYkwA&callback=initMap">
			</script>
			
			
		<%-- <canvas id="canvas" height="250" width="350">
		</canvas> --%>
		<!-- <p style="page-break-before: always"> -->
	</div>
	<!-- <footer>
    Invoice was created on a computer and is valid without the signature and seal.
  </footer> -->
	<script type="text/javascript">
		function getPDF() {
			try {
				var HTML_Width = $(".canvas_div_pdf").width();
				var HTML_Height = $(".canvas_div_pdf").height();
				var top_left_margin = 15;
				var PDF_Width = HTML_Width + (top_left_margin * 2);
				var PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
				var canvas_image_width = HTML_Width;
				var canvas_image_height = HTML_Height;
				var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;
				HTML_Height = (totalPDFPages + 1) * PDF_Height;
				canvas_image_height = HTML_Height;
				
				console.log("HTML_Height" + "  " + HTML_Height);
				
				console.log("PDF_Height" + "  " + PDF_Height);
				
				console.log("canvas_image_height" + "  " + canvas_image_height);
				console.log("totalPDFPages" + totalPDFPages);

				var data = $("#wData").css("height");

				var hailStormHeight = $("#hailStormEvent").css("height");

				var weatherReportHeight = $("#weatherReport").css("height");

				var headerHeight = $("#header").css("height");

				console.log("ddddd" + " " + data + " " + hailStormHeight + " " + weatherReportHeight + " " + headerHeight);
				
				var afterWeatherReportDiv =  $("#afterWeatherReport").position();

				var num = afterWeatherReportDiv.top;

				var afterWeatherDivPosition = parseInt(num.toFixed(0));

				console.log("afterDiv Start" + afterWeatherReportDiv.top + " " + num.toFixed(0));
				
				if(afterWeatherDivPosition / PDF_Height > 0 && afterWeatherDivPosition / PDF_Height < 1){
					var test = PDF_Height * 1;
					var test1 = test - afterWeatherDivPosition;

					$("#afterWeatherReport").height(test1);
					$("#weatherReport").height(afterWeatherDivPosition);
					var afterWeatherReportHeight = $("#afterWeatherReport").css("height");
					var weatherReportHeight = $("#weatherReport").css("height");
					console.log("qqqqqqqqqqqqqqq" + " " + afterWeatherReportHeight + " " + weatherReportHeight);
					
					var hailVerificationMapHeight = $("#hailVerificationMap").css("height");
					var disclaimerDivHeight = $("#disclaimer").css("height");

					console.log("rrrrrrrrrrr" + " " + disclaimerDivHeight);
					var totalSectionsHeight = afterWeatherDivPosition + parseInt(afterWeatherReportHeight) + parseInt(hailVerificationMapHeight) + parseInt(disclaimerDivHeight);
					
					
					console.log("totalSectionsHeight"+ " " + afterWeatherDivPosition + " " + parseInt(afterWeatherReportHeight) + " " + parseInt(hailVerificationMapHeight) + " " + totalSectionsHeight);
					var tempPages = Math.ceil(totalSectionsHeight / PDF_Height);
					console.log("tempPages"+ " " + tempPages);
					var roundedHeight = PDF_Height * tempPages;
					console.log("roundedHeight"+ " " + roundedHeight);
					var heightDiff = roundedHeight - totalSectionsHeight;
					console.log("heightDiff"+ " " + heightDiff);

					$("#hailVerificationMap").height(heightDiff + parseInt(hailVerificationMapHeight));
					var finalHailVerificationMapHeight = $("#hailVerificationMap").css("height");
					console.log("finalHailVerificationMapHeight"+ " " + finalHailVerificationMapHeight);
					var HTML_Height = afterWeatherDivPosition + parseInt(afterWeatherReportHeight) + parseInt(finalHailVerificationMapHeight) + parseInt(disclaimerDivHeight);
				} else if (afterWeatherDivPosition / PDF_Height > 1 && afterWeatherDivPosition / PDF_Height < 2){
					var test = PDF_Height * 2;
					var test1 = test - afterWeatherDivPosition;

					$("#afterWeatherReport").height(test1);
					$("#weatherReport").height(afterWeatherDivPosition);
					var afterWeatherReportHeight = $("#afterWeatherReport").css("height");
					var weatherReportHeight = $("#weatherReport").css("height");
					console.log("qqqqqqqqqqqqqqq" + " " + afterWeatherReportHeight + " " + weatherReportHeight);
					
					var hailVerificationMapHeight = $("#hailVerificationMap").css("height");
					var disclaimerDivHeight = $("#disclaimer").css("height");
					var totalSectionsHeight = afterWeatherDivPosition + parseInt(afterWeatherReportHeight) + parseInt(hailVerificationMapHeight) + parseInt(disclaimerDivHeight);
					
					
					console.log("totalSectionsHeight"+ " " + afterWeatherDivPosition + " " + parseInt(afterWeatherReportHeight) + " " + parseInt(hailVerificationMapHeight) + " " + totalSectionsHeight);
					var tempPages = Math.ceil(totalSectionsHeight / PDF_Height);
					console.log("tempPages"+ " " + tempPages);
					var roundedHeight = PDF_Height * tempPages;
					console.log("roundedHeight"+ " " + roundedHeight);
					var heightDiff = roundedHeight - totalSectionsHeight;
					console.log("heightDiff"+ " " + heightDiff);

					$("#hailVerificationMap").height(heightDiff + parseInt(hailVerificationMapHeight));
					var finalHailVerificationMapHeight = $("#hailVerificationMap").css("height");
					console.log("finalHailVerificationMapHeight"+ " " + finalHailVerificationMapHeight);
					var HTML_Height = afterWeatherDivPosition + parseInt(afterWeatherReportHeight) + parseInt(finalHailVerificationMapHeight) + parseInt(disclaimerDivHeight);

					}else if (afterWeatherDivPosition / PDF_Height > 2 && afterWeatherDivPosition / PDF_Height < 3){
						var test = PDF_Height * 3;
						var test1 = test - afterWeatherDivPosition;

						$("#afterWeatherReport").height(test1);
						$("#weatherReport").height(afterWeatherDivPosition);
						var afterWeatherReportHeight = $("#afterWeatherReport").css("height");
						var weatherReportHeight = $("#weatherReport").css("height");
						console.log("qqqqqqqqqqqqqqq" + " " + afterWeatherReportHeight + " " + weatherReportHeight);
						
						var hailVerificationMapHeight = $("#hailVerificationMap").css("height");
						var disclaimerDivHeight = $("#disclaimer").css("height");
						var totalSectionsHeight = afterWeatherDivPosition + parseInt(afterWeatherReportHeight) + parseInt(hailVerificationMapHeight) + parseInt(disclaimerDivHeight);
						
						
						console.log("totalSectionsHeight"+ " " + afterWeatherDivPosition + " " + parseInt(afterWeatherReportHeight) + " " + parseInt(hailVerificationMapHeight) + " " + totalSectionsHeight);
						var tempPages = Math.ceil(totalSectionsHeight / PDF_Height);
						console.log("tempPages"+ " " + tempPages);
						var roundedHeight = PDF_Height * tempPages;
						console.log("roundedHeight"+ " " + roundedHeight);
						var heightDiff = roundedHeight - totalSectionsHeight;
						console.log("heightDiff"+ " " + heightDiff);

						$("#hailVerificationMap").height(heightDiff + parseInt(hailVerificationMapHeight));
						var finalHailVerificationMapHeight = $("#hailVerificationMap").css("height");
						console.log("finalHailVerificationMapHeight"+ " " + finalHailVerificationMapHeight);
						var HTML_Height = afterWeatherDivPosition + parseInt(afterWeatherReportHeight) + parseInt(finalHailVerificationMapHeight) + parseInt(disclaimerDivHeight);

						}

				console.log("Final HTML" +" " + HTML_Height);
				
				console.log("Final HailVerificationMapHeight" +" " + finalHailVerificationMapHeight);

				totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;
				canvas_image_height = HTML_Height;

				html2canvas($(".canvas_div_pdf")[0], {
					useCORS: true,
					allowTaint : true
				})
						.then(
								function(canvas) {
									canvas.getContext('2d');
									console.log(canvas.height + "  "
											+ canvas.width);
									var imgData = canvas.toDataURL(
											"image/jpeg", 1.0).replace("image/png", "image/octet-stream");
									var pdf = new jsPDF('p', 'pt', [ PDF_Width,
											PDF_Height ]);
									pdf.addImage(imgData, 'JPG',
											top_left_margin, top_left_margin,
											canvas_image_width,
											canvas_image_height);
									for (var i = 1; i <= totalPDFPages; i++) {
										pdf.addPage(PDF_Width, PDF_Height);
										pdf
												.addImage(
														imgData,
														'JPG',
														top_left_margin,
														-(PDF_Height * i)
																+ (top_left_margin * 4),
														canvas_image_width,
														canvas_image_height);
									}
									var test=pdf.output('blob');
									console.log("d1d1d1" + test);
									var formData = new FormData();
									formData.set("files", test,"WeatherAssessment-"+${UID}+".pdf");
									formData.set("userid",${userid});
									formData.set("claimid","${claimid}");
									formData.set("reportType","Wind");
									var xhr = new XMLHttpRequest();
							        xhr.open( 'post', '/rapidviewmain/api/emailPdf', true ); //Post to php Script to save to server
							        xhr.send(formData);
									//pdf.save("WeatherAssessmentReport-"+${UID}+".pdf");
									setTimeout(function() {
										window.open('', '_self').close();
									}, 5000);
								});
			} catch (err) {
				console.log(err);
			}

		};

		$(document).bind("contextmenu", function(e) {
			e.preventDefault();
		});
		$(document).keydown(function(e) {
			if (e.which === 123) {
				return false;
			}
		})
	</script>
</body>
</html>