<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Spring Boot Example</title>
<link href="/bootstrap.min.css" rel="stylesheet">
<script src="/jquery-2.2.1.min.js"></script>
<script src="/bootstrap.min.js"></script>
</head>
<body>
	<div style="padding-left: 10%">
		<img
			src="${pageContext.request.contextPath}/resources/static/images/logo1.png"
			width="193px" height="47px">
		<h3>RAPID VIEW USER EMAIL VERIFICATION</h3>
		<h5>${message}</h5>
		<h5>
			<a href=${url}><strong>Click
					here</strong></a> to view the login page.
		</h5>
	</div>

</body>
</html>