<!DOCTYPE html>
<html>
<style>
body {
	font-family: 'Roboto';
}
</style>
<body>
	<div style="padding-left: 10%">
		<img
			src="${pageContext.request.contextPath}/resources/static/images/logo1.png"
			width="193px" height="47px">
		<h3>YOU DONT HAVE ACCESS TO VIEW THIS PAGE</h3>
		<h6>403 FORBIDDEN ERROR</h6>
		<button onclick="CloseWindow()">Close</button>
	</div>
	<script type="text/javascript">
		function CloseWindow() {
			try {
					window.open('', '_self').close();
			} catch (err) {
				console.log(err);
			}

		};
	</script>
</body>
</html>