<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/static/canvasjs.min.js"></script>
<script src='https://cdn.plot.ly/plotly-latest.min.js'></script>
<script type="text/javascript">

	window.onload = function() {
		function Median(data) {
			return Quartile_50(data);
		}

		function Quartile_25(data) {
			return Quartile(data, 0.25);
		}

		function Quartile_50(data) {
			return Quartile(data, 0.5);
		}

		function Quartile_75(data) {
			return Quartile(data, 0.75);
		}

		function Quartile(data, q) {
			data = Array_Sort_Numbers(data);
			var pos = ((data.length) - 1) * q;
			var base = Math.floor(pos);
			var rest = pos - base;
			if ((data[base + 1] !== undefined)) {
				return data[base] + rest * (data[base + 1] - data[base]);
			} else {
				return data[base];
			}
		}

		function Array_Sort_Numbers(inputarray) {
			return inputarray.sort(function(a, b) {
				return a - b;
			});
		}

		function Array_Sum(t) {
			return t.reduce(function(a, b) {
				return a + b;
			}, 0);
		}

		function Array_Average(data) {
			return Array_Sum(data) / data.length;
		}

		function Array_Stdev(tab) {
			var i, j, total = 0, mean = 0, diffSqredArr = [];
			for (i = 0; i < tab.length; i += 1) {
				total += tab[i];
			}
			mean = total / tab.length;
			for (j = 0; j < tab.length; j += 1) {
				diffSqredArr.push(Math.pow((tab[j] - mean), 2));
			}
			return (Math.sqrt(diffSqredArr.reduce(function(firstEl, nextEl) {
				return firstEl + nextEl;
			}) / tab.length));
		}

		var boxplotdata = ${boxplotdata};
		var labels = [];
		boxplotdata.forEach(function(item) {
			var date = item.beginDate;
			if (labels.indexOf(date) === -1) {
				labels.push(date);
			}

		});
		var findata = [];
		var finsizedata=[];
		for (var i = 0; i < labels.length; i++) {
			var yaxis = [];
			var ysize =[];
			var ytest = [];
			var ysizetest =[];
			var test = {};
			var sizetest ={};
			var mind, maxd, q1d, q2d, q3d,mins, maxs, q1s, q2s, q3s;
			boxplotdata.forEach(function(obj) {
				if (obj.beginDate === labels[i]) {
					var distance = obj.distance;
					var mag =obj.magnitude;
					yaxis.push(distance);
					ysize.push(mag);
					mind = Math.min.apply(null, yaxis);
					maxd = Math.max.apply(null, yaxis);
					q1d = Quartile_25(yaxis);
					q3d = Quartile_75(yaxis);
					q2d = Quartile_50(yaxis);
					mins = Math.min.apply(null, ysize);
					maxs = Math.max.apply(null, ysize);
					q1s = Quartile_25(ysize);
					q3s = Quartile_75(ysize);
					q2s = Quartile_50(ysize);
				}
			});
			ytest.push(mind);
			ytest.push(q1d);
			ytest.push(q3d);
			ytest.push(maxd);
			ytest.push(q2d);
			ysizetest.push(mins);
			ysizetest.push(q1s);
			ysizetest.push(q3s);
			ysizetest.push(maxs);
			ysizetest.push(q2s);
			test = {
				label : labels[i],
				y : ytest
			};
			sizetest = {
					label : labels[i],
					y : ysizetest
					};
			findata.push(test);
			finsizedata.push(sizetest);
		}
		;
		var chart = new CanvasJS.Chart("chartContainer1", {
			title : {
				text : "Distance from Claim (Miles)"
			},
			axisY : {
				title : "Distance"
			},
			axisX : {
				title : "Event Dates"
			},
			data : [ {
				type : "boxAndWhisker",
				dataPoints : findata
			} ]
		});
		console.log("CHART", chart);
		chart.render();
		var chart = new CanvasJS.Chart("chartContainer2", {
			title : {
				text : "Hail Size (Inches)"
			},
			axisY : {
				title : "Hail Size"
			},
			axisX : {
				title : "Event Dates"
			},
			data : [ {
				type : "boxAndWhisker",
				dataPoints : finsizedata
			} ]
		});
		console.log("CHART", chart);
		chart.render();

		var damagedata=${id};
		var damagearray = [];
		damagedata.forEach(function(item){
			if(item.damageSize>0){
		var damsize =item.damageSize;
		}
		damagearray.push(damsize);
			});

		var Damage = {
		  y: damagearray,
		  type: 'box'
		};

		var data = [Damage];

		Plotly.newPlot('myDiv', data);
	}
		
</script>
<meta charset="utf-8" content="initial-scale=1.0, user-scalable=no">
<title>Report-${cd.claimId}</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/static/css/style.css"
	media="all" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<style>
.download-button {
	margin-top: 10px;
	/* border: 1px solid black;
  border-radius: 10pt; */
	float: left;
	height: auto;
	width: 97%;
	margin-left: 10px;
}

#canvas {
	background: #ffffff;
	box-shadow: 5px 5px 5px #ccc;
	border: 5px solid #eee;
	margin-bottom: 20px;
	padding-bottom: 20px;
}
</style>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
    </style>
</head>
<body>
	<div class="canvas_div_pdf">
		<header class="clearfix">
			<div id="logo">
				<img
					src="${pageContext.request.contextPath}/resources/static/images/logo1.png"
					width="193px" height="47px">
			</div>
			<div id="company">
				<h2 class="name">${cd.claimId}-ClaimReport</h2>
				<div>${pd.insuranceCompany}</div>
				<div>${ReportGenDateTime} - ${UID}</div>
				<!-- <div><a href="mailto:company@example.com">company@example.com</a></div> -->
			</div>
		</header>
		<main>
			<div class="claimInfo">
				<p style="font: 15px; font-weight: bold;">Claimant Information</p>
				<div>
					<span>Name:</span> <span>${cd.claimantFirstName}
						${cd.claimantLastName} </span>
				</div>
				<div>
					<span>Address:</span> <span>${cd.address},
						${cd.city},${cd.state}, ${cd.zipCode} </span>
				</div>
				<div>
					<span>Number:</span> <span>${cd.phoneNumber}</span>
				</div>
				<div>
					<span>Email:</span> <span>${cd.email}</span>
				</div>
			</div>
			<div class="policyInfo">
				<div
					style="background-color: black; border-radius: 10pt 10pt 0 0; padding: 0; height: 30px;">
					<span
						style="margin: 0 auto; color: white; text-align: center; display: block; line-height: 1.8rem;">Policy
						Information</span>
				</div>
				<div
					style="padding-top: 5px; padding-bottom: 5px; float: left; width: 50%; text-align: center;">
					<div>Policy Number:</div>
					<div>${pd.policyNumber}</div>
				</div>
				<div
					style="padding-top: 5px; padding-bottom: 5px; float: left; width: 50%; text-align: center;">
					<div>Insurance Company:</div>
					<div>${pd.insuranceCompany}</div>
				</div>
				<div style="clear: both;"></div>
				<div style="float: left; width: 33.3%; text-align: center;">
					<div style="padding: 5px; background-color: black; color: white;">Policy
						Limit</div>
					<div style="padding-top: 5px; padding-bottom: 5px;">30000</div>
				</div>
				<div style="float: left; width: 33.3%; text-align: center;">
					<div style="padding: 5px; background-color: black; color: white;">Deductible</div>
					<div style="padding-top: 5px; padding-bottom: 5px;">${pd.deductibleAmount}</div>
				</div>
				<div style="float: left; width: 33.3%; text-align: center;">
					<div style="padding: 5px; background-color: black; color: white;">Coverage
						Amount</div>
					<div style="padding-top: 5px; padding-bottom: 5px;">${pd.policyCoverage}</div>
				</div>
				<div style="float: left; width: 100%; text-align: center;">
					<div style="padding: 5px; background-color: black; color: white;">Endorsements</div>
					<div style="padding-top: 5px; padding-bottom: 5px;">Issued</div>
				</div>
			</div>
			<div class="claim-info">
				<div
					style="background-color: black; border-radius: 10pt 10pt 0 0; padding: 0; height: 30px;">
					<span
						style="margin: 0 auto; color: white; text-align: center; display: block; line-height: 1.8rem;">
						Claim Overview</span>
				</div>
				<table>
					<tr>
						<td class="lg-td">Estimator</td>
						<td class="sm-td">Roger Hines</td>
					</tr>
					<tr>
						<td class="lg-td">Number</td>
						<td class="sm-td">${cd.phoneNumber}</td>
					</tr>
					<tr>
						<td class="lg-td">Claim Number</td>
						<td class="sm-td">${cd.claimId}</td>
					</tr>
					<tr>
						<td class="lg-td">Claim Type</td>
						<td class="sm-td">${cd.incidentType}</td>
					</tr>
					<tr>
						<td class="lg-td">Recommendation</td>
						<td class="sm-td">10000</td>
					</tr>
					<tr>
						<td class="lg-td">Estimated Amount</td>
						<td class="sm-td">20000</td>
					</tr>
				</table>
				<div style="clear: both; margin-top: 20px;"></div>
				<div style="float: left; width: 25%; text-align: center;">
					<div style="padding: 5px; background-color: black; color: white;">Date
						of Loss</div>
					<div style="padding: 5px;">${cd.incidentDate}</div>
				</div>

				<div style="float: left; width: 25%; text-align: center;">
					<div style="padding: 5px; background-color: black; color: white;">Date
						Received</div>
					<div style="padding: 5px;">${cd.claimDate}</div>
				</div>
				<div style="float: left; width: 25%; text-align: center;">
					<div style="padding: 5px; background-color: black; color: white;">Date
						Inspected</div>
					<div style="padding: 5px;">2020-04-05</div>
				</div>
				<div style="float: left; width: 25%; text-align: center;">
					<div style="padding: 5px; background-color: black; color: white;">Date
						Entered</div>
					<div style="padding: 5px;">${cd.claimDate}</div>
				</div>
			</div>
			<div class="claim-info">
				<h1 style="text-align: center">WEATHER REPORT</h1>
				<br />
				<div style="float: left; width: 100%;">

					<table style="width: 100%;">
						<tr>
							<td style="width: 20%;"><b>Claim ID</b></td>
							<td style="width: 4%;">:</td>
							<td>${cd.claimId}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Insured/Property Owner</b></td>
							<td style="width: 4%;">:</td>
							<td>${cd.claimantFirstName}${cd.claimantLastName}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Address</b></td>
							<td style="width: 4%;">:</td>
							<td>${cd.address},${cd.city}, ${cd.state}. ${cd.zipCode}.</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Coordinates</b></td>
							<td style="width: 4%;">:</td>
							<td>Latitude : ${latitude}, Longitude : ${longitude}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Date Range</b></td>
							<td style="width: 4%;">:</td>
							<td>${pd.startDate}to ${pd.claimDate}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Report Generated</b></td>
							<td style="width: 4%;">:</td>
							<td>${ReportGenDateTime}</td>
						</tr>
					</table>
				</div>
				<c:set var="count" value="${weathereventscount *20.8}"></c:set>
				<div class='hello'
					style="float: left; width: 100%; height: ${count}%">

					<br />
					<h3 style="text-align: center">HAIL STORM EVENTS</h3>
					<table style="width: 100%;">
						<col></col>
						<tr>
							<th style="border-right: 1px solid #5d5b6a; width: 25%;">Date</th>
							<th colspan="4" style="width: 75%; align: center">Estimated
								Max Hail Size</th>
						</tr>
						<tr>
							<th style="border-bottom: 1px solid #5d5b6a;"></th>
							<th style="border: 1px solid #5d5b6a;">0 - 1 mi</th>
							<th style="border: 1px solid #5d5b6a;">1 - 3 mi</th>
							<th style="border: 1px solid #5d5b6a;">3 - 5 mi</th>
							<th style="border: 1px solid #5d5b6a;">5 - 10 mi</th>
						</tr>

						<c:forEach var="wd" items="${wd}">
							<tr>
								<td align="center" style="border-right: 1px solid #5d5b6a;">${wd.eventdate}</td>
								<td align="center" style="border-right: 1px solid #5d5b6a;">${wd.zeroonemag}</td>
								<td align="center" style="border-right: 1px solid #5d5b6a;">${wd.onethreemag}</td>
								<td align="center" style="border-right: 1px solid #5d5b6a;">${wd.threefivemag}</td>
								<td align="center">${wd.fivetenmag}</td>
							</tr>
						</c:forEach>
					</table>
				</div>
				<br /> <br />
			</div>
			<div class="mapdiv" align="center">
				<h2 style="text-align: center">Hail Verification Map</h2>
				<div id="map"
					style="height: 300px; width: 100%; border-radius: 10pt;"></div>
			</div>
			<br />
			<div class="claim-info">
				<br /> <br />
				<div id="chartContainer1" style="height: 300px; width: 100%;"></div>
				<br /> <br />
				<div id="chartContainer2" style="height: 300px; width: 100%;"></div>
			</div>
			<script>
				function initMap() {
					var gmapdata = ${gmapdata};
					var clong = parseFloat(gmapdata[0].HomeLongitude);
					var clat = parseFloat(gmapdata[0].HomeLatitude);
					console.log("longlat", clong, clat);
					var map = new google.maps.Map(document
							.getElementById('map'), {
						zoom : 11,
						center : {
							lat : clat,
							lng : clong
						}
					});
					var myLatLng = {
						lat : clat,
						lng : clong
					};
					var marker = new google.maps.Marker({
						position : myLatLng,
						map : map,
						animation: google.maps.Animation.DROP,
						icon : defaultmarker
					});
					var centerString="<b>Home Location</b><br/>"+"<b>Latitude : </b>"+clat+"<br/>"+"<b>Longitude : </b>"+ clong;
					var infowindow = new google.maps.InfoWindow({
				          content: centerString
				        });
					marker.addListener('click', function() {
				          infowindow.open(map, marker);
				        });
					var defaultmarker ={
							scaledSize: new google.maps.Size(25,25)};
					var image = {
						url : "${pageContext.request.contextPath}/resources/static/images/orangemapmarker.png",
						scaledSize : new google.maps.Size(22, 25)
					};
					gmapdata.forEach(function(markerdata) {
						var yellowmarker = {
							lat : parseFloat(markerdata.Latitude),
							lng : parseFloat(markerdata.Longitude)
						};
						var contentString="<b>Distance from Home : </b>"+markerdata.Distance+"<br/>"+"<b>Hailsize : </b>"+markerdata.Hailsize+"<br/>"+"<b>Event Date : </b>"+markerdata.eventDate;
						var infowindow = new google.maps.InfoWindow({
					          content: contentString
					        });
						var marker = new google.maps.Marker({
							position : yellowmarker,
							map : map,
							animation: google.maps.Animation.DROP,
							icon : image
						});
						marker.addListener('click', function() {
					          infowindow.open(map, marker);
					        });
					});
				}
			</script>
			<script async defer
				src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8y69GuUs-jLmtu5JWHN0DuyNfnyLYkwA&callback=initMap">
    		</script>
			<%-- <div class="claim-info">
				<div
					style="background-color: black; border-radius: 10pt 10pt 0 0; padding: 0; height: 30px;">
					<span
						style="margin: 0 auto; color: white; text-align: center; display: block; line-height: 1.8rem;">
						Risk Assesment</span>
				</div>
				<div></div>
				<div style="float: right; margin-right: 20px; margin-top: 20px;">
					<canvas id="canvas" height="250" width="350">
        </canvas>
				</div>
			</div>
			<div class="financial-assesment">
				<div
					style="background-color: black; border-radius: 10pt 10pt 0 0; padding: 0; height: 30px;">
					<span
						style="margin: 0 auto; color: white; text-align: center; display: block; line-height: 1.8rem;">
						Financial Assesment</span>
				</div>
				<div style="padding: 10px;"></div>
				<div style="clear: both; margin-top: 15px;"></div>
				<div style="float: left; width: 20%; text-align: center;">
					<div style="padding: 14px; background-color: black; color: white;">Description</div>
					<div style="padding: 5px;">$50000</div>
				</div>

				<div style="float: left; width: 20%; text-align: center;">
					<div style="padding: 14px; background-color: black; color: white;">RCV</div>
					<div style="padding: 5px; border-left: solid 1px; height: 70px;">
					</div>
				</div>
				<div style="float: left; width: 20%; text-align: center;">
					<div style="padding: 5px; background-color: black; color: white;">Recoverable
						Depreciation</div>
					<div style="padding: 5px; border-left: solid 1px; height: 70px;">
					</div>
				</div>
				<div style="float: left; width: 20%; text-align: center;">
					<div style="padding: 5px; background-color: black; color: white;">Non
						- Recoverable Depreciation</div>
					<div style="padding: 5px; border-left: solid 1px; height: 70px;">
					</div>
				</div>
				<div style="float: left; width: 20%; text-align: center;">
					<div style="padding: 14px; background-color: black; color: white;">ACV</div>
					<div style="padding: 5px; border-left: solid 1px; height: 70px;">
					</div>
				</div>
			</div>
			<div class="financial-assesment">
				<div
					style="background-color: black; border-radius: 10pt 10pt 0 0; padding: 0; height: 30px;">
					<span
						style="margin: 0 auto; color: white; text-align: center; display: block; line-height: 1.8rem;">
						List of Materials</span>
				</div>
				<div style="padding: 10px;"></div>
				<div style="clear: both; margin-top: 15px;"></div>
				<div style="float: left; width: 14.28%; text-align: center;">
					<div style="padding: 23px; background-color: grey; color: black;">Description</div>
					<div style="padding: 5px;">$50000</div>
				</div>

				<div style="float: left; width: 14.28%; text-align: center;">
					<div
						style="padding: 23px; background-color: grey; color: black; border-left: solid 1px;">Quantity</div>
					<div style="padding: 5px; border-left: solid 1px; height: 70px;">
					</div>
				</div>
				<div style="float: left; width: 14.28%; text-align: center;">
					<div
						style="padding: 23px; background-color: grey; color: black; border-left: solid 1px;">Unit
						Price</div>
					<div style="padding: 5px; border-left: solid 1px; height: 70px;">
					</div>
				</div>
				<div style="float: left; width: 14.28%; text-align: center;">
					<div
						style="padding: 23px; background-color: grey; color: black; border-left: solid 1px;">Taxes</div>
					<div style="padding: 5px; border-left: solid 1px; height: 70px;">
					</div>
				</div>
				<div style="float: left; width: 14.28%; text-align: center;">
					<div
						style="padding: 14px; background-color: grey; color: black; border-left: solid 1px;">Replacement
						Cost Total</div>
					<div style="padding: 5px; border-left: solid 1px; height: 70px;">
					</div>
				</div>
				<div style="float: left; width: 14.28%; text-align: center;">
					<div
						style="padding: 14px; background-color: grey; color: black; border-left: solid 1px;">Less
						Depriciation</div>
					<div style="padding: 5px; border-left: solid 1px; height: 70px;">
					</div>
				</div>
				<div style="float: left; width: 14.28%; text-align: center;">
					<div
						style="padding: 23px; background-color: grey; color: black; border-left: solid 1px;">ACV</div>
					<div style="padding: 5px; border-left: solid 1px; height: 70px;">
					</div>
				</div>
			</div>
			<div class="claim-info">
				<div
					style="background-color: black; border-radius: 10pt 10pt 0 0; padding: 0; height: 30px;">
					<span
						style="margin: 0 auto; color: white; text-align: center; display: block; line-height: 1.8rem;">
						Structural Information</span>
				</div>
				<div
					style="float: left; margin-left: 12px; margin-top: 10px; width: 48%;">
					<div>Roof Style:</div>
					<div>Number of Chimneys:</div>
					<div>Type of Reeof Covering:</div>
					<div>Number of Roof Squares:</div>
					<div style="margin-top: 10px;">
						<img
							src="${pageContext.request.contextPath}/resources/static/images/Hail-damage-1.PNG"
							height="250" width="250">
					</div>
				</div>
				<div style="float: left; margin-left: 12px; margin-top: 10px;">
					<div>Largest Peak:</div>
					<div>Number of Peaks:</div>
					<div>Length of Peaks:</div>
					<div>Length of Valleys:</div>
					<div style="margin-top: 10px;">
						<img
							src="${pageContext.request.contextPath}/resources/static/images/Hail-damage-2.PNG"
							height="250" width="250">
					</div>
				</div>
			</div> --%>
			<div  class="claim-info">
				<h2 style="text-align: center; background-color: white;">
					Surface Damage Details</h2>
			
			<c:forEach var="id" items="${id}">
				<c:if test="${id.sourceImageUrl != null}">
					<c:if test="${id.analysedImageUrl != null}">
					<c:set var="loop" scope="session" value="${loop+1}" />
						<div class="surface-image-details"
							style="background-color: white; width: 97%; border: 1px solid #5d5b6a;">
							<div style="background-color: white; width: 100%;">
								<div style="padding: 10px;font-weight: bold;">Image File Name :
									${id.sourceImageName}</div>
								<div style="margin-left: 10px;">
									<span style="float: left; width: 20%;font-weight: bold;">Face#: <span>
											1</span></span> <span style="float: left; width: 20%;font-weight: bold;">Orientation:
										<span> N</span>
									</span> <span style="float: left; width: 20%;font-weight: bold;">Dimensions: <span>
											45 x 25</span></span> <span style="float: left; width: 20%;font-weight: bold;">Area: <span>
											1125</span></span> <span style="float: left; width: 20%;font-weight: bold;">Slope/Pich:
										<span> 1</span>
									</span>
								</div>
								<div
									style="float: left; margin-left: 12px; margin-top: 10px; width: 34%;">
									<div
										style="text-align: center; border: 1px solid #5d5b6a; width: 248px;font-weight: bold;">Before
										Image</div>
									<div>
										<img src="${id.sourceImageUrl}" alt='Image Not Available'
											height="200" width="250">
									</div>
								</div>
								<div
									style="float: left; margin-left: 12px; margin-top: 10px; width: 34%;">
									<div
										style="text-align: center; border: 1px solid #5d5b6a; width: 248px;font-weight: bold;">After
										Image</div>
									<div>
										<img src="${id.analysedImageUrl}" alt='Image Not Analysed Yet'
											height="200" width="250">
									</div>
								</div>
								<div
									style="float: left; margin-left: 12px; margin-top: 10px; width: 25%;">
									<div style="text-align: center; border: 1px solid #5d5b6a;font-weight: bold;">Analysis</div>
									<table style="width: 100.5%;">
										<tr>
											<td style="font-weight: bold;">Assessed</td>
											<td>:</td>
											<td>${id.classType}</td>
										</tr>
										<tr>
											<td style="font-weight: bold;">ER Score</td>
											<td>:</td>
											<td>${id.detectionScore}</td>
										</tr>
										<tr>
											<td style="font-weight: bold;">Damage Size</td>
											<td>:</td>
											<td>${id.damageSize}inches</td>
										</tr>
										<tr>
											<td style="font-weight: bold;">Shingle Type</td>
											<td>:</td>
											<td>${id.shingleType}</td>
										</tr>
									</table>
								</div>
							</div>
							</div>
	</c:if>
	</c:if>
	</c:forEach>
	</div>
				<c:if test="${loop==null}">
			<div class="surface-image-details"><center><h3>No Images Analyzed yet</h3></center></div>
			</c:if>
			<div class="claim-info" align="center">
			<h2>Damage Detection BoxPlot</h2>
			<div id='myDiv' align="center" style="height: 350px; width: 40%;"></div>
			</div>
			<div class="claim-info" align="left">
			<div style="margin-left: 1%">
			<p style="font-size: 30px;"><b>Legal Disclaimer</b></p>
			<p style="font-size: 12px;">The AIKnights makes GIS and additional data available as a service on the express condition that users who view, download, transfer or otherwise access or use the GIS and data expressly agree to these Terms of Use. If you do not accept these terms, you may not view or otherwise use the GIS or associated data for any purpose. These restrictions on use of the GIS or other data apply to you and to any transferee, or other end user of yours (collectively, "users").</p>
			<p style="font-size: 12px;">The AIKnights' offered data is expressly provided AS IS from open sources and assumes no responsibility or legal liability concerning the GIS or reported weather data's guarantees of completeness, accuracy, or timeliness. It further assumes no responsibility or liability for any errors or omissions in the weather content of this site. We strive to make the information on this website as timely and accurate as possible, making no claims, promises, or guarantees about the accuracy, completeness, or adequacy of the public data used, and expressly disclaims liability for errors and omissions in the contents of the report.</p> 
			<p style="font-size: 12px;">The information provided using this Web site is only intended to advance the knowledge and awareness of historic weather information related to the identified location which is available to the public.  We believe that this information is provided in good faith.</p>
			<p style="font-size: 12px;">The images of buildings are provided from other sources and used in good faith and 'fair use' to represent the address in the claim per section 107 of the US Copyright Law.</p>
			<p style="font-size: 12px;">This application and its data may be updated at irregular intervals. All users should check for updates regularly and ensure the most current version of the application or its data is being used</p>
			</div>
			</div>
	</main>
	</div>
	<!-- <footer>
    Invoice was created on a computer and is valid without the signature and seal.
  </footer> -->
	<script type="text/javascript">
  $(document).bind("contextmenu",function(e){
  	e.preventDefault();
  });
  $(document).keydown(function(e){
  	if(e.which === 123){
  		return false;
  	}
  })
</script>
</body>
</html>