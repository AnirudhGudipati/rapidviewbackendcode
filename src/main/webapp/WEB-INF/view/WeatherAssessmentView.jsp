<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Weather Assessment Report</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/static/css/style.css"
	media="all" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/static/canvasjs.min.js"></script>
<script type="text/javascript">

	window.onload = function() {
		function Median(data) {
			return Quartile_50(data);
		}

		function Quartile_25(data) {
			return Quartile(data, 0.25);
		}

		function Quartile_50(data) {
			return Quartile(data, 0.5);
		}

		function Quartile_75(data) {
			return Quartile(data, 0.75);
		}

		function Quartile(data, q) {
			data = Array_Sort_Numbers(data);
			var pos = ((data.length) - 1) * q;
			var base = Math.floor(pos);
			var rest = pos - base;
			if ((data[base + 1] !== undefined)) {
				return data[base] + rest * (data[base + 1] - data[base]);
			} else {
				return data[base];
			}
		}

		function Array_Sort_Numbers(inputarray) {
			return inputarray.sort(function(a, b) {
				return a - b;
			});
		}

		function Array_Sum(t) {
			return t.reduce(function(a, b) {
				return a + b;
			}, 0);
		}

		function Array_Average(data) {
			return Array_Sum(data) / data.length;
		}

		function Array_Stdev(tab) {
			var i, j, total = 0, mean = 0, diffSqredArr = [];
			for (i = 0; i < tab.length; i += 1) {
				total += tab[i];
			}
			mean = total / tab.length;
			for (j = 0; j < tab.length; j += 1) {
				diffSqredArr.push(Math.pow((tab[j] - mean), 2));
			}
			return (Math.sqrt(diffSqredArr.reduce(function(firstEl, nextEl) {
				return firstEl + nextEl;
			}) / tab.length));
		}

		var boxplotdata = ${boxplotdata};
		var labels = [];
		boxplotdata.forEach(function(item) {
			console.log("AAA", item);
			var date = item.beginDate;
			if (labels.indexOf(date) === -1) {
				labels.push(date);
			}
			console.log("labels", labels);

		});
		var findata = [];
		var finsizedata=[];
		for (var i = 0; i < labels.length; i++) {
			console.log("inside For", labels[i]);
			var yaxis = [];
			var ysize =[];
			var ytest = [];
			var ysizetest =[];
			var test = {};
			var sizetest ={};
			var mind, maxd, q1d, q2d, q3d,mins, maxs, q1s, q2s, q3s;
			boxplotdata.forEach(function(obj) {
				if (obj.beginDate === labels[i]) {
					var distance = obj.distance;
					var mag =obj.magnitude;
					yaxis.push(distance);
					ysize.push(mag);
					mind = Math.min.apply(null, yaxis);
					maxd = Math.max.apply(null, yaxis);
					q1d = Quartile_25(yaxis);
					q3d = Quartile_75(yaxis);
					q2d = Quartile_50(yaxis);
					mins = Math.min.apply(null, ysize);
					maxs = Math.max.apply(null, ysize);
					q1s = Quartile_25(ysize);
					q3s = Quartile_75(ysize);
					q2s = Quartile_50(ysize);
				}
			});
			ytest.push(mind);
			ytest.push(q1d);
			ytest.push(q3d);
			ytest.push(maxd);
			ytest.push(q2d);
			ysizetest.push(mins);
			ysizetest.push(q1s);
			ysizetest.push(q3s);
			ysizetest.push(maxs);
			ysizetest.push(q2s);
			test = {
				label : labels[i],
				y : ytest
			};
			sizetest = {
					label : labels[i],
					y : ysizetest
					};
			findata.push(test);
			finsizedata.push(sizetest);
		}
		;
		var chart = new CanvasJS.Chart("chartContainer1", {
			title : {
				text : "Distance from Claim (Miles)"
			},
			axisY : {
				title : "Distance"
			},
			axisX : {
				title : "Event Dates"
			},
			data : [ {
				type : "boxAndWhisker",
				dataPoints : findata
			} ]
		});
		console.log("CHART", chart);
		chart.render();
		var chart = new CanvasJS.Chart("chartContainer2", {
			title : {
				text : "Hail Size (Inches)"
			},
			axisY : {
				title : "Hail Size"
			},
			axisX : {
				title : "Event Dates"
			},
			data : [ {
				type : "boxAndWhisker",
				dataPoints : finsizedata
			} ]
		});
		console.log("CHART", chart);
		chart.render();
		}
</script>
<style>
.download-button {
	margin-top: 10px;
	/* border: 1px solid black;
  border-radius: 10pt; */
	float: left;
	height: auto;
	width: 97%;
	margin-left: 10px;
}

#canvas {
	background: #ffffff;
	box-shadow: 5px 5px 5px #ccc;
	border: 5px solid #eee;
	margin-bottom: 20px;
	padding-bottom: 20px;
}
</style>
</head>
<body>
		<header class="clearfix">
			<div id="logo">
				<img
					src="${pageContext.request.contextPath}/resources/static/images/logo1.png"
					width="193px" height="47px">
			</div>
			<div id="company">
				<h2 class="name">Weather Assessment Report</h2>
				<div></div>
				<div>${ReportGenDateTime} - ${UID}</div>
			</div>
		</header>
		<main>
			<div class="claim-info">
				<h1 style="text-align: center">WEATHER ASSESSMENT REPORT</h1>
				<br />
				<div style="float: left; width: 100%;">
				
					<table style="width: 100%;">
						<tr>
							<td style="width: 20%;"><b>Claim ID</b></td>
							<td style="width: 4%;">:</td>
							<td>${claimid}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Owner Name</b></td>
							<td style="width: 4%;">:</td>
							<td>${ownername}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Insurance Carrier Name</b></td>
							<td style="width: 4%;">:</td>
							<td>${carrier}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Address</b></td>
							<td style="width: 4%;">:</td>
							<td>${address}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Coordinates</b></td>
							<td style="width: 4%;">:</td>
							<td>Latitude : ${latitude}, Longitude : ${longitude}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Date Range</b></td>
							<td style="width: 4%;">:</td>
							<td>${policystartDate}  to ${claimDate}</td>
						</tr>
						<tr>
							<td style="width: 20%;"><b>Report Generated</b></td>
							<td style="width: 4%;">:</td>
							<td>${ReportGenDateTime}</td>
						</tr>
					</table>
				</div>
				<c:set var = "count" value="${weathereventscount *20.8}"></c:set>
				<div class='hello' style="float: left; width: 100%; height: ${count}%">

					<br />
					<h3 style="text-align: center">HAIL STORM EVENTS</h3>
					<table style="width: 100%;">
						<col></col>
						<tr>
							<th style="border-right: 1px solid #5d5b6a;width:25%;">Date</th>
							<th colspan="4" style="width:75%;align:center">Estimated Max Hail Size</th>
						</tr>
						<tr>
							<th style="border-bottom: 1px solid #5d5b6a;">  </th>
							<th style="border: 1px solid #5d5b6a;">0 - 1 mi</th>
							<th style="border: 1px solid #5d5b6a;">1 - 3 mi</th>
							<th style="border: 1px solid #5d5b6a;">3 - 5 mi</th>
							<th style="border: 1px solid #5d5b6a;">5 - 10 mi</th>
						</tr>

						<c:forEach var="wd" items="${wd}">
							<tr>
								<td align="center" style="border-right: 1px solid #5d5b6a;">${wd.eventdate}</td>
								<td align="center" style="border-right: 1px solid #5d5b6a;">${wd.zeroonemag}</td>
								<td align="center" style="border-right: 1px solid #5d5b6a;">${wd.onethreemag}</td>
								<td align="center" style="border-right: 1px solid #5d5b6a;">${wd.threefivemag}</td>
								<td align="center">${wd.fivetenmag}</td>
							</tr>
						</c:forEach>
					</table>
				</div>
				<br /> <br />
			</div>
			<div class="mapdiv" align="center">
				<h2 style="text-align: center">Hail Verification Map</h2>
				<div id="map"
					style="height: 300px; width: 100%; border-radius: 10pt;"></div>
			</div>
			<script>
			function initMap() {
				var gmapdata = ${gmapdata};
				var clong = parseFloat(gmapdata[0].HomeLongitude);
				var clat = parseFloat(gmapdata[0].HomeLatitude);
				console.log("longlat", clong, clat);
				var map = new google.maps.Map(document
						.getElementById('map'), {
					zoom : 11,
					center : {
						lat : clat,
						lng : clong
					}
				});
				var myLatLng = {
					lat : clat,
					lng : clong
				};
				var marker = new google.maps.Marker({
					position : myLatLng,
					map : map,
					animation: google.maps.Animation.DROP,
					icon : defaultmarker
				});
				var centerString="<b>Home Location</b><br/>"+"<b>Latitude : </b>"+clat+"<br/>"+"<b>Longitude : </b>"+ clong;
				var infowindow = new google.maps.InfoWindow({
			          content: centerString
			        });
				marker.addListener('click', function() {
			          infowindow.open(map, marker);
			        });
				var defaultmarker ={
						scaledSize: new google.maps.Size(25,25)};
				var image = {
					url : "${pageContext.request.contextPath}/resources/static/images/orangemapmarker.png",
					scaledSize : new google.maps.Size(22, 25)
				};
				gmapdata.forEach(function(markerdata) {
					var yellowmarker = {
						lat : parseFloat(markerdata.Latitude),
						lng : parseFloat(markerdata.Longitude)
					};
					var contentString="<b>Distance from Home : </b>"+markerdata.Distance+"<br/>"+"<b>Hailsize : </b>"+markerdata.Hailsize+"<br/>"+"<b>Event Date : </b>"+markerdata.eventDate;
					var infowindow = new google.maps.InfoWindow({
				          content: contentString
				        });
					var marker = new google.maps.Marker({
						position : yellowmarker,
						map : map,
						animation: google.maps.Animation.DROP,
						icon : image
					});
					marker.addListener('click', function() {
				          infowindow.open(map, marker);
				        });
				});
			}
			</script>
			<script async defer
				src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8y69GuUs-jLmtu5JWHN0DuyNfnyLYkwA&callback=initMap">
			</script>
			<br />
			<br/>
			<div class="claim-info">
			<br/>
			<br/>	
			<div id="chartContainer1" style="height: 300px; width: 100%;"></div>
			<br/>
			<br/>
			<div id="chartContainer2" style="height: 300px; width: 100%;"></div>
			</div>
			<div class="claim-info" align="left">
			<div style="margin-left: 1%">
			<p style="font-size: 30px;"><b>Legal Disclaimer</b></p>
			<p style="font-size: 12px;">The AIKnights makes GIS and additional data available as a service on the express condition that users who view, download, transfer or otherwise access or use the GIS and data expressly agree to these Terms of Use. If you do not accept these terms, you may not view or otherwise use the GIS or associated data for any purpose. These restrictions on use of the GIS or other data apply to you and to any transferee, or other end user of yours (collectively, "users").</p>
			<p style="font-size: 12px;">The AIKnights' offered data is expressly provided AS IS from open sources and assumes no responsibility or legal liability concerning the GIS or reported weather data's guarantees of completeness, accuracy, or timeliness. It further assumes no responsibility or liability for any errors or omissions in the weather content of this site. We strive to make the information on this website as timely and accurate as possible, making no claims, promises, or guarantees about the accuracy, completeness, or adequacy of the public data used, and expressly disclaims liability for errors and omissions in the contents of the report.</p> 
			<p style="font-size: 12px;">The information provided using this Web site is only intended to advance the knowledge and awareness of historic weather information related to the identified location which is available to the public.  We believe that this information is provided in good faith.</p>
			<p style="font-size: 12px;">The images of buildings are provided from other sources and used in good faith and 'fair use' to represent the address in the claim per section 107 of the US Copyright Law.</p>
			<p style="font-size: 12px;">This application and its data may be updated at irregular intervals. All users should check for updates regularly and ensure the most current version of the application or its data is being used</p>
			</div>
			</div>
			</main>
	<script type="text/javascript">
  $(document).bind("contextmenu",function(e){
  	e.preventDefault();
  });
  $(document).keydown(function(e){
  	if(e.which === 123){
  		return false;
  	}
  })
</script>
</body>
</html>