package com.aiknights.com.rapidviewmain.Validator;

import java.util.HashMap;
import java.util.Map;

public class PasswordValidator {
	public Map<String, String> passwordValidation(String userName, String password) {
		final Map<String, String> responseMap = new HashMap<>();
		boolean valid = true;
		if (password.length() > 15 || password.length() < 8) {
			responseMap.put("passowrdLength", "Password should be less than 15 and more than 8 characters in length.");
			System.out.println("Password should be less than 15 and more than 8 characters in length.");
			valid = false;
		}
		if (password.indexOf(userName) > -1) {
			responseMap.put("passowrduserName", "Password Should not be same as user name");
			System.out.println("Password Should not be same as user name");
			valid = false;
		}
		String upperCaseChars = "(.*[A-Z].*)";
		if (!password.matches(upperCaseChars)) {
			responseMap.put("passowrdUpperCase", "Password should contain atleast one upper case alphabet");
			System.out.println("Password should contain atleast one upper case alphabet");
			valid = false;
		}
		String lowerCaseChars = "(.*[a-z].*)";
		if (!password.matches(lowerCaseChars)) {
			responseMap.put("passowrdLowerCase", "Password should contain atleast one lower case alphabet");
			System.out.println("Password should contain atleast one lower case alphabet");
			valid = false;
		}
		String numbers = "(.*[0-9].*)";
		if (!password.matches(numbers)) {
			responseMap.put("passowrdAtleastNumber", "Password should contain atleast one number.");
			System.out.println("Password should contain atleast one number.");
			valid = false;
		}
		String specialChars = "(.*[,~,!,@,#,$,%,^,&,*,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?].*$)";
		if (!password.matches(specialChars)) {
			responseMap.put("passowrdSpecialCharacter", "Password should contain atleast one special character");
			System.out.println("Password should contain atleast one special character");
			valid = false;
		}
		if (valid) {

			responseMap.put("passowrd", "Password is valid.");
			System.out.println("Password is valid.");
		}
		return responseMap;
	}
}
