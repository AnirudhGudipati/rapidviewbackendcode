package com.aiknights.com.rapidviewmain.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.UserDetailsRepository;
import com.aiknights.com.rapidviewmain.models.ManageUsersModel;
import com.aiknights.com.rapidviewmain.models.UserAndRoleDetailsModel;
import com.aiknights.com.rapidviewmain.models.UserDetails;

@Service
public class UserDetailsService {
	
	@Autowired
	private UserDetailsRepository userRepository;
	
	@Autowired
	private EmailNotificationService emailservice;
	
	@Autowired
	private UserDetailsService userdetailsservice;
	
	@Autowired
	private GetIDService getIDService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	public UserDetails getUserDetailsByUserName(final String userName) {
		return userRepository.findUserDetailsByUserName(userName);
	}
	
	public UserDetails getUserDetailsByUserID(final String userId) {
		return userRepository.findUserDetailsByUserID(userId);
	}
	
	public UserDetails getUserDetailsByEmailID(final String emailID) {
		return userRepository.findUserDetailsByEmailID(emailID);
	}
	
	public List<UserAndRoleDetailsModel> getAllUsers(String Rolename) {
		return userRepository.getAllUsers(Rolename);
	}
	
	public List<ManageUsersModel> manageUsersAsAdmin(String CompanyName) {
		return userRepository.manageUsersAsAdmin(CompanyName);
	}
	
	public boolean updatePasswordByUserName(String userName,String password) {
		return userRepository.updatePasswordByUserName(userName,password);
	}

	public boolean activateUserAsAdmin(String userID) {
		return userRepository.activateUserAsAdmin(userID);
	}
	
	public boolean deactivateUserAsAdmin(String userID) {
		return userRepository.deactivateUserAsAdmin(userID);
	}
	
	public boolean toggleForceChangePasswordByUsername(String userName) {
		return userRepository.toggleForceChangePasswordByUsername(userName);
	}
	
	public boolean InsertAdminTicketIntoDB(String ticket_ID, String ticket_Notes, String ticket_Context, String user_ID, String admin_ID) {
		return userRepository.InsertAdminTicketIntoDB(ticket_ID, ticket_Notes, ticket_Context, user_ID, admin_ID);
	}
	
	public Map<String, String> resetPasswordAsAdmin(String userID) {
		final Map<String, String> responsemap = new HashMap<>();
		UserDetails userData = userdetailsservice.getUserDetailsByUserID(userID);
		String userName = userData.getUserName();
		String passString = getIDService.getAlphaNumericString(6) + "123@rapidView";
		String tempPass = passwordEncoder.encode(passString);
		boolean changePasswordresult = userRepository.updatePasswordByUserName(userName, tempPass);
		if (changePasswordresult == true) {
			boolean toggleForceChangePassword = userRepository.toggleForceChangePasswordByUsername(userName);
			if(toggleForceChangePassword==true) {
				String EmailAddress = userData.getEmail();
				String Subject = "RapidView Password Reset Request";
				String Body = "Hi " + userData.getUserName() + ","
						+ "\r\n\r\nWe’ve received a change request for your RapidView account. "
						+ "On your Request, your Admin had reset your RapidView Account's password to the below mentioned Temporary Password. Use the Temporary Password to log-in to your RapidView Account and Change the Password Immediately\r\n\r\n"
						+ passString
						+ "\r\n\r\n"
						+ "If this request did not come from you, change your account password immediately to prevent further unauthorized access.";
				String Status = emailservice.sendemailcontroller(EmailAddress, Subject, Body);
				Boolean emailStatus = Boolean.parseBoolean(Status);
				if (emailStatus == true) {
					responsemap.put("Status", "Success");
				} else {
					responsemap.put("Status", "Failed");
					responsemap.put("Error", "Failed to send Email attatched with the temporary password");
				}
			}else {
				responsemap.put("Status", "Failed");
				responsemap.put("Error", "Failed to toggle Flag for forcing user to change Password after Login");
			}
			
		} else {
			responsemap.put("Status", "Failed");
			responsemap.put("Error", "Failed to Reset to a Temporary Password");
		}
		return responsemap;
	}
}
