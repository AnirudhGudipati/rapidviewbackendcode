package com.aiknights.com.rapidviewmain.service;

import java.util.ArrayList;
import java.util.List;

import com.aiknights.com.rapidviewmain.models.FlaskServiceChildrenModel;
import com.aiknights.com.rapidviewmain.models.GoogleLiveMapDataModel;
import com.aiknights.com.rapidviewmain.models.NexRadServiceChildrenModel;
import com.aiknights.com.rapidviewmain.models.PLSRWindServiceChildrenModel;
import com.aiknights.com.rapidviewmain.models.ReportsStageDBModel;
import com.aiknights.com.rapidviewmain.models.ThunderWindServiceChildrenModel;
import com.aiknights.com.rapidviewmain.models.WeatherDataforBoxChartsModel;
import com.aiknights.com.rapidviewmain.models.WeatherOutputModel;
import com.aiknights.com.rapidviewmain.models.WeatherStageDatabaseModel;

public class WeatherDataConversionService {

	public List<WeatherOutputModel> ConvertToWeatherOutputModelFromStage(List<WeatherStageDatabaseModel> flist){
		List<WeatherOutputModel> wt= new ArrayList<WeatherOutputModel>();
		for (int i = 0; i < flist.size(); i++) {
			WeatherOutputModel wom = new WeatherOutputModel();
			String tempdate = flist.get(i).getEventdate();
			String mag = flist.get(i).getHailsize();
			double dist = Double.parseDouble(flist.get(i).getDistance());
			wom.setEventdate(tempdate);
			if (dist <= 1) {
				wom.setZeroonemag(mag);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			} else if (dist > 1 && dist <= 3) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(mag);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			} else if (dist > 3 && dist <= 5) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(mag);
				wom.setFivetenmag(null);
			} else if (dist > 5 && dist <= 10) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(mag);
			} else {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			}
			wt.add(wom);
		}		
		return wt;

	}
	
	public List<WeatherOutputModel> ConvertToWeatherOutputModelFromReportsStage(List<ReportsStageDBModel> flist){
		List<WeatherOutputModel> wt= new ArrayList<WeatherOutputModel>();
		for (int i = 0; i < flist.size(); i++) {
			WeatherOutputModel wom = new WeatherOutputModel();
			String tempdate = flist.get(i).getEventdate();
			String mag = flist.get(i).getHailsize();
			double dist = Double.parseDouble(flist.get(i).getDistance());
			wom.setEventdate(tempdate);
			if (dist <= 1) {
				wom.setZeroonemag(mag);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			} else if (dist > 1 && dist <= 3) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(mag);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			} else if (dist > 3 && dist <= 5) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(mag);
				wom.setFivetenmag(null);
			} else if (dist > 5 && dist <= 10) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(mag);
			} else {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			}
			wt.add(wom);
		}		
		return wt;

	}

	public List<WeatherOutputModel> ConvertToWeatherOutputModel(List<FlaskServiceChildrenModel> flist,List<NexRadServiceChildrenModel> nlist){
		List<WeatherOutputModel> wt= new ArrayList<WeatherOutputModel>();
		for (int i = 0; i < flist.size(); i++) {
			WeatherOutputModel wom = new WeatherOutputModel();
			String tempdate = flist.get(i).getBegin_date();
			String mag = flist.get(i).getMagnitude();
			double dist = flist.get(i).getDistance();
			wom.setEventdate(tempdate);
			if (dist <= 1) {
				wom.setZeroonemag(mag);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			} else if (dist > 1 && dist <= 3) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(mag);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			} else if (dist > 3 && dist <= 5) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(mag);
				wom.setFivetenmag(null);
			} else if (dist > 5 && dist <= 10) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(mag);
			} else {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			}
			wt.add(wom);
		}
		for (int i = 0; i < nlist.size(); i++) {
			WeatherOutputModel wom = new WeatherOutputModel();
			String tempdate = nlist.get(i).getEvent_date();
			String mag = nlist.get(i).getMaxsize();
			double dist = nlist.get(i).getDistance();
			wom.setEventdate(tempdate);
			if (dist <= 1) {
				wom.setZeroonemag(mag);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			} else if (dist > 1 && dist <= 3) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(mag);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			} else if (dist > 3 && dist <= 5) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(mag);
				wom.setFivetenmag(null);
			} else if (dist > 5 && dist <= 10) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(mag);
			} else {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			}
			wt.add(wom);
		}

		return wt;

	}
	
	public List<WeatherOutputModel> ConvertToWeatherWindDataOutputModel(List<PLSRWindServiceChildrenModel> nlist, List<ThunderWindServiceChildrenModel> tlist){
		List<WeatherOutputModel> wt= new ArrayList<WeatherOutputModel>();
		for (int i = 0; i < nlist.size(); i++) {
			WeatherOutputModel wom = new WeatherOutputModel();
			String tempdate = nlist.get(i).getEvent_date();
			String mag = nlist.get(i).getMagnitude();
			double dist = nlist.get(i).getDistance();
			wom.setEventdate(tempdate);
			if (dist <= 1) {
				wom.setZeroonemag(mag);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			} else if (dist > 1 && dist <= 3) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(mag);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			} else if (dist > 3 && dist <= 5) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(mag);
				wom.setFivetenmag(null);
			} else if (dist > 5 && dist <= 10) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(mag);
			} else {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			}
			wt.add(wom);
		}
		for (int i = 0; i < tlist.size(); i++) {
			WeatherOutputModel wom = new WeatherOutputModel();
			String tempdate = tlist.get(i).getBegin_date();
			String mag = tlist.get(i).getMagnitude();
			double dist = tlist.get(i).getDistance();
			wom.setEventdate(tempdate);
			if (dist <= 1) {
				wom.setZeroonemag(mag);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			} else if (dist > 1 && dist <= 3) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(mag);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			} else if (dist > 3 && dist <= 5) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(mag);
				wom.setFivetenmag(null);
			} else if (dist > 5 && dist <= 10) {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(mag);
			} else {
				wom.setZeroonemag(null);
				wom.setOnethreemag(null);
				wom.setThreefivemag(null);
				wom.setFivetenmag(null);
			}
			wt.add(wom);
		}
		return wt;
	}

	public List<WeatherDataforBoxChartsModel> ConvertToBoxPlotDataModel(List<FlaskServiceChildrenModel> flist,List<NexRadServiceChildrenModel> nlist){
		List<WeatherDataforBoxChartsModel> wt= new ArrayList<WeatherDataforBoxChartsModel>();
		for (int i = 0; i < flist.size(); i++) {
			WeatherDataforBoxChartsModel wom = new WeatherDataforBoxChartsModel();
			wom.setBeginDate(flist.get(i).getBegin_date());
			wom.setDistance(flist.get(i).getDistance());
			wom.setMagnitude(Double.parseDouble(flist.get(i).getMagnitude()));
			wt.add(wom);
		}
		for (int i = 0; i < nlist.size(); i++) {
			WeatherDataforBoxChartsModel wom = new WeatherDataforBoxChartsModel();
			wom.setBeginDate(nlist.get(i).getEvent_date());
			wom.setDistance(nlist.get(i).getDistance());
			wom.setMagnitude(Double.parseDouble(nlist.get(i).getMaxsize()));
			wt.add(wom);
		}

		return wt;

	}
	
	public List<WeatherDataforBoxChartsModel> ConvertToBoxPlotWindDataModel(List<ThunderWindServiceChildrenModel> tlist,List<PLSRWindServiceChildrenModel> plist){
		List<WeatherDataforBoxChartsModel> wt= new ArrayList<WeatherDataforBoxChartsModel>();
		for (int i = 0; i < tlist.size(); i++) {
			WeatherDataforBoxChartsModel wom = new WeatherDataforBoxChartsModel();
			wom.setBeginDate(tlist.get(i).getBegin_date());
			wom.setDistance(tlist.get(i).getDistance());
			wom.setMagnitude(Double.parseDouble(tlist.get(i).getMagnitude()));
			wt.add(wom);
		}
		for (int i = 0; i < plist.size(); i++) {
			WeatherDataforBoxChartsModel wom = new WeatherDataforBoxChartsModel();
			wom.setBeginDate(plist.get(i).getEvent_date());
			wom.setDistance(plist.get(i).getDistance());
			wom.setMagnitude(Double.parseDouble(plist.get(i).getMagnitude()));
			wt.add(wom);
		}

		return wt;

	}

	public List<WeatherStageDatabaseModel> ConvertToWeatherStageModel(String claimid, List<FlaskServiceChildrenModel> flist,List<NexRadServiceChildrenModel> nlist){
		List<WeatherStageDatabaseModel> wt= new ArrayList<WeatherStageDatabaseModel>();
		for (int i = 0; i < flist.size(); i++) {
			WeatherStageDatabaseModel wom = new WeatherStageDatabaseModel();
			wom.setClaimid(claimid);
			wom.setEventdate(flist.get(i).getBegin_date());
			wom.setHailsize(flist.get(i).getMagnitude());
			wom.setLatitude(flist.get(i).getBegin_lat());
			wom.setLongitude(flist.get(i).getBegin_lon());
			wom.setDistance(Double.toString(flist.get(i).getDistance()));
			wt.add(wom);
		}
		for (int i = 0; i < nlist.size(); i++) {
			WeatherStageDatabaseModel wom = new WeatherStageDatabaseModel();
			wom.setClaimid(claimid);
			wom.setEventdate(nlist.get(i).getEvent_date());
			wom.setHailsize(nlist.get(i).getMaxsize());
			wom.setLatitude(nlist.get(i).getLat());
			wom.setLongitude(nlist.get(i).getLon());
			wom.setDistance(Double.toString(nlist.get(i).getDistance()));
			wt.add(wom);
		}

		return wt;
	}
	
	public List<WeatherStageDatabaseModel> ConvertToWeatherWindStageModel(String claimid, List<ThunderWindServiceChildrenModel> tlist,List<PLSRWindServiceChildrenModel> plist){
		List<WeatherStageDatabaseModel> wt= new ArrayList<WeatherStageDatabaseModel>();
		for (int i = 0; i < tlist.size(); i++) {
			WeatherStageDatabaseModel wom = new WeatherStageDatabaseModel();
			wom.setClaimid(claimid);
			wom.setEventdate(tlist.get(i).getBegin_date());
			wom.setHailsize(tlist.get(i).getMagnitude());
			wom.setLatitude(tlist.get(i).getBegin_lat());
			wom.setLongitude(tlist.get(i).getBegin_lon());
			wom.setDistance(Double.toString(tlist.get(i).getDistance()));
			wt.add(wom);
		}
		for (int i = 0; i < plist.size(); i++) {
			WeatherStageDatabaseModel wom = new WeatherStageDatabaseModel();
			wom.setClaimid(claimid);
			wom.setEventdate(plist.get(i).getEvent_date());
			wom.setHailsize(plist.get(i).getMagnitude());
			wom.setLatitude(plist.get(i).getLat());
			wom.setLongitude(plist.get(i).getLon());
			wom.setDistance(Double.toString(plist.get(i).getDistance()));
			wt.add(wom);
		}

		return wt;
	}
	
	public List<ReportsStageDBModel> ConvertToReportsWeatherStageModel(String reportid, List<FlaskServiceChildrenModel> flist,List<NexRadServiceChildrenModel> nlist){
		List<ReportsStageDBModel> wt= new ArrayList<ReportsStageDBModel>();
		for (int i = 0; i < flist.size(); i++) {
			ReportsStageDBModel wom = new ReportsStageDBModel();
			wom.setReportid(reportid);
			wom.setEventdate(flist.get(i).getBegin_date());
			wom.setHailsize(flist.get(i).getMagnitude());
			wom.setLatitude(flist.get(i).getBegin_lat());
			wom.setLongitude(flist.get(i).getBegin_lon());
			wom.setDistance(Double.toString(flist.get(i).getDistance()));
			wt.add(wom);
		}
		for (int i = 0; i < nlist.size(); i++) {
			ReportsStageDBModel wom = new ReportsStageDBModel();
			wom.setReportid(reportid);
			wom.setEventdate(nlist.get(i).getEvent_date());
			wom.setHailsize(nlist.get(i).getMaxsize());
			wom.setLatitude(nlist.get(i).getLat());
			wom.setLongitude(nlist.get(i).getLon());
			wom.setDistance(Double.toString(nlist.get(i).getDistance()));
			wt.add(wom);
		}

		return wt;
	}
	
	public List<ReportsStageDBModel> ConvertToReportsWeatherWindStageModel(String reportid, List<ThunderWindServiceChildrenModel> tlist,List<PLSRWindServiceChildrenModel> plist){
		List<ReportsStageDBModel> wt= new ArrayList<ReportsStageDBModel>();
		for (int i = 0; i <tlist.size(); i++) {
			ReportsStageDBModel wom = new ReportsStageDBModel();
			wom.setReportid(reportid);
			wom.setEventdate(tlist.get(i).getBegin_date());
			wom.setHailsize(tlist.get(i).getMagnitude());
			wom.setLatitude(tlist.get(i).getBegin_lat());
			wom.setLongitude(tlist.get(i).getBegin_lon());
			wom.setDistance(Double.toString(tlist.get(i).getDistance()));
			wt.add(wom);
		}
		for (int i = 0; i < plist.size(); i++) {
			ReportsStageDBModel wom = new ReportsStageDBModel();
			wom.setReportid(reportid);
			wom.setEventdate(plist.get(i).getEvent_date());
			wom.setHailsize(plist.get(i).getMagnitude());
			wom.setLatitude(plist.get(i).getLat());
			wom.setLongitude(plist.get(i).getLon());
			wom.setDistance(Double.toString(plist.get(i).getDistance()));
			wt.add(wom);
		}

		return wt;
	}

	public List<GoogleLiveMapDataModel> ConvertToGmapDataModel(List<FlaskServiceChildrenModel> flist, List<NexRadServiceChildrenModel> nlist,String latitude, String longitude) {
		List<GoogleLiveMapDataModel> wt= new ArrayList<GoogleLiveMapDataModel>();
		int count =0;
		for (int i = 0; i < flist.size(); i++) {
			if(flist.get(i).getDistance()<=5) {
				count=count+1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setDistance(flist.get(i).getDistance());
				wom.setEventDate(flist.get(i).getBegin_date());
				wom.setHailsize(Double.parseDouble(flist.get(i).getMagnitude()));
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(flist.get(i).getBegin_lat()));
				wom.setLongitude(Double.parseDouble(flist.get(i).getBegin_lon()));
				wt.add(wom);
			}
		}
		for (int i = 0; i < nlist.size(); i++) {
			if(nlist.get(i).getDistance()<=5) {
				count=count+1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setDistance(nlist.get(i).getDistance());
				wom.setEventDate(nlist.get(i).getEvent_date());
				wom.setHailsize(Double.parseDouble(nlist.get(i).getMaxsize()));
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(nlist.get(i).getLat()));
				wom.setLongitude(Double.parseDouble(nlist.get(i).getLon()));
				wt.add(wom);
			}
		}
		if(count==0)
		{
			for (int i = 0; i < flist.size(); i++) {
				count=count+1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setDistance(flist.get(i).getDistance());
				wom.setEventDate(flist.get(i).getBegin_date());
				wom.setHailsize(Double.parseDouble(flist.get(i).getMagnitude()));
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(flist.get(i).getBegin_lat()));
				wom.setLongitude(Double.parseDouble(flist.get(i).getBegin_lon()));
				wt.add(wom);
			}
			for (int i = 0; i < nlist.size(); i++) {
				count=count+1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setDistance(nlist.get(i).getDistance());
				wom.setEventDate(nlist.get(i).getEvent_date());
				wom.setHailsize(Double.parseDouble(nlist.get(i).getMaxsize()));
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(nlist.get(i).getLat()));
				wom.setLongitude(Double.parseDouble(nlist.get(i).getLon()));
				wt.add(wom);
			}
		}
		return wt;
	}
	
	public List<GoogleLiveMapDataModel> ConvertToGmapWindDataModel(List<ThunderWindServiceChildrenModel> tlist, List<PLSRWindServiceChildrenModel> plist,String latitude, String longitude) {
		List<GoogleLiveMapDataModel> wt= new ArrayList<GoogleLiveMapDataModel>();
		int count =0;
		for (int i = 0; i < tlist.size(); i++) {
			if(tlist.get(i).getDistance()<=5) {
				count=count+1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setDistance(tlist.get(i).getDistance());
				wom.setEventDate(tlist.get(i).getBegin_date());
				wom.setHailsize(Double.parseDouble(tlist.get(i).getMagnitude()));
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(tlist.get(i).getBegin_lat()));
				wom.setLongitude(Double.parseDouble(tlist.get(i).getBegin_lon()));
				wt.add(wom);
			}
		}
		for (int i = 0; i < plist.size(); i++) {
			if(plist.get(i).getDistance()<=5) {
				count=count+1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setDistance(plist.get(i).getDistance());
				wom.setEventDate(plist.get(i).getEvent_date());
				wom.setHailsize(Double.parseDouble(plist.get(i).getMagnitude()));
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(plist.get(i).getLat()));
				wom.setLongitude(Double.parseDouble(plist.get(i).getLon()));
				wt.add(wom);
			}
		}
		if(count==0)
		{
			for (int i = 0; i < tlist.size(); i++) {
				count=count+1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setDistance(tlist.get(i).getDistance());
				wom.setEventDate(tlist.get(i).getBegin_date());
				wom.setHailsize(Double.parseDouble(tlist.get(i).getMagnitude()));
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(tlist.get(i).getBegin_lat()));
				wom.setLongitude(Double.parseDouble(tlist.get(i).getBegin_lon()));
				wt.add(wom);
			}
			for (int i = 0; i < plist.size(); i++) {
				count=count+1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setDistance(plist.get(i).getDistance());
				wom.setEventDate(plist.get(i).getEvent_date());
				wom.setHailsize(Double.parseDouble(plist.get(i).getMagnitude()));
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(plist.get(i).getLat()));
				wom.setLongitude(Double.parseDouble(plist.get(i).getLon()));
				wt.add(wom);
			}
		}
		return wt;
	}
}


