package com.aiknights.com.rapidviewmain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.TokenDBRepository;
import com.aiknights.com.rapidviewmain.models.TokenDBModel;

@Service
public class TokenDBService {

	@Autowired
	TokenDBRepository dbRepo;
	
	public List<TokenDBModel> getTokenDetailsBasedOnTokenID(String token) {
		return dbRepo.getTokenDetailsBasedOnTokenID(token);
	}
	
	public boolean insertNewTokenDetailsToDB(String token,String CreatedDateTime,String IsUsed,String userId) {
		return dbRepo.insertNewTokenDetailsToDB(token,CreatedDateTime,IsUsed,userId);
	}
	
	public boolean markTokenAsUsedByToken(String token) {
		return dbRepo.markTokenAsUsedByToken(token);
	}
	
	public boolean validateSessionByToken(String token) {
		return dbRepo.validateSessionByToken(token);
	}
	
	public List<TokenDBModel> getAllOpenTokens(){
		return dbRepo.getAllOpenTokens();
	}
}
