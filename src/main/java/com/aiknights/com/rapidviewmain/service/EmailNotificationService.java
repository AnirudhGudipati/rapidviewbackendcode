package com.aiknights.com.rapidviewmain.service;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import com.aiknights.com.rapidviewmain.models.EmailUserModel;

@Service
public class EmailNotificationService {
	@Value("${spring.mail.username}")
	String frommail;
	
	private JavaMailSender javaMailSender;
	
	@Autowired
	public EmailNotificationService(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}
	
	public void sendMailWithAttachment(String to, String subject, String body, String attachmentPath,String filename) 
	{
	    MimeMessage mimeMessage=javaMailSender.createMimeMessage();
	    try {
	    	MimeMessageHelper helper=new MimeMessageHelper(mimeMessage,true);
	    	helper.setFrom(frommail);;
	    	helper.setTo(to);
	    	helper.setSubject(subject);
	    	helper.setText(body);
	    	MimeMultipart multipart = new MimeMultipart();
	    	final MimeBodyPart  messageBodyPart = new MimeBodyPart();
	    	messageBodyPart.setContent(body, "text/html;charset=UTF-8");
	    	multipart.addBodyPart(messageBodyPart);
	    	mimeMessage.setContent(multipart);
	    	if(!filename.equals("")) {
	    		MimeBodyPart bodyPart = new MimeBodyPart();
		        DataSource source = new FileDataSource(attachmentPath);
		        bodyPart.setDataHandler(new DataHandler(source));
		        bodyPart.setFileName(filename);
		        multipart.addBodyPart(bodyPart);
	    	}
	    	javaMailSender.send(mimeMessage);
	    }catch(Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	public void sendNotification(EmailUserModel user) throws MailException {
		SimpleMailMessage mail = new SimpleMailMessage();
		String emaillist = EmailUserModel.getEmailAddress();
		String[] emaillistarr =null;
		emaillistarr = emaillist.split(",");
		mail.setTo(emaillistarr);
		mail.setFrom(frommail);
		mail.setSubject(EmailUserModel.getSubject());
		mail.setText(EmailUserModel.getBody());
		javaMailSender.send(mail);
	}
	
	public String sendemailcontroller(String EmailAddress, String Subject, String Body) {
		EmailUserModel emailuser = new EmailUserModel();
		emailuser.setEmailAddress(EmailAddress);
		emailuser.setSubject(Subject);
		emailuser.setBody(Body);
		String Status = "true";
		try {
			sendNotification(emailuser);
			
		}catch(MailException e) {
			Status="false";
			System.out.println("Error"+e.getMessage());
		}
		if(Status.equals("true")) {
			return "true";
		}
		else{
			return "false";
		}
	}
}
