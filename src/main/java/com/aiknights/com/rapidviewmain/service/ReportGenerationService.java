package com.aiknights.com.rapidviewmain.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.ReportGenerationRepository;
import com.aiknights.com.rapidviewmain.dto.ReportTabDTO;

@Service
public class ReportGenerationService {


	@Autowired
	private ReportGenerationRepository reportGenerationRepository;

	public Map<String,ReportTabDTO> getReportTabInfoUsingClaimId(final String claimId) {
		
		Map<String,ReportTabDTO> map = new HashMap<>();
		ReportTabDTO tab= reportGenerationRepository.getReportDetails(claimId);
		if(tab!= null)
		{
			map.put("reportTab", tab);
		}
		else
		{
			map.put("reportTab", new ReportTabDTO());
		}
		return map;
	}


}
