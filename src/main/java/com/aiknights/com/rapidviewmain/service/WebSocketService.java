package com.aiknights.com.rapidviewmain.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.models.UserAndRoleDetailsModel;
import com.aiknights.com.rapidviewmain.models.WebSocketMessageModel;

@Service
@EnableScheduling
public class WebSocketService {

	@Autowired
	SimpMessagingTemplate template;

	@Autowired
	NotificationDBService dbservice;

	@Autowired
	UserDetailsService userdetailservice;

	public Boolean sendMessageService(String SenderUserID,String ReceiverUserID,String ReceiverUserGroup, String Notification_Type_ID,String Notification_Claim_ID, String Notification_Subject, String Message_content) {
		String servicestatus = "0";
		System.out.println("----------------------------------------------------------------------------------------------");
		try{
			String Notification_Object_ID = Long.toString(GetIDService.getID());
			String Notification_Sender_UID = Long.toString(GetIDService.getID());
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			df.setTimeZone(TimeZone.getTimeZone("GMT"));
			Date dateo = new Date();
			//String Notification_Type_ID
			//String Notification_Claim_ID
			String Created_on = df.format(dateo);
			//String Notification_Subject
			//String MessasgeContent
			String Read_Status,Receiver_Read_Status,Sender_Read_Status;
			Read_Status =  Receiver_Read_Status = Sender_Read_Status ="false";
			String IsDeleted,Receiver_IsDeleted,Sender_IsDeleted; 
			IsDeleted = Receiver_IsDeleted = Sender_IsDeleted="false";
			System.out.println(Notification_Type_ID);
			if(Notification_Type_ID.equals("BROAD_CAST")) {
				String Notification_Topic ="/topic/broadcast";
				Boolean MessageInsertOutput = dbservice.putMessagetoDB(Notification_Object_ID,Notification_Topic,Notification_Type_ID,Notification_Claim_ID,Created_on,Notification_Subject,Message_content,Read_Status,IsDeleted);
				Boolean SenderInsertOutput = dbservice.putMessageSendertoDB(Notification_Sender_UID, Notification_Object_ID, SenderUserID, Sender_Read_Status, Sender_IsDeleted);
				System.out.println("MessageInsertOutput : "+MessageInsertOutput+" SenderInsertOutput : "+SenderInsertOutput);
				List<UserAndRoleDetailsModel> userdetails = userdetailservice.getAllUsers("");
				for(int i =0;i<userdetails.size();i++) {
					String Receiver_User_ID = userdetails.get(i).getUserId();
					ReceiverUserID= Receiver_User_ID;
					String Notification_Receiver_UID = Long.toString(GetIDService.getID());
					Boolean ReceiverInsertOutput = dbservice.putMessageReceivertoDB(Notification_Receiver_UID, Notification_Object_ID, ReceiverUserID, Receiver_Read_Status, Receiver_IsDeleted);
					System.out.println("ReceiverInsertOutput : "+ReceiverInsertOutput);
				}
				WebSocketMessageModel message = new WebSocketMessageModel();
				message.setNotification_Object_ID(Notification_Object_ID);
				message.setNotification_Topic(Notification_Topic);
				message.setNotification_Type_ID(Notification_Type_ID);
				message.setNotification_Claim_ID(Notification_Claim_ID);
				message.setCreated_on(Created_on);
				message.setNotification_Subject(Notification_Subject);
				message.setMessage_content(Message_content);
				message.setRead_Status(Read_Status);
				message.setIsDeleted(IsDeleted);
				message.setSenderUserID(SenderUserID);
				message.setSender_Read_Status(Sender_Read_Status);
				message.setSender_IsDeleted(Sender_IsDeleted);
				message.setReceiverUserID("broadcast");
				message.setReceiver_Read_Status(Receiver_Read_Status);
				message.setReceiver_IsDeleted(Receiver_IsDeleted);
				template.convertAndSend(Notification_Topic,message);
				System.out.println("Message Sent to Channel/Topic : "+Notification_Topic);
			}
			if(Notification_Type_ID.equals("USER_GROUP")) {
				String Notification_Topic ="/topic/"+ReceiverUserGroup;
				Boolean MessageInsertOutput = dbservice.putMessagetoDB(Notification_Object_ID,Notification_Topic,Notification_Type_ID,Notification_Claim_ID,Created_on,Notification_Subject,Message_content,Read_Status,IsDeleted);
				Boolean SenderInsertOutput = dbservice.putMessageSendertoDB(Notification_Sender_UID, Notification_Object_ID, SenderUserID, Sender_Read_Status, Sender_IsDeleted);
				System.out.println("MessageInsertOutput : "+MessageInsertOutput+" SenderInsertOutput : "+SenderInsertOutput);
				List<UserAndRoleDetailsModel> userdetails = userdetailservice.getAllUsers(ReceiverUserGroup);
				for(int i =0;i<userdetails.size();i++) {
					String Receiver_User_ID = userdetails.get(i).getUserId();
					ReceiverUserID= Receiver_User_ID;
					String Notification_Receiver_UID = Long.toString(GetIDService.getID());
					Boolean ReceiverInsertOutput = dbservice.putMessageReceivertoDB(Notification_Receiver_UID, Notification_Object_ID, ReceiverUserID, Read_Status, IsDeleted);
					System.out.println("ReceiverInsertOutput : "+ReceiverInsertOutput);
				}
				WebSocketMessageModel message = new WebSocketMessageModel();
				message.setNotification_Object_ID(Notification_Object_ID);
				message.setNotification_Topic(Notification_Topic);
				message.setNotification_Type_ID(Notification_Type_ID);
				message.setNotification_Claim_ID(Notification_Claim_ID);
				message.setCreated_on(Created_on);
				message.setNotification_Subject(Notification_Subject);
				message.setMessage_content(Message_content);
				message.setRead_Status(Read_Status);
				message.setIsDeleted(IsDeleted);
				message.setSenderUserID(SenderUserID);
				message.setSender_Read_Status(Sender_Read_Status);
				message.setSender_IsDeleted(Sender_IsDeleted);
				message.setReceiverUserID(ReceiverUserGroup);
				message.setReceiver_Read_Status(Receiver_Read_Status);
				message.setReceiver_IsDeleted(Receiver_IsDeleted);
				template.convertAndSend(Notification_Topic,message);
				System.out.println("Message Sent to Channel/Topic : "+Notification_Topic);
			}
			if(Notification_Type_ID.equals("SPECIFIC_USER")) {
				if(ReceiverUserID!="") {
					String Notification_Topic ="/topic/"+ReceiverUserID;
					String Notification_Receiver_UID = Long.toString(GetIDService.getID());
					Boolean MessageInsertOutput = dbservice.putMessagetoDB(Notification_Object_ID,Notification_Topic,Notification_Type_ID,Notification_Claim_ID,Created_on,Notification_Subject,Message_content,Read_Status,IsDeleted);
					Boolean SenderInsertOutput = dbservice.putMessageSendertoDB(Notification_Sender_UID, Notification_Object_ID, SenderUserID, Read_Status, IsDeleted);
					Boolean ReceiverInsertOutput = dbservice.putMessageReceivertoDB(Notification_Receiver_UID, Notification_Object_ID, ReceiverUserID, Read_Status, IsDeleted);
					System.out.println("MessageInsertOutput : "+MessageInsertOutput+" SenderInsertOutput : "+SenderInsertOutput+" ReceiverInsertOutput : "+ReceiverInsertOutput);
					WebSocketMessageModel message = new WebSocketMessageModel();
					message.setNotification_Object_ID(Notification_Object_ID);
					message.setNotification_Topic(Notification_Topic);
					message.setNotification_Type_ID(Notification_Type_ID);
					message.setNotification_Claim_ID(Notification_Claim_ID);
					message.setCreated_on(Created_on);
					message.setNotification_Subject(Notification_Subject);
					message.setMessage_content(Message_content);
					message.setRead_Status(Read_Status);
					message.setIsDeleted(IsDeleted);
					message.setSenderUserID(SenderUserID);
					message.setSender_Read_Status(Sender_Read_Status);
					message.setSender_IsDeleted(Sender_IsDeleted);
					message.setReceiverUserID(ReceiverUserID);
					message.setReceiver_Read_Status(Receiver_Read_Status);
					message.setReceiver_IsDeleted(Receiver_IsDeleted);
					template.convertAndSend(Notification_Topic,message);
					System.out.println("Message Sent to Channel/Topic : "+Notification_Topic);
				}
			}
			servicestatus = "1";	
			System.out.println("----------------------------------------------------------------------------------------------");
		}catch(Exception e){
			servicestatus = "0";
		}
		if(servicestatus== "1") {
			return true;
		}else {
			return false;
		}
	}
}
