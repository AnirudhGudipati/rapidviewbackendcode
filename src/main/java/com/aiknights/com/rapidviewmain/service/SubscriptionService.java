package com.aiknights.com.rapidviewmain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.SubscriptionDBRepository;
import com.aiknights.com.rapidviewmain.models.SubscriptionPropertiesModel;
import com.aiknights.com.rapidviewmain.models.SubscriptionPropertiesResponce;

@Service
public class SubscriptionService {
	@Autowired
	SubscriptionDBRepository subscriptionDBRepository;

	public List<SubscriptionPropertiesModel> getSubscriptionProperties() {

		return subscriptionDBRepository.getSubscriptionProperties();
	}

	public List<SubscriptionPropertiesResponce> getSubscriptionDetails() {
		return subscriptionDBRepository.getSubscriptionDetails();
	}

}
