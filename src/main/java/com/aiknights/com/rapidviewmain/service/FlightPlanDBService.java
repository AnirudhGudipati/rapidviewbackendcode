package com.aiknights.com.rapidviewmain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

import com.aiknights.com.rapidviewmain.DAO.FlightPlanDBRepo;
import com.aiknights.com.rapidviewmain.models.FlightPlanObjectDBModel;
import com.aiknights.com.rapidviewmain.models.FlightPlanPropertiesDBModel;
import com.aiknights.com.rapidviewmain.models.WayPointActionDBModel;
import com.aiknights.com.rapidviewmain.models.WayPointDataDBModel;

@Service
public class FlightPlanDBService {

	@Autowired
	FlightPlanDBRepo flightplandbrepo;

	public boolean putFlighPlanDataToDB(String flight_Plan_ID,String claim_ID,String claim_Address)
	{
		return flightplandbrepo.putFlighPlanDataToDB(flight_Plan_ID,claim_ID,claim_Address);
	}

	public boolean putFlightPlanPropertiesToDB(String flight_Plan_ID,String user_ID,String flight_Plan_Name,String flight_Plan_Version,String flight_Plan_Format)
	{
		return flightplandbrepo.putFlightPlanPropertiesToDB(flight_Plan_ID,user_ID,flight_Plan_Name,flight_Plan_Version,flight_Plan_Format);
	}
	public boolean putWayPointDataToDB(String flight_Plan_ID, String way_Point_ID,String way_Point_Key, String latitude, String longitude,String altitude, String above_Ground, String speed, String curve_size, String heading, String pOI,String gimbal_Option, String gimbal_Value, String interval_Option, String interval_Value)
	{
		return flightplandbrepo.putWayPointDataToDB(flight_Plan_ID, way_Point_ID, way_Point_Key, latitude, longitude,altitude, above_Ground, speed, curve_size, heading, pOI,gimbal_Option, gimbal_Value, interval_Option, interval_Value);
	}

	public boolean putActionDatatoDB(String flight_Action_ID, String way_Point_ID, String action_Number,	String selected_Action_Option, String selected_Action_Value) 
	{
		return flightplandbrepo.putActionDatatoDB(flight_Action_ID,way_Point_ID,action_Number,selected_Action_Option,selected_Action_Value);
	}
	
	public List<FlightPlanObjectDBModel> getFlightPlanObjectByFlightPlanID(String flightPlanID)
	{
		return flightplandbrepo.getFlightPlanObjectByFlightPlanID(flightPlanID);
	}
	
	public List<FlightPlanPropertiesDBModel> getFlightPlanPropertiesByFlightPlanID(String flightPlanID)
	{
		return flightplandbrepo.getFlightPlanPropertiesByFlightPlanID(flightPlanID);
	}
	
	public List<WayPointDataDBModel> getWayPointsByFlightPlanID(String flightPlanID)
	{
		return flightplandbrepo.getWayPointsByFlightPlanID(flightPlanID);
	}
	
	public List<WayPointActionDBModel> getWayPointActionsByWayPointID(String wayPointID)
	{
		return flightplandbrepo.getWayPointActionsByWayPointID(wayPointID);
	}

}
