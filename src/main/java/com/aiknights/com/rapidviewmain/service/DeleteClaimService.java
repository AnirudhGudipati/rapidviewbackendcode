package com.aiknights.com.rapidviewmain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.DeleteClaimRepository;

@Service
public class DeleteClaimService {

	@Autowired
	DeleteClaimRepository deleteclaimRepo;
	
	public boolean deleteAdjusterClaimDataByClaimID(String claim_id) {
		// TODO Auto-generated method stub
		
		return deleteclaimRepo.deleteAdjusterClaimDataByClaimID(claim_id);
	}

	public boolean deleteClaimByClaimID(String claim_id) {
		// TODO Auto-generated method stub
		return deleteclaimRepo.deleteClaimByClaimID(claim_id);
	}

	public boolean deletePolicyByClaimID(String claim_id) {
		return deleteclaimRepo.deletePolicyByClaimID(claim_id);
		
	}

	public String getPOlicyNumberByClaimId(String claim_id) {
		// TODO Auto-generated method stub
		return deleteclaimRepo.getPOlicyNumberByClaimId(claim_id);
	}

	public boolean checkClaimIDInIncidentEvidence(String claim_id) {
		// TODO Auto-generated method stub
		return deleteclaimRepo.checkClaimIDInIncidentEvidence(claim_id);
	}

	public boolean deleteIncidentEvdenceClaimDataByClaimID(String claim_id) {
		// TODO Auto-generated method stub
		return deleteclaimRepo.deleteIncidentEvdenceClaimDataByClaimID(claim_id);
	}

	public boolean deleteCreateReportClaimDataByClaimID(String claim_id) {
		return deleteclaimRepo.deleteCreateReportClaimDataByClaimID(claim_id);
	}

	public boolean checkClaimIDInRiskAssesment(String claim_id) {
		return deleteclaimRepo.checkClaimIDInRisKAssesment(claim_id);
	}

	public boolean deleteRiskAssesmentClaimDataByClaimID(String claim_id) {
		return deleteclaimRepo.deleteRiskAssesmentClaimDataByClaimID(claim_id);
	}

	public boolean checkClaimIDInCreateReport(String claim_id) {
		return deleteclaimRepo.checkClaimIDInCreateReport(claim_id);
	}

	public boolean checkClaimIDInWeatherCheck(String claim_id) {
		return deleteclaimRepo.checkClaimIDInWeatherCheck(claim_id);
	}

	public boolean deleteWeatherCheckClaimByDataByClaimID(String claim_id) {
		return deleteclaimRepo.deleteWeatherCheckClaimByDataByClaimID(claim_id);
	}

	
}
