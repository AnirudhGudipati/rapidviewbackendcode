package com.aiknights.com.rapidviewmain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.ClaimNotificationRepository;
import com.aiknights.com.rapidviewmain.models.ClaimNotification;

@Service
public class ClaimNotificationService {

	@Autowired
	private ClaimNotificationRepository claimRepository;

	public List<ClaimNotification> getAssigedClaimsForUser(final int userId) {
		return claimRepository.getAssignedClaimsForUser(userId);
	}
	
	public List<ClaimNotification> manageClaimsAsAdmin(String companyName) {
		return claimRepository.manageClaimsAsAdmin(companyName);
	}
	
	public List<ClaimNotification> getAllClaims() {
		return claimRepository.getAllClaims();
	}
	
	public ClaimNotification getClaimByClaimId(final String claimId) {
		return claimRepository.getClaimByClaimId(claimId);
	}
	
	public ClaimNotification getClaimWithPolicyDataByClaimId(final String claimId) {
		return claimRepository.getClaimWithPolicyDataByClaimId(claimId);
	}
	
}
