package com.aiknights.com.rapidviewmain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.NotificationDBRepository;
import com.aiknights.com.rapidviewmain.models.WebSocketMessageModel;

@Service
public class NotificationDBService {

	@Autowired
	NotificationDBRepository notificationDBRepo;

	public Boolean putMessagetoDB(String notification_Object_ID,String notification_Topic,String notification_Type_ID,String notification_Claim_ID,String created_on,String notification_Subject,String message_content,String read_Status,String isDeleted) {
		return notificationDBRepo.putMessagetoDB(notification_Object_ID,notification_Topic,notification_Type_ID,notification_Claim_ID,created_on,notification_Subject,message_content,read_Status,isDeleted);
	}

	public Boolean putMessageSendertoDB(String notification_Sender_UID, String notification_Object_ID, String senderUserID, String Sender_read_Status, String Sender_isDeleted) {
		return notificationDBRepo.putSendertoDB(notification_Sender_UID, notification_Object_ID, senderUserID, Sender_read_Status, Sender_isDeleted);
	}

	public Boolean putMessageReceivertoDB(String notification_Receiver_UID, String notification_Object_ID, String receiverUserID, String Receiver_read_Status, String Receiver_isDeleted) {
		return notificationDBRepo.putReceivertoDB(notification_Receiver_UID, notification_Object_ID, receiverUserID, Receiver_read_Status, Receiver_isDeleted);
	}

	public List<WebSocketMessageModel> getMessagesOfUser(String UserID) {
		return notificationDBRepo.getMessagesOfUser(UserID);
	}

	public boolean markNotificationAsRead(String userID, String notification_Object_ID) {
		return notificationDBRepo.markNotificationAsRead(userID,notification_Object_ID);
		
	}
	
	public boolean deletedNotificationAsReceiver(String userID, String notification_Object_ID) {
		return notificationDBRepo.deletedNotificationAsReceiver(userID,notification_Object_ID);
	}

}