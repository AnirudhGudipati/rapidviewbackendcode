package com.aiknights.com.rapidviewmain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.UploadWeatherAssessmentDetailstoDBDAORepository;
import com.aiknights.com.rapidviewmain.models.PaypalPaymentModel;
import com.aiknights.com.rapidviewmain.models.WeatherAssessmentDataModel;

@Service
public class UploadWeatherAssessmentDetailstoDBService {

	@Autowired
	private UploadWeatherAssessmentDetailstoDBDAORepository uploadtoDBRepo;
	
	public int UploadWeatherAssessmentDetailstoDB(String OwnerName,String CarrierName, String Claimid, String Address,String PolicyStartDate,String ClaimDate) {
		return uploadtoDBRepo.UploadWeatherAssessmentDetailstoDB(OwnerName, CarrierName, Claimid, Address, PolicyStartDate, ClaimDate);
	}

	public int UploadWeatherAssessmentDetails(WeatherAssessmentDataModel weatherAssessmentDataModel) {
		return uploadtoDBRepo.UploadWeatherAssessmentDetailsto(weatherAssessmentDataModel);
		
	}

	public List<WeatherAssessmentDataModel> getAssigedWeatherassessmentForUser(int userId) {
		// TODO Auto-generated method stub
		return uploadtoDBRepo.getAssigedWeatherassessmentForUser(userId);
	}
	
	public List<WeatherAssessmentDataModel> manageWeatherReportsAsAdmin(String companyName) {
		// TODO Auto-generated method stub
		return uploadtoDBRepo.manageWeatherReportsAsAdmin(companyName);
	}

	public boolean  getWeatherAssessmentDetailsByReportId(String reportId) {

		return uploadtoDBRepo.getWeatherAssessmentDetailsByReportId(reportId);
	}

	public boolean uploadpayPalPaymentInfo(PaypalPaymentModel payPalPayment,String UserId,byte[] paypalPaymentObject) {
		
		return uploadtoDBRepo.uploadpayPalPaymentInfo(payPalPayment,UserId,paypalPaymentObject);
	}

	public WeatherAssessmentDataModel getWeatherReportById(String reportId) {
		return uploadtoDBRepo.getWeatherReportById(reportId);
	}
	
	public int MarkWeatherStagingCompleted(String reportId) {
		return uploadtoDBRepo.MarkWeatherStagingCompleted(reportId);
	}

}
