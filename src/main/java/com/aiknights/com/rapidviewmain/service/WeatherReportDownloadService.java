package com.aiknights.com.rapidviewmain.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.WeatherReportDownloadRepository;

@Service
public class WeatherReportDownloadService {
	@Autowired
	WeatherReportDownloadRepository weatherReportDownloadRepository;

	public String getweatherReportDownloadCountByUserName(String userName) {

		String downloadCount = weatherReportDownloadRepository.getweatherReportDownloadCountByUserName(userName);
		return downloadCount;

	}

	public boolean updateLatestDownloadCount(String userName, int latestDwonloadCount) {
		boolean updateStatus = weatherReportDownloadRepository.updateLatestDownloadCount(userName, latestDwonloadCount);

		return updateStatus;
	}

	public Map<String, Object> getAvailableCountByUserName(String userName) {
		// TODO Auto-generated method stub
		return weatherReportDownloadRepository.getAvailableCountByUserName(userName);
	}

}
