package com.aiknights.com.rapidviewmain.service;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.UserDetailsRepository;
import com.aiknights.com.rapidviewmain.DAO.UserRepository;
import com.aiknights.com.rapidviewmain.constants.RapidViewMainConstants;
import com.aiknights.com.rapidviewmain.models.User;
import com.aiknights.com.rapidviewmain.models.UserDetails;

@Service
public class LoginService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserDetailsRepository userDetailsRepository;

	@Autowired
	private RoleService roleService;
	@Autowired
	UserDetailsService userDetailsService;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	public ResponseEntity<Map<String, String>> isValidUser(final String userName, final String password) {
		final Map<String, String> responseMap = new HashMap<>();
		final User userLoginDetails = userRepository.findUserByUserName(userName);
		// UserDetails userdetails =
		// userDetailsService.getUserDetailsByUserName(userName);
		String userRole = roleService.getRoleByUser(userName);

		if (userLoginDetails == null) {
			responseMap.put(RapidViewMainConstants.USER_NAME, userName);
			responseMap.put(RapidViewMainConstants.STATUS, RapidViewMainConstants.USER_NOT_EXIST);
			return new ResponseEntity<>(responseMap, HttpStatus.NOT_FOUND);
		} else {
			if (passwordEncoder().matches(password, userLoginDetails.getPassword())) {
				final UserDetails userDetails = userDetailsRepository.findUserDetailsByUserName(userName);
				if (!userRole.equalsIgnoreCase("UNASSIGNEDROLE")) {

					LocalDate license_EndDate = userRepository.findLicenseEndDateByUserName(userName);
					String SubscriptionType = userRepository.findSubscriptionTypeByUserName(userName);
					String MembershipLevel = userRepository.findMembershipLevelByUserName(userName);
					String ProductId = userRepository.findProductIdByUserName(userName);
					String ProductType = userRepository.findProductTypeById(ProductId);

					responseMap.put(RapidViewMainConstants.USER_NAME, userLoginDetails.getUsername());
					responseMap.put(RapidViewMainConstants.STATUS, RapidViewMainConstants.VALID);
					responseMap.put(RapidViewMainConstants.FIRST_NAME, userDetails.getFirstName());
					responseMap.put(RapidViewMainConstants.LAST_NAME, userDetails.getLastName());
					responseMap.put(RapidViewMainConstants.USER_ID, String.valueOf(userDetails.getUserId()));
					responseMap.put(RapidViewMainConstants.EMAIL_VERIFICATION_STATUS,
							String.valueOf(userDetails.getIsEmailVerified()));
					responseMap.put(RapidViewMainConstants.FORCE_CHANGED_PASSWORD,
							userDetails.getForceChangePassword());
					responseMap.put(RapidViewMainConstants.ACTIVE_USER, userDetails.getIsActive());

					if (license_EndDate != null) {
						LocalDate CurrentDate = LocalDate.now();
						Period i = CurrentDate.until(license_EndDate);
						long NumberOfDaysLeftforTrail = ChronoUnit.DAYS.between(CurrentDate, license_EndDate);

						if (NumberOfDaysLeftforTrail <= 0) {
							responseMap.put(RapidViewMainConstants.LICENSE_STATUS, "Expired");
							responseMap.put(RapidViewMainConstants.LICENSE_STATUS_MESSSAGE, "You currently have "
									+ NumberOfDaysLeftforTrail + " days left in your trial offer");
							responseMap.put(RapidViewMainConstants.EXPIRY_DATE, license_EndDate.toString());
						} else {
							responseMap.put(RapidViewMainConstants.LICENSE_STATUS, "Active");
							responseMap.put(RapidViewMainConstants.LICENSE_STATUS_MESSSAGE, "You currently have "
									+ NumberOfDaysLeftforTrail + " days left in your trial offer");
							responseMap.put(RapidViewMainConstants.EXPIRY_DATE, license_EndDate.toString());
						}

					}
					responseMap.put(RapidViewMainConstants.PRODUCT_TYPE, ProductType);
					responseMap.put(RapidViewMainConstants.SUBSCRIPTION_TYPE, SubscriptionType);
					responseMap.put(RapidViewMainConstants.MEMBERSHIP_LEVEL, MembershipLevel);

				}

				else {
					responseMap.put(RapidViewMainConstants.USER_NAME, userLoginDetails.getUsername());
					responseMap.put(RapidViewMainConstants.STATUS, RapidViewMainConstants.VALID);
					responseMap.put(RapidViewMainConstants.FIRST_NAME, userDetails.getFirstName());
					responseMap.put(RapidViewMainConstants.LAST_NAME, userDetails.getLastName());
					responseMap.put(RapidViewMainConstants.USER_ID, String.valueOf(userDetails.getUserId()));
					responseMap.put(RapidViewMainConstants.EMAIL_VERIFICATION_STATUS,
							String.valueOf(userDetails.getIsEmailVerified()));
					responseMap.put(RapidViewMainConstants.LICENSE_STATUS, "Active");
					responseMap.put(RapidViewMainConstants.FORCE_CHANGED_PASSWORD,
							userDetails.getForceChangePassword());
					responseMap.put(RapidViewMainConstants.ACTIVE_USER, userDetails.getIsActive());

				}
				return new ResponseEntity<>(responseMap, HttpStatus.OK);
			} else {
				responseMap.put(RapidViewMainConstants.USER_NAME, userLoginDetails.getUsername());
				responseMap.put(RapidViewMainConstants.STATUS, RapidViewMainConstants.IN_VALID);
				return new ResponseEntity<>(responseMap, HttpStatus.FORBIDDEN);
			}
		}
	}
}
