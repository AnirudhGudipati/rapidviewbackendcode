package com.aiknights.com.rapidviewmain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.WeatherDBRepository;
import com.aiknights.com.rapidviewmain.models.ReportsStageDBModel;
import com.aiknights.com.rapidviewmain.models.StormEventsAlertModel;
import com.aiknights.com.rapidviewmain.models.WeatherEDBModel;
import com.aiknights.com.rapidviewmain.models.WeatherNexRadModel;
import com.aiknights.com.rapidviewmain.models.WeatherPLSRWindModel;
import com.aiknights.com.rapidviewmain.models.WeatherStageDatabaseModel;
import com.aiknights.com.rapidviewmain.models.WeatherThunderWindModel;

@Service
public class WeatherDBService {

	@Autowired
	private WeatherDBRepository weatherDBRepository;
	
	public List<WeatherEDBModel> getWeatherDBDataByClaimId(final String startdate, String enddate, String latitude, String longitude, String startdist, String enddist) {
		return weatherDBRepository.getWeatherDBByClaimid(startdate,enddate,latitude,longitude,startdist,enddist);
	}
	
	public List<WeatherNexRadModel> getWeatherNexRadDataByClaimId(final String startdate, String enddate, String latitude, String longitude, String startdist, String enddist) {
		return weatherDBRepository.getWeatherNexRadByClaimid(startdate,enddate,latitude,longitude,startdist,enddist);
	}
	
	public List<WeatherPLSRWindModel> getWeatherPLSRWindDataByClaimid(final String startdate,final String enddate,final String latitude, final String longitude, final String startdist, final String enddist){
		return weatherDBRepository.getWeatherPLSRWindByClaimid(startdate,enddate,latitude,longitude,startdist,enddist);
	}
	
	public List<WeatherThunderWindModel> getWeatherThunderWindDataByClaimid(final String startdate,final String enddate, final String latitude, final String longitude, final String startdist, final String enddist){
		return weatherDBRepository.getWeatherThunderWindByClaimid(startdate,enddate,latitude,longitude,startdist,enddist);
	}
	
	public boolean stageWeatherDataByClaimID(List<WeatherStageDatabaseModel> wt) {
		return weatherDBRepository.stageWeatherDataByClaimID(wt);
	}
	
	public boolean stageWeatherDataCheckbyClaimID(String claimid) {
		return weatherDBRepository.stageWeatherDataCheckbyClaimID(claimid);
	}

	public List<WeatherStageDatabaseModel> getWeatherDataFromStageByClaimID(String claimid) {
		return weatherDBRepository.getWeatherDataFromStageByClaimID(claimid);
	}
	
	public boolean stageWeatherDataByReportID(List<ReportsStageDBModel> wt) {
		return weatherDBRepository.stageWeatherDataByReportID(wt);
	}
	
	public boolean stageWeatherDataCheckbyReportID(String reportid) {
		return weatherDBRepository.stageWeatherDataCheckbyReportID(reportid);
	}

	public List<ReportsStageDBModel> getWeatherDataFromStageByReportID(String reportid) {
		return weatherDBRepository.getWeatherDataFromStageByReportID(reportid);
	}
	public List<StormEventsAlertModel> getEventSeverity(String state){
		return weatherDBRepository.getEventsSeverity(state);
	}
	public List<String> getWeatherSubscriptionUserList(){
		return weatherDBRepository.getWeatherSubscriptionUserList();
	}
	public List<String> getStatesForSubscribedUser(String userid){
		return weatherDBRepository.getStatesForSubscribedUser(userid);
	}
	public String convertToAlertNotificationMessage(List<String> states,List<StormEventsAlertModel> events) {
		return weatherDBRepository.convertToAlertNotificationMessage(states, events);
	}
	public String convertToAlertNotificationMessageEmail(List<String> states,List<StormEventsAlertModel> events) {
		return weatherDBRepository.convertToAlertNotificationMessageEmail(states, events);
	}
}
