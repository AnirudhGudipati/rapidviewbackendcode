package com.aiknights.com.rapidviewmain.service;

import java.util.ArrayList;
import java.util.List;

import com.aiknights.com.rapidviewmain.models.WeatherOutputModel;

public class WeatherReportifyService {
	int count;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<WeatherOutputModel> Reportify(List<WeatherOutputModel> wt) {
		List<WeatherOutputModel> ft = new ArrayList<WeatherOutputModel>();
		String fdateold = "";
		count = 1;
		for (int i = 0; i < wt.size(); i++) {
			WeatherOutputModel fom = new WeatherOutputModel();
			String fdate = wt.get(i).getEventdate();
			double fzeroone;
			double fonethree;
			double fthreefive;
			double ffiveten;
			if (wt.get(i).getZeroonemag() != null) {
				fzeroone = Double.parseDouble(wt.get(i).getZeroonemag());
			} else {
				fzeroone = 0;
			}
			if (wt.get(i).getOnethreemag() != null) {
				fonethree = Double.parseDouble(wt.get(i).getOnethreemag());
			} else {
				fonethree = 0;
			}
			if (wt.get(i).getThreefivemag() != null) {
				fthreefive = Double.parseDouble(wt.get(i).getThreefivemag());
			} else {
				fthreefive = 0;
			}
			if (wt.get(i).getFivetenmag() != null) {
				ffiveten = Double.parseDouble(wt.get(i).getFivetenmag());
			} else {
				ffiveten = 0;
			}
			if (fdate.compareTo(fdateold) == 0) {
				String predate = wt.get(i - 1).getEventdate();
				double prezeroone = 0;
				double preonethree = 0;
				double prethreefive = 0;
				double prefiveten = 0;
				if (wt.get(i - 1).getZeroonemag() != null) {
					prezeroone = Double.parseDouble(wt.get(i - 1).getZeroonemag());
				}
				if (wt.get(i - 1).getOnethreemag() != null) {
					preonethree = Double.parseDouble(wt.get(i - 1).getOnethreemag());
				}
				if (wt.get(i - 1).getThreefivemag() != null) {
					prethreefive = Double.parseDouble(wt.get(i - 1).getThreefivemag());
				}
				if (wt.get(i - 1).getFivetenmag() != null) {
					prefiveten = Double.parseDouble(wt.get(i - 1).getFivetenmag());
				}
				double maxzeroone = Double.max(prezeroone, fzeroone);
				double maxonethree = Double.max(preonethree, fonethree);
				double maxthreefive = Double.max(prethreefive, fthreefive);
				double maxfiveten = Double.max(prefiveten, ffiveten);
				String smaxzeroone = Double.toString(maxzeroone);
				String smaxonethree = Double.toString(maxonethree);
				String smaxthreefive = Double.toString(maxthreefive);
				String smaxfiveten = Double.toString(maxfiveten);
				fom.setEventdate(predate);
				fom.setZeroonemag(smaxzeroone);
				fom.setOnethreemag(smaxonethree);
				fom.setThreefivemag(smaxthreefive);
				fom.setFivetenmag(smaxfiveten);
				ft.add(fom);
				ft.get(i - 1).setCorrect("delete");
				wt.get(i).setFivetenmag(smaxfiveten);
				wt.get(i).setOnethreemag(smaxonethree);
				wt.get(i).setThreefivemag(smaxthreefive);
				wt.get(i).setZeroonemag(smaxzeroone);
			} else {
				fom.setEventdate(fdate);
				String okzeronone = Double.toString(fzeroone);
				String okonethree = Double.toString(fonethree);
				String okthreefive = Double.toString(fthreefive);
				String okfiveten = Double.toString(ffiveten);
				fom.setZeroonemag(okzeronone);
				fom.setOnethreemag(okonethree);
				fom.setThreefivemag(okthreefive);
				fom.setFivetenmag(okfiveten);
				ft.add(fom);
			}
			fdateold = fdate;
		}
		List<WeatherOutputModel> xt = new ArrayList<WeatherOutputModel>();
		for (int i = 0; i < ft.size(); i++) {
			WeatherOutputModel xom = new WeatherOutputModel();
			if (ft.get(i).getCorrect() != "delete") {

				xom.setEventdate(ft.get(i).getEventdate());
				xom.setFivetenmag(ft.get(i).getFivetenmag());
				xom.setOnethreemag(ft.get(i).getOnethreemag());
				xom.setThreefivemag(ft.get(i).getThreefivemag());
				xom.setZeroonemag(ft.get(i).getZeroonemag());
				xt.add(xom);
				count = count + 1;
			}
		}
		for (int i = 0; i < xt.size(); i++) {
			if (xt.get(i).getZeroonemag().compareTo("0.0")==0) {
				xt.get(i).setZeroonemag("--");
			}
			if (xt.get(i).getOnethreemag().compareTo("0.0")==0) {
				xt.get(i).setOnethreemag("--");
			}
			if (xt.get(i).getThreefivemag().compareTo("0.0")==0) {
				xt.get(i).setThreefivemag("--");
			}
			if (xt.get(i).getFivetenmag().compareTo("0.0")==0) {
				xt.get(i).setFivetenmag("--");
			}
		}
		setCount(count);
		return xt;
		
	}
}
