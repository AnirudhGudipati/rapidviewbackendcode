package com.aiknights.com.rapidviewmain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.UserProfileRepository;
import com.aiknights.com.rapidviewmain.models.UserDetails;

@Service
public class UserProfileService {
	@Autowired
	UserProfileRepository userProfileRepository;

	public UserDetails getProfileDetailsByUserId(String user_Id) {
		 return userProfileRepository.getProfileDetailsByUserId(user_Id);
	
	}

	public boolean updateProfileDetailsByUserName(String userName,String address,String contactNumber) {
		return userProfileRepository.updateProfileDetailsByUserName(userName,address,contactNumber);
	}

}
