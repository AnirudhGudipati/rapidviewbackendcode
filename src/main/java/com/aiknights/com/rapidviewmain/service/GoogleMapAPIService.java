package com.aiknights.com.rapidviewmain.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

import com.aiknights.com.rapidviewmain.models.GMapOutputParentModel;

public class GoogleMapAPIService {
	@Value("${gmapapikey}")
	String key;
	public GMapOutputParentModel GetLatLongService(String address){

		
		RestTemplate rt = new RestTemplate();
		String GmapRequest="https://maps.googleapis.com/maps/api/geocode/json?address="
				+ address + "&location_type=ROOFTOP&key=AIzaSyA8y69GuUs-jLmtu5JWHN0DuyNfnyLYkwA";
		System.out.println(GmapRequest);
		GMapOutputParentModel wm = rt.getForObject(GmapRequest, GMapOutputParentModel.class);
		return wm;
	}
}
