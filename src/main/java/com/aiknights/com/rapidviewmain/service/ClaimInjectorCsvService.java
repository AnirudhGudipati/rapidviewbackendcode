package com.aiknights.com.rapidviewmain.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import com.aiknights.com.rapidviewmain.DAO.ClaimInjectorCsvRepository;
import com.aiknights.com.rapidviewmain.controller.WeatherDBController;
import com.aiknights.com.rapidviewmain.dto.EndorsementResultDTO;
import com.aiknights.com.rapidviewmain.dto.PolicyResultDTO;
import com.aiknights.com.rapidviewmain.models.AdjusterClaimCsvModel;
import com.aiknights.com.rapidviewmain.models.ClaimInjectorCsv;
import com.aiknights.com.rapidviewmain.models.ClaimInjectorExcelModel;
import com.aiknights.com.rapidviewmain.models.ClaimNotificationCsv;
import com.aiknights.com.rapidviewmain.models.EndorsementCsvModel;
import com.aiknights.com.rapidviewmain.models.PolicyCheckCsvModel;
import com.aiknights.com.rapidviewmain.models.PropertyCsvModel;

@Service
public class ClaimInjectorCsvService {

	@Autowired
	private ClaimInjectorCsvRepository claimCsvRepository;

	@Autowired
	private WeatherDBController wdbController;

	public boolean insertIntoClaimTable(ClaimNotificationCsv claim_notification,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) throws ParseException {
		return claimCsvRepository.insertIntoClaimNotificationCsv(claim_notification, namedParameterJdbcTemplate);
	}

	public PolicyResultDTO insertIntoPolicyCheckCsv(PolicyCheckCsvModel policyCheckCsvModel,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) throws ParseException {
		return claimCsvRepository.insertIntoPolicyCheckCsv(policyCheckCsvModel, namedParameterJdbcTemplate);
	}

	public boolean insertIntoPropertyTable(PropertyCsvModel property_data,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) throws ParseException {
		return claimCsvRepository.insertIntoPropertyTableCsv(property_data, namedParameterJdbcTemplate);
	}

	public EndorsementResultDTO insertIntoEndorsementTable(EndorsementCsvModel endorsement_Csv_Model,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) throws ParseException {
		return claimCsvRepository.insertIntoEndorsementTableCsv(endorsement_Csv_Model, namedParameterJdbcTemplate);
	}

	public boolean insertIntoAdjusterClaimTable(AdjusterClaimCsvModel adjuster_Claim_Model,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) throws ParseException {
		return claimCsvRepository.insertIntoAdjusterClaimTableCsv(adjuster_Claim_Model, namedParameterJdbcTemplate);
	}

	public boolean deleteUltil(String tableName, String key, String value) {
		return claimCsvRepository.deleteUltil(tableName, key, value);
	}

	/*
	 * Method to set the values to ClaimNotificationCsv table from CSVData
	 */

	public boolean insertIntoClaimTableSetting(ClaimInjectorExcelModel csvDataList,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate, PolicyResultDTO output_policy,
			String policyIncidentDate, String policyClaimDate) throws ParseException {

		ClaimNotificationCsv claim_Data = new ClaimNotificationCsv();

		claim_Data.setClaimId(csvDataList.getClaimId());
		claim_Data.setClaimantFirstName(csvDataList.getClaimantFirstName());
		claim_Data.setClaimantLastName(csvDataList.getClaimantLastName());
		claim_Data.setEmail(csvDataList.getEmail());
		claim_Data.setPhoneNumber(csvDataList.getPhoneNumber());
		claim_Data.setAddress(csvDataList.getPropertyAddress());
		claim_Data.setCity(csvDataList.getCity());
		claim_Data.setState(csvDataList.getState());
		claim_Data.setZipCode(csvDataList.getZipCode());
		claim_Data.setPolicyNumber(csvDataList.getPolicyNumber());
		claim_Data.setIncidentType(csvDataList.getIncidentType());
		claim_Data.setIncidentDate(policyIncidentDate);
		claim_Data.setIncidentTime(csvDataList.getIncidentTime());
		claim_Data.setClaimDate(policyClaimDate);
		claim_Data.setClaimAmount("100");
		claim_Data.setClaimstatus(csvDataList.getClaim_Status());
		claim_Data.setPolicy_id(Integer.toString(output_policy.getPolicy_id_pk()));
		claim_Data.setClaimDescription(csvDataList.getClaimDescription());
		return insertIntoClaimTable(claim_Data, namedParameterJdbcTemplate);

	}

	/*
	 * Method to set the values to PolicyCheckCsvModel from CSVData
	 */

	public PolicyResultDTO insertIntoPolicyTableSetting(ClaimInjectorExcelModel csvDataList,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate, EndorsementResultDTO output_Endorsement,
			String policyStartDate, String policyEndDate, String policyLastUpdateDate) throws ParseException {

		PolicyCheckCsvModel policy_data = new PolicyCheckCsvModel();

		policy_data.setPolicy_id(null);
		policy_data.setCoverageAmt(csvDataList.getCoverageAmt());
		policy_data.setDeductible(csvDataList.getDeductibleAmount());
		policy_data.setEndDate(policyEndDate);
		policy_data.setEndorsementId(Integer.toString(output_Endorsement.getEndorsement_id_pk()));

		policy_data.setInsuranceCompany(csvDataList.getInsuranceCompany());
		policy_data.setMortgageCompany("LIBERTY MUTUAL INSURANCE");
		policy_data.setPolicyLastUpdateD(policyLastUpdateDate);
		policy_data.setPolicyLimit(csvDataList.getPolicyLimitAmount());
		policy_data.setPolicyNumber(csvDataList.getPolicyNumber());
		policy_data.setPolicyStatus(csvDataList.getPolicyStatus());
		policy_data.setPolicyType("Hail Strom Coverage");
		policy_data.setPriorClaims(csvDataList.getPriorClaims());
		policy_data.setStartDate(policyStartDate);
		policy_data.setPolicyCoverage(csvDataList.getPolicyCoverage());

		return insertIntoPolicyCheckCsv(policy_data, namedParameterJdbcTemplate);

	}

	/*
	 * Method to set the values to PropertyCsvModel from CSVData
	 */
	public boolean insertIntoPropertyTableSetting(ClaimInjectorExcelModel csvDataList,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate, PolicyResultDTO output_policy,
			String propertyPurchaseDate) throws ParseException {

		PropertyCsvModel property_data = new PropertyCsvModel();

		property_data.setProperty_id(null);
		property_data.setPropertyType(csvDataList.getProperty_Type());
		property_data.setPropertyPurchaseDate(propertyPurchaseDate);
		property_data.setCity("Athens");
		property_data.setState("GA");
		property_data.setStreetAddress(csvDataList.getPropertyAddress());
		property_data.setZipcode("30009");
		property_data.setPolicy_id(Integer.toString(output_policy.getPolicy_id_pk()));

		return insertIntoPropertyTable(property_data, namedParameterJdbcTemplate);

	}

	/*
	 * Method to set the values to EndorsementCsvModel from CSVData
	 */

	public EndorsementResultDTO insertIntoEndorsementTableSetting(ClaimInjectorExcelModel csvDataList,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) throws ParseException {

		EndorsementCsvModel endorsement_Csv_Model = new EndorsementCsvModel();

		endorsement_Csv_Model.setEndorsement_id(null);
		endorsement_Csv_Model.setEndorsement_type(csvDataList.getEndorsement_type());
		endorsement_Csv_Model.setEndorsement_type_desc(csvDataList.getEndorsement_type_desc());
		endorsement_Csv_Model.setHail(null);
		endorsement_Csv_Model.setHurricane(null);
		endorsement_Csv_Model.setWater(null);
		endorsement_Csv_Model.setWind(null);

		return insertIntoEndorsementTable(endorsement_Csv_Model, namedParameterJdbcTemplate);
	}

	/*
	 * Method to set the values to AdjusterClaimModel from CSVData
	 */

	public boolean insertIntoAdjusterClaimTableSetting(ClaimInjectorExcelModel claimInjectorCsv,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) throws ParseException {

		AdjusterClaimCsvModel adjuster_Claim_Model = new AdjusterClaimCsvModel();

		adjuster_Claim_Model.setAdjusterId(claimInjectorCsv.getAdjuster_ID());
		adjuster_Claim_Model.setAdjuster_Email(claimInjectorCsv.getAdjuster_email());
		adjuster_Claim_Model.setAdjuster_FirstName(claimInjectorCsv.getAdjuster_FName());
		adjuster_Claim_Model.setAdjuster_LastName(claimInjectorCsv.getAdjuster_LName());
		adjuster_Claim_Model.setAdjuster_PhoneNumber(claimInjectorCsv.getAdjuster_phone());
		adjuster_Claim_Model.setAdjusterAddress("AdjusterAddress");
		adjuster_Claim_Model.setAdjusterCompany("AdjusterCompany");
		adjuster_Claim_Model.setAssignedDate("2020-01-10");
		adjuster_Claim_Model.setClaimId(claimInjectorCsv.getClaimId());
		adjuster_Claim_Model.setExpertise("setExpertise");

		return insertIntoAdjusterClaimTable(adjuster_Claim_Model, namedParameterJdbcTemplate);
	}

	public ClaimInjectorCsv editcalimModelSetting(Map<String, String> editClaimDetails) {
		ClaimInjectorCsv editCalimModel = new ClaimInjectorCsv();
		editCalimModel.setPropertyAddress(editClaimDetails.get("propertyAddress").toString());
		editCalimModel.setAdjuster_email(editClaimDetails.get("adjuster_email"));
		editCalimModel.setAdjuster_FName(editClaimDetails.get("adjuster_FName"));
		editCalimModel.setAdjuster_ID(editClaimDetails.get("adjuster_ID"));
		editCalimModel.setAdjuster_LName(editClaimDetails.get("adjuster_LName"));
		editCalimModel.setAdjuster_phone(editClaimDetails.get("adjuster_phone"));
		editCalimModel.setCity(editClaimDetails.get("city"));
		editCalimModel.setClaim_Status(editClaimDetails.get("claim_Status"));
		editCalimModel.setClaimantFirstName(editClaimDetails.get("claimantFirstName"));
		editCalimModel.setClaimantLastName(editClaimDetails.get("claimantLastName"));
		editCalimModel.setClaimDate(editClaimDetails.get("claimDate"));
		editCalimModel.setClaimId(editClaimDetails.get("claimId"));
		editCalimModel.setCoverageAmt(editClaimDetails.get("coverageAmt"));
		editCalimModel.setDeductibleAmount(editClaimDetails.get("deductibleAmount"));
		editCalimModel.setEmail(editClaimDetails.get("email"));
		editCalimModel.setPolicyEndDate(editClaimDetails.get("policyEndDate"));
		editCalimModel.setEndorsement_type(editClaimDetails.get("endorsement_type"));
		editCalimModel.setEndorsement_type_desc(editClaimDetails.get("endorsement_type_desc"));
		editCalimModel.setClaimDescription(editClaimDetails.get("claimDescription"));
		editCalimModel.setIncidentDate(editClaimDetails.get("incidentDate"));
		editCalimModel.setIncidentTime(editClaimDetails.get("incidentTime"));
		editCalimModel.setIncidentType(editClaimDetails.get("incidentType"));
		editCalimModel.setInjectorStatus(editClaimDetails.get(""));
		editCalimModel.setInsuranceCompany(editClaimDetails.get("insuranceCompany"));
		editCalimModel.setPhoneNumber(editClaimDetails.get("phoneNumber"));
		editCalimModel.setPolicyCoverage(editClaimDetails.get("policyCoverage"));
		editCalimModel.setPolicyLastUpdateDate(editClaimDetails.get("policyLastUpdateDate"));
		editCalimModel.setPolicyLimitAmount(editClaimDetails.get("policyLimitAmount"));
		editCalimModel.setPolicyNumber(editClaimDetails.get("policyNumber"));
		editCalimModel.setPolicyStatus(editClaimDetails.get("policyStatus"));
		editCalimModel.setPriorClaims(editClaimDetails.get("priorClaims"));
		editCalimModel.setProperty_Purchase_Date(editClaimDetails.get("property_Purchase_Date"));
		editCalimModel.setProperty_Type(editClaimDetails.get("property_Type"));
		editCalimModel.setPolicyStartDate(editClaimDetails.get("policyStartDate"));
		editCalimModel.setState(editClaimDetails.get("state"));
		editCalimModel.setZipCode(editClaimDetails.get("zipCode"));

		return editCalimModel;
	}

	@Async
	public Future<String> CompleteWeatherStagging(List<ClaimInjectorExcelModel> successCsvDataList) {

		List<ClaimInjectorExcelModel> csvDataList_success = new ArrayList<ClaimInjectorExcelModel>();
		csvDataList_success.addAll(successCsvDataList);
		String stageoutput = null;

		for (int i = 0; i < csvDataList_success.size(); i++) {

			if (csvDataList_success.get(0).getInjectorStatus() == "Success") {
				stageoutput = wdbController.stageWeatherDataByClaimID(csvDataList_success.get(i).getClaimId());
				System.out.println("Weather stage status" + stageoutput);
				System.out.println("Weather stage completed ");
			} else {
				System.out.println("Weather stage not requried for failuer claims ");
			}

		}
		return new AsyncResult<>(stageoutput);

	}

	public boolean chcekClaimIdIsDuplicate(ClaimInjectorExcelModel claimInjectorCsv) {

		return claimCsvRepository.chcekIsClaimIdDuplicate(claimInjectorCsv);
	}

	public boolean getByPolicyNumber(String policyNumber) {
		return claimCsvRepository.getByPolicyNumber(policyNumber);

	}

}
