package com.aiknights.com.rapidviewmain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.DashboardAPIRepository;
import com.aiknights.com.rapidviewmain.models.DashboardClaimCategoryModel;
import com.aiknights.com.rapidviewmain.models.DashboardTotalClaimsModel;

@Service
public class DashboardAPIService {

	@Autowired
	private DashboardAPIRepository dashboardrepo;
	
	public DashboardTotalClaimsModel getTotalClaimsData(String userid) {
		return dashboardrepo.getTotalClaimsData(userid);
	}

	public DashboardClaimCategoryModel getClaimCategoryData(String userid) {
		return dashboardrepo.getClaimCategoryData(userid);
	}

}
