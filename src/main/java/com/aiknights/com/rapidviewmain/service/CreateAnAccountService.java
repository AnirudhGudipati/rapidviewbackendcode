package com.aiknights.com.rapidviewmain.service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.CreateAnAccountRepository;
import com.aiknights.com.rapidviewmain.controller.WeatherReportDownloadController;
import com.aiknights.com.rapidviewmain.dto.RoleResultsDTO;
import com.aiknights.com.rapidviewmain.dto.UserDetailsResultsDTO;
import com.aiknights.com.rapidviewmain.models.CreateProfileRequestModel;
import com.aiknights.com.rapidviewmain.models.LicensingModel;
import com.aiknights.com.rapidviewmain.models.Resource;
import com.aiknights.com.rapidviewmain.models.Role;
import com.aiknights.com.rapidviewmain.models.User;
import com.aiknights.com.rapidviewmain.models.UserDetails;
import com.aiknights.com.rapidviewmain.models.WeatherDownloadCountModel;

@Service
public class CreateAnAccountService {
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	CreateAnAccountRepository createAnAccountRepository;
	@Autowired
	RoleService roleservice;
	private static final Logger logger = LoggerFactory.getLogger(WeatherReportDownloadController.class);

	public boolean insertIntoUsersSetting(Map<String, String> accountDetails,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		Date date = new Date();
		System.out.println(new Timestamp(date.getTime()));
		User user = new User();
		user.setUsername(accountDetails.get("userNameOfAccount"));
		user.setPassword(passwordEncoder.encode((accountDetails.get("passwordOfAccount"))));
		user.setEnabled(true);
		user.setCreatedDate(new Timestamp(date.getTime()));
		return createAnAccountRepository.insertIntoUsers(user, namedParameterJdbcTemplate);

	}

	public UserDetailsResultsDTO insertIntoUserDetailsSetting(Map<String, String> accountDetails,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		String address;
		UserDetails userDetails = new UserDetails();
		userDetails.setEmail(accountDetails.get("emailOfAccount"));
		userDetails.setFirstName(accountDetails.get("firstNameOfAccount"));
		userDetails.setLastName(accountDetails.get("lastNameOfAccount"));
		userDetails.setUserName(accountDetails.get("userNameOfAccount"));
		userDetails.setCity(accountDetails.get("cityOfAccount"));
		userDetails.setcompanyName(accountDetails.get("companyOfAccount"));
		userDetails.setContactNumber(accountDetails.get("contactNumber"));
		address = accountDetails.get("addressLine1OfAccount") + "" + accountDetails.get("addressLine2OfAccount") + ","
				+ accountDetails.get("stateOfAccount") + " " + accountDetails.get("zipcodeOfAccount") + ","
				+ accountDetails.get("countryOfAccount");
		userDetails.setAddress(address);
		userDetails.setIsProfileCreated("0");
		userDetails.setIsEmailVerified("0");
		userDetails.setIsActive("1");
		userDetails.setWeatheSubscription("0");
		userDetails.setForceChangePassword("0");
		return createAnAccountRepository.insertIntoUserDetails(userDetails, namedParameterJdbcTemplate);

	}

	public RoleResultsDTO insertIntoRoleSetting(Map<String, String> accountDetails,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		Role roleDetails = new Role();
		roleDetails.setRolename("UNASSIGNEDROLE");
		roleDetails.setUsername(accountDetails.get("userNameOfAccount"));
		return createAnAccountRepository.insertIntoRole(roleDetails, namedParameterJdbcTemplate);

	}

	public RoleResultsDTO insertIntoRoleSettingProfile(CreateProfileRequestModel accountDetails,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		Role roleDetails = new Role();
		roleDetails.setRolename(accountDetails.getRoleType());
		roleDetails.setUsername(accountDetails.getUserName());
		List<String> RoleforUser = roleservice.getRolesByUser(accountDetails.getUserName());
		if (RoleforUser.get(0).toString() != null) {
			return createAnAccountRepository.updateIntoRole(roleDetails, namedParameterJdbcTemplate);
		} else {
			return createAnAccountRepository.insertIntoRole(roleDetails, namedParameterJdbcTemplate);
		}

	}

	public boolean insertIntoResourcesSetting(CreateProfileRequestModel accountDetails,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		Resource resource = new Resource();
		String productType = accountDetails.getProductType();
		String productId = createAnAccountRepository.findProductIDWithProductType(productType);
		List<String> allocatedResourcesList = createAnAccountRepository.getallocatedResourcesByProductID(productId);

		for (int i = 0; i < allocatedResourcesList.size(); i++) {
			resource.setUserName(accountDetails.getUserName());
			resource.setResourceName(allocatedResourcesList.get(i).toString());
			createAnAccountRepository.insertIntoResource(resource, namedParameterJdbcTemplate);
		}

		return true;
	}

	public boolean findByEmail(Map<String, String> accountDetails,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {

		String emailId = accountDetails.get("emailOfAccount");
		return createAnAccountRepository.findByEmail(emailId);
	}

	public boolean findByUserName(Map<String, String> accountDetails) {

		String userName = accountDetails.get("userNameOfAccount");
		return createAnAccountRepository.findByUserName(userName);

	}

	public boolean insertIntolicenseDetails(CreateProfileRequestModel profileDetails,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		LicensingModel licensing = new LicensingModel();
		String LicenseType = profileDetails.getSubscriptionType();
		// String MembershipType = profileDetails.get("membershipType");
		String UserName = profileDetails.getUserName();
		String License_ID = Long.toString(GetIDService.getID());
		boolean licensingStatus = false;

		if (LicenseType.equalsIgnoreCase("trialUser")) {
			// Adding one Day to the current date
			LocalDate license_StartDate = LocalDate.now();
			System.out.println("Adding one day to current date: " + license_StartDate);
			// Adding one Day to the current date
			LocalDate license_EndDate = LocalDate.now().plusDays(15);
			System.out.println("Adding one day to current date: " + license_EndDate);
			Period i = license_StartDate.until(license_EndDate);
			i.getDays();
			String productId = createAnAccountRepository.findProductIDWithProductType(profileDetails.getProductType());
			if (profileDetails.getProductType().equalsIgnoreCase("RAPID View Weather")) {
				// Getting the DownloadCount and amount to be paid based on the subscriptionType
				List<WeatherDownloadCountModel> DownloadCountList = createAnAccountRepository
						.getDownloadCountSubscriptionType(LicenseType);
				logger.info("DownloadCount for   " + LicenseType + " is "
						+ DownloadCountList.get(0).getWeather_Download_MaxCount());

				String WeatherDownloadID = Long.toString(GetIDService.getID());

				// Adding Download count to user and amount to be paid
				boolean downloadCountInsertaionStatus = createAnAccountRepository.insertIntoWeatherDownloadcount(
						WeatherDownloadID, UserName, DownloadCountList.get(0).getWeather_Download_MaxCount(),
						DownloadCountList.get(0).getAmountToBe_Paid());
			}

			licensing.setProduct_ID(productId);
			licensing.setLicense_Type(LicenseType);
			licensing.setLicense_ID("Trial User");
			licensing.setUsername(profileDetails.getUserName());
			licensing.setLicense_StartDate(license_StartDate);
			licensing.setLicense_EndDate(license_EndDate);
			// licensing.setMembership_Level(profileDetails.get("membershipType"));
			licensingStatus = createAnAccountRepository.insertIntolicenseDetails(licensing);
		}

		else {
			// Adding one Day to the current date
			LocalDate license_StartDate = LocalDate.now();
			LocalDate license_EndDate = null;
			System.out.println("Adding one day to current date: " + license_StartDate);
			if (LicenseType.equalsIgnoreCase("payPerUse")) {

				// Adding one Day to the current date
				license_EndDate = LocalDate.of(2999, Month.DECEMBER, 31);
				System.out.println("Adding one day to current date: " + license_EndDate);
				Period i = license_StartDate.until(license_EndDate);
				i.getDays();
				licensing.setLicense_ID("Pay Per Use");
			}
			if (LicenseType.equalsIgnoreCase("monthly")) {
				// Adding one Day to the current date
				license_EndDate = LocalDate.now().plusDays(30);
				licensing.setLicense_ID("Monthly");
			}
			if (LicenseType.equalsIgnoreCase("halfYearly")) {
				// Adding one Day to the current date
				license_EndDate = LocalDate.now().plusDays(180);
				licensing.setLicense_ID("Half Yearly");
			}

			if (LicenseType.equalsIgnoreCase("annual")) {
				// Adding one Day to the current date
				license_EndDate = LocalDate.now().plusDays(365);
				licensing.setLicense_ID("Annual");
			}

			String productId = createAnAccountRepository.findProductIDWithProductType(profileDetails.getProductType());

			licensing.setProduct_ID(productId);
			licensing.setLicense_Type(LicenseType);

			licensing.setUsername(profileDetails.getUserName());
			licensing.setLicense_StartDate(license_StartDate);
			licensing.setLicense_EndDate(license_EndDate);
			// licensing.setMembership_Level(profileDetails.get("membershipType"));
			licensingStatus = createAnAccountRepository.insertIntolicenseDetails(licensing);
			logger.info("licensingStatus " + licensingStatus);

			if (profileDetails.getProductType().equalsIgnoreCase("Rapid View Weather")) {
				// Getting the DownloadCount and amount to be paid based on the subscriptionType
				List<WeatherDownloadCountModel> DownloadCountList = createAnAccountRepository
						.getDownloadCountSubscriptionType(LicenseType);

				String WeatherDownloadID = Long.toString(GetIDService.getID());

				// Adding Download count to user and amount to be paid
				boolean downloadCountInsertaionStatus = createAnAccountRepository.insertIntoWeatherDownloadcount(
						WeatherDownloadID, UserName, DownloadCountList.get(0).getWeather_Download_MaxCount(),
						DownloadCountList.get(0).getAmountToBe_Paid());
			}
		}

		return licensingStatus;

	}

	public String findEmailByUserName(String userName) {

		return createAnAccountRepository.findEmailByUserName(userName);
	}

	public boolean enableUserByID(String userId) {
		return createAnAccountRepository.enableUserByID(userId);
	}

	public boolean updateUserDetailsByID(int userId, CreateProfileRequestModel profileDetails) {
		return createAnAccountRepository.updateUserDetailsByID(userId, profileDetails);
	}

	public boolean insertIntoweatherAlerts(CreateProfileRequestModel profileDetails, int userid) {
		if (profileDetails.getWeatherAlerts().equalsIgnoreCase("true")) {
			return createAnAccountRepository.insertIntoweatherAlerts(profileDetails, userid);
		} else {
			return createAnAccountRepository.insertIntoweatherAlertsUserState(profileDetails, userid);
		}

	}

	public boolean markisEmailVerified(int userId) {
		return createAnAccountRepository.markisEmailVerified(userId);
	}
	
	public boolean editProductAsAdmin(String userName,String productID) {
		return createAnAccountRepository.editProductAsAdmin(userName,productID);
	}

	public boolean editRoleAsAdmin(String userName,String roleName) {
		return createAnAccountRepository.editRoleAsAdmin(userName,roleName);
	}
	
	public boolean editSubscriptionAsAdmin(String userName,String subscriptiontName) {
		return createAnAccountRepository.editSubscriptionAsAdmin(userName,subscriptiontName);
	}

}
