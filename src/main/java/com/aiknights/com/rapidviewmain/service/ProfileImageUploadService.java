package com.aiknights.com.rapidviewmain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.ProfileImageUploadRepository;

@Service
public class ProfileImageUploadService {

	@Autowired
	ProfileImageUploadRepository profileImageUploadRepository;

	public boolean uploadProfileImageFile(String profileImageName, String userId, String profile_image_path) {
		return profileImageUploadRepository.uploadProfileImageFile(profileImageName, userId, profile_image_path);

	}

	public String getProfileImageById(String userId) {
		return profileImageUploadRepository.getProfileImageById(userId);
	}

	public boolean isUserExist(String userId) {
		return profileImageUploadRepository.isUserExist(userId);
	}

	public boolean updateProfileImgaeFile(String userId, String profile_image_path, String profileImageName) {
		return profileImageUploadRepository.updateProfileImgaeFile(userId, profile_image_path, profileImageName);
	}

	public boolean deleteOldProfileUrl(String userId) {
		return profileImageUploadRepository.deleteOldProfileUrl(userId);
	}

}
