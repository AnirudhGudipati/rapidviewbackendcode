package com.aiknights.com.rapidviewmain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiknights.com.rapidviewmain.DAO.WorkOrderDBRepo;
import com.aiknights.com.rapidviewmain.models.WorkOrderDetailsResponseModel;

@Service
public class WorkOrderDBService {

	@Autowired
	WorkOrderDBRepo wODBRepo;
	
	public boolean putWorkOrderToDB(String work_Order_ID, String flight_Plan_ID, String work_Order_Status, String assigned_To, String assigned_By, String expected_Start_Date, String expected_End_Date, String created_Date, String notes, String work_Order_Approval_Status) {
		return wODBRepo.putWorkOrderToDB(work_Order_ID,flight_Plan_ID,work_Order_Status,assigned_To,assigned_By,expected_Start_Date,expected_End_Date,created_Date,notes,work_Order_Approval_Status);
	}

	public boolean updateDefaultWorkOrderByID(String workOrderStatus, String workOrderID, String assignedTo, String woStartDate, String woEndDate,	String notes) {
		return wODBRepo.updateDefaultWorkOrderByID(workOrderStatus,workOrderID,assignedTo,woStartDate,woEndDate,notes);
	}

	public List<WorkOrderDetailsResponseModel> getOrdersAsAdjuster(String userID){
		return wODBRepo.getOrdersAsAdjuster(userID);
	}
	
	public List<WorkOrderDetailsResponseModel> getOrdersAsPilot(String userID){
		return wODBRepo.getOrdersAsPilot(userID);
	}
	public List<WorkOrderDetailsResponseModel> getMarketPlaceOrders(){
		return wODBRepo.getMarketPlaceOrders();
	}
	
	public List<WorkOrderDetailsResponseModel> getOrderByWOID(String workOrderID){
		return wODBRepo.getOrderByWOID(workOrderID);
	}
		
	public boolean updateWorkOrderStatusByID(String workOrderID,String workOrderStatus, String workOrderApprovalStatus) {
		return wODBRepo.updateWorkOrderStatusByID(workOrderID,workOrderStatus,workOrderApprovalStatus);
	}
	
	public boolean updateWorkOrderReopenCount(String workOrderID,String NewCount) {
		return wODBRepo.updateWorkOrderReopenCount(workOrderID,NewCount);
	}

	public boolean updateWorkOrderMarketPlaceAssignment(String workOrderID, String workOrderStatus, String assignedTo) {
		return wODBRepo.updateWorkOrderMarketPlaceAssignment(workOrderID,workOrderStatus,assignedTo);
	}

	public boolean reOpenWorkOrderUpdateMethod(String workOrderID, String approvalStatus, String reopenComment) {
		return wODBRepo.reOpenWorkOrderUpdateMethod(workOrderID, approvalStatus, reopenComment);
	}
	
	public boolean updateWorkOrderApprovalStatus(String workOrderID, String approvalStatus) {
		return wODBRepo.updateWorkOrderApprovalStatus(workOrderID, approvalStatus);
	}
	
	public String checkAdjusterEventAvailability(String adjuster_ID, String Date, String Start_Time,String EndTime) {
		return wODBRepo.checkAdjusterEventAvailability(adjuster_ID, Date, Start_Time,EndTime);
	}
	
	public String checkPilotEventAvailability(String pilot_ID, String Date, String Start_Time,String EndTime) {
		return wODBRepo.checkPilotEventAvailability(pilot_ID, Date, Start_Time,EndTime);
	}

	public boolean createEvent(String workOrderID, String eventID, String eventLink, String adjuster_ID, String pilot_ID,String eventSystem, String startTime, String endTime, String eventDate, String timePeriod) {
		return wODBRepo.createEvent(workOrderID,eventID,eventLink,adjuster_ID,pilot_ID,eventSystem,startTime,endTime,eventDate,timePeriod);
	}
}
