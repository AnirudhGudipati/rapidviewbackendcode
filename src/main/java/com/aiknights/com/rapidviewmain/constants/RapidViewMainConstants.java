package com.aiknights.com.rapidviewmain.constants;

public final class RapidViewMainConstants {

	private RapidViewMainConstants() {

	}

	public static final String USER_NAME = "userName";
	public static final String STATUS = "status";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String VALID = "valid";
	public static final String IN_VALID = "invalid";
	public static final String USER_NOT_EXIST = "user not exist";
	public static final String ROLE = "role";
	public static final String USER_ID = "userid";
	public static final String DETECT_IMAGE_URL = "http://23.127.132.57:5000/api/detect_image";
	public static final String DETAIL_IMAGE_URL = "http://23.127.132.57:5000/api/detect_detail";
	public static final String LICENSE_STATUS = "license_status";
	public static final String LICENSE_STATUS_MESSSAGE = "license_status_message";

	public static final String PRODUCT_TYPE = "ProductType";
	public static final String SUBSCRIPTION_TYPE = "SubscriptionType";
	public static final String MEMBERSHIP_LEVEL = "MembershipLevel";
	public static final String EXPIRY_DATE = "ExpiryDate";
	public static final String EMAIL_VERIFICATION_STATUS = "emailVerificationStatus";
	public static final String POLICY_DATE_VALIDATION = "Policy End date must be after start date";
	public static final String INCIDENT_DATE_VALIDATION = "Incident date must be  in between the Policystartdate and  Policyenddate";
	public static final String FORCE_CHANGED_PASSWORD = "forceChangePassword";
	public static final String ACTIVE_USER = "isUserActive";
	public static final String ADMIN_CONTEXT_RESETPASSWORD = "ResetPassword";
	public static final String ADMIN_CONTEXT_EDIT_PROFILE = "EditProfile";
	public static final String ADMIN_CONTEXT_ACTIVATE_USER = "ActivateUser";
	public static final String ADMIN_CONTEXT_DEACTIVATE_USER = "DeActivateUser";

}
