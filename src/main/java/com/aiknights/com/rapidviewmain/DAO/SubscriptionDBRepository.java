package com.aiknights.com.rapidviewmain.DAO;

import java.util.List;

import com.aiknights.com.rapidviewmain.models.GenericSubscriptionDetailsModel;
import com.aiknights.com.rapidviewmain.models.SubscriptionPropertiesModel;
import com.aiknights.com.rapidviewmain.models.SubscriptionPropertiesResponce;

public interface SubscriptionDBRepository {

	List<SubscriptionPropertiesModel> getSubscriptionProperties();

	List<SubscriptionPropertiesResponce> getSubscriptionDetails();
	
	List<GenericSubscriptionDetailsModel> getAllSubscriptionDetailsForGivenSubscriptionTable(String SubscriptionTable);

}
