package com.aiknights.com.rapidviewmain.DAO;

import java.util.List;

import com.aiknights.com.rapidviewmain.models.ClaimNotification;

public interface ClaimNotificationRepository {
	
	ClaimNotification getClaimByClaimId(final String claimId);
	
	List<ClaimNotification> getAssignedClaimsForUser(final int userId);
	
	List<ClaimNotification> manageClaimsAsAdmin(String companyName);
	
	ClaimNotification getClaimWithPolicyDataByClaimId(final String claimId);

	List<ClaimNotification> getAllClaims();
	

}
