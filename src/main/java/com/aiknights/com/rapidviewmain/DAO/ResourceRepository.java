package com.aiknights.com.rapidviewmain.DAO;

import java.util.List;

import com.aiknights.com.rapidviewmain.models.Resource;

public interface ResourceRepository {

	Resource getResourceById(final int resourceId);

	List<String> getResourcesByUserName(final String userName);
}
