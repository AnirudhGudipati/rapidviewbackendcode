package com.aiknights.com.rapidviewmain.DAO;

import java.util.List;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.aiknights.com.rapidviewmain.dto.RoleResultsDTO;
import com.aiknights.com.rapidviewmain.dto.UserDetailsResultsDTO;
import com.aiknights.com.rapidviewmain.models.CreateProfileRequestModel;
import com.aiknights.com.rapidviewmain.models.LicensingModel;
import com.aiknights.com.rapidviewmain.models.Resource;
import com.aiknights.com.rapidviewmain.models.Role;
import com.aiknights.com.rapidviewmain.models.User;
import com.aiknights.com.rapidviewmain.models.UserDetails;
import com.aiknights.com.rapidviewmain.models.WeatherDownloadCountModel;

public interface CreateAnAccountRepository {

	boolean insertIntoUsers(User user, NamedParameterJdbcTemplate namedParameterJdbcTemplate);

	UserDetailsResultsDTO insertIntoUserDetails(UserDetails userDetails,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate);

	RoleResultsDTO insertIntoRole(Role roleDetails, NamedParameterJdbcTemplate namedParameterJdbcTemplate);

	boolean insertIntoResource(Resource resource, NamedParameterJdbcTemplate namedParameterJdbcTemplate);

	boolean findByEmail(String emailId);

	boolean findByUserName(String userName);

	RoleResultsDTO updateIntoRole(Role roleDetails, NamedParameterJdbcTemplate namedParameterJdbcTemplate);

	boolean insertIntolicenseDetails(LicensingModel licensing);

	String findProductIDWithProductType(String string);

	List<String> getallocatedResourcesByProductID(String productId);

	List<WeatherDownloadCountModel> getDownloadCountSubscriptionType(String MembershipType);

	boolean insertIntoWeatherDownloadcount(String WeatherDownloadID, String userName,String Weather_Download_MaxCount,String AmountToBe_Paid );

	String getpaymentInfoBySubscriptionType(String Subscription_Type);

	boolean insertIntoPaymentInfo(String Payment_InfoID, String AmountToBe_Paid, String username, String Payment_Status);

	String findEmailByUserName(String userName);

	boolean enableUserByID(String userId);

	boolean updateUserDetailsByID(int userId,CreateProfileRequestModel profileDetails);

	boolean insertIntoweatherAlerts(CreateProfileRequestModel profileDetails,int userid);

	boolean insertIntoweatherAlertsUserState(CreateProfileRequestModel profileDetails, int userid);

	boolean markisEmailVerified(int userId);

	boolean editProductAsAdmin(String userName, String productID);

	boolean editRoleAsAdmin(String userName, String roleName);

	boolean editSubscriptionAsAdmin(String userName, String subscriptiontName);


}
