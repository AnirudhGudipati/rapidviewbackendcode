package com.aiknights.com.rapidviewmain.DAO;

public interface ProfileImageUploadRepository {

	boolean uploadProfileImageFile(String profileImageName, String userId, String profile_image_path);

	String getProfileImageById(String userId);

	boolean isUserExist(String userId);

	boolean updateProfileImgaeFile(String userId, String profile_image_path,String profileImageName);

	boolean deleteOldProfileUrl(String userId);

}
