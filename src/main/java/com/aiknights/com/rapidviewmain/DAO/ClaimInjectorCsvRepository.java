package com.aiknights.com.rapidviewmain.DAO;

import java.text.ParseException;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.aiknights.com.rapidviewmain.dto.EndorsementResultDTO;
import com.aiknights.com.rapidviewmain.dto.PolicyResultDTO;
import com.aiknights.com.rapidviewmain.models.AdjusterClaimCsvModel;
import com.aiknights.com.rapidviewmain.models.ClaimInjectorExcelModel;
import com.aiknights.com.rapidviewmain.models.ClaimNotificationCsv;
import com.aiknights.com.rapidviewmain.models.EndorsementCsvModel;
import com.aiknights.com.rapidviewmain.models.PolicyCheckCsvModel;
import com.aiknights.com.rapidviewmain.models.PropertyCsvModel;

public interface ClaimInjectorCsvRepository {

	boolean insertIntoClaimNotificationCsv(ClaimNotificationCsv claim_notification,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) throws ParseException;

	boolean insertIntoPropertyTableCsv(PropertyCsvModel property_data,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) throws ParseException;

	EndorsementResultDTO insertIntoEndorsementTableCsv(EndorsementCsvModel endorsement_Csv_Model,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate);

	boolean insertIntoAdjusterClaimTableCsv(AdjusterClaimCsvModel adjuster_Claim_Model,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate);

	PolicyResultDTO insertIntoPolicyCheckCsv(PolicyCheckCsvModel policyCheckCsvModel,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate);

	public boolean deleteUltil(String tableName, String key, String value);

	boolean chcekIsClaimIdDuplicate(ClaimInjectorExcelModel claimInjectorCsv);

	boolean getByPolicyNumber(String policyNumber);

}
