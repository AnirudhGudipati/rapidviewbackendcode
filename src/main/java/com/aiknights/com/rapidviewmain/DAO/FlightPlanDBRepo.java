package com.aiknights.com.rapidviewmain.DAO;

import java.util.List;

import com.aiknights.com.rapidviewmain.models.FlightPlanObjectDBModel;
import com.aiknights.com.rapidviewmain.models.FlightPlanPropertiesDBModel;
import com.aiknights.com.rapidviewmain.models.WayPointActionDBModel;
import com.aiknights.com.rapidviewmain.models.WayPointDataDBModel;

public interface FlightPlanDBRepo {

	boolean putFlighPlanDataToDB(String flight_Plan_ID, String claim_ID, String claim_Address);

	boolean putFlightPlanPropertiesToDB(String flight_Plan_ID, String user_ID,String flight_Plan_Name, String flight_Plan_Version,String flight_Plan_Format);

	boolean putWayPointDataToDB(String flight_Plan_ID, String way_Point_ID,String way_Point_Key, String latitude, String longitude,String altitude, String above_Ground, String speed, String curve_size, String heading, String pOI,String gimbal_Option, String gimbal_Value, String interval_Option, String interval_Value);

	boolean putActionDatatoDB(String flight_Action_ID, String way_Point_ID, String action_Number,String selected_Action_Option, String selected_Action_Value);

	List<FlightPlanObjectDBModel> getFlightPlanObjectByFlightPlanID(String flightPlanID);

	List<FlightPlanPropertiesDBModel> getFlightPlanPropertiesByFlightPlanID(String flightPlanID);

	List<WayPointDataDBModel> getWayPointsByFlightPlanID(String flightPlanID);

	List<WayPointActionDBModel> getWayPointActionsByWayPointID(String wayPointID);

}
