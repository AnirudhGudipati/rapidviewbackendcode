package com.aiknights.com.rapidviewmain.DAO;

import java.util.List;

import com.aiknights.com.rapidviewmain.models.WorkOrderDetailsResponseModel;

public interface WorkOrderDBRepo {

	boolean putWorkOrderToDB(String work_Order_ID, String flight_Plan_ID, String work_Order_Status, String assigned_To, String assigned_By, String expected_Start_Date, String expected_End_Date,String created_Date, String notes, String work_Order_Approval_Status);

	boolean updateDefaultWorkOrderByID(String workOrderStatus, String workOrderID, String assignedTo, String woStartDate, String woEndDate, String notes);

	List<WorkOrderDetailsResponseModel> getOrdersAsAdjuster(String userID);

	List<WorkOrderDetailsResponseModel> getOrdersAsPilot(String userID);

	boolean updateWorkOrderStatusByID(String workOrderID, String workOrderStatus, String workOrderApprovalStatus);

	List<WorkOrderDetailsResponseModel> getMarketPlaceOrders();

	boolean updateWorkOrderMarketPlaceAssignment(String workOrderID, String workOrderStatus, String assignedTo);

	List<WorkOrderDetailsResponseModel> getOrderByWOID(String workOrderID);

	boolean updateWorkOrderApprovalStatus(String workOrderID, String approvalStatus);

	boolean updateWorkOrderReopenCount(String workOrderID, String NewCount);

	boolean reOpenWorkOrderUpdateMethod(String workOrderID, String approvalStatus, String reopenComment);

	String checkAdjusterEventAvailability(String adjuster_ID, String date, String start_Time, String endTime);

	String checkPilotEventAvailability(String pilot_ID, String date, String start_Time, String endTime);

	boolean createEvent(String workOrderID, String eventID, String eventLink, String adjuster_ID, String pilot_ID,String eventSystem, String startTime, String endTime, String eventDate, String timePeriod);

}
