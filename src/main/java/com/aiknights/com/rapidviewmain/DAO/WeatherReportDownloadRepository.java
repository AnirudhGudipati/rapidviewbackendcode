package com.aiknights.com.rapidviewmain.DAO;

import java.util.Map;

public interface WeatherReportDownloadRepository {

	String getweatherReportDownloadCountByUserName(String userName);

	boolean updateLatestDownloadCount(String userName, int latestDwonloadCount);

	Map<String, Object> getAvailableCountByUserName(String userName);

}
