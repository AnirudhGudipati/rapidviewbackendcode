package com.aiknights.com.rapidviewmain.DAO;

import com.aiknights.com.rapidviewmain.models.DashboardClaimCategoryModel;
import com.aiknights.com.rapidviewmain.models.DashboardTotalClaimsModel;

public interface DashboardAPIRepository {

	DashboardTotalClaimsModel getTotalClaimsData(String userid);

	DashboardClaimCategoryModel getClaimCategoryData(String userid);

}
