package com.aiknights.com.rapidviewmain.DAO;

import java.util.List;

import com.aiknights.com.rapidviewmain.models.Role;

public interface RoleRepository {

	Role getRoleByRoleId(final int roleId);
	
	List<String> getRolesByUsername(final String userName);

	String getRoleByUser(String userName);
}
