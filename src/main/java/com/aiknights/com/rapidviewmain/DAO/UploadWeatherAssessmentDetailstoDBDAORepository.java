package com.aiknights.com.rapidviewmain.DAO;

import java.util.List;

import com.aiknights.com.rapidviewmain.models.PaypalPaymentModel;
import com.aiknights.com.rapidviewmain.models.WeatherAssessmentDataModel;

public interface UploadWeatherAssessmentDetailstoDBDAORepository {

	public int UploadWeatherAssessmentDetailstoDB(String OwnerName, String CarrierName, String Claimid, String Address,
			String PolicyStartDate, String ClaimDate);

	int UploadWeatherAssessmentDetailsto(WeatherAssessmentDataModel weatherAssessmentDataModel);

	public List<WeatherAssessmentDataModel> getAssigedWeatherassessmentForUser(int userId);
	
	public List<WeatherAssessmentDataModel> manageWeatherReportsAsAdmin(String companyName);

	public boolean getWeatherAssessmentDetailsByReportId(String reportId);

	public WeatherAssessmentDataModel getWeatherReportById(String reportId);

	public boolean uploadpayPalPaymentInfo(PaypalPaymentModel payPalPayment, String UserId, byte[] paypalPaymentObject);

	public int MarkWeatherStagingCompleted(String reportID);

}
