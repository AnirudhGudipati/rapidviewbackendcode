package com.aiknights.com.rapidviewmain.DAO;



import java.util.List;

import com.aiknights.com.rapidviewmain.models.ManageUsersModel;
import com.aiknights.com.rapidviewmain.models.UserAndRoleDetailsModel;
import com.aiknights.com.rapidviewmain.models.UserDetails;

public interface UserDetailsRepository {

	UserDetails findUserDetailsByUserName(final String userName);

	UserDetails findUserDetailsByUserID(String userId);

	List<UserAndRoleDetailsModel> getAllUsers(String Rolename);
	
	List<ManageUsersModel> manageUsersAsAdmin(String CompanyName);

	UserDetails findUserDetailsByEmailID(String emailID);

	boolean updatePasswordByUserName(String userName, String password);

	boolean activateUserAsAdmin(String userID);

	boolean deactivateUserAsAdmin(String userID);
	
	boolean toggleForceChangePasswordByUsername(String userName);
	
	boolean InsertAdminTicketIntoDB(String ticket_ID, String ticket_Notes, String ticket_Context, String user_ID, String admin_ID);
	
}
