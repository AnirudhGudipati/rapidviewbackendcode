package com.aiknights.com.rapidviewmain.DAO;

import java.util.List;

import com.aiknights.com.rapidviewmain.models.ReportsStageDBModel;
import com.aiknights.com.rapidviewmain.models.StormEventsAlertModel;
import com.aiknights.com.rapidviewmain.models.WeatherEDBModel;
import com.aiknights.com.rapidviewmain.models.WeatherNexRadModel;
import com.aiknights.com.rapidviewmain.models.WeatherPLSRWindModel;
import com.aiknights.com.rapidviewmain.models.WeatherStageDatabaseModel;
import com.aiknights.com.rapidviewmain.models.WeatherThunderWindModel;

public interface WeatherDBRepository {

	List<WeatherEDBModel> getWeatherDBByClaimid(final String startdate, String enddate, String latitude, String longitude, String startdist, String enddist);
	List<WeatherNexRadModel> getWeatherNexRadByClaimid(final String startdate, String enddate, String latitude, String longitude, String startdist, String enddist);
	List<WeatherPLSRWindModel> getWeatherPLSRWindByClaimid(final String startdate,final String enddate,final String latitude, final String longitude, final String startdist, final String enddist);
	List<WeatherThunderWindModel> getWeatherThunderWindByClaimid(final String startdate,final String enddate, final String latitude, final String longitude, final String startdist, final String enddist);
	boolean stageWeatherDataByClaimID(List<WeatherStageDatabaseModel> wt);
	boolean stageWeatherDataCheckbyClaimID(String claimid);
	List<WeatherStageDatabaseModel> getWeatherDataFromStageByClaimID(String claimid);
	boolean stageWeatherDataByReportID(List<ReportsStageDBModel> wt);
	boolean stageWeatherDataCheckbyReportID(String claimid);
	List<ReportsStageDBModel> getWeatherDataFromStageByReportID(String claimid);
	List<StormEventsAlertModel> getEventsSeverity(String state);
	List<String> getWeatherSubscriptionUserList(); 
	List<String> getStatesForSubscribedUser(String userid);
	String convertToAlertNotificationMessage(List<String> states,List<StormEventsAlertModel> events);
	String convertToAlertNotificationMessageEmail(List<String> states, List<StormEventsAlertModel> events);
}
