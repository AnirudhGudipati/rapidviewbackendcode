package com.aiknights.com.rapidviewmain.DAO;

import java.util.List;

import com.aiknights.com.rapidviewmain.models.TokenDBModel;

public interface TokenDBRepository {

	List<TokenDBModel> getTokenDetailsBasedOnTokenID(String token);

	boolean insertNewTokenDetailsToDB(String token,String CreatedDateTime,String IsUsed,String userId);

	boolean markTokenAsUsedByToken(String token);

	boolean validateSessionByToken(String token);

	List<TokenDBModel> getAllOpenTokens();

}
