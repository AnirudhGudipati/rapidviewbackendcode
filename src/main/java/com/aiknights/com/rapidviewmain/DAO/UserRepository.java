package com.aiknights.com.rapidviewmain.DAO;

import java.time.LocalDate;
import java.util.List;

import com.aiknights.com.rapidviewmain.models.ProductCertainResourceDBTableModel;
import com.aiknights.com.rapidviewmain.models.ProductsTableDBModel;
import com.aiknights.com.rapidviewmain.models.User;

public interface UserRepository {

	User findUserByUserName(final String userName);

	LocalDate findLicenseEndDateByUserName(String userName);

	List<String> findassignedLicenesByUserName(String userName);

	String findSubscriptionTypeByUserName(String userName);

	String findMembershipLevelByUserName(String userName);

	String findProductIdByUserName(String userName);

	String findProductTypeById(String productId);

	List<ProductsTableDBModel> getAllProductsData();

	List<String> getDistinctAvailableRolesFromGivenProductTable(String ProductTableName);

	List<String> getResourcesAvailableOfGivenRoleFromGivenProductTable(String ProductTableName, String RoleName);

	List<ProductCertainResourceDBTableModel> getResourcesDataAvailableOfGivenRoleFromGivenProductTable(
			String ProductTableName, String RoleName);

	boolean updateRoleResourceActiveStatusForGivenRoleResourceProductTableName(
			String ProductTableName, String RoleName, String ResourceName, String ActiveStatus);
	
	boolean updateSubscriptionPropertiesForGivenTypeSubscriptionTableName(
			String SubscriptionTableName, String subscriptionType, String subscriptionPrice, String downloadCount);

	boolean updateCommonSubscriptionProperties(String subscriptionType, String subscriptionPeriod);
}
