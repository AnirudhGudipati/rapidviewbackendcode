package com.aiknights.com.rapidviewmain.DAO;

public interface DeleteClaimRepository {

	 boolean deleteAdjusterClaimDataByClaimID(String claim_id);

	boolean deleteClaimByClaimID(String claim_id);

	boolean deletePolicyByClaimID(String claim_id);

	String getPOlicyNumberByClaimId(String claim_id);

	boolean checkClaimIDInIncidentEvidence(String claim_id);

	boolean deleteIncidentEvdenceClaimDataByClaimID(String claim_id);

	boolean deleteCreateReportClaimDataByClaimID(String claim_id);

	boolean checkClaimIDInRisKAssesment(String claim_id);

	boolean deleteRiskAssesmentClaimDataByClaimID(String claim_id);

	boolean checkClaimIDInCreateReport(String claim_id);

	boolean checkClaimIDInWeatherCheck(String claim_id);

	boolean deleteWeatherCheckClaimByDataByClaimID(String claim_id);

}
