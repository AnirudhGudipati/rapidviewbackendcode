package com.aiknights.com.rapidviewmain.DAO;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.aiknights.com.rapidviewmain.models.AnalyseReponseInDetail;
import com.aiknights.com.rapidviewmain.models.DeleteIncidentImagesInput;

public interface ImageEvidenceRepository {
	
	public int[] saveUploadedImages(List<AnalyseReponseInDetail> input,String claimId);
	
	public int updateAnalysedImageDatatoDB(AnalyseReponseInDetail input);
	
	public List<AnalyseReponseInDetail> getIncidentEvidenceForClaim(String claimId);
	
	public int[]  deleteIncidentEvidenceImages(List<DeleteIncidentImagesInput> deleteImagesInput) throws DataAccessException;

	public int changeAnalysisStatetoZero(String imageId);

}
