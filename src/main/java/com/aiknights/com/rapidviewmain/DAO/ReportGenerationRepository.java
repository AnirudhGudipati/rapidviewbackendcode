package com.aiknights.com.rapidviewmain.DAO;

import com.aiknights.com.rapidviewmain.dto.ReportTabDTO;

public interface ReportGenerationRepository {
	
	
	ReportTabDTO getReportDetails(final String claimId);
	

}
