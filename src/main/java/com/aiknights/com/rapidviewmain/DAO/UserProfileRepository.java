package com.aiknights.com.rapidviewmain.DAO;

import com.aiknights.com.rapidviewmain.models.UserDetails;

public interface UserProfileRepository {

	 UserDetails getProfileDetailsByUserId(String user_Id);

	boolean updateProfileDetailsByUserName(String userName,String address,String contactNumber);

}
