package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.RoleRepository;
import com.aiknights.com.rapidviewmain.models.Role;

@Repository
public class RoleDAOImpl implements RoleRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Role getRoleByRoleId(final int roleId) {
		// TODO not used now
		return null;
	}

	@Override
	public List<String> getRolesByUsername(final String userName) {
		final String GET_ROLES_BY_USER_NAME = "select rolename from role where username = ?";
		List<String> rolesList = new ArrayList<>();
		final List<String> roles = jdbcTemplate.queryForList(GET_ROLES_BY_USER_NAME, new Object[] { userName },
				String.class);
		rolesList.addAll(roles);
		return rolesList;
	}

	@Override
	public String getRoleByUser(String userName) {
		final String GET_ROLES_BY_USER_NAME = "select rolename from role where username = ?";
		String RoleType = null;
		try {
			RoleType = jdbcTemplate.queryForObject(GET_ROLES_BY_USER_NAME, new Object[] { userName }, String.class);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return RoleType;
	}
}
