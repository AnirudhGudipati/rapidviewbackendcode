package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.CreateAnAccountRepository;
import com.aiknights.com.rapidviewmain.dto.RoleResultsDTO;
import com.aiknights.com.rapidviewmain.dto.UserDetailsResultsDTO;
import com.aiknights.com.rapidviewmain.models.CreateProfileRequestModel;
import com.aiknights.com.rapidviewmain.models.LicensingModel;
import com.aiknights.com.rapidviewmain.models.Resource;
import com.aiknights.com.rapidviewmain.models.Role;
import com.aiknights.com.rapidviewmain.models.User;
import com.aiknights.com.rapidviewmain.models.UserDetails;
import com.aiknights.com.rapidviewmain.models.WeatherDownloadCountModel;

@Repository
public class CreateAnAccountDAOImpl implements CreateAnAccountRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;
	@SuppressWarnings("unused")
	private int userInsertStatus;

	@Override
	public boolean insertIntoUsers(User user, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		int userStatus = 0;
		final String INSERT_INTO_USERS = "INSERT INTO  users" + "(username,password,enabled,CreationDate)"
				+ "values(:username,:password,:enabled,:CreationDate)";
		try {
			KeyHolder holder = new GeneratedKeyHolder();
			SqlParameterSource parameters = new MapSqlParameterSource().addValue("username", user.getUsername())
					.addValue("password", user.getPassword()).addValue("enabled", user.getEnabled())
					.addValue("CreationDate", user.getCreatedDate());
			userStatus = namedParameterJdbcTemplate.update(INSERT_INTO_USERS, parameters, holder);

		} catch (Exception e) {
			// TODO: handle exception
		}
		if (userStatus == 1) {
			return true;
		}
		return false;
	}

	@Override
	public UserDetailsResultsDTO insertIntoUserDetails(UserDetails userDetails,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		UserDetailsResultsDTO userDetailsResultsDTO = new UserDetailsResultsDTO();
		int status_user_details = 0;
		int useridpk = 0;
		final String INSERT_INTO_USERS = "INSERT INTO  user_details"
				+ "(userid,username,firstname,lastname,email,city,company_name,address,contactNumber,weatherSubscription,address1,address2,state,country,zipCode,isProfileCreated,isActive,isEmailVerified,forceChangePassword)"
				+ "values(:userid,:username,:firstname,:lastname,:email,:city,:company_name,:address,:contactNumber,:weatherSubscription,"
				+ ":address1,:address2,:state,:country,:zipCode,:isProfileCreated,:isActive,:isEmailVerified,:forceChangePassword)";

		try {
			KeyHolder holder = new GeneratedKeyHolder();
			SqlParameterSource parameters = new MapSqlParameterSource().addValue("userid", userDetails.getUserId())
					.addValue("username", userDetails.getUserName()).addValue("firstname", userDetails.getFirstName())
					.addValue("lastname", userDetails.getLastName()).addValue("email", userDetails.getEmail())
					.addValue("city", userDetails.getCity()).addValue("company_name", userDetails.getcompanyName())
					.addValue("address", userDetails.getAddress())
					.addValue("contactNumber", userDetails.getContactNumber())
					.addValue("weatherSubscription", userDetails.getWeatheSubscription())
					.addValue("address1", userDetails.getAddress1()).addValue("address2", userDetails.getAddress2())
					.addValue("state", userDetails.getState()).addValue("country", userDetails.getCountry())
					.addValue("zipCode", userDetails.getZipCode())
					.addValue("isProfileCreated", userDetails.getIsProfileCreated())
					.addValue("isActive", userDetails.getIsActive())
					.addValue("isEmailVerified", userDetails.getIsEmailVerified())
					.addValue("forceChangePassword", userDetails.getForceChangePassword());

			status_user_details = namedParameterJdbcTemplate.update(INSERT_INTO_USERS, parameters, holder,
					new String[] { "userid" });
			System.out.println("inserion status  " + status_user_details);
			useridpk = holder.getKey().intValue();
			System.out.println("userid" + status_user_details);

		} catch (Exception e) {
			// TODO: handle exception
		}
		if (status_user_details == 1) {
			userDetailsResultsDTO.setInsertion_status(true);
			userDetailsResultsDTO.setUserid_pk(useridpk);
			return userDetailsResultsDTO;
		}
		userDetailsResultsDTO.setInsertion_status(false);
		userDetailsResultsDTO.setUserid_pk(useridpk);
		return userDetailsResultsDTO;
	}

	@Override
	public RoleResultsDTO insertIntoRole(Role roleDetails, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		RoleResultsDTO roleResultsDTO = new RoleResultsDTO();
		int roleidPk = 0;
		int status_role = 0;
		final String INSERT_INTO_USERS = "INSERT INTO  role" + "(roleid,rolename,username)"
				+ "values(:roleid,:rolename,:username)";

		try {
			KeyHolder holder = new GeneratedKeyHolder();
			SqlParameterSource parameters = new MapSqlParameterSource().addValue("roleid", roleDetails.getRoleId())
					.addValue("rolename", roleDetails.getRolename()).addValue("username", roleDetails.getUsername());
			status_role = namedParameterJdbcTemplate.update(INSERT_INTO_USERS, parameters, holder,
					new String[] { "roleid" });
			System.out.println("inserion status  " + status_role);
			roleidPk = holder.getKey().intValue();
			System.out.println("roleid" + roleidPk);

		} catch (Exception e) {
			// TODO: handle exception
		}
		if (status_role == 1) {
			roleResultsDTO.setInsertion_status(true);
			roleResultsDTO.setRoleid_pk(roleidPk);
			return roleResultsDTO;
		}
		roleResultsDTO.setInsertion_status(false);
		roleResultsDTO.setRoleid_pk(roleidPk);
		return roleResultsDTO;
	}

	@Override
	public boolean insertIntoResource(Resource resource, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		int resourceidpk = 0;
		int status_resource = 0;
		final String INSERT_INTO_RESOURCES = "INSERT INTO  resources" + "(resourceid,resourcename,username)"
				+ "values(:resourceid,:resourcename,:username)";

		try {
			KeyHolder holder = new GeneratedKeyHolder();
			SqlParameterSource parameters = new MapSqlParameterSource().addValue("resourceid", resource.getResouceId())
					.addValue("resourcename", resource.getResourceName()).addValue("username", resource.getUserName());
			status_resource = namedParameterJdbcTemplate.update(INSERT_INTO_RESOURCES, parameters, holder,
					new String[] { "resourceid" });
			System.out.println("inserion status  " + status_resource);
			resourceidpk = holder.getKey().intValue();
			System.out.println("roleid" + resourceidpk);

		} catch (Exception e) {
			// TODO: handle exception
		}
		if (status_resource == 1) {
			return true;
		}
		return false;
	}

	@Override
	public boolean findByEmail(String emailId) {
		final String EMAIL_ID_CHCEK = "select count(*) from user_details where email=?";

		boolean result = false;
		int count = jdbcTemplate.queryForObject(EMAIL_ID_CHCEK, new Object[] { emailId }, Integer.class);
		if (count > 0) {
			result = true;
		}
		return result;
	}

	@Override
	public boolean findByUserName(String userName) {
		final String USER_NAME_CHCEK = "select count(*) from users where username=?";

		boolean result = false;
		int count = jdbcTemplate.queryForObject(USER_NAME_CHCEK, new Object[] { userName }, Integer.class);
		if (count > 0) {
			result = true;
		}
		return result;
	}

	@Override
	public RoleResultsDTO updateIntoRole(Role roleDetails, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		RoleResultsDTO resultset = new RoleResultsDTO();
		try {
			final String sql = "update role set rolename = \"" + roleDetails.getRolename() + "\" where username =\""
					+ roleDetails.getUsername() + "\";";

			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				resultset.setInsertion_status(true);
			} else {
				resultset.setInsertion_status(false);
			}
		} catch (Exception e) {
			resultset.setInsertion_status(false);
		}
		return resultset;
	}

	@Override
	public boolean insertIntolicenseDetails(LicensingModel licensing) {
		try {
			final String sql = "insert into Licenesing (LicenseID,LicenseType,LicenseStartDate,LicenseEndDate,ProductID,username,MembershipLevel) values(?,?,?,?,?,?,?);";
			int output = jdbcTemplate.update(sql, licensing.getLicense_ID(), licensing.getLicense_Type(),
					licensing.getLicense_StartDate(), licensing.getLicense_EndDate(), licensing.getProduct_ID(),
					licensing.getUsername(), licensing.getMembership_Level());
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public String findProductIDWithProductType(String productName) {
		String ProductID = null;
		try {
			final String sql = "select ProductID from Products where ProductName =?";

			ProductID = (String) jdbcTemplate.queryForObject(sql, new Object[] { productName }, String.class);

		} catch (Exception e) {
			// TODO: handle exception
		}

		return ProductID;

	}

	@Override
	public List<String> getallocatedResourcesByProductID(String productId) {
		final String GET_ROLES_BY_USER_NAME = "select AllocatedResource from Product_Resources where ProductID= ?";
		List<String> allocatedResourceList = new ArrayList<>();
		final List<String> allocatedResource = jdbcTemplate.queryForList(GET_ROLES_BY_USER_NAME,
				new Object[] { productId }, String.class);

		allocatedResourceList.addAll(allocatedResource);
		return allocatedResourceList;

	}

	@Override
	public List<WeatherDownloadCountModel> getDownloadCountSubscriptionType(String MembershipType) {
		List<WeatherDownloadCountModel> weatherDownloadCountList = null;
		try {
			final String sql = "select * from Weather_Subscription_Properties where Subscription_Type=?";
			weatherDownloadCountList = jdbcTemplate.query(sql, new WeatherDownloadCountRowMapper(), MembershipType);

		} catch (Exception e) {

		}

		return weatherDownloadCountList;
	}

	@Override
	public boolean insertIntoWeatherDownloadcount(String WeatherDownloadID, String userName,
			String Weather_Download_MaxCount, String AmountToBe_Paid) {
		try {
			final String sql = "insert into Weather_Download_count values(?,?,?,?)";
			int output = jdbcTemplate.update(sql, WeatherDownloadID, userName, Weather_Download_MaxCount,
					AmountToBe_Paid);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public String getpaymentInfoBySubscriptionType(String Subscription_Type) {
		String AmountToBe_Paid = null;
		try {
			final String sql = "select AmountToBe_Paid from Subscription_Properties where Subscription_Type =?";

			AmountToBe_Paid = (String) jdbcTemplate.queryForObject(sql, new Object[] { Subscription_Type },
					String.class);

		} catch (Exception e) {
			// TODO: handle exception
		}

		return AmountToBe_Paid;
	}

	@Override
	public boolean insertIntoPaymentInfo(String Payment_InfoID, String AmountToBe_Paid, String username,
			String Payment_Status) {
		String sql = "insert into Payment_Info values (?,?,?,?,?)";
		int output = jdbcTemplate.update(sql, Payment_InfoID, username, AmountToBe_Paid, Payment_Status, null);
		if (output == 1) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String findEmailByUserName(String userName) {
		String emailId = null;
		try {
			final String sql = "SELECT email from user_details where username =?";

			emailId = (String) jdbcTemplate.queryForObject(sql, new Object[] { userName }, String.class);

		} catch (Exception e) {
			// TODO: handle exception
		}

		return emailId;
	}

	@Override
	public boolean enableUserByID(String userId) {
		try {
			final String sql = "Update users set enabled =\"1\" where username = \"" + userId + "\"";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean updateUserDetailsByID(int userId, CreateProfileRequestModel profileDetails) {
		String address = profileDetails.getAddressLine1() + "" + profileDetails.getAddressLine2() + ","
				+ profileDetails.getState() + " " + profileDetails.getZip() + "," + profileDetails.getCountry();

		try {

			String sql = "update user_details  set address1=\"" + profileDetails.getAddressLine1() + "\", address2=\""
					+ profileDetails.getAddressLine2() + "\", state=\"" + profileDetails.getState() + "\", city=\""
					+ profileDetails.getCity() + "\", country=\"" + profileDetails.getCountry() + "\", address=\""
					+ address + "\", zipCode=\"" + profileDetails.getZip() + "\", weatherSubscription=\""
					+ profileDetails.getSubscriptionType() + "\", isProfileCreated=\"" + "1" + "\" WHERE userid ="
					+ userId + ";";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean insertIntoweatherAlerts(CreateProfileRequestModel profileDetails, int userid) {
		try {
			final String sql = "insert into Weather_Alerts_Subscription values(?,?)";
			int output = jdbcTemplate.update(sql, userid, profileDetails.getWeatherStates());
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean insertIntoweatherAlertsUserState(CreateProfileRequestModel profileDetails, int userid) {
		try {
			final String sql = "insert into Weather_Alerts_Subscription values(?,?)";
			int output = jdbcTemplate.update(sql, userid, profileDetails.getState());
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean markisEmailVerified(int userId) {
		try {
			final String sql = "Update user_details set isEmailVerified =\"1\" where userid = \"" + userId + "\"";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean editProductAsAdmin(String userName, String productID) {
		try {
			final String sql = "update Licenesing set ProductID = "+productID+" where username =\""+userName+"\";";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean editRoleAsAdmin(String userName, String roleName) {
		try {
			final String sql = "update role set rolename =\""+roleName+"\" where username =\""+userName+"\"";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean editSubscriptionAsAdmin(String userName, String subscriptiontName) {
		try {
			final String sql = "update Licenesing set LicenseType = \""+subscriptiontName+"\" where username =\""+userName+"\";";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
