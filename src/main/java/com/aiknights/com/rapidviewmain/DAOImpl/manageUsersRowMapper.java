package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.ManageUsersModel;

public class manageUsersRowMapper implements RowMapper<ManageUsersModel> {

	@Override
	public ManageUsersModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		ManageUsersModel userDetails = new ManageUsersModel();
		userDetails.setUserId(rs.getString("USERID"));
		userDetails.setUserName(rs.getString("USERNAME"));
		userDetails.setFirstName(rs.getString("FIRSTNAME"));
		userDetails.setLastName(rs.getString("LASTNAME"));
		userDetails.setEmail(rs.getString("EMAIL"));
		userDetails.setCity(rs.getString("city"));
		userDetails.setCompanyName(rs.getString("COMPANY"));
		userDetails.setFullAddress(rs.getString("FULLADDRESS"));
		userDetails.setRolename(rs.getString("ROLE"));
		userDetails.setProductType(rs.getString("PRODUCTNAME"));
		userDetails.setSubscription(rs.getString("LICENSE_TYPE"));
		userDetails.setMembership(rs.getString("MEMBERSHIP"));
		userDetails.setContactnumber(rs.getString("CONTACTNUMBER"));
		userDetails.setProfilePhotoURL(rs.getString("PROFILEPICTUREURL"));
		userDetails.setEnabledStatus(rs.getString("ENABLED"));
		userDetails.setActiveStatus(rs.getString("ACTIVE"));
		userDetails.setEmailVerificationStatus(rs.getString("EMAILVERIFICATION"));
		userDetails.setAddressOne(rs.getString("ADDRESS_ONE"));
		userDetails.setAddressTwo(rs.getString("ADDRESS_TWO"));
		userDetails.setState(rs.getString("STATE"));
		userDetails.setCountry(rs.getString("COUNTRY"));
		userDetails.setZip(rs.getString("ZIP"));
		userDetails.setProfileCreationStatus(rs.getString("PROFILECREATION"));
		return userDetails;
	}

}
