package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.DashboardClaimCategoryModel;

public class DashboardClaimCategoryRowMapper implements RowMapper<DashboardClaimCategoryModel> {

	@Override
	public DashboardClaimCategoryModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		DashboardClaimCategoryModel dashboardclaimcategorymodel = new DashboardClaimCategoryModel();
		dashboardclaimcategorymodel.setTotalclaims(rs.getString("Total"));
		dashboardclaimcategorymodel.setWaterclaims(rs.getString("Water"));
		dashboardclaimcategorymodel.setHailclaims(rs.getString("Hail"));
		dashboardclaimcategorymodel.setWindclaims(rs.getString("Wind"));
		return dashboardclaimcategorymodel;
	}

}
