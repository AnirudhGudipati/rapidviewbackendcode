package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.UserDetailsRepository;
import com.aiknights.com.rapidviewmain.models.ManageUsersModel;
import com.aiknights.com.rapidviewmain.models.UserAndRoleDetailsModel;
import com.aiknights.com.rapidviewmain.models.UserDetails;

@Repository
public class UserDetailsDAOImpl implements UserDetailsRepository{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Value("${rapid.super.Company.Name}")
	private String superCompanyName;
	
	@Override
	public UserDetails findUserDetailsByUserName(final String userName) {
		//UserDetails userDetais = null;
		final String GET_USERDETAILS_BY_USERNAME = "select * from user_details where username=?";
		try {
			UserDetails userDetais  = jdbcTemplate.queryForObject(GET_USERDETAILS_BY_USERNAME, new UserDetailsRowMapper(), userName);
			return userDetais;
		} catch (EmptyResultDataAccessException  e) {
			return null;
		}
	}
	
	@Override
	public UserDetails findUserDetailsByEmailID(final String emailID) {
		final String GET_USERDETAILS_BY_EMAIL = "select * from user_details where email=?";
		try {
			UserDetails userDetais  = jdbcTemplate.queryForObject(GET_USERDETAILS_BY_EMAIL, new UserDetailsRowMapper(), emailID);
			return userDetais;
		} catch (EmptyResultDataAccessException  e) {
			return null;
		}
	}
	
	@Override
	public UserDetails findUserDetailsByUserID(final String userId) {
		//UserDetails userDetais = null;
		final String GET_USERDETAILS_BY_USERNAME = "select * from user_details where userid=?";
		try {
			UserDetails userDetais  = jdbcTemplate.queryForObject(GET_USERDETAILS_BY_USERNAME, new UserDetailsRowMapper(), userId);
			return userDetais;
		} catch (EmptyResultDataAccessException  e) {
			return null;
		}
	}

	@Override
	public List<UserAndRoleDetailsModel> getAllUsers(String Rolename) {
		String get_all_users_sql ="";
		if(Rolename == "") {
			get_all_users_sql = "select * from user_details u,role r where u.username=r.username;";
		}else {
			get_all_users_sql = "select * from user_details u,role r where u.username=r.username and r.rolename=\""+Rolename+"\";";
		}
		try {
			List<UserAndRoleDetailsModel> userDetails  = jdbcTemplate.query(get_all_users_sql,new UserAndRoleRowMapper());
			return userDetails;
		} catch (EmptyResultDataAccessException  e) {
			return null;
		}
	}

	@Override
	public List<ManageUsersModel> manageUsersAsAdmin(String CompanyName) {
		String sql ="";
		String fields = " us.username USERNAME ,us.enabled ENABLED,ud.isActive ACTIVE,ud.isEmailVerified EMAILVERIFICATION,ud.address2 ADDRESS_ONE,ud.address1 ADDRESS_TWO,ud.state STATE,ud.country COUNTRY,ud.zipCode ZIP,ud.isProfileCreated PROFILECREATION ,us.CreationDate,ud.userid USERID ,ud.username,ud.firstname FIRSTNAME ,ud.lastname LASTNAME ,ud.email EMAIL ,ud.city,ud.company_name COMPANY ,ud.address FULLADDRESS ,ud.contactNumber CONTACTNUMBER ,r.roleid,r.rolename ROLE ,r.username,l.LicenseID,l.LicenseType LICENSE_TYPE,l.LicenseStartDate,l.LicenseEndDate,l.ProductID,l.username,l.MembershipLevel MEMBERSHIP,p.ProductID,p.ProductName PRODUCTNAME ,p.ProductTableName,pp.profile_image_name,pp.profile_image_url PROFILEPICTUREURL ,pp.userid ";
		if(CompanyName.equals(superCompanyName)) {
		sql ="select"+fields+"from users us JOIN user_details ud ON us.username =ud.username JOIN role r ON r.username =us.username JOIN Licenesing l ON l.username =us.username JOIN Products p on p.ProductID =l.ProductID LEFT JOIN Profile_Pictures pp ON pp.userid =ud.userid;";
		}else {
		sql ="select"+fields+"from users us JOIN user_details ud ON us.username =ud.username JOIN role r ON r.username =us.username JOIN Licenesing l ON l.username =us.username JOIN Products p on p.ProductID =l.ProductID LEFT JOIN Profile_Pictures pp ON pp.userid =ud.userid WHERE ud.company_name =\""+CompanyName+"\";";
		}
		try {
			List<ManageUsersModel> userDetails  = jdbcTemplate.query(sql,new manageUsersRowMapper());
			return userDetails;
		} catch (EmptyResultDataAccessException  e) {
			return null;
		}
	}
	
	@Override
	public boolean updatePasswordByUserName(String userName, String password) {
		try {
			final String sql = "Update users set password =\""+password+"\" where username = \""+userName+"\"";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean activateUserAsAdmin(String userID) {
		try {
			final String sql = "Update user_details set isActive =\"1\" where userid = \""+userID+"\"";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean deactivateUserAsAdmin(String userID) {
		try {
			final String sql = "Update user_details set isActive =\"0\" where userid = \""+userID+"\"";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean toggleForceChangePasswordByUsername(String userName) {
		try {
			final String sql = "Update user_details set forceChangePassword =\"1\" where username = \""+userName+"\"";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean InsertAdminTicketIntoDB(String ticket_ID, String ticket_Notes, String ticket_Context, String user_ID,
			String admin_ID) {
		try {
			final String sql = "INSERT into Admin_Tickets (Ticket_ID, Ticket_Notes, Ticket_Context, Admin_ID, User_ID) values (\""+ticket_ID+"\",\""+ticket_Notes+"\",\""+ticket_Context+"\",\""+admin_ID+"\",\""+user_ID+"\");";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
