package com.aiknights.com.rapidviewmain.DAOImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.ProfileImageUploadRepository;

@Repository
public class ProfileImageUploadRepositoryImpl implements ProfileImageUploadRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public boolean uploadProfileImageFile(String profileImageName, String userId, String profile_image_path) {
		try {
			final String sql = "INSERT into Profile_Pictures (profile_image_name,profile_image_url,userid) values(?,?,?);";
			int output = jdbcTemplate.update(sql, profileImageName, profile_image_path, userId);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public String getProfileImageById(String userId) {
		String profileImageUrl = null;
		try {
			final String sql = "SELECT profile_image_url from Profile_Pictures where userid =?";

			profileImageUrl = (String) jdbcTemplate.queryForObject(sql, new Object[] { userId }, String.class);

		} catch (Exception e) {
			// TODO: handle exception
		}

		return profileImageUrl;
	}

	@Override
	public boolean isUserExist(String userId) {
		final String USER_NAME_CHCEK = "select count(*) from Profile_Pictures where userid=?";

		boolean result = false;
		int count = jdbcTemplate.queryForObject(USER_NAME_CHCEK, new Object[] { userId }, Integer.class);
		if (count > 0) {
			result = true;
		}
		return result;
	}

	@Override
	public boolean updateProfileImgaeFile(String userId, String profile_image_path, String profileImageName) {

		final String sql = "update Profile_Pictures set profile_image_url = \"" + profile_image_path
				+ "\" where userid =\"" + userId + "\";";

		int output = jdbcTemplate.update(sql);
		if (output == 1) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean deleteOldProfileUrl(String userId) {
		final String INSERT_SQL = "Delete from " + "Profile_Pictures" + " where " + "userid" + " =\"" + userId + "\"";

		int out_delete = jdbcTemplate.update(INSERT_SQL);
		System.out.println("Delete status  " + out_delete);

		if (out_delete == 1) {
			return true;
		} else {
			return false;
		}
	}

}
