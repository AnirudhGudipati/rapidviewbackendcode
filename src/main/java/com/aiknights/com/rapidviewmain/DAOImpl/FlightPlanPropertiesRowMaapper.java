package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.FlightPlanPropertiesDBModel;

public class FlightPlanPropertiesRowMaapper implements RowMapper<FlightPlanPropertiesDBModel> {

	@Override
	public FlightPlanPropertiesDBModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		FlightPlanPropertiesDBModel fppm = new FlightPlanPropertiesDBModel();
		fppm.setFlight_Plan_Format(rs.getString("Flight_Plan_Format"));
		fppm.setFlight_Plan_ID(rs.getString("Flight_Plan_ID"));
		fppm.setFlight_Plan_Name(rs.getString("Flight_Plan_Name"));
		fppm.setFlight_Plan_Version(rs.getString("Flight_Plan_Version"));
		fppm.setUser_ID(rs.getString("User_ID"));
		return fppm;
	}

}
