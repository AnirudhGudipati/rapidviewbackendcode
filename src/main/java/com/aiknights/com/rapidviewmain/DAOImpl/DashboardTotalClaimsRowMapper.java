package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.DashboardTotalClaimsModel;

public class DashboardTotalClaimsRowMapper implements RowMapper<DashboardTotalClaimsModel>{

	@Override
	public DashboardTotalClaimsModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		DashboardTotalClaimsModel dashboardmodel = new DashboardTotalClaimsModel();
		dashboardmodel.setTotalclaims(rs.getString("Total"));
		dashboardmodel.setInprogressclaims(rs.getString("InProgress"));
		dashboardmodel.setCompletedclaims(rs.getString("Completed"));
		dashboardmodel.setRejectedclaims(rs.getString("Rejected"));
		return dashboardmodel;
	}

}
