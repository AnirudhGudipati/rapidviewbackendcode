package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.UploadWeatherAssessmentDetailstoDBDAORepository;
import com.aiknights.com.rapidviewmain.models.PaypalPaymentModel;
import com.aiknights.com.rapidviewmain.models.WeatherAssessmentDataModel;
import com.aiknights.com.rapidviewmain.service.GetIDService;

@Repository
public class UploadWeatherAssessmentDetailstoDBDAOImpl implements UploadWeatherAssessmentDetailstoDBDAORepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClaimNotificationDAOImpl.class);
	
	@Value("${rapid.super.Company.Name}")
	private String superCompanyName;
	
	@Autowired
	private JdbcTemplate jt;

	@Override
	public int UploadWeatherAssessmentDetailstoDB(String OwnerName, String CarrierName, String Claimid, String Address,
			String PolicyStartDate, String ClaimDate) {
		String sql = "insert into weatherassessmentdata values (?,?,?,?,?,?)";
		int output = jt.update(sql, Claimid, OwnerName, CarrierName, Address, PolicyStartDate, ClaimDate);
		if (output == 1) {
			return 1;
		} else {
			return 0;
		}

	}

	@Override
	public int UploadWeatherAssessmentDetailsto(WeatherAssessmentDataModel weatherAssessmentDataModel) {
		String sql = "insert into weatherassessmentdata (WeatherAssessmentDataID,ClaimID,OwnerName,CarrierName,Address,PolicyStartDate,ClaimDate,UserID,Report_Received_Date,StagingStatus,reportType,latitude,longitude) values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
		int output = jt.update(sql, weatherAssessmentDataModel.getReportId(), weatherAssessmentDataModel.getClaimid(),
				weatherAssessmentDataModel.getOwnername(), weatherAssessmentDataModel.getCarriername(),
				weatherAssessmentDataModel.getAddress(), weatherAssessmentDataModel.getPolicystartdate(),
				weatherAssessmentDataModel.getClaimdate(), weatherAssessmentDataModel.getUserid(),
				weatherAssessmentDataModel.getReport_Received_Date(), weatherAssessmentDataModel.getStagingstatus(),
				weatherAssessmentDataModel.getReportType(), weatherAssessmentDataModel.getLatitude(),
				weatherAssessmentDataModel.getLongitude());
		if (output == 1) {
			return 1;
		} else {
			return 0;
		}
	}
	
	@Override
	public int MarkWeatherStagingCompleted(String reportID) {
		String sql = "update weatherassessmentdata set StagingStatus =\"true\" where WeatherAssessmentDataID =\""+reportID+"\";";
		int output = jt.update(sql);
		if (output == 1) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public List<WeatherAssessmentDataModel> getAssigedWeatherassessmentForUser(int userId) {
		final String sql = "select * from weatherassessmentdata where UserID=?";
		List<WeatherAssessmentDataModel> weatherAssessmentDataList = jt.query(sql, new Object[] { userId },
				new WeatherAssessmentDataRowMapper());
		LOGGER.info("weatherAssessmentData for a user are: {}", weatherAssessmentDataList);
		return weatherAssessmentDataList;
	}
	
	@Override
	public List<WeatherAssessmentDataModel> manageWeatherReportsAsAdmin(String companyName) {
		final String sql;
		if(companyName.equals(superCompanyName)) {
			sql = "select * from weatherassessmentdata wd,user_details ud where wd.UserID =ud.userid limit 300;";
		}else {
			sql = "select * from weatherassessmentdata wd,user_details ud where wd.UserID =ud.userid and ud.company_name =\""+companyName+"\" limit 300;";
		}
		List<WeatherAssessmentDataModel> weatherAssessmentDataList = jt.query(sql, new WeatherAssessmentDataRowMapper());
		return weatherAssessmentDataList;
	}

	@Override
	public boolean getWeatherAssessmentDetailsByReportId(String reportId) {
		final String sql = "select count(*) from  weatherassessmentdata where WeatherAssessmentDataID=?";

		boolean result = false;
		int count = jt.queryForObject(sql, new Object[] { reportId }, Integer.class);
		if (count > 0) {
			result = true;
		}
		return result;
	}

	@Override
	public WeatherAssessmentDataModel getWeatherReportById(String reportId) {
		final String sql = "select * from  weatherassessmentdata where WeatherAssessmentDataID=?";
		try {
			final WeatherAssessmentDataModel weatherAssessmentData = jt.queryForObject(sql,
					new WeatherAssessmentDataRowMapper(), reportId);
			LOGGER.info("weatherAssessmentData for a given reportId id is: {}", weatherAssessmentData);
			return weatherAssessmentData;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public boolean uploadpayPalPaymentInfo(PaypalPaymentModel payPalPayment, String UserId,byte[] paypalPaymentObject) {

		String Payment_InfoID = String.valueOf(GetIDService.getID());
		String Payment_Status = payPalPayment.getPurchase_units().get(0).getPayments().getCaptures().get(0).getStatus();
		String Payment_Date   = payPalPayment.getPurchase_units().get(0).getPayments().getCaptures().get(0).getCreate_time();
		String payerEmail     = payPalPayment.getPayer().getEmail_address();
		String payerAddress   = payPalPayment.getPayer().getAddress().getCountry_code();
		String payerName      = payPalPayment.getPayer().getName().getGiven_name()+ payPalPayment.getPayer().getName().getSurname();
		String transactionId  = payPalPayment.getPurchase_units().get(0).getPayments().getCaptures().get(0).getId();
		String paidAmount = payPalPayment.getPurchase_units().get(0).getPayments().getCaptures().get(0).getAmount().getValue();
		String AmountToBe_Paid = "75";

		String sql = "INSERT INTO Payment_Info\n"
				+ "(Payment_InfoID, userid, AmountToBe_Paid, Payment_Status, Payment_Date, payerEmail, payerAddress, payerName, transactionId, paidAmount,payPalPaymentInfoObj)\n"
				+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?);\n" + "";
		int output = jt.update(sql, Payment_InfoID, UserId,AmountToBe_Paid,Payment_Status, Payment_Date, payerEmail,
				payerAddress, payerName, transactionId, paidAmount,paypalPaymentObject);
		if (output == 1) {
			return true;
		} else {
			return false;
		}
	}

}
