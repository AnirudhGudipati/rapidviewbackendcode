package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.aiknights.com.rapidviewmain.DAO.WeatherDBRepository;
import com.aiknights.com.rapidviewmain.models.ReportsStageDBModel;
import com.aiknights.com.rapidviewmain.models.StormEventsAlertModel;
import com.aiknights.com.rapidviewmain.models.WeatherEDBModel;
import com.aiknights.com.rapidviewmain.models.WeatherNexRadModel;
import com.aiknights.com.rapidviewmain.models.WeatherPLSRWindModel;
import com.aiknights.com.rapidviewmain.models.WeatherStageDatabaseModel;
import com.aiknights.com.rapidviewmain.models.WeatherThunderWindModel;

@Repository
public class WeatherRepositoryDBDAOImpl implements WeatherDBRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<WeatherEDBModel> getWeatherDBByClaimid(final String startdate,final String enddate, final String latitude, final String longitude, final String startdist, final String enddist) {
		final String GET_WEATHER_DB_BY_CLAIM_ID = "CALL GET_NEAREST_HAIL(DATE(\""+startdate+"\"),DATE(\""+enddate+"\"),"+latitude+","+longitude+","+startdist+","+enddist+");";
		try {
			final List<WeatherEDBModel> weatherdbdata = jdbcTemplate.query(GET_WEATHER_DB_BY_CLAIM_ID, new WeatherDBRowMapper());
			return weatherdbdata;
		}catch(EmptyResultDataAccessException e){
			return null;
		}
	}
	
	@Override
	public List<WeatherNexRadModel> getWeatherNexRadByClaimid(final String startdate,final String enddate, final String latitude, final String longitude, final String startdist, final String enddist) {
		final String GET_WEATHER_DB_BY_CLAIM_ID = "CALL GET_NEAREST_NEXRAD_HAIL(DATE(\""+startdate+"\"),DATE(\""+enddate+"\"),"+latitude+","+longitude+","+startdist+","+enddist+");";
		try {
			final List<WeatherNexRadModel> weathernexraddata = jdbcTemplate.query(GET_WEATHER_DB_BY_CLAIM_ID, new WeatherNexRadRowMapper());
			return weathernexraddata;
		}catch(EmptyResultDataAccessException e){
			return null;
		}
	}
	
	@Override
	public List<WeatherPLSRWindModel> getWeatherPLSRWindByClaimid(final String startdate,final String enddate,final String latitude, final String longitude, final String startdist, final String enddist) {
		final String GET_WEATHER_DB_BY_CLAIM_ID = "CALL GET_NEAREST_NEXRAD_WIND(DATE(\""+startdate+"\"),DATE(\""+enddate+"\"),"+latitude+","+longitude+","+startdist+","+enddist+");";
		try {
			final List<WeatherPLSRWindModel> weatherplsrwinddata = jdbcTemplate.query(GET_WEATHER_DB_BY_CLAIM_ID, new WeatherPLSRWindRowMapper());
			return weatherplsrwinddata;
		}catch(EmptyResultDataAccessException e){
			return null;
		}
	}
	
	@Override
	public List<WeatherThunderWindModel> getWeatherThunderWindByClaimid(final String startdate,final String enddate, final String latitude, final String longitude, final String startdist, final String enddist) {
		final String GET_WEATHER_DB_BY_CLAIM_ID = "CALL GET_NEAREST_THUNDERSTORM_WIND(DATE(\""+startdate+"\"),DATE(\""+enddate+"\"),"+latitude+","+longitude+","+startdist+","+enddist+");";
		try {
			final List<WeatherThunderWindModel> weatherdbdata = jdbcTemplate.query(GET_WEATHER_DB_BY_CLAIM_ID, new WeatherThunderWindRowMapper());
			return weatherdbdata;
		}catch(EmptyResultDataAccessException e){
			return null;
		}
	}
	
	@Override
	public List<WeatherStageDatabaseModel> getWeatherDataFromStageByClaimID(String claimid) {
		final String sql ="select * from claims_weather_stage where ClaimID=\""+claimid+"\";";
		final List<WeatherStageDatabaseModel> weatherdata = jdbcTemplate.query(sql, new StageRowMapper());
		return weatherdata;
	}

	@Override
	public boolean stageWeatherDataCheckbyClaimID(String claimid) {
		final String checksql = "select * from claims_weather_stage where ClaimID=\""+claimid+"\" limit 1;";
		List<WeatherStageDatabaseModel> output =jdbcTemplate.query(checksql, new StageRowMapper());
		if(output.size()!=0) {
			return false;
		}
		else {
			return true;
		}
	}
	@Override
	public boolean stageWeatherDataByClaimID(List<WeatherStageDatabaseModel> wt) {
		try {
		for (int i = 0; i < wt.size(); i++) {
			final String sql = "insert into claims_weather_stage (ClaimID, Event_Date, Hailsize, Latitude, Longitude, Distance) values (?,?,?,?,?,?)" ;
				int output = jdbcTemplate.update(sql,wt.get(i).getClaimid(),wt.get(i).getEventdate(),wt.get(i).getHailsize(),wt.get(i).getLatitude(),wt.get(i).getLongitude(),wt.get(i).getDistance());
		}
		return true;
		}catch(Exception e){
			return false;
		}
	}
	
	@Override
	public List<ReportsStageDBModel> getWeatherDataFromStageByReportID(String reportID) {
		final String sql ="select * from reports_weather_stage where ReportID=\""+reportID+"\";";
		final List<ReportsStageDBModel> weatherdata = jdbcTemplate.query(sql, new ReportsStageRowMapper());
		return weatherdata;
	}

	@Override
	public boolean stageWeatherDataCheckbyReportID(String reportID) {
		final String checksql = "select * from reports_weather_stage where ReportID=\""+reportID+"\" limit 1;";
		List<ReportsStageDBModel> output =jdbcTemplate.query(checksql, new ReportsStageRowMapper());
		if(output.size()!=0) {
			return false;
		}
		else {
			return true;
		}
	}
	@Override
	public boolean stageWeatherDataByReportID(List<ReportsStageDBModel> wt) {
		try {
		for (int i = 0; i < wt.size(); i++) {
			final String sql = "insert into reports_weather_stage (ReportID, Event_Date, Hailsize, Latitude, Longitude, Distance) values (?,?,?,?,?,?)" ;
				int output = jdbcTemplate.update(sql,wt.get(i).getReportid(),wt.get(i).getEventdate(),wt.get(i).getHailsize(),wt.get(i).getLatitude(),wt.get(i).getLongitude(),wt.get(i).getDistance());
		}
		return true;
		}catch(Exception e){
			return false;
		}
	}
	@Override
	public List<StormEventsAlertModel> getEventsSeverity(String state){
		try {
			final String sql="call SEND_STORM_ALERTS(\""+state+"\");";
			List<StormEventsAlertModel> events =jdbcTemplate.query(sql,new StormEventsAlertsRowMapper());
			return events;
		}catch(Exception e){
			System.out.println(e.getMessage());
			return null;
		}
	}
	@Override
	public List<String>getWeatherSubscriptionUserList(){
		try {
			final String sql="select distinct userid from Storm_Events_Subscription;";
			List<String> users=jdbcTemplate.queryForList(sql,String.class);
			return users;
		}catch(Exception e) {
			return null;
		}
	}
	@Override
	public List<String> getStatesForSubscribedUser(String userid){
		try {
			final String sql="select state from Storm_Events_Subscription where userid="+"\""+userid+"\"";
			List<String> states=jdbcTemplate.queryForList(sql,String.class);
			return states;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}
	@Override
	public String convertToAlertNotificationMessage(List<String> states,List<StormEventsAlertModel> events) {
		String message="";
		int i=0;
		for(String state:states) {
			String messageForState="Hail Events in "+state+":\n\n";
			for(int j=i;j<events.size();j++,i++) {
				if(events.get(j).getState().equals(state)) {
					String m="Sev-"+events.get(j).getSeverity()+" Hail Events in "+events.get(j).getCounty()+"\n";
					messageForState=messageForState+m;
				}
				else {
					break;
				}
			}
			message+=messageForState+"\n";
		}
		message+="Hail Severity Classifications:\n" + 
				"\n" + 
				"Sev-1: Hail size 0.75-1 inches\n" + 
				"Sev-2: Hail size 1-2 inches\n" + 
				"Sec-3: Hail size 2-3 Inches\n" + 
				"Sev-4: Hail size 3-4 Inches\n" + 
				"Sev-5: Hail size Above 4 inches";
		return message;
	}
	@Override
	public String convertToAlertNotificationMessageEmail(List<String> states,List<StormEventsAlertModel> events) {
		String message="";
		int i=0;
		for(String state:states) {
			String messageForState="<b>Hail Events in "+state+":</b><br><br>";
			for(int j=i;j<events.size();j++,i++) {
				if(events.get(j).getState().equals(state)) {
					String m;
					if(Integer.parseInt(events.get(j).getSeverity())>=3) {
						m="<b>Sev-"+events.get(j).getSeverity()+" Hail Events in "+events.get(j).getCounty()+"</b><br>";
					}
					else {
						m="Sev-"+events.get(j).getSeverity()+" Hail Events in "+events.get(j).getCounty()+"<br>";
					}
					messageForState=messageForState+m;
				}
				else {
					break;
				}
			}
			message+=messageForState+"<br>";
		}
		message+="<b>Hail Severity Classifications:</b><br><br>" + 
				"Sev-1: Hail size 0.75-1 inches<br>" + 
				"Sev-2: Hail size 1-2 inches<br>" + 
				"Sec-3: Hail size 2-3 Inches<br>" + 
				"Sev-4: Hail size 3-4 Inches<br>" + 
				"Sev-5: Hail size Above 4 inches";
		return message;
	}
}
