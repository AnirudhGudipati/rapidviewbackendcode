package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.ConflictsDBOutputModel;

public class ConflictsRowMapper implements RowMapper<ConflictsDBOutputModel> {

	@Override
	public ConflictsDBOutputModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		ConflictsDBOutputModel data= new ConflictsDBOutputModel();
		data.setConflicts(rs.getString("Conflicts"));
		return data;
	}

}
