package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.SubscriptionDBRepository;
import com.aiknights.com.rapidviewmain.models.GenericSubscriptionDetailsModel;
import com.aiknights.com.rapidviewmain.models.SubscriptionPropertiesModel;
import com.aiknights.com.rapidviewmain.models.SubscriptionPropertiesResponce;
import com.aiknights.com.rapidviewmain.models.WeatherSubscriptionPropertiesModel;

@Repository
public class SubscriptionDAOImpl implements SubscriptionDBRepository {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<SubscriptionPropertiesModel> getSubscriptionProperties() {
		final String sql = "select * from Subscription_Properties";

		List<SubscriptionPropertiesModel> SubscriptionProperties1 = null;
		try {
			SubscriptionProperties1 = jdbcTemplate.query(sql, new SubscriptionPropertiesRowMapper());
		} catch (Exception e) {
			// TODO: handle exception
		}

		return SubscriptionProperties1;
	}

	@Override
	public List<SubscriptionPropertiesResponce> getSubscriptionDetails() {
		final String WEATHER_SUBSCRIPTION_DEATILS_SQL = "select * from Weather_Subscription_Properties";
		SubscriptionPropertiesResponce subscriptionPropertiesResponce = new SubscriptionPropertiesResponce();
		List<SubscriptionPropertiesResponce> SubscriptionDetails = new ArrayList<SubscriptionPropertiesResponce>();
		List<WeatherSubscriptionPropertiesModel> weatherSubscriptionDetails;
		try {
			weatherSubscriptionDetails = jdbcTemplate.query(WEATHER_SUBSCRIPTION_DEATILS_SQL,
					new WeatherSubscriptionPropertiesRowMapper());
			subscriptionPropertiesResponce.setWeatherProperties(weatherSubscriptionDetails);
			SubscriptionDetails.add(subscriptionPropertiesResponce);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return SubscriptionDetails;
	}

	@Override
	public List<GenericSubscriptionDetailsModel> getAllSubscriptionDetailsForGivenSubscriptionTable(
			String SubscriptionTable) {
		final String sql = "select * from " + SubscriptionTable
				+ " st,Subscription_Properties sp where st.Subscription_Type =sp.Subscription_Type ;";

		List<GenericSubscriptionDetailsModel> SubscriptionProperties = null;
		try {
			SubscriptionProperties = jdbcTemplate.query(sql, new GenericSubscriptionPropertiesRowMapper());
		} catch (Exception e) {
			return null;
		}

		return SubscriptionProperties;
	}

}
