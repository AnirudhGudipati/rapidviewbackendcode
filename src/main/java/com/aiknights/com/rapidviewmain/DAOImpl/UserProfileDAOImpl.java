package com.aiknights.com.rapidviewmain.DAOImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.UserProfileRepository;
import com.aiknights.com.rapidviewmain.models.UserDetails;

@Repository
public class UserProfileDAOImpl implements UserProfileRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	private static final Logger LOGGER = LoggerFactory.getLogger(ClaimNotificationDAOImpl.class);

	@Override
	public UserDetails getProfileDetailsByUserId(String user_Id) {

		final String GET_PROFILE_DETAILS = "select * from user_details INNER  JOIN Licenesing  ON  Licenesing.username = user_details.username where Licenesing.username=?;";
		UserDetails userProfileDetaiols = jdbcTemplate.queryForObject(GET_PROFILE_DETAILS,
				new UserProfileDetailsRowMapper(), user_Id);

		LOGGER.info("User Details  for a user are: {}", userProfileDetaiols);
		return userProfileDetaiols;

	}

	@Override
	public boolean updateProfileDetailsByUserName(String userName, String address, String contactNumber) {
		// String sql = "update user_details set contactNumber=?,address=? where
		// username="+notification_Object_ID+\";\"";

		final String sql = "Update user_details set contactNumber =\"" + contactNumber + "\" where username = \""
				+ userName + "\"";
		final String sql1 = "Update user_details set address =\"" + address + "\" where username = \"" + userName
				+ "\"";
		int output = jdbcTemplate.update(sql);
		int output1 = jdbcTemplate.update(sql1);
		if (output == 1) {
			return true;
		} else {
			return false;
		}
	}

}
