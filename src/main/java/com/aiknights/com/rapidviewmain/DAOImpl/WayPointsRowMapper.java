package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.WayPointDataDBModel;

public class WayPointsRowMapper implements RowMapper<WayPointDataDBModel>{

	@Override
	public WayPointDataDBModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		WayPointDataDBModel wpdm = new WayPointDataDBModel();
		wpdm.setAbove_Ground(rs.getString("Above_Ground"));
		wpdm.setAltitude(rs.getString("Altitude"));
		wpdm.setCurve_size(rs.getString("Curve_size"));
		wpdm.setFlight_Plan_ID(rs.getString("Flight_Plan_ID"));
		wpdm.setGimbal_Option(rs.getString("Gimbal_Option"));
		wpdm.setGimbal_Value(rs.getString("Gimbal_Value"));
		wpdm.setHeading(rs.getString("Heading"));
		wpdm.setInterval_Option(rs.getString("Interval_Option"));
		wpdm.setInterval_Value(rs.getString("Interval_Value"));
		wpdm.setLatitude(rs.getString("Latitude"));
		wpdm.setLongitude(rs.getString("Longitude"));
		wpdm.setPOI(rs.getString("POI"));
		wpdm.setSpeed(rs.getString("Speed"));
		wpdm.setWay_Point_ID(rs.getString("Way_Point_ID"));
		wpdm.setWay_Point_Key(rs.getString("Way_Point_Key"));
		return wpdm;
	}

}
