package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.WeatherAssessmentDataModel;

public class WeatherAssessmentDataRowMapper implements RowMapper<WeatherAssessmentDataModel> {

	@Override
	public WeatherAssessmentDataModel mapRow(ResultSet rs, int rowNum) throws SQLException {

		WeatherAssessmentDataModel weatherAssessmentDataforuser = new WeatherAssessmentDataModel();
		weatherAssessmentDataforuser.setAddress(rs.getString("Address"));
		weatherAssessmentDataforuser.setCarriername(rs.getString("CarrierName"));
		weatherAssessmentDataforuser.setClaimdate(rs.getString("ClaimDate"));
		weatherAssessmentDataforuser.setClaimid(rs.getString("ClaimID"));
		weatherAssessmentDataforuser.setOwnername(rs.getString("OwnerName"));
		weatherAssessmentDataforuser.setPolicystartdate(rs.getString("PolicyStartDate"));
		weatherAssessmentDataforuser.setUserid(rs.getString("UserID"));
		weatherAssessmentDataforuser.setReportId(rs.getString("WeatherAssessmentDataID"));
		weatherAssessmentDataforuser.setReport_Received_Date(rs.getString("Report_Received_Date"));
		weatherAssessmentDataforuser.setStagingstatus(rs.getString("StagingStatus"));
		weatherAssessmentDataforuser.setLatitude(rs.getString("latitude"));
		weatherAssessmentDataforuser.setLongitude(rs.getString("longitude"));
		weatherAssessmentDataforuser.setReportType(rs.getString("reportType"));
		return weatherAssessmentDataforuser;
	}

}
