package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.TokenDBRepository;
import com.aiknights.com.rapidviewmain.models.TokenDBModel;

@Repository
public class TokenDBRepositoryImpl implements TokenDBRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<TokenDBModel> getTokenDetailsBasedOnTokenID(String token) {
		final String token_sql = "select * from confirmation_Tokens where Token = \""+token+"\"";
		try {
			final List<TokenDBModel> tokendata = jdbcTemplate.query(token_sql,new TokenDBRowMapper());
			return tokendata;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public boolean insertNewTokenDetailsToDB(String token,String CreatedDateTime,String IsUsed,String userId) {
		try {
			final String sql = "INSERT into confirmation_Tokens (Token,Created_DateTime,IsUsed,user_id) values(?,?,?,?);";
			int output = jdbcTemplate.update(sql, token, CreatedDateTime, IsUsed, userId);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean markTokenAsUsedByToken(String token) {
		try {
			final String sql = "Update confirmation_Tokens set IsUsed =\"1\" where Token = \""+token+"\"";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean validateSessionByToken(String token) {
		try {
			final String sql = "Update confirmation_Tokens set IsUsed =\"0\" where Token = \""+token+"\"";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	
	
	@Override
	public List<TokenDBModel> getAllOpenTokens() {
		final String token_sql = "select * from confirmation_Tokens  where IsUsed !=\"1\";";
		try {
			final List<TokenDBModel> tokendata = jdbcTemplate.query(token_sql,new TokenDBRowMapper());
			return tokendata;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

}
