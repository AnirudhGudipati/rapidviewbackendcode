package com.aiknights.com.rapidviewmain.DAOImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.WeatherRepository;
import com.aiknights.com.rapidviewmain.models.WeatherCheck;

@Repository
public class WeatherRepositoryDAOImpl implements WeatherRepository{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(WeatherRepositoryDAOImpl.class);

	@Override
	public WeatherCheck getWeatherById(int wetherId) {
		// TODO Not Reuired Now
		return null;
	}

	@Override
	public WeatherCheck getWeatherByClaimId(final String claimid) {
		final String GET_WEATHER_BY_CLAIM_ID = "select * from weather_check where claim_id = ?;";
		try {
		final WeatherCheck weatherCheck = jdbcTemplate.queryForObject(GET_WEATHER_BY_CLAIM_ID, new WeatherCheckRowMapper(),claimid );
		LOGGER.info("Weather Object for given claim id is: {}", weatherCheck);
		return weatherCheck;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
		
	}

}
