package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.ReportGenerationRepository;
import com.aiknights.com.rapidviewmain.dto.ReportTabDTO;

@Repository
public class ReportGenerationDAOImpl implements ReportGenerationRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportGenerationDAOImpl.class);
	

	@Override
	public ReportTabDTO getReportDetails(final String claimId) {
		 final String GET_CLAIMS_BY_USER_ID = "SELECT * FROM claim_notification CN JOIN policy_check PC ON PC.policy_id=CN.policy_id " + 
		 		
		 		"		  LEFT JOIN create_report CR ON CR.claim_id = CN.claim_id " +
		 		"LEFT JOIN risk_assesment RA ON RA.claim_id=CN.claim_id"+
		 		"		  WHERE CN.claim_id=?";
		 List<ReportTabDTO> reportList = jdbcTemplate.query(GET_CLAIMS_BY_USER_ID, new Object[] {claimId}, new ReportTabRowMapper());
		 LOGGER.info("Reports for a user are: {}", reportList);
		 if(reportList != null && reportList.size()>0 )
		 {
			 return reportList.get(0);
		 }
		 else
		 {
			 return new ReportTabDTO();
		 }
	}

}
