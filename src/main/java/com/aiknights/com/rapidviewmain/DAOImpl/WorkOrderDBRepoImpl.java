package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.WorkOrderDBRepo;
import com.aiknights.com.rapidviewmain.models.ConflictsDBOutputModel;
import com.aiknights.com.rapidviewmain.models.WorkOrderDetailsResponseModel;

@Repository
public class WorkOrderDBRepoImpl implements WorkOrderDBRepo {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public boolean putWorkOrderToDB(String work_Order_ID, String flight_Plan_ID, String work_Order_Status,
			String assigned_To, String assigned_By, String expected_Start_Date, String expected_End_Date,
			String created_Date, String notes, String work_Order_Approval_Status) {
		try {
			final String sql = "INSERT into Work_Orders (Work_Order_ID,Flight_Plan_ID,Work_Order_Status,Assigned_To,Assigned_By,WO_E_Start_Date,WO_E_End_Date,WO_Notes,WO_Creation_Date,WO_Approval_Status) values(?,?,?,?,?,?,?,?,?,?);";
			int output = jdbcTemplate.update(sql, work_Order_ID, flight_Plan_ID, work_Order_Status, assigned_To,
					assigned_By, expected_Start_Date, expected_End_Date, notes, created_Date,
					work_Order_Approval_Status);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean updateDefaultWorkOrderByID(String workOrderStatus, String workOrderID, String assignedTo,
			String woStartDate, String woEndDate, String notes) {
		try {
			final String sql = "UPDATE Work_Orders set Work_Order_Status =\"" + workOrderStatus + "\",Assigned_To ="
					+ assignedTo + ",WO_E_Start_Date =\"" + woStartDate + "\",WO_E_End_Date=\"" + woEndDate
					+ "\",WO_Notes =\"" + notes + "\" WHERE Work_Order_ID=\"" + workOrderID + "\";";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<WorkOrderDetailsResponseModel> getOrdersAsAdjuster(String userID) {
		final String orders_assigned_by_query = "select w.Work_Order_ID,w.Flight_Plan_ID,w.Work_Order_Status,w.Assigned_To,w.Assigned_By,w.ReAssignCount,w.WO_ReOpen_Comment,w.WO_Notes,w.WO_E_Start_Date,w.WO_Creation_Date,w.WO_E_End_Date,w.WO_Approval_Status,fpo.Claim_ID ,fpo.Claim_Address ,fpp.Flight_Plan_Name,ud.email Adjuster_Email,cn.phone_number Claimant_PHN,cn.firstname Claimant_FN,cn.lastname Claimant_LN,cn.email Claimant_Email,we.EventLink Meeting_Link,(Select email from user_details ud1 where userid =w.Assigned_To ) Pilot_Email from Work_Orders w LEFT JOIN WO_Events we ON w.Work_Order_ID =we.WorkOrderID,Flight_Plan_Properties fpp,Flight_Plan_Object fpo,user_details ud,claim_notification cn where w.Flight_Plan_ID =fpp.Flight_Plan_ID and w.Flight_Plan_ID =fpo.Flight_Plan_ID and w.Assigned_By =ud.userid and fpo.Claim_ID =cn.claim_id and w.Work_Order_Status !=\"Pending\" and Assigned_By =\""
				+ userID + "\";";
		try {
			final List<WorkOrderDetailsResponseModel> ordersdata = jdbcTemplate.query(orders_assigned_by_query,
					new OrderDataRowMapper());
			return ordersdata;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public List<WorkOrderDetailsResponseModel> getOrdersAsPilot(String userID) {
		final String orders_assigned_to_query = "select w.Work_Order_ID,w.Flight_Plan_ID,w.Work_Order_Status,w.Assigned_To,w.Assigned_By,w.ReAssignCount,w.WO_ReOpen_Comment,w.WO_Notes,w.WO_E_Start_Date,w.WO_Creation_Date,w.WO_E_End_Date,w.WO_Approval_Status,fpo.Claim_ID ,fpo.Claim_Address ,fpp.Flight_Plan_Name,ud.email Adjuster_Email,cn.phone_number Claimant_PHN,cn.firstname Claimant_FN,cn.lastname Claimant_LN,cn.email Claimant_Email,we.EventLink Meeting_Link,(Select email from user_details ud1 where userid =w.Assigned_To ) Pilot_Email from Work_Orders w LEFT JOIN WO_Events we ON w.Work_Order_ID =we.WorkOrderID,Flight_Plan_Properties fpp,Flight_Plan_Object fpo,user_details ud,claim_notification cn where w.Flight_Plan_ID =fpp.Flight_Plan_ID and w.Flight_Plan_ID =fpo.Flight_Plan_ID and w.Assigned_By =ud.userid and fpo.Claim_ID =cn.claim_id and w.Work_Order_Status !=\"Pending\" and Assigned_To =\""
				+ userID + "\";";
		try {
			final List<WorkOrderDetailsResponseModel> ordersdata = jdbcTemplate.query(orders_assigned_to_query,
					new OrderDataRowMapper());
			return ordersdata;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public List<WorkOrderDetailsResponseModel> getMarketPlaceOrders() {
		final String orders_assigned_to_query = "select w.Work_Order_ID,w.Flight_Plan_ID,w.Work_Order_Status,w.Assigned_To,w.Assigned_By,w.WO_Notes,w.WO_E_Start_Date,w.WO_E_End_Date,w.WO_Creation_Date,w.WO_Approval_Status,w.ReAssignCount,w.WO_ReOpen_Comment,fpo.Claim_ID ,fpo.Claim_Address ,fpp.Flight_Plan_Name,ud.email,cn.phone_number from Work_Orders w,Flight_Plan_Properties fpp,Flight_Plan_Object fpo,user_details ud,claim_notification cn where w.Flight_Plan_ID =fpp.Flight_Plan_ID and w.Flight_Plan_ID =fpo.Flight_Plan_ID and w.Assigned_By =ud.userid and fpo.Claim_ID =cn.claim_id and w.Work_Order_Status =\"Marketplace\";";
		try {
			final List<WorkOrderDetailsResponseModel> ordersdata = jdbcTemplate.query(orders_assigned_to_query,
					new MarketPlaceOrdersDataRowMapper());
			return ordersdata;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public List<WorkOrderDetailsResponseModel> getOrderByWOID(String workOrderID) {
		final String orders_assigned_to_query = "select w.Work_Order_ID,w.Flight_Plan_ID,w.Work_Order_Status,w.Assigned_To,w.Assigned_By,w.WO_Notes,w.WO_E_Start_Date,w.WO_E_End_Date,w.WO_Creation_Date,w.WO_Approval_Status,w.ReAssignCount,w.WO_ReOpen_Comment,fpo.Claim_ID ,fpo.Claim_Address ,fpp.Flight_Plan_Name,ud.email,cn.phone_number from Work_Orders w,Flight_Plan_Properties fpp,Flight_Plan_Object fpo,user_details ud,claim_notification cn where w.Flight_Plan_ID =fpp.Flight_Plan_ID and w.Flight_Plan_ID =fpo.Flight_Plan_ID and w.Assigned_By =ud.userid and fpo.Claim_ID =cn.claim_id and w.Work_Order_ID =\""
				+ workOrderID + "\";";
		try {
			final List<WorkOrderDetailsResponseModel> ordersdata = jdbcTemplate.query(orders_assigned_to_query,
					new OrderDataRowMapper());
			return ordersdata;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public boolean updateWorkOrderStatusByID(String workOrderID, String workOrderStatus,
			String workOrderApprovalStatus) {
		try {
			final String sql = "UPDATE Work_Orders set Work_Order_Status =\"" + workOrderStatus
					+ "\",WO_Approval_Status =\"" + workOrderApprovalStatus + "\" WHERE Work_Order_ID=\"" + workOrderID
					+ "\";";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean updateWorkOrderMarketPlaceAssignment(String workOrderID, String workOrderStatus, String assignedTo) {
		try {
			final String sql = "UPDATE Work_Orders set Work_Order_Status =\"" + workOrderStatus + "\",Assigned_To =\""
					+ assignedTo + "\" WHERE Work_Order_ID=\"" + workOrderID + "\";";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean reOpenWorkOrderUpdateMethod(String workOrderID, String approvalStatus, String reopenComment) {
		try {
			final String sql = "UPDATE Work_Orders set WO_Approval_Status =\"" + approvalStatus
					+ "\",WO_ReOpen_Comment =\"" + reopenComment + "\" WHERE Work_Order_ID=\"" + workOrderID + "\";";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean updateWorkOrderReopenCount(String workOrderID, String NewCount) {
		try {
			final String sql = "UPDATE Work_Orders set ReAssignCount =\"" + NewCount + "\" WHERE Work_Order_ID=\""
					+ workOrderID + "\";";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean updateWorkOrderApprovalStatus(String workOrderID, String approvalStatus) {
		try {
			final String sql = "UPDATE Work_Orders set WO_Approval_Status =\"" + approvalStatus
					+ "\" WHERE Work_Order_ID=\"" + workOrderID + "\";";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public String checkAdjusterEventAvailability(String adjuster_ID, String date, String start_Time, String endTime) {
		String Availability = null;
		try {
			final String sql = "Select Count(*) as Conflicts from WO_Events we where Adjuster_ID =" + adjuster_ID
					+ " and EventDate =\"" + date + "\" and ((\"" + start_Time
					+ "\" BETWEEN StartTime and EndTime) or (\"" + endTime + "\" BETWEEN StartTime and EndTime));";
			List<ConflictsDBOutputModel> output = jdbcTemplate.query(sql, new ConflictsRowMapper());
			if (output.get(0).getConflicts().equals("0")) {
				Availability = "Free";
				return Availability;
			} else {
				Availability = "Busy";
				return Availability;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Availability;
		}
	}

	@Override
	public String checkPilotEventAvailability(String pilot_ID, String date, String start_Time, String endTime) {
		String Availability = null;
		try {
			final String sql = "Select Count(*) as Conflicts from WO_Events we where Pilot_ID =" + pilot_ID
					+ " and EventDate =\"" + date + "\" and ((\"" + start_Time
					+ "\" BETWEEN StartTime and EndTime) or (\"" + endTime + "\" BETWEEN StartTime and EndTime));";
			List<ConflictsDBOutputModel> output = jdbcTemplate.query(sql, new ConflictsRowMapper());
			if (output.get(0).getConflicts().equals("0")) {
				Availability = "Free";
				return Availability;
			} else {
				Availability = "Busy";
				return Availability;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Availability;
		}
	}

	@Override
	public boolean createEvent(String workOrderID, String eventID, String eventLink, String adjuster_ID, String pilot_ID,String eventSystem, String startTime, String endTime, String eventDate, String timePeriod) {
		try {
			final String sql = "INSERT into WO_Events (workOrderID, eventID, eventLink, adjuster_ID, pilot_ID,eventSystem, startTime, endTime, eventDate, timePeriod) values(?,?,?,?,?,?,?,?,?,?);";
			int output = jdbcTemplate.update(sql, workOrderID, eventID, eventLink, adjuster_ID, pilot_ID,eventSystem, startTime, endTime, eventDate, timePeriod);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
