package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.SubscriptionPropertiesModel;

public class SubscriptionPropertiesRowMapper implements RowMapper<SubscriptionPropertiesModel> {

	@Override
	public SubscriptionPropertiesModel mapRow(ResultSet rs, int rowNum) throws SQLException {

		SubscriptionPropertiesModel subscriptionproperties = new SubscriptionPropertiesModel();
		subscriptionproperties.setNumber_of_Days(rs.getString("Number_of_Days"));
		subscriptionproperties.setSubscription_Type(rs.getString("Subscription_Type"));
		subscriptionproperties.setSubscription_Type_For_UI(rs.getString("Subscription_Type_For_UI"));

		return subscriptionproperties;
	}

}
