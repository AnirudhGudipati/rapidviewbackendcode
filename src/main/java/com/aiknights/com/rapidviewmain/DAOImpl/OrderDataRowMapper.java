package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.WorkOrderDetailsResponseModel;

public class OrderDataRowMapper implements RowMapper<WorkOrderDetailsResponseModel> {

	@Override
	public WorkOrderDetailsResponseModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		WorkOrderDetailsResponseModel orderdata = new WorkOrderDetailsResponseModel();
		orderdata.setAdjusterEmail(rs.getString("Adjuster_Email"));
		orderdata.setAssignedBy(rs.getString("Assigned_By"));
		orderdata.setAssignedTo(rs.getString("Assigned_To"));
		orderdata.setClaimantMobile(rs.getString("Claimant_PHN"));
		orderdata.setClaimID(rs.getString("Claim_ID"));
		orderdata.setFlightPlanID(rs.getString("Flight_Plan_ID"));
		orderdata.setNotes(rs.getString("WO_Notes"));
		orderdata.setOrderStatus(rs.getString("Work_Order_Status"));
		orderdata.setPropertyAddress(rs.getString("Claim_Address"));
		orderdata.setWorkOrderID(rs.getString("Work_Order_ID"));
		orderdata.setFlightPlanName(rs.getString("Flight_Plan_Name"));
		orderdata.setExpectedEndDate(rs.getString("WO_E_End_Date"));
		orderdata.setExpectedStartDate(rs.getString("WO_E_Start_Date"));
		orderdata.setCreationDate(rs.getString("WO_Creation_Date"));
		orderdata.setWorkOrderApprovalStatus(rs.getString("WO_Approval_Status"));
		orderdata.setwOReopenComment(rs.getString("WO_ReOpen_Comment"));
		orderdata.setwOReopenCount(rs.getString("ReAssignCount"));
		orderdata.setClaimant_FN(rs.getString("Claimant_FN"));
		orderdata.setClaimant_LN(rs.getString("Claimant_LN"));
		orderdata.setClaimant_Email(rs.getString("Claimant_Email"));
		orderdata.setMeeting_link(rs.getString("Meeting_Link"));
		orderdata.setPilotEmail(rs.getString("Pilot_Email"));
		return orderdata;
	}

}
