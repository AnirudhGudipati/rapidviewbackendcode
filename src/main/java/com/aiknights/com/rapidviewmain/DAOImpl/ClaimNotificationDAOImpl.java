package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.ClaimNotificationRepository;
import com.aiknights.com.rapidviewmain.models.ClaimNotification;

@Repository
public class ClaimNotificationDAOImpl implements ClaimNotificationRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Value("${rapid.super.Company.Name}")
	private String superCompanyName;

	@Override
	public ClaimNotification getClaimByClaimId(final String claimId) {
		final String GET_CLAIM_BY_CLAIMID = "select * from claim_notification INNER  JOIN policy_check  ON  claim_notification.policy_id = policy_check.policy_id where claim_notification.claim_id=?;";
		try {
			final ClaimNotification claimNotification = jdbcTemplate.queryForObject(GET_CLAIM_BY_CLAIMID,
					new ClaimNotificationRowMapper(), claimId);
			return claimNotification;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public List<ClaimNotification> getAssignedClaimsForUser(final int userId) {
		final String GET_CLAIMS_BY_USER_ID = "select * from claim_notification INNER  JOIN policy_check  ON  claim_notification.policy_id = policy_check.policy_id where claim_notification.claim_id IN(select claim_id from adjuster_claim where adjuster_id = ?) limit 300;";
		List<ClaimNotification> claimList = jdbcTemplate.query(GET_CLAIMS_BY_USER_ID, new Object[] { userId },
				new ClaimNotificationRowMapper());
		return claimList;
	}

	@Override
	public List<ClaimNotification> getAllClaims() {
		final String get_all_Claims = "select * from claim_notification INNER  JOIN policy_check  ON  claim_notification.policy_id = policy_check.policy_id;";
		List<ClaimNotification> claimList = jdbcTemplate.query(get_all_Claims, new ClaimNotificationRowMapper());
		return claimList;
	}

	@Override
	public ClaimNotification getClaimWithPolicyDataByClaimId(String claimId) {
		final String GET_CLAIM_POLICY_DATE_BY_CLAIM_ID = "select * from claim_notification INNER  JOIN policy_check  ON  claim_notification.policy_id = policy_check.policy_id where claim_notification.claim_id=?;";
		try {
			final ClaimNotification claimNotification = jdbcTemplate.queryForObject(GET_CLAIM_POLICY_DATE_BY_CLAIM_ID,
					new ClaimPolicyNotificationRowMapper(), claimId);
			return claimNotification;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public List<ClaimNotification> manageClaimsAsAdmin(String companyName) {
		final String sql;
		if(companyName.equals(superCompanyName)) {
			sql = "SELECT * FROM claim_notification INNER JOIN policy_check ON claim_notification.policy_id = policy_check.policy_id WHERE claim_notification.claim_id IN(SELECT claim_id FROM adjuster_claim ac, user_details ud WHERE ac.adjuster_id = ud.userid) limit 300;";
		}else {
			sql = "SELECT * FROM claim_notification INNER JOIN policy_check ON claim_notification.policy_id = policy_check.policy_id WHERE claim_notification.claim_id IN(SELECT claim_id FROM adjuster_claim ac, user_details ud WHERE ac.adjuster_id = ud.userid and ud.company_name = \""
					+ companyName + "\") limit 300;";
		}
		try {
			List<ClaimNotification> claimList = jdbcTemplate.query(sql, new ClaimNotificationRowMapper());
			return claimList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
