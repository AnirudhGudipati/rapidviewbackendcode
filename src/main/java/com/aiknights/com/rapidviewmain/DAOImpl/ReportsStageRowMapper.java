package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.ReportsStageDBModel;

public class ReportsStageRowMapper implements RowMapper<ReportsStageDBModel>{

	@Override
	public ReportsStageDBModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		ReportsStageDBModel weatherStageDatabaseModel = new ReportsStageDBModel();
		weatherStageDatabaseModel.setReportid(rs.getString("ReportID"));
		weatherStageDatabaseModel.setEventdate(rs.getString("Event_Date"));
		weatherStageDatabaseModel.setHailsize(rs.getString("Hailsize"));
		weatherStageDatabaseModel.setLatitude(rs.getString("Latitude"));
		weatherStageDatabaseModel.setLongitude(rs.getString("longitude"));
		weatherStageDatabaseModel.setDistance(rs.getString("Distance"));
		return weatherStageDatabaseModel;
	}

}
