package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.ResourceRepository;
import com.aiknights.com.rapidviewmain.models.Resource;

@Repository
public class ResourceDAOImpl implements ResourceRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Resource getResourceById(int resourceId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getResourcesByUserName(final String userName) {

		final String GET_ROLES_BY_USER_NAME = "select rolename from role where username = ?";
		String RoleForUser = (String) jdbcTemplate.queryForObject(GET_ROLES_BY_USER_NAME, new Object[] { userName },
				String.class);
		String ProductTableName = null;

		if (!RoleForUser.equalsIgnoreCase("UNASSIGNEDROLE")) {
			final String PRODUCT_TABLE_NAME_SQL = "SELECT ProductTableName from Products where ProductID =(SELECT ProductID from Licenesing  where username=?)";
			ProductTableName = (String) jdbcTemplate.queryForObject(PRODUCT_TABLE_NAME_SQL, new Object[] { userName },
					String.class);
		} else {
			ProductTableName = "Rapid_View_User_Table";
		}

		final String GET_RESOURCES = "SELECT ResourceName from " + ProductTableName
				+ " where IsActive='1' and RoleName =(SELECT RoleName from role where username=?)";
		List<String> resourceList = new ArrayList<>();
		final List<String> resources = jdbcTemplate.queryForList(GET_RESOURCES, new Object[] { userName },
				String.class);
		resourceList.addAll(resources);
		return resourceList;
	}

}
