package com.aiknights.com.rapidviewmain.DAOImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.DashboardAPIRepository;
import com.aiknights.com.rapidviewmain.models.DashboardClaimCategoryModel;
import com.aiknights.com.rapidviewmain.models.DashboardTotalClaimsModel;

@Repository
public class DashboardRepositoryImpl implements DashboardAPIRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public DashboardTotalClaimsModel getTotalClaimsData(String userid) {
		final String sql = "select count(*) as Total,(select count(*) from claim_notification  c,adjuster_claim a where c.claim_id=a.claim_id and a.adjuster_id=\""+userid+"\" and c.status=\"ACTIVE\") as InProgress,\r\n" + 
				"        (select count(*) from claim_notification c,adjuster_claim a where c.claim_id=a.claim_id and a.adjuster_id=\""+userid+"\" and c.status=\"COMPLETED\") as Completed,\r\n" + 
				"        (select count(*) from claim_notification c,adjuster_claim a where c.claim_id=a.claim_id and a.adjuster_id=\""+userid+"\" and c.status=\"REJECTED\") as Rejected  from claim_notification c,adjuster_claim a where c.claim_id=a.claim_id and a.adjuster_id=\""+userid+"\";";
		final DashboardTotalClaimsModel totalclaimsdata = jdbcTemplate.queryForObject(sql, new DashboardTotalClaimsRowMapper());
		return totalclaimsdata;
	}

	@Override
	public DashboardClaimCategoryModel getClaimCategoryData(String userid) {
		final String sql = "select count(*) as Total,(select count(*) from claim_notification  c,adjuster_claim a where c.claim_id=a.claim_id and a.adjuster_id=\""+userid+"\" and (incident_type=\"flood\" or incident_type=\"water\")) as Water,\r\n" + 
				"        (select count(*) from claim_notification c,adjuster_claim a where c.claim_id=a.claim_id and a.adjuster_id=\""+userid+"\" and (incident_type=\"Hail\" or incident_type=\"hail\")) as Hail,\r\n" + 
				"        (select count(*) from claim_notification c,adjuster_claim a where c.claim_id=a.claim_id and a.adjuster_id=\""+userid+"\" and (incident_type=\"wind\")) as Wind  from claim_notification c,adjuster_claim a where c.claim_id=a.claim_id and a.adjuster_id=\""+userid+"\";";
		final DashboardClaimCategoryModel claimcategorydata = jdbcTemplate.queryForObject(sql, new DashboardClaimCategoryRowMapper());
		return claimcategorydata;
	}

}
