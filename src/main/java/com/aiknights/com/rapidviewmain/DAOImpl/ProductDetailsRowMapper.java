package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.ProductsTableDBModel;

public class ProductDetailsRowMapper implements RowMapper<ProductsTableDBModel> {

	@Override
	public ProductsTableDBModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		ProductsTableDBModel data = new ProductsTableDBModel();
		data.setProductID(rs.getString("ProductID"));
		data.setProductName(rs.getString("ProductName"));
		data.setProductTableName(rs.getString("ProductTableName"));
		data.setSubscriptionPropertiesTableName(rs.getString("SubscriptionPropertiesTableName"));
		return data;
	}
}
