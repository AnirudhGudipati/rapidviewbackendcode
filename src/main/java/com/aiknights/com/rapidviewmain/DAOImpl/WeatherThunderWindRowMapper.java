package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.WeatherThunderWindModel;

public class WeatherThunderWindRowMapper implements RowMapper<WeatherThunderWindModel> {

	@Override
	public WeatherThunderWindModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		WeatherThunderWindModel weatherdbmodel = new WeatherThunderWindModel();
		weatherdbmodel.setBegin_location(rs.getString("begin_location"));
		weatherdbmodel.setMagnitude(rs.getString("magnitude"));
		weatherdbmodel.setEvent_type(rs.getString("event_type"));
		weatherdbmodel.setBegin_lat(rs.getString("begin_lat"));
		weatherdbmodel.setBegin_lon(rs.getString("begin_lon"));
		weatherdbmodel.setBegin_date(rs.getString("begin_date"));
		weatherdbmodel.setBegin_time(rs.getString("begin_time"));
		weatherdbmodel.setDistance(rs.getString("distance"));
		weatherdbmodel.setEvent_narrative(rs.getString("event_narrative"));
		return weatherdbmodel;
	}

}
