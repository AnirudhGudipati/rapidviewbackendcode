package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.ImageEvidenceRepository;
import com.aiknights.com.rapidviewmain.models.AnalyseReponseInDetail;
import com.aiknights.com.rapidviewmain.models.DeleteIncidentImagesInput;

@Repository
public class ImageEvidenceRepositoryImpl implements ImageEvidenceRepository{
	
	@Autowired
	private JdbcTemplate jt;

	@Override
	public int[] saveUploadedImages(List<AnalyseReponseInDetail> input,String claimId) {
		
		//storing damage size as -1.0 default 
		
		String sql = "insert into incident_evidence values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		List<Object[]> args = input.stream().map(i->new Object[] {i.getSourceImageId(),i.getSourceImageUrl(),null,null,null,null,null,null,null,null,claimId,-1.0,i.getShingleType(),i.getSourceImageName(),-1}).collect(Collectors.toList());
		int[] output = jt.batchUpdate(sql, args);
		return output;
	}

	@Override
	public int updateAnalysedImageDatatoDB(AnalyseReponseInDetail input) {
		String sql = "update incident_evidence set analysed_image_url=?,analysed_imageid=?,assessment_blurry_error=?,imageName=?,assessment_scale_error=?,assessment_status=?,class_type=?,detection_score=?,damage_size=?,analysisstate=? where imageid=?";
		String classType = input.getClassType()!=null?input.getClassType().toString():null;
		int analysisstate =1;
		int output = jt.update(sql, input.getAnalysedImageUrl(),input.getAnalysedImageId(),input.getAssessmentBlurrinessError(),input.getImageName(),input.getAssessmentScaleError(),input.getAssessmentStatus(),classType,input.getDetectionScore(),input.getDamageSize(),analysisstate,input.getSourceImageId());
		return output;
	}

	@Override
	public List<AnalyseReponseInDetail> getIncidentEvidenceForClaim(String claimId) {
		//String sql = "select imageid,source_image_url,analysed_image_url,analysed_imageid,assessment_blurry_error,imageName,assessment_scale_error,assessment_status,class_type,detection_score from incident_evidence were claim_id=?";
		String sql = "Select * from incident_evidence where claim_id = ?";
		final List<AnalyseReponseInDetail> list = jt.query(sql, new IncidentEvidenceRowMapper(), claimId);
		return list;
	}

	@Override
	public int[] deleteIncidentEvidenceImages(List<DeleteIncidentImagesInput> deleteImagesInput) throws DataAccessException {
		String sql = "delete from incident_evidence where imageid=?";
		List<Object[]> args = deleteImagesInput.stream().map(i->new Object[] {i.getSourceImageId()}).collect(Collectors.toList());
		int[] output = jt.batchUpdate(sql, args);
		return output;
	}

	@Override
	public int changeAnalysisStatetoZero(String imageId) {
		String sql ="update incident_evidence set analysisstate=0 where imageid=?;";
		int output = jt.update(sql,imageId);
		return output;
	}

}
