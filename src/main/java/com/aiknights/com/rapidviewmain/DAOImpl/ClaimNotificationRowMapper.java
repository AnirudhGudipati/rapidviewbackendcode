package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.ClaimNotification;
import com.aiknights.com.rapidviewmain.models.ClaimStatus;

public class ClaimNotificationRowMapper implements RowMapper<ClaimNotification> {

	@Override
	public ClaimNotification mapRow(ResultSet rs, int rowNum) throws SQLException {
		ClaimNotification claimNotification = new ClaimNotification();
		claimNotification.setClaimId(rs.getString("claim_id"));
		claimNotification.setClaimantFirstName(rs.getString("firstname"));
		claimNotification.setClaimantLastName(rs.getString("lastname"));
		claimNotification.setEmail(rs.getString("email"));
		claimNotification.setPhoneNumber(rs.getString("phone_number"));
		claimNotification.setAddress(rs.getString("street_address"));
		claimNotification.setCity(rs.getString("city"));
		claimNotification.setState(rs.getString("state"));
		claimNotification.setZipCode(rs.getInt("zipcode"));
		claimNotification.setPolicyNumber(rs.getString("policy_number"));
		claimNotification.setIncidentType(rs.getString("incident_type"));
		claimNotification.setIncidentDate(rs.getDate("incident_date").toString());
		claimNotification.setIncidentTime(rs.getTime("incident_time").toString());
		claimNotification.setClaimDate(rs.getDate("claim_date").toString());
		claimNotification.setClaimAmount(rs.getInt("claim_amount"));
		claimNotification.setClaimstatus(ClaimStatus.valueOf(rs.getString("status")));
		claimNotification.setInsurance_company(rs.getString("insurance_company"));
		claimNotification.setMortgageCompany(rs.getString("mortgage_company"));
		claimNotification.setInsuranceCompany(rs.getString("insurance_company"));
		claimNotification.setStartDate(rs.getDate("start_date"));
		claimNotification.setEndDate(rs.getDate("end_date"));
		claimNotification.setClaimDescription(rs.getString("claim_description"));
		claimNotification.setCoverageAmt(rs.getInt("CoverageAmt"));
		claimNotification.setPolicyLimit(rs.getString("Policy_Limit"));
		claimNotification.setPolicyStatus(rs.getString("policyStatus"));
		claimNotification.setPolicyLastUpdateDate(rs.getDate("PolicyLastUpdateDate"));
		claimNotification.setPriorClaims(rs.getInt("priorClaims"));
		claimNotification.setPolicyCoverage(rs.getInt("policy_coverage"));
		claimNotification.setPolicyType(rs.getString("policy_type"));
		claimNotification.setDeductibleAmount(rs.getInt("deductible"));
		
		

		return claimNotification;
	}

}
