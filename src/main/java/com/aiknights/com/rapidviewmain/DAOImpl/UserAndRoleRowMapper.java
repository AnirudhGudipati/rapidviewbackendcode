package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.UserAndRoleDetailsModel;

public class UserAndRoleRowMapper implements RowMapper<UserAndRoleDetailsModel> {

	@Override
	public UserAndRoleDetailsModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserAndRoleDetailsModel userDetails = new UserAndRoleDetailsModel();
		userDetails.setUserId(rs.getString("userid"));
		userDetails.setUserName(rs.getString("username"));
		userDetails.setFirstName(rs.getString("firstname"));
		userDetails.setLastName(rs.getString("lastname"));
		userDetails.setEmail(rs.getString("email"));
		userDetails.setCity(rs.getString("city"));
		userDetails.setCompanyName(rs.getString("company_name"));
		userDetails.setAddress(rs.getString("address"));
		userDetails.setRolename(rs.getString("rolename"));
		return userDetails;
	}

}
