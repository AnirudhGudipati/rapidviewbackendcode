package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.WeatherPLSRWindModel;

public class WeatherPLSRWindRowMapper implements RowMapper<WeatherPLSRWindModel> {

	@Override
	public WeatherPLSRWindModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		WeatherPLSRWindModel weatherplsrwindmodel = new WeatherPLSRWindModel();
		weatherplsrwindmodel.setEvent(rs.getString("event"));
		weatherplsrwindmodel.setEvent_date(rs.getString("EVENT_DATE"));
		weatherplsrwindmodel.setEvent_time(rs.getString("EVENT_TIME"));
		weatherplsrwindmodel.setMagnitude(rs.getString("MAGNITUDE"));
		weatherplsrwindmodel.setLat(rs.getString("lat"));
		weatherplsrwindmodel.setLon(rs.getString("lon"));
		weatherplsrwindmodel.setDistance(rs.getString("distance"));
		return weatherplsrwindmodel;
	}

}
