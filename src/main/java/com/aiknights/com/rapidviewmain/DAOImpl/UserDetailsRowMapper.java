package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.UserDetails;

public class UserDetailsRowMapper implements RowMapper<UserDetails> {

	@Override
	public UserDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserDetails userDetails = new UserDetails();
		userDetails.setUserId(rs.getInt("userid"));
		userDetails.setUserName(rs.getString("username"));
		userDetails.setFirstName(rs.getString("firstname"));
		userDetails.setLastName(rs.getString("lastname"));
		userDetails.setEmail(rs.getString("email"));
		userDetails.setCity(rs.getString("city"));
		userDetails.setIsEmailVerified(rs.getString("isEmailVerified"));
		userDetails.setIsActive(rs.getString("isActive"));
		userDetails.setWeatheSubscription(rs.getString("weatherSubscription"));
		userDetails.setIsProfileCreated(rs.getString("isProfileCreated"));
		userDetails.setForceChangePassword(rs.getString("forceChangePassword"));
		userDetails.setcompanyName(rs.getString("company_name"));
		return userDetails;
	}

}
