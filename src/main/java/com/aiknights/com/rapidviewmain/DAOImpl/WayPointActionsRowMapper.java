package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.WayPointActionDBModel;

public class WayPointActionsRowMapper implements RowMapper<WayPointActionDBModel>{

	@Override
	public WayPointActionDBModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		WayPointActionDBModel wpam = new WayPointActionDBModel();
		wpam.setAction_Number(rs.getString("Action_Number"));
		wpam.setAction_Option(rs.getString("Action_Option"));
		wpam.setAction_Value(rs.getString("Action_Value"));
		wpam.setFlight_Action_ID(rs.getString("Flight_Action_ID"));
		wpam.setWay_Point_ID(rs.getString("Way_Point_ID"));
		return wpam;
	}

}
