package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.TokenDBModel;

public class TokenDBRowMapper implements RowMapper<TokenDBModel> {

	@Override
	public TokenDBModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		TokenDBModel data = new TokenDBModel();
		data.setCreatedDateTime(rs.getString("Created_DateTime"));
		data.setIsUsed(rs.getString("IsUsed"));
		data.setTokenID(rs.getString("Token"));
		data.setUserID(rs.getString("user_id"));
		return data;
	}

}
