package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.NotificationDBRepository;
import com.aiknights.com.rapidviewmain.models.WebSocketMessageModel;

@Repository
public class NotificationDBRepositoryImpl implements NotificationDBRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public boolean putMessagetoDB(String Notification_Object_ID,String Notification_Topic,String Notification_Type_ID,String Notification_Claim_ID,String Created_on,String Notification_Subject,String Message_content,String Read_Status,String IsDeleted) {
		try {
		final String sql = "insert into Notification_object (Notification_Object_ID, Notification_Topic, Notification_Type_ID, Created_on, Notification_Subject, Notification_Claim_ID, Message_content, Read_Status, IsDeleted) values(?,?,?,?,?,?,?,?,?);";
		int output = jdbcTemplate.update(sql,Notification_Object_ID,Notification_Topic,Notification_Type_ID,Created_on,Notification_Subject,Notification_Claim_ID,Message_content,Read_Status,IsDeleted);
		if(output == 1) {
		return true;
		}else
		{
			return false;
		}
		}catch(Exception e) {
			return false;
		}
	}

	@Override
	public boolean putSendertoDB(String notification_Sender_UID, String notification_Object_ID, String senderUserID,String Sender_read_Status, String Sender_isDeleted) {
		try {
			final String sql = "insert into Notification_Sender (Sender_UID, Notification_Object_ID, Sender_User_ID, Sender_Read_Status, Sender_IsDeleted) values(?,?,?,?,?);";
			int output = jdbcTemplate.update(sql,notification_Sender_UID, notification_Object_ID, senderUserID,Sender_read_Status, Sender_isDeleted);
			if(output == 1) {
			return true;
			}else
			{
				return false;
			}
			}catch(Exception e) {
				return false;
			}
	}

	@Override
	public boolean putReceivertoDB(String notification_Receiver_UID, String notification_Object_ID,	String receiverUserID, String Receiver_read_Status, String Receiver_isDeleted) {
		try {
			final String sql = "insert into Notification_Receiver (Receiver_UID, Notification_Object_ID, Receiver_User_ID, Receiver_Read_Status, Receiver_IsDeleted) values(?,?,?,?,?);";
			int output = jdbcTemplate.update(sql,notification_Receiver_UID, notification_Object_ID,	receiverUserID, Receiver_read_Status, Receiver_isDeleted);
			if(output == 1) {
			return true;
			}else
			{
				return false;
			}
			}catch(Exception e) {
				return false;
			}
	}
	
	@Override
	public List<WebSocketMessageModel> getMessagesOfUser(String userID) {
		try {
			final String sql = "select n1.Notification_Object_ID, n1.Notification_Topic, n1.Notification_Type_ID, n1.Created_on, n1.Notification_Subject, n1.Notification_Claim_ID, n1.Message_content, n1.Read_Status, n1.IsDeleted,n2.Receiver_UID, n2.Notification_Object_ID, n2.Receiver_User_ID, n2.Receiver_Read_Status, n2.Receiver_IsDeleted,n3.Sender_UID, n3.Notification_Object_ID, n3.Sender_User_ID, n3.Sender_Read_Status, n3.Sender_IsDeleted from Notification_object n1,Notification_Receiver n2,Notification_Sender n3 where n1.Notification_Object_ID=n2.Notification_Object_ID and n1.Notification_Object_ID=n3.Notification_Object_ID and n2.Receiver_IsDeleted=\"false\" and n2.Receiver_User_ID =\""+userID+"\";";
			final List<WebSocketMessageModel> messages = jdbcTemplate.query(sql,new NotificationRowMapper());
			return messages;
		}catch(Exception e) {
			return null;
		}
	}

	@Override
	public boolean markNotificationAsRead(String userID, String notification_Object_ID) {
		try {
			final String sql = "update Notification_Receiver set Receiver_Read_Status = \"true\" where Receiver_User_ID = "+userID+" and Notification_Object_ID = "+notification_Object_ID+";";
			int output = jdbcTemplate.update(sql);
			if(output == 1) {
			return true;
			}else
			{
				return false;
			}
			}catch(Exception e) {
				return false;
			}
	}

	@Override
	public boolean deletedNotificationAsReceiver(String userID, String notification_Object_ID) {
		try {
			final String sql = "update Notification_Receiver set Receiver_IsDeleted = \"true\" where Receiver_User_ID = "+userID+" and Notification_Object_ID = "+notification_Object_ID+";";
			int output = jdbcTemplate.update(sql);
			if(output == 1) {
			return true;
			}else
			{
				return false;
			}
			}catch(Exception e) {
				return false;
			}
	}
	

}
