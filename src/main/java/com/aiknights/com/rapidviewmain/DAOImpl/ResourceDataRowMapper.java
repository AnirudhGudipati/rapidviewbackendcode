package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.ProductCertainResourceDBTableModel;

public class ResourceDataRowMapper implements RowMapper<ProductCertainResourceDBTableModel>{

	@Override
	public ProductCertainResourceDBTableModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		ProductCertainResourceDBTableModel data = new ProductCertainResourceDBTableModel();
		data.setIsActive(rs.getString("IsActive"));
		data.setResourceName(rs.getString("ResourceName"));
		data.setRoleName(rs.getString("RoleName"));
		return data;
	}

}
