package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.GenericSubscriptionDetailsModel;

public class GenericSubscriptionPropertiesRowMapper implements RowMapper<GenericSubscriptionDetailsModel> {

	@Override
	public GenericSubscriptionDetailsModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		GenericSubscriptionDetailsModel data = new GenericSubscriptionDetailsModel();
		data.setAmountToBe_Paid(rs.getString("AmountToBe_Paid"));
		data.setNumber_of_Days(rs.getString("Number_of_Days"));
		data.setSubscription_Type(rs.getString("Subscription_Type"));
		data.setSubscription_Type_For_UI(rs.getString("Subscription_Type_For_UI"));
		data.setWeather_Download_MaxCount(rs.getString("Weather_Download_MaxCount"));
		return data;
	}

}
