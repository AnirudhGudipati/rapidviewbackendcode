package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.UserDetails;

public class UserProfileDetailsRowMapper implements RowMapper<UserDetails> {

	@Override
	public UserDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserDetails userDetails = new UserDetails();
		userDetails.setAddress(rs.getString("address"));
		userDetails.setCity(rs.getString("city"));
		userDetails.setcompanyName(rs.getString("company_name"));
		userDetails.setFirstName(rs.getString("firstname"));
		userDetails.setLastName(rs.getString("lastname"));
		userDetails.setEmail(rs.getString("email"));
		userDetails.setMembership_Level(rs.getString("MembershipLevel"));
		userDetails.setLicense_Type(rs.getString("LicenseType"));
		userDetails.setProduct_ID(rs.getString("ProductID"));
		userDetails.setContactNumber(rs.getString("contactNumber"));

		return userDetails;
	}

}
