package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.WebSocketMessageModel;

public class NotificationRowMapper implements RowMapper<WebSocketMessageModel>{

	@Override
	public WebSocketMessageModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		WebSocketMessageModel message =new WebSocketMessageModel();
		message.setNotification_Object_ID(rs.getString("Notification_Object_ID"));
		message.setNotification_Topic(rs.getString("Notification_Topic"));
		message.setNotification_Type_ID(rs.getString("Notification_Type_ID"));
		message.setNotification_Claim_ID(rs.getString("Notification_Claim_ID"));
		message.setCreated_on(rs.getString("Created_on"));
		message.setNotification_Subject(rs.getString("Notification_Subject"));
		message.setMessage_content(rs.getString("Message_content"));
		message.setRead_Status(rs.getString("Read_Status"));
		message.setIsDeleted(rs.getString("IsDeleted"));
		message.setSenderUserID(rs.getString("Sender_User_ID"));
		message.setSender_Read_Status(rs.getString("Sender_Read_Status"));
		message.setSender_IsDeleted(rs.getString("Sender_IsDeleted"));
		message.setReceiverUserID(rs.getString("Receiver_User_ID"));
		message.setReceiver_Read_Status(rs.getString("Receiver_Read_Status"));
		message.setReceiver_IsDeleted(rs.getString("Receiver_IsDeleted"));
		return message;
		
	}

}
