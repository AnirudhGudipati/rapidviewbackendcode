package com.aiknights.com.rapidviewmain.DAOImpl;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.StormEventsAlertModel;

public class StormEventsAlertsRowMapper implements RowMapper<StormEventsAlertModel> {
	@Override
	public StormEventsAlertModel  mapRow(ResultSet rs, int rowNum) throws SQLException {
		StormEventsAlertModel stormEventsAlertModel=new StormEventsAlertModel();
		stormEventsAlertModel.setCounty(rs.getString("COUNTY"));
		stormEventsAlertModel.setState(rs.getString("STATE"));
		stormEventsAlertModel.setSeverity((rs.getString("SEVERITY")));
		return stormEventsAlertModel;
	}
}
