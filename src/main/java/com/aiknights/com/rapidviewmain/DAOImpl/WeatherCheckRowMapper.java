package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.WeatherCheck;

public class WeatherCheckRowMapper implements RowMapper<WeatherCheck>{

	@Override
	public WeatherCheck mapRow(ResultSet rs, int rowNum) throws SQLException {
		WeatherCheck weatherCheck = new WeatherCheck();
		weatherCheck.setWeatherId(rs.getInt("weather_id"));
		weatherCheck.setClaimId(rs.getString("claim_id"));
		weatherCheck.setHeatMap(rs.getString("heatmap"));
		weatherCheck.setMapDate(rs.getDate("mapdate").toString());
		weatherCheck.setHomeOrientation(rs.getString("home_orientation"));
		weatherCheck.setWeatherAnomolies(rs.getString("weather_anomolies"));
		weatherCheck.setHailSize(rs.getFloat("hailsize"));
		weatherCheck.setWindGusts(rs.getInt("windgusts"));
		weatherCheck.setWindDirection(rs.getString("wind_direction"));
		weatherCheck.setRoofSlope(rs.getString("roof_slope"));
		weatherCheck.setChatter(rs.getString("chatter"));
		weatherCheck.setImpactFront(rs.getFloat("impact_front"));
		weatherCheck.setImpactLeft(rs.getFloat("impact_left"));
		weatherCheck.setImpactBack(rs.getFloat("impact_back"));
		weatherCheck.setImpactRight(rs.getFloat("impact_right"));
		weatherCheck.setAvgHighTemp(rs.getFloat("avg_high_temp"));
		weatherCheck.setAvgLowTemp(rs.getFloat("avg_low_temp"));
		weatherCheck.setAvgTemp(rs.getFloat("avg_temp"));
		weatherCheck.setAvgRainfall(rs.getFloat("avg_rainfall"));
		weatherCheck.setAvgDaysSine(rs.getInt("avg_days_shine"));
		weatherCheck.setAvgSnowfall(rs.getInt("anual_snowfall"));
		return weatherCheck;
	}

}
