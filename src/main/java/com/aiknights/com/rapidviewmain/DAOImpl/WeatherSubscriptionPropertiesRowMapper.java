package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.WeatherSubscriptionPropertiesModel;

public class WeatherSubscriptionPropertiesRowMapper implements RowMapper<WeatherSubscriptionPropertiesModel> {

	@Override
	public WeatherSubscriptionPropertiesModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		WeatherSubscriptionPropertiesModel weatherSubscriptionProperties = new WeatherSubscriptionPropertiesModel();
		weatherSubscriptionProperties.setAmountToBe_Paid(rs.getString("AmountToBe_Paid"));
		weatherSubscriptionProperties.setSubscription_Type(rs.getString("Subscription_Type"));
		weatherSubscriptionProperties.setWeather_Download_MaxCount(rs.getString("Weather_Download_MaxCount"));
		weatherSubscriptionProperties.setSubscription_Type_For_UI(rs.getString("Subscription_Type_For_UI"));
		return weatherSubscriptionProperties;

	}

}
