package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.WeatherStageDatabaseModel;

public class StageRowMapper implements RowMapper<WeatherStageDatabaseModel>{

	@Override
	public WeatherStageDatabaseModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		WeatherStageDatabaseModel weatherStageDatabaseModel = new WeatherStageDatabaseModel();
		weatherStageDatabaseModel.setClaimid(rs.getString("ClaimID"));
		weatherStageDatabaseModel.setEventdate(rs.getString("Event_Date"));
		weatherStageDatabaseModel.setHailsize(rs.getString("Hailsize"));
		weatherStageDatabaseModel.setLatitude(rs.getString("Latitude"));
		weatherStageDatabaseModel.setLongitude(rs.getString("longitude"));
		weatherStageDatabaseModel.setDistance(rs.getString("Distance"));
		return weatherStageDatabaseModel;
	}

}
