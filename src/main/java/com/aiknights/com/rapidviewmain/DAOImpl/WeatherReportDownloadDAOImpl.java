package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.WeatherReportDownloadRepository;

@Repository
public class WeatherReportDownloadDAOImpl implements WeatherReportDownloadRepository {
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public String getweatherReportDownloadCountByUserName(String userName) {

		String downloadCount = null;

		try {
			final String sql = "select Weather_Download_MaxCount from Weather_Download_count where username =?";

			downloadCount = (String) jdbcTemplate.queryForObject(sql, new Object[] { userName }, String.class);

		} catch (Exception e) {

		}

		return downloadCount;
	}

	@Override
	public boolean updateLatestDownloadCount(String userName, int latestDwonloadCount) {
		int output = 0;
		try {
			final String sql = "update Weather_Download_count set Weather_Download_MaxCount = " + latestDwonloadCount
					+ " where username =\"" + userName + "\";";
			output = jdbcTemplate.update(sql);

		} catch (Exception e) {

		}
		if (output == 1) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Map<String, Object> getAvailableCountByUserName(String userName) {
		String sql = "select Weather_Download_MaxCount,AmountToBe_Paid from Weather_Download_count WHERE username=?";
		Map<String, Object> availableCountByUserName = (Map<String, Object>) jdbcTemplate.queryForMap(sql,
				new Object[] { userName });
		return availableCountByUserName;
	}

}
