package com.aiknights.com.rapidviewmain.DAOImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.UserRepository;
import com.aiknights.com.rapidviewmain.models.ProductCertainResourceDBTableModel;
import com.aiknights.com.rapidviewmain.models.ProductsTableDBModel;
import com.aiknights.com.rapidviewmain.models.User;

@Repository
public class UserDAOImpl implements UserRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public User findUserByUserName(String userName) {

		final String GET_USER_BY_USERNAME = "select * from users where username=?";
		try {
			User userLoginDetails = jdbcTemplate.queryForObject(GET_USER_BY_USERNAME, new UserRowMapper(), userName);
			final List<String> roles = getRolesByUsername(userName);
			userLoginDetails.getRoles().addAll(roles);
			return userLoginDetails;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	private List<String> getRolesByUsername(final String userName) {
		final String GET_ROLES_BY_USER_NAME = "select rolename from role where username = ?";
		List<String> rolesList = new ArrayList<>();
		final List<String> roles = jdbcTemplate.queryForList(GET_ROLES_BY_USER_NAME, new Object[] { userName },
				String.class);
		rolesList.addAll(roles);
		return rolesList;
	}

	@Override
	public LocalDate findLicenseEndDateByUserName(String userName) {
		final String sql = "select LicenseEndDate from Licenesing where username = ?";
		LocalDate LicenseEndDate = null;
		try {
			LicenseEndDate = (LocalDate) jdbcTemplate.queryForObject(sql, new Object[] { userName }, LocalDate.class);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return LicenseEndDate;
	}

	@Override
	public List<String> findassignedLicenesByUserName(String userName) {
		final String sql = "select * from Licenesing where username = ?";
		List<String> assignedLicenesList = new ArrayList<>();
		List<String> assignedLicenes = null;
		try {
			assignedLicenesList = jdbcTemplate.queryForList(sql, new Object[] { userName }, String.class);
		} catch (Exception e) {
			// TODO: handle exception
		}
		assignedLicenesList.addAll(assignedLicenes);
		return assignedLicenesList;
	}

	@Override
	public String findSubscriptionTypeByUserName(String userName) {
		final String sql = "select LicenseType from Licenesing where username = ?";
		String LicenseType = null;
		try {
			LicenseType = jdbcTemplate.queryForObject(sql, new Object[] { userName }, String.class);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return LicenseType;
	}

	@Override
	public String findMembershipLevelByUserName(String userName) {
		final String sql = "select MembershipLevel from Licenesing where username = ?";
		String MembershipLevel = null;
		try {
			MembershipLevel = jdbcTemplate.queryForObject(sql, new Object[] { userName }, String.class);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return MembershipLevel;
	}

	@Override
	public String findProductIdByUserName(String userName) {
		final String sql = "select ProductID from Licenesing where username = ?";
		String ProductID = null;
		try {
			ProductID = jdbcTemplate.queryForObject(sql, new Object[] { userName }, String.class);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return ProductID;
	}

	@Override
	public String findProductTypeById(String productId) {
		String ProductName = null;
		try {
			final String sql = "select ProductName from Products where ProductID =?";

			ProductName = (String) jdbcTemplate.queryForObject(sql, new Object[] { productId }, String.class);

		} catch (Exception e) {
			// TODO: handle exception
		}

		return ProductName;
	}

	@Override
	public List<ProductsTableDBModel> getAllProductsData() {
		try {
			final String sql = "select * from Products;";
			List<ProductsTableDBModel> ProductDetails = jdbcTemplate.query(sql, new ProductDetailsRowMapper());
			return ProductDetails;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<String> getDistinctAvailableRolesFromGivenProductTable(String ProductTableName) {
		try {
			final String sql = "select DISTINCT (RoleName) from " + ProductTableName + ";";
			List<String> Roles = new ArrayList<>();
			List<String> Role = jdbcTemplate.queryForList(sql, String.class);
			Roles.addAll(Role);
			return Roles;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<String> getResourcesAvailableOfGivenRoleFromGivenProductTable(String ProductTableName,
			String RoleName) {
		try {
			final String sql = "select ResourceName from " + ProductTableName + " wprt where RoleName =\"" + RoleName
					+ "\";";
			List<String> Resources = new ArrayList<>();
			List<String> Resource = jdbcTemplate.queryForList(sql, String.class);
			Resources.addAll(Resource);
			return Resources;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<ProductCertainResourceDBTableModel> getResourcesDataAvailableOfGivenRoleFromGivenProductTable(
			String ProductTableName, String RoleName) {
		try {
			final String sql = "select * from " + ProductTableName + " wprt where RoleName =\"" + RoleName + "\";";

			List<ProductCertainResourceDBTableModel> Resources = jdbcTemplate.query(sql, new ResourceDataRowMapper());
			return Resources;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public boolean updateRoleResourceActiveStatusForGivenRoleResourceProductTableName(String ProductTableName,
			String RoleName, String ResourceName, String ActiveStatus) {
		try {
			final String sql = "UPDATE "+ProductTableName+" SET IsActive =\""+ActiveStatus+"\" where RoleName =\""+RoleName+"\" and ResourceName =\""+ResourceName+"\";";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean updateSubscriptionPropertiesForGivenTypeSubscriptionTableName(String SubscriptionTableName,
			String subscriptionType, String subscriptionPrice, String downloadCount) {
		try {
			final String sql = "UPDATE "+SubscriptionTableName+" SET Weather_Download_MaxCount = \""+downloadCount+"\" , AmountToBe_Paid = \""+subscriptionPrice+"\" where Subscription_Type =\""+subscriptionType+"\";";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	public boolean updateCommonSubscriptionProperties(String subscriptionType, String subscriptionPeriod) {
		try {
			final String sql = "UPDATE Subscription_Properties set Number_of_Days = \""+subscriptionPeriod+"\" WHERE Subscription_Type =\""+subscriptionType+"\";";
			int output = jdbcTemplate.update(sql);
			if (output == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
