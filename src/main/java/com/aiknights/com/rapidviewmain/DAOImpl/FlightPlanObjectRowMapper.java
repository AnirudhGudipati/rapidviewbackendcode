package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.FlightPlanObjectDBModel;

public class FlightPlanObjectRowMapper implements RowMapper<FlightPlanObjectDBModel>{

	@Override
	public FlightPlanObjectDBModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		FlightPlanObjectDBModel fpom = new FlightPlanObjectDBModel();
		fpom.setClaim_Address(rs.getString("Claim_Address"));
		fpom.setClaim_ID(rs.getString("Claim_ID"));
		fpom.setFlight_Plan_ID(rs.getString("Flight_Plan_ID"));
		return fpom;
	}

}
