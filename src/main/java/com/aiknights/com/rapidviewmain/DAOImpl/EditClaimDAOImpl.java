package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.EditClaimRepository;
import com.aiknights.com.rapidviewmain.dto.EndorsementResultDTO;
import com.aiknights.com.rapidviewmain.dto.PolicyResultDTO;
import com.aiknights.com.rapidviewmain.models.AdjusterClaimCsvModel;
import com.aiknights.com.rapidviewmain.models.ClaimInjectorCsv;
import com.aiknights.com.rapidviewmain.models.ClaimNotificationCsv;
import com.aiknights.com.rapidviewmain.models.EndorsementCsvModel;
import com.aiknights.com.rapidviewmain.models.PolicyCheckCsvModel;
import com.aiknights.com.rapidviewmain.models.PropertyCsvModel;

@Repository
public class EditClaimDAOImpl implements EditClaimRepository {
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public boolean insertIntoClaimNotificationCsv(ClaimNotificationCsv claim_notification,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) throws ParseException {

		int status = 0;

		final String INSERT_SQL = "INSERT INTO  claim_notification"
				+ "(claim_id,firstname,lastname,email,phone_number,street_address,city,state,zipcode,policy_number,incident_type,incident_date,incident_time,claim_date,claim_amount,status,policy_id,claim_description)"
				+ " values(:claim_id,:firstname,:lastname,:email,:phone_number,:street_address,:city,:state,:zipcode,:policy_number,:incident_type,:incident_date,:incident_time,:claim_date,:claim_amount,:status,:policy_id,:claim_description)";

		/* "INSERT INTO USERS(name, address, email) values(:name,:address,:email)"; */

		try {
			KeyHolder holder = new GeneratedKeyHolder();
			SqlParameterSource parameters = new MapSqlParameterSource()
					.addValue("claim_id", claim_notification.getClaimId())
					.addValue("firstname", claim_notification.getClaimantFirstName())
					.addValue("lastname", claim_notification.getClaimantLastName())
					.addValue("email", claim_notification.getEmail())
					.addValue("phone_number", claim_notification.getPhoneNumber())
					.addValue("street_address", claim_notification.getAddress())
					.addValue("city", claim_notification.getCity()).addValue("state", claim_notification.getState())
					.addValue("zipcode", claim_notification.getZipCode())
					.addValue("policy_number", claim_notification.getPolicyNumber())
					.addValue("incident_type", claim_notification.getIncidentType())
					.addValue("incident_date", claim_notification.getIncidentDate())
					.addValue("incident_time", claim_notification.getIncidentTime())
					.addValue("claim_date", claim_notification.getIncidentDate())
					.addValue("claim_amount", claim_notification.getClaimAmount())
					.addValue("status", claim_notification.getClaimstatus())
					.addValue("policy_id", claim_notification.getPolicy_id())
					.addValue("claim_description", claim_notification.getClaimDescription());

			status = namedParameterJdbcTemplate.update(INSERT_SQL, parameters, holder);
			System.out.println("inserion ststus  " + status);

		} catch (Exception e) {
			// TODO: handle exception

		}

		if (status == 1) {
			return true;
		}
		return false;
	}

	@Override
	public PolicyResultDTO insertIntoPolicyCheckCsv(PolicyCheckCsvModel policyCheckCsvModel,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		PolicyResultDTO policyResultDTO = new PolicyResultDTO();
		int policy_id_pk = 0;
		final String INSERT_SQL = "INSERT INTO  policy_check"
				+ "(policy_number,insurance_company,start_date,end_date,policy_type,policy_coverage,endorsement_id,deductible,mortgage_company,Policy_Limit,CoverageAmt,policyStatus,PolicyLastUpdateDate,priorClaims,policy_id)"
				+ " values(:policy_number,:insurance_company,:start_date,:end_date,:policy_type,:policy_coverage,:endorsement_id,:deductible,:mortgage_company,:Policy_Limit,:CoverageAmt,:policyStatus,:PolicyLastUpdateDate,:priorClaims,:policy_id)";
		int status_PolicyCheckCsv = 0;
		try {

			KeyHolder holder = new GeneratedKeyHolder();
			SqlParameterSource parameters = new MapSqlParameterSource()
					.addValue("policy_number", policyCheckCsvModel.getPolicyNumber())
					.addValue("insurance_company", policyCheckCsvModel.getInsuranceCompany())
					.addValue("start_date", policyCheckCsvModel.getStartDate())
					.addValue("end_date", policyCheckCsvModel.getEndDate())
					.addValue("policy_type", policyCheckCsvModel.getPolicyType())
					.addValue("policy_coverage", policyCheckCsvModel.getPolicyCoverage())
					.addValue("endorsement_id", policyCheckCsvModel.getEndorsementId())
					.addValue("deductible", policyCheckCsvModel.getDeductible())
					.addValue("mortgage_company", policyCheckCsvModel.getMortgageCompany())
					.addValue("Policy_Limit", policyCheckCsvModel.getPolicyLimit())
					.addValue("CoverageAmt", policyCheckCsvModel.getCoverageAmt())
					.addValue("deductibleAmount", policyCheckCsvModel.getDeductible())
					.addValue("policyStatus", policyCheckCsvModel.getPolicyStatus())
					.addValue("PolicyLastUpdateDate", policyCheckCsvModel.getPolicyLastUpdateD())
					.addValue("priorClaims", policyCheckCsvModel.getPriorClaims())
					.addValue("policy_id", policyCheckCsvModel.getPolicy_id());

			status_PolicyCheckCsv = namedParameterJdbcTemplate.update(INSERT_SQL, parameters, holder,
					new String[] { "policy_id" });
			System.out.println("status_PolicyCheckCsv -inserion status  " + status_PolicyCheckCsv);
			policy_id_pk = holder.getKey().intValue();
			System.out.println("policy_id" + policy_id_pk);

			/*
			 * status_PolicyCheckCsv = namedParameterJdbcTemplate.update(INSERT_SQL,
			 * parameters, holder); System.out.println("inserion ststus  " +
			 * status_PolicyCheckCsv);
			 */

		} catch (Exception e) {
			// TODO: handle exception

		}
		if (status_PolicyCheckCsv == 1) {
			policyResultDTO.setPolicy_id_pk(policy_id_pk);
			policyResultDTO.setInsertion_status(true);
			return policyResultDTO;
		} else {
			policyResultDTO.setPolicy_id_pk(policy_id_pk);
			policyResultDTO.setInsertion_status(false);
			return policyResultDTO;
		}
	}

	@Override
	public boolean insertIntoPropertyTableCsv(PropertyCsvModel property_data,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) throws ParseException {

		int status_property = 0;
		final String INSERT_SQL = "INSERT INTO  property"
				+ "(property_id,property_type,property_purchase_date,property_street_address,property_City,property_State,property_ZipCode,policy_id)"
				+ " values(:property_id,:property_type,:property_purchase_date,:property_street_address,:property_City,:property_State,:property_ZipCode,:policy_id)";

		try {
			KeyHolder holder = new GeneratedKeyHolder();
			SqlParameterSource parameters = new MapSqlParameterSource()
					.addValue("property_id", property_data.getProperty_id())
					.addValue("property_type", property_data.getPropertyType())
					.addValue("property_purchase_date", property_data.getPropertyPurchaseDate())
					.addValue("property_street_address", property_data.getStreetAddress())
					.addValue("property_City", "property_City").addValue("property_State", "property_State")
					.addValue("property_ZipCode", "30606").addValue("policy_id", property_data.getPolicy_id());

			status_property = namedParameterJdbcTemplate.update(INSERT_SQL, parameters, holder);
			System.out.println("Status" + status_property);
		} catch (Exception e) {
			// TODO: handle exception
		}

		// TODO Auto-generated method stub

		if (status_property == 1) {
			return true;
		}
		return false;
	}

	@Override
	public EndorsementResultDTO insertIntoEndorsementTableCsv(EndorsementCsvModel Endorsement_CsvModel,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		EndorsementResultDTO endorsementResult = new EndorsementResultDTO();
		int endorsement_id_pk = 0;
		int status_ad_tb = 0;

		final String INSERT_SQL = "INSERT INTO  endorsement_list"
				+ "(endorsement_id,hail,wind,water,hurricane,Endorsment_Type,Endorsment_Type_Desc)"
				+ " values(:endorsement_id,:hail,:wind,:water,:hurricane,:Endorsment_Type,:Endorsment_Type_Desc)";

		try {
			KeyHolder holder = new GeneratedKeyHolder();
			SqlParameterSource parameters = new MapSqlParameterSource()
					.addValue("endorsement_id", Endorsement_CsvModel.getEndorsement_id())
					.addValue("hail", Endorsement_CsvModel.getHail()).addValue("wind", Endorsement_CsvModel.getWind())
					.addValue("water", Endorsement_CsvModel.getWater())
					.addValue("hurricane", Endorsement_CsvModel.getHurricane())
					.addValue("Endorsment_Type", Endorsement_CsvModel.getEndorsement_type())
					.addValue("Endorsment_Type_Desc", Endorsement_CsvModel.getEndorsement_type_desc()

					);
			status_ad_tb = namedParameterJdbcTemplate.update(INSERT_SQL, parameters, holder,
					new String[] { "endorsement_id" });
			System.out.println("inserion status  " + status_ad_tb);
			endorsement_id_pk = holder.getKey().intValue();
			System.out.println("endorsement_id" + endorsement_id_pk);

		} catch (Exception e) {
			// TODO: handle exception

		}
		if (status_ad_tb == 1) {

			endorsementResult.setInsertion_status(true);
			endorsementResult.setEndorsement_id_pk(endorsement_id_pk);
			return endorsementResult;
		}
		endorsementResult.setInsertion_status(false);
		endorsementResult.setEndorsement_id_pk(endorsement_id_pk);
		return endorsementResult;

	}

	@Override
	public boolean insertIntoAdjusterClaimTableCsv(AdjusterClaimCsvModel adjuster_Claim_Model,
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {

		final String INSERT_SQL = "INSERT INTO  adjuster_claim"
				+ "(adjuster_id,claim_id,adjuster_company,adjuster_address,expertise,assigned_date,adjuster_FirstName,adjuster_LastName,adjuster_PhoneNumber,adjuster_Email)"
				+ " values(:adjuster_id,:claim_id,:adjuster_company,:adjuster_address,:expertise,:assigned_date,:adjuster_FirstName,:adjuster_LastName,:adjuster_PhoneNumber,:adjuster_Email)";
		int status_Adjuster_table = 0;
		try {
			KeyHolder holder = new GeneratedKeyHolder();
			SqlParameterSource parameters = new MapSqlParameterSource()
					.addValue("adjuster_id", adjuster_Claim_Model.getAdjusterId())
					.addValue("claim_id", adjuster_Claim_Model.getClaimId())
					.addValue("adjuster_company", adjuster_Claim_Model.getAdjusterCompany())
					.addValue("adjuster_address", adjuster_Claim_Model.getAdjusterAddress())
					.addValue("expertise", adjuster_Claim_Model.getExpertise())
					.addValue("assigned_date", adjuster_Claim_Model.getAssignedDate())
					.addValue("adjuster_FirstName", adjuster_Claim_Model.getAdjuster_FirstName())
					.addValue("adjuster_LastName", adjuster_Claim_Model.getAdjuster_LastName())
					.addValue("adjuster_PhoneNumber", adjuster_Claim_Model.getAdjuster_PhoneNumber())
					.addValue("adjuster_Email", adjuster_Claim_Model.getAdjuster_Email());

			status_Adjuster_table = namedParameterJdbcTemplate.update(INSERT_SQL, parameters, holder);
			System.out.println("inserion ststus  " + status_Adjuster_table);

		} catch (Exception e) {
			// TODO: handle exception

		}
		if (status_Adjuster_table == 1) {
			return true;
		} else {
			return false;
		}
	}

	public Date ultil(String incident_date) throws ParseException {

		java.sql.Date dateDB = null;
		String date = incident_date;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); // your template here
		java.util.Date dateStr;

		dateStr = formatter.parse(date);
		dateDB = new java.sql.Date(dateStr.getTime());

		return dateDB;

	}

	@Override
	public boolean deleteUltil(String tableName, String key, String value) {

		final String INSERT_SQL = "Delete from " + tableName + " where " + key + " =\"" + value + "\"";

		int out_delete = jdbcTemplate.update(INSERT_SQL);
		System.out.println("Delete status  " + out_delete);
		return true;

	}

	@Override
	public boolean chcekIsClaimIdDuplicate(ClaimInjectorCsv claimInjectorCsv) {

		final String CLAIM_ID_CHCEK = "select count(*) from  claim_notification where claim_id=?";

		boolean result = false;
		int count = jdbcTemplate.queryForObject(CLAIM_ID_CHCEK, new Object[] { claimInjectorCsv.getClaimId() },
				Integer.class);
		if (count > 0) {
			result = true;
		}
		return result;
	}

	@Override
	public boolean getByPolicyNumber(String policyNumber) {

		final String CLAIM_ID_CHCEK = "select count(*) from  policy_check where policy_number=?";

		boolean result = false;
		int count = jdbcTemplate.queryForObject(CLAIM_ID_CHCEK, new Object[] { policyNumber }, Integer.class);
		if (count > 0) {
			result = true;
		}
		return result;
	}

}
