package com.aiknights.com.rapidviewmain.DAOImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.DeleteClaimRepository;

@Repository
public class DeleteClaimDAOImpl implements DeleteClaimRepository {
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public boolean deleteAdjusterClaimDataByClaimID(String claim_id) {

		final String INSERT_SQL = "Delete from " + "adjuster_claim" + " where " + "claim_id" + " =\""
				+ claim_id + "\"";

		int out_delete = jdbcTemplate.update(INSERT_SQL);
		System.out.println("Delete status  " + out_delete);

		if (out_delete == 1) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean deleteClaimByClaimID(String claim_id) {
		final String INSERT_SQL = "Delete from " + "claim_notification" + " where " + "claim_id" + " =\""
				+ claim_id + "\"";

		int out_delete = jdbcTemplate.update(INSERT_SQL);
		System.out.println("Delete status  " + out_delete);
		if (out_delete == 1) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean deletePolicyByClaimID(String policy_number) {

		final String INSERT_SQL = "Delete from " + "policy_check" + " where " + "policy_number" + " =\""
				+ policy_number + "\"";

		int out_delete = jdbcTemplate.update(INSERT_SQL);
		System.out.println("Delete status  " + out_delete);

		if (out_delete == 1) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String getPOlicyNumberByClaimId(String claim_id) {

		final String GET_POICY_ID_BY_CLAIM_ID = "select policy_number from claim_notification where claim_id =?";

		String policy_number = (String) jdbcTemplate.queryForObject(GET_POICY_ID_BY_CLAIM_ID, new Object[] { claim_id },
				String.class);
		return policy_number;
	}

	@Override
	public boolean checkClaimIDInIncidentEvidence(String claim_id) {

		final String CLAIM_ID_CHCEK = "select count(*) from incident_evidence where claim_id=?";

		boolean result = false;
		int count = jdbcTemplate.queryForObject(CLAIM_ID_CHCEK, new Object[] { claim_id }, Integer.class);
		if (count > 0) {
			result = true;
		}
		return result;

	}

	@Override
	public boolean checkClaimIDInCreateReport(String claim_id) {
		final String CLAIM_ID_CHCEK = "select count(*) from create_report where claim_id=?";

		boolean result = false;
		int count = jdbcTemplate.queryForObject(CLAIM_ID_CHCEK, new Object[] { claim_id }, Integer.class);
		if (count > 0) {
			result = true;
		}
		return result;
	}

	@Override
	public boolean checkClaimIDInRisKAssesment(String claim_id) {
		final String CLAIM_ID_CHCEK = "select count(*) from risk_assesment where claim_id=?";

		boolean result = false;
		int count = jdbcTemplate.queryForObject(CLAIM_ID_CHCEK, new Object[] { claim_id }, Integer.class);
		if (count > 0) {
			result = true;
		}
		return result;
	}
	

	@Override
	public boolean checkClaimIDInWeatherCheck(String claim_id) {
		final String CLAIM_ID_CHCEK = "select count(*) from weather_check where claim_id=?";

		boolean result = false;
		int count = jdbcTemplate.queryForObject(CLAIM_ID_CHCEK, new Object[] { claim_id }, Integer.class);
		if (count > 0) {
			result = true;
		}
		return result;
	}

	@Override
	public boolean deleteIncidentEvdenceClaimDataByClaimID(String claim_id) {
		final String INSERT_SQL = "Delete from " + "incident_evidence" + " where " + "claim_id" + " =\""
				+ claim_id + "\"";

		int out_delete = jdbcTemplate.update(INSERT_SQL);
		System.out.println("Delete status  " + out_delete);
		if (out_delete == 1) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean deleteCreateReportClaimDataByClaimID(String claim_id) {
		final String INSERT_SQL = "Delete from " + "create_report" + " where " + "claim_id" + " =\""
				+ claim_id + "\"";

		int out_delete = jdbcTemplate.update(INSERT_SQL);
		System.out.println("Delete status  " + out_delete);
		if (out_delete == 1) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean deleteRiskAssesmentClaimDataByClaimID(String claim_id) {
		final String INSERT_SQL = "Delete from " + "risk_assesment" + " where " + "claim_id" + " =\""
				+ claim_id + "\"";

		int out_delete = jdbcTemplate.update(INSERT_SQL);
		System.out.println("Delete status  " + out_delete);
		if (out_delete == 1) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean deleteWeatherCheckClaimByDataByClaimID(String claim_id) {
		final String INSERT_SQL = "Delete from " + "weather_check" + " where " + "claim_id" + " =\""
				+ claim_id + "\"";

		int out_delete = jdbcTemplate.update(INSERT_SQL);
		System.out.println("Delete status  " + out_delete);
		if (out_delete == 1) {
			return true;
		} else {
			return false;
		}
	}


}
