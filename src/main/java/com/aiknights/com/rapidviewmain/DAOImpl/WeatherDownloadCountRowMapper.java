package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.WeatherDownloadCountModel;

public class WeatherDownloadCountRowMapper implements RowMapper<WeatherDownloadCountModel> {

	@Override
	public WeatherDownloadCountModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		WeatherDownloadCountModel weatherDownloadCountModel = new WeatherDownloadCountModel();

		weatherDownloadCountModel.setAmountToBe_Paid(rs.getString("amountToBe_Paid"));
		weatherDownloadCountModel.setWeather_Download_MaxCount(rs.getString("weather_Download_MaxCount"));
		return weatherDownloadCountModel;
	}

}
