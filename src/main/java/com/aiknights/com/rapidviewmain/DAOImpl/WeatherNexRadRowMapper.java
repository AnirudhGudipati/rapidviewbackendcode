package com.aiknights.com.rapidviewmain.DAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aiknights.com.rapidviewmain.models.WeatherNexRadModel;

public class WeatherNexRadRowMapper implements RowMapper<WeatherNexRadModel>{
	@Override
	public WeatherNexRadModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		WeatherNexRadModel weathernexradmodel = new WeatherNexRadModel();
		weathernexradmodel.setEvent_date(rs.getString("event_date"));
		weathernexradmodel.setMaxsize(rs.getString("maxsize"));
		weathernexradmodel.setProb(rs.getString("prob"));
		weathernexradmodel.setSevprob(rs.getString("sevprob"));
		weathernexradmodel.setLat(rs.getString("lat"));
		weathernexradmodel.setLon(rs.getString("lon"));
		weathernexradmodel.setDistance(rs.getString("distance"));
		return weathernexradmodel;
	}
	
}
