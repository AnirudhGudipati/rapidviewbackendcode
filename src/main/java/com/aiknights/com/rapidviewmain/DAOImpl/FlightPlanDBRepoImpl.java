package com.aiknights.com.rapidviewmain.DAOImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aiknights.com.rapidviewmain.DAO.FlightPlanDBRepo;
import com.aiknights.com.rapidviewmain.models.FlightPlanObjectDBModel;
import com.aiknights.com.rapidviewmain.models.FlightPlanPropertiesDBModel;
import com.aiknights.com.rapidviewmain.models.WayPointActionDBModel;
import com.aiknights.com.rapidviewmain.models.WayPointDataDBModel;

@Repository
public class FlightPlanDBRepoImpl implements FlightPlanDBRepo{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public boolean putFlighPlanDataToDB(String flight_Plan_ID, String claim_ID, String claim_Address) {
		try {
			final String sql = "INSERT INTO Flight_Plan_Object (Flight_Plan_ID,Claim_ID,Claim_Address) VALUES (?,?,?);";
			int output = jdbcTemplate.update(sql,flight_Plan_ID,claim_ID,claim_Address);
			if(output == 1) {
			return true;
			}else
			{
				return false;
			}
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
	}

	@Override
	public boolean putFlightPlanPropertiesToDB(String flight_Plan_ID,String user_ID, String flight_Plan_Name,String flight_Plan_Version, String flight_Plan_Format) {
		try {
			final String sql = "INSERT INTO Flight_Plan_Properties(Flight_Plan_ID,User_ID,Flight_Plan_Name,Flight_Plan_Version,Flight_Plan_Format) VALUES (?,?,?,?,?);";
			int output = jdbcTemplate.update(sql,flight_Plan_ID,user_ID, flight_Plan_Name,flight_Plan_Version, flight_Plan_Format);
			if(output == 1) {
			return true;
			}else
			{
				return false;
			}
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
	}

	@Override
	public boolean putWayPointDataToDB(String flight_Plan_ID, String way_Point_ID, String way_Point_Key,String latitude, String longitude,String altitude, String above_Ground, String speed, String curve_size, String heading, String pOI,String gimbal_Option, String gimbal_Value, String interval_Option, String interval_Value) {
		try {
			final String sql = "INSERT INTO Way_Point_Data(Flight_Plan_ID,Way_Point_ID,Way_Point_Key,Latitude,Longitude,Altitude,Above_Ground,Speed,Curve_size,Heading,POI,Gimbal_Option,Gimbal_Value,Interval_Option,Interval_Value) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
			int output = jdbcTemplate.update(sql,flight_Plan_ID, way_Point_ID, way_Point_Key, latitude, longitude,altitude, above_Ground, speed, curve_size, heading, pOI,gimbal_Option, gimbal_Value, interval_Option, interval_Value);
			if(output == 1) {
			return true;
			}else
			{
				return false;
			}
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
	}

	@Override
	public boolean putActionDatatoDB(String flight_Action_ID, String way_Point_ID, String action_Number,String selected_Action_Option, String selected_Action_Value) {
		try {
			final String sql = "INSERT INTO Flight_Action_Data(Flight_Action_ID,Way_Point_ID,Action_Number,Action_Option,Action_Value) VALUES (?,?,?,?,?);";
			int output = jdbcTemplate.update(sql,flight_Action_ID, way_Point_ID, action_Number, selected_Action_Option, selected_Action_Value);
			if(output == 1) {
			return true;
			}else
			{
				return false;
			}
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
	}

	@Override
	public List<FlightPlanObjectDBModel> getFlightPlanObjectByFlightPlanID(String flightPlanID) {
		final String flight_plan_object_query = "Select * from Flight_Plan_Object where Flight_Plan_ID=\""+flightPlanID+"\"";
		try {
			final List<FlightPlanObjectDBModel> flightplanobjectdata = jdbcTemplate.query(flight_plan_object_query, new FlightPlanObjectRowMapper());
			return flightplanobjectdata;
		}catch(EmptyResultDataAccessException e) {
			return null;	
		}		
	}

	@Override
	public List<FlightPlanPropertiesDBModel> getFlightPlanPropertiesByFlightPlanID(String flightPlanID) {
		final String flight_plan_properties_query = "Select * from Flight_Plan_Properties where Flight_Plan_ID=\""+flightPlanID+"\"";
		try {
			final List<FlightPlanPropertiesDBModel> flightplanpropertiesdata = jdbcTemplate.query(flight_plan_properties_query, new FlightPlanPropertiesRowMaapper());
			return flightplanpropertiesdata;
		}catch(EmptyResultDataAccessException e) {
			return null;	
		}	
	}

	@Override
	public List<WayPointDataDBModel> getWayPointsByFlightPlanID(String flightPlanID) {
		final String way_points_query = "Select * from Way_Point_Data where Flight_Plan_ID=\""+flightPlanID+"\"";
		try {
			final List<WayPointDataDBModel> waypointsdata = jdbcTemplate.query(way_points_query, new WayPointsRowMapper());
			return waypointsdata;
		}catch(EmptyResultDataAccessException e) {
			return null;	
		}	
	}

	@Override
	public List<WayPointActionDBModel> getWayPointActionsByWayPointID(String wayPointID) {
		final String way_points_actions_query = "Select * from Flight_Action_Data where Way_Point_ID=\""+wayPointID+"\"";
		try {
			final List<WayPointActionDBModel> waypointactiondata = jdbcTemplate.query(way_points_actions_query, new WayPointActionsRowMapper());
			return waypointactiondata;
		}catch(EmptyResultDataAccessException e) {
			return null;	
		}
	}
}
