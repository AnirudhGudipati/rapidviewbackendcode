package com.aiknights.com.rapidviewmain.models;

public class WeatherThunderWindModel {

	private String begin_location;
	private String magnitude;
	private String event_type;
	private String begin_lat;
	private String begin_lon;
	private String begin_date;
	private String begin_time;
	private String distance;
	private String event_narrative;
	
	public WeatherThunderWindModel() {
		super();
	}

	public WeatherThunderWindModel(String begin_location, String magnitude, String event_type, String begin_lat,
			String begin_lon, String begin_date, String begin_time, String distance, String event_narrative) {
		super();
		this.begin_location = begin_location;
		this.magnitude = magnitude;
		this.event_type = event_type;
		this.begin_lat = begin_lat;
		this.begin_lon = begin_lon;
		this.begin_date = begin_date;
		this.begin_time = begin_time;
		this.distance = distance;
		this.event_narrative = event_narrative;
	}

	public String getBegin_location() {
		return begin_location;
	}

	public void setBegin_location(String begin_location) {
		this.begin_location = begin_location;
	}

	public String getMagnitude() {
		return magnitude;
	}

	public void setMagnitude(String magnitude) {
		this.magnitude = magnitude;
	}

	public String getEvent_type() {
		return event_type;
	}

	public void setEvent_type(String event_type) {
		this.event_type = event_type;
	}

	public String getBegin_lat() {
		return begin_lat;
	}

	public void setBegin_lat(String begin_lat) {
		this.begin_lat = begin_lat;
	}

	public String getBegin_lon() {
		return begin_lon;
	}

	public void setBegin_lon(String begin_lon) {
		this.begin_lon = begin_lon;
	}

	public String getBegin_date() {
		return begin_date;
	}

	public void setBegin_date(String begin_date) {
		this.begin_date = begin_date;
	}

	public String getBegin_time() {
		return begin_time;
	}

	public void setBegin_time(String begin_time) {
		this.begin_time = begin_time;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getEvent_narrative() {
		return event_narrative;
	}

	public void setEvent_narrative(String event_narrative) {
		this.event_narrative = event_narrative;
	}
	

}
