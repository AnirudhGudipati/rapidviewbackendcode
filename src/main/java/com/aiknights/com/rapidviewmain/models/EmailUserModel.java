package com.aiknights.com.rapidviewmain.models;

public class EmailUserModel {
	String FirstName;
	String LastName;
	static String EmailAddress;
	static String Subject;
	static String Body;
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public static String getEmailAddress() {
		return EmailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}
	public static String getSubject() {
		return Subject;
	}
	public void setSubject(String subject) {
		Subject = subject;
	}
	public static String getBody() {
		return Body;
	}
	public void setBody(String body) {
		Body = body;
	}
	
}
