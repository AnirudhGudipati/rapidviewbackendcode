package com.aiknights.com.rapidviewmain.models;

import java.io.Serializable;

public class PaypalPurchaseUnitsShippingNameModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6451577598398458069L;
	String full_name;

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

}
