package com.aiknights.com.rapidviewmain.models;

public class WeatherDataforBoxChartsModel implements Comparable<WeatherDataforBoxChartsModel> {

	String beginDate;
	double distance;
	double magnitude;

	public String getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public double getMagnitude() {
		return magnitude;
	}

	public void setMagnitude(double magnitude) {
		this.magnitude = magnitude;
	}

	@Override
	public String toString() {
		return "{\"beginDate\":\"" + beginDate + "\", \"distance\":" + distance + ", \"magnitude\":" + magnitude + "}";
	}

	@Override
	public int compareTo(WeatherDataforBoxChartsModel row) {
		return this.beginDate.compareTo(row.getBeginDate());
	}
}
