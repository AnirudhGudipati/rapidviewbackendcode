package com.aiknights.com.rapidviewmain.models;

public class WorkOrderEventSlotsModel {

	String StartTime;
	String EndTime;
	public String getStartTime() {
		return StartTime;
	}
	public void setStartTime(String startTime) {
		StartTime = startTime;
	}
	public String getEndTime() {
		return EndTime;
	}
	public void setEndTime(String endTime) {
		EndTime = endTime;
	}
	
}
