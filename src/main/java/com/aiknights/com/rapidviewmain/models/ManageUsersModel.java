package com.aiknights.com.rapidviewmain.models;

public class ManageUsersModel {
	private String userId;
	private String userName;
	private String firstName;
	private String lastName;
	private String email;
	private String city;
	private String companyName;
	private String fullAddress;
	private String rolename;
	private String productType;
	private String subscription;
	private String membership;
	private String contactnumber;
	private String profilePhotoURL;
	private String enabledStatus;
	private String activeStatus;
	private String addressOne;
	private String addressTwo;
	private String state;
	private String country;
	private String zip;
	private String profileCreationStatus;
	private String emailVerificationStatus;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getFullAddress() {
		return fullAddress;
	}
	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getSubscription() {
		return subscription;
	}
	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}
	public String getMembership() {
		return membership;
	}
	public void setMembership(String membership) {
		this.membership = membership;
	}
	public String getContactnumber() {
		return contactnumber;
	}
	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}
	public String getProfilePhotoURL() {
		return profilePhotoURL;
	}
	public void setProfilePhotoURL(String profilePhotoURL) {
		this.profilePhotoURL = profilePhotoURL;
	}
	public String getEnabledStatus() {
		return enabledStatus;
	}
	public void setEnabledStatus(String enabledStatus) {
		this.enabledStatus = enabledStatus;
	}
	public String getActiveStatus() {
		return activeStatus;
	}
	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}
	public String getEmailVerificationStatus() {
		return emailVerificationStatus;
	}
	public void setEmailVerificationStatus(String emailVerificationStatus) {
		this.emailVerificationStatus = emailVerificationStatus;
	}
	public String getAddressOne() {
		return addressOne;
	}
	public void setAddressOne(String addressOne) {
		this.addressOne = addressOne;
	}
	public String getAddressTwo() {
		return addressTwo;
	}
	public void setAddressTwo(String addressTwo) {
		this.addressTwo = addressTwo;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getProfileCreationStatus() {
		return profileCreationStatus;
	}
	public void setProfileCreationStatus(String profileCreationStatus) {
		this.profileCreationStatus = profileCreationStatus;
	}
	@Override
	public String toString() {
		return "ManageUsersModel [userId=" + userId + ", userName=" + userName + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", email=" + email + ", city=" + city + ", companyName=" + companyName
				+ ", fullAddress=" + fullAddress + ", rolename=" + rolename + ", productType=" + productType
				+ ", subscription=" + subscription + ", membership=" + membership + ", contactnumber=" + contactnumber
				+ ", profilePhotoURL=" + profilePhotoURL + ", enabledStatus=" + enabledStatus + ", activeStatus="
				+ activeStatus + ", addressOne=" + addressOne + ", addressTwo=" + addressTwo + ", state=" + state
				+ ", country=" + country + ", zip=" + zip + ", profileCreationStatus=" + profileCreationStatus
				+ ", emailVerificationStatus=" + emailVerificationStatus + "]";
	}
}
