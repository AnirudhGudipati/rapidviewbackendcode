package com.aiknights.com.rapidviewmain.models;

public class SubscriptionPropertiesModel {
	String Subscription_Type;
	String Number_of_Days;
	String Subscription_Type_For_UI;

	public String getSubscription_Type() {
		return Subscription_Type;
	}

	public void setSubscription_Type(String subscription_Type) {
		Subscription_Type = subscription_Type;
	}

	public String getNumber_of_Days() {
		return Number_of_Days;
	}

	public void setNumber_of_Days(String number_of_Days) {
		Number_of_Days = number_of_Days;
	}

	public String getSubscription_Type_For_UI() {
		return Subscription_Type_For_UI;
	}

	public void setSubscription_Type_For_UI(String subscription_Type_For_UI) {
		Subscription_Type_For_UI = subscription_Type_For_UI;
	}

	@Override
	public String toString() {
		return "SubscriptionPropertiesModel [Subscription_Type=" + Subscription_Type + ", Number_of_Days="
				+ Number_of_Days + ", Subscription_Type_For_UI=" + Subscription_Type_For_UI
				+ ", getSubscription_Type()=" + getSubscription_Type() + ", getNumber_of_Days()=" + getNumber_of_Days()
				+ ", getSubscription_Type_For_UI()=" + getSubscription_Type_For_UI() + "]";
	}
	

}
