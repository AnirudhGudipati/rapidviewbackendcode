package com.aiknights.com.rapidviewmain.models;

public class FlightPlanObjectDBModel {
	String Flight_Plan_ID;
	String Claim_ID;
	String Claim_Address;
	public String getFlight_Plan_ID() {
		return Flight_Plan_ID;
	}
	public void setFlight_Plan_ID(String flight_Plan_ID) {
		Flight_Plan_ID = flight_Plan_ID;
	}
	public String getClaim_ID() {
		return Claim_ID;
	}
	public void setClaim_ID(String claim_ID) {
		Claim_ID = claim_ID;
	}
	public String getClaim_Address() {
		return Claim_Address;
	}
	public void setClaim_Address(String claim_Address) {
		Claim_Address = claim_Address;
	}
}
