package com.aiknights.com.rapidviewmain.models;

import java.io.Serializable;

public class PaypalPurchaseUnitsAmountModel  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1515207629391403352L;
	String value;
	String currency_code;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getCurrency_code() {
		return currency_code;
	}

	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}

}
