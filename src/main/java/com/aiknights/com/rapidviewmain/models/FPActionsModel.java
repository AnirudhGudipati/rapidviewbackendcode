package com.aiknights.com.rapidviewmain.models;

public class FPActionsModel {
	String action;
	String selectedOption;
	String selectedValue;
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getSelectedOption() {
		return selectedOption;
	}
	public void setSelectedOption(String selectedOption) {
		this.selectedOption = selectedOption;
	}
	public String getSelectedValue() {
		return selectedValue;
	}
	public void setSelectedValue(String selectedValue) {
		this.selectedValue = selectedValue;
	}
	@Override
	public String toString() {
		return "FPActionsModel [action=" + action + ", selectedOption=" + selectedOption + ", selectedValue="
				+ selectedValue + "]";
	}
	
}
