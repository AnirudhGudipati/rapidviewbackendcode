package com.aiknights.com.rapidviewmain.models;

public class GoogleLiveMapDataModel {
	Double HomeLatitude;
	Double HomeLongitude;
	Double Latitude;
	Double Longitude;
	String Label;
	String draggable;
	Double Distance;
	Double Hailsize;
	String eventDate;

	public Double getHomeLatitude() {
		return HomeLatitude;
	}

	public void setHomeLatitude(Double homeLatitude) {
		HomeLatitude = homeLatitude;
	}

	public Double getHomeLongitude() {
		return HomeLongitude;
	}

	public void setHomeLongitude(Double homeLongitude) {
		HomeLongitude = homeLongitude;
	}

	public Double getLatitude() {
		return Latitude;
	}

	public void setLatitude(Double latitude) {
		Latitude = latitude;
	}

	public Double getLongitude() {
		return Longitude;
	}

	public void setLongitude(Double longitude) {
		Longitude = longitude;
	}

	public String getLabel() {
		return Label;
	}

	public void setLabel(String label) {
		Label = label;
	}

	public String getDraggable() {
		return draggable;
	}

	public void setDraggable(String draggable) {
		this.draggable = draggable;
	}

	public Double getDistance() {
		return Distance;
	}

	public void setDistance(Double distance) {
		Distance = distance;
	}

	public Double getHailsize() {
		return Hailsize;
	}

	public void setHailsize(Double hailsize) {
		Hailsize = hailsize;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	@Override
	public String toString() {
		return "{\"HomeLatitude\":\"" + HomeLatitude + "\", \"HomeLongitude\":\"" + HomeLongitude
				+ "\", \"Latitude\":\"" + Latitude + "\", \"Longitude\":\"" + Longitude + "\", \"Label\":\"" + Label
				+ "\",\"draggable\":\"" + draggable + "\", \"Distance\":\"" + Distance + "\",\"Hailsize\":\""
				+ Hailsize + "\",\"eventDate\":\"" + eventDate + "\"}";
	}

}
