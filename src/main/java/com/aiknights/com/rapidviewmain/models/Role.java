package com.aiknights.com.rapidviewmain.models;

public class Role {

	private String roleId;
	private String rolename;
	private String username;

	public Role() {
		super();
	}

	public Role(String roleId, String rolename) {
		super();
		this.roleId = roleId;
		this.rolename = rolename;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
