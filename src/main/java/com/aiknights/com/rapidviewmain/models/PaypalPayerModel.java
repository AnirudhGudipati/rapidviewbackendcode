package com.aiknights.com.rapidviewmain.models;

import java.io.Serializable;

public class PaypalPayerModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3678327188056442323L;
	String email_address;
	String payer_id;
	PaypalPayerAddressModel address;
	PaypalPayerNameModel name;

	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public String getPayer_id() {
		return payer_id;
	}

	public void setPayer_id(String payer_id) {
		this.payer_id = payer_id;
	}

	public PaypalPayerAddressModel getAddress() {
		return address;
	}

	public void setAddress(PaypalPayerAddressModel address) {
		this.address = address;
	}

	public PaypalPayerNameModel getName() {
		return name;
	}

	public void setName(PaypalPayerNameModel name) {
		this.name = name;
	}

}
