package com.aiknights.com.rapidviewmain.models;

public class WeatherOutputModel implements Comparable<WeatherOutputModel> {

	String eventdate;
	String zeroonemag;
	String onethreemag;
	String threefivemag;
	String fivetenmag;
	String correct;

	public String getEventdate() {
		return eventdate;
	}

	public void setEventdate(String eventdate) {
		this.eventdate = eventdate;
	}

	public String getZeroonemag() {
		return zeroonemag;
	}

	public void setZeroonemag(String zeroonemag) {
		this.zeroonemag = zeroonemag;
	}

	public String getOnethreemag() {
		return onethreemag;
	}

	public void setOnethreemag(String onethreemag) {
		this.onethreemag = onethreemag;
	}

	public String getThreefivemag() {
		return threefivemag;
	}

	public void setThreefivemag(String threefivemag) {
		this.threefivemag = threefivemag;
	}

	public String getFivetenmag() {
		return fivetenmag;
	}

	public void setFivetenmag(String fivetenmag) {
		this.fivetenmag = fivetenmag;
	}

	public String getCorrect() {
		return correct;
	}

	public void setCorrect(String correct) {
		this.correct = correct;
	}

	@Override
	public String toString() {
		return "WeatherOutputModel [eventdate=" + eventdate + ", zeroonemag=" + zeroonemag + ", onethreemag="
				+ onethreemag + ", threefivemag=" + threefivemag + ", fivetenmag=" + fivetenmag + ", correct=" + correct
				+ ", getEventdate()=" + getEventdate() + ", getZeroonemag()=" + getZeroonemag() + ", getOnethreemag()="
				+ getOnethreemag() + ", getThreefivemag()=" + getThreefivemag() + ", getFivetenmag()=" + getFivetenmag()
				+ ", getCorrect()=" + getCorrect() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

	@Override
	public int compareTo(WeatherOutputModel row) {
		return this.eventdate.compareTo(row.getEventdate());

	}

}
