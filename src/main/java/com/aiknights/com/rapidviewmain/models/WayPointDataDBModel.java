package com.aiknights.com.rapidviewmain.models;

public class WayPointDataDBModel {
	String Flight_Plan_ID;
	String Way_Point_ID;
	String Way_Point_Key;
	String Latitude;
	String Longitude;
	String Altitude;
	String Above_Ground;
	String Speed;
	String Curve_size;
	String Heading;
	String POI;
	String Gimbal_Option;
	String Gimbal_Value;
	String Interval_Option;
	String Interval_Value;
	public String getFlight_Plan_ID() {
		return Flight_Plan_ID;
	}
	public void setFlight_Plan_ID(String flight_Plan_ID) {
		Flight_Plan_ID = flight_Plan_ID;
	}
	public String getWay_Point_ID() {
		return Way_Point_ID;
	}
	public void setWay_Point_ID(String way_Point_ID) {
		Way_Point_ID = way_Point_ID;
	}
	public String getWay_Point_Key() {
		return Way_Point_Key;
	}
	public void setWay_Point_Key(String way_Point_Key) {
		Way_Point_Key = way_Point_Key;
	}
	public String getLatitude() {
		return Latitude;
	}
	public void setLatitude(String latitude) {
		Latitude = latitude;
	}
	public String getLongitude() {
		return Longitude;
	}
	public void setLongitude(String longitude) {
		Longitude = longitude;
	}
	public String getAltitude() {
		return Altitude;
	}
	public void setAltitude(String altitude) {
		Altitude = altitude;
	}
	public String getAbove_Ground() {
		return Above_Ground;
	}
	public void setAbove_Ground(String above_Ground) {
		Above_Ground = above_Ground;
	}
	public String getSpeed() {
		return Speed;
	}
	public void setSpeed(String speed) {
		Speed = speed;
	}
	public String getCurve_size() {
		return Curve_size;
	}
	public void setCurve_size(String curve_size) {
		Curve_size = curve_size;
	}
	public String getHeading() {
		return Heading;
	}
	public void setHeading(String heading) {
		Heading = heading;
	}
	public String getPOI() {
		return POI;
	}
	public void setPOI(String pOI) {
		POI = pOI;
	}
	public String getGimbal_Option() {
		return Gimbal_Option;
	}
	public void setGimbal_Option(String gimbal_Option) {
		Gimbal_Option = gimbal_Option;
	}
	public String getGimbal_Value() {
		return Gimbal_Value;
	}
	public void setGimbal_Value(String gimbal_Value) {
		Gimbal_Value = gimbal_Value;
	}
	public String getInterval_Option() {
		return Interval_Option;
	}
	public void setInterval_Option(String interval_Option) {
		Interval_Option = interval_Option;
	}
	public String getInterval_Value() {
		return Interval_Value;
	}
	public void setInterval_Value(String interval_Value) {
		Interval_Value = interval_Value;
	}
	@Override
	public String toString() {
		return "WayPointDataDBModel [Flight_Plan_ID=" + Flight_Plan_ID + ", Way_Point_ID=" + Way_Point_ID
				+ ", Way_Point_Key=" + Way_Point_Key + ", Latitude=" + Latitude + ", Longitude=" + Longitude
				+ ", Altitude=" + Altitude + ", Above_Ground=" + Above_Ground + ", Speed=" + Speed + ", Curve_size="
				+ Curve_size + ", Heading=" + Heading + ", POI=" + POI + ", Gimbal_Option=" + Gimbal_Option
				+ ", Gimbal_Value=" + Gimbal_Value + ", Interval_Option=" + Interval_Option + ", Interval_Value="
				+ Interval_Value + "]";
	}
	
}
