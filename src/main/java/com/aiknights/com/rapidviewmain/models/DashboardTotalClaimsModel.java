package com.aiknights.com.rapidviewmain.models;

public class DashboardTotalClaimsModel {

	private String totalclaims;
	private String inprogressclaims;
	private String completedclaims;
	private String rejectedclaims;
	private String inprogressclaimspercentage;
	private String completedclaimspercentage;
	private String rejectedclaimspercentage;
	public String getTotalclaims() {
		return totalclaims;
	}
	public void setTotalclaims(String totalclaims) {
		this.totalclaims = totalclaims;
	}
	public String getInprogressclaims() {
		return inprogressclaims;
	}
	public void setInprogressclaims(String inprogressclaims) {
		this.inprogressclaims = inprogressclaims;
	}
	public String getCompletedclaims() {
		return completedclaims;
	}
	public void setCompletedclaims(String completedclaims) {
		this.completedclaims = completedclaims;
	}
	public String getRejectedclaims() {
		return rejectedclaims;
	}
	public void setRejectedclaims(String rejectedclaims) {
		this.rejectedclaims = rejectedclaims;
	}
	public String getInprogressclaimspercentage() {
		return inprogressclaimspercentage;
	}
	public void setInprogressclaimspercentage(String inprogressclaimspercentage) {
		this.inprogressclaimspercentage = inprogressclaimspercentage;
	}
	public String getCompletedclaimspercentage() {
		return completedclaimspercentage;
	}
	public void setCompletedclaimspercentage(String completedclaimspercentage) {
		this.completedclaimspercentage = completedclaimspercentage;
	}
	public String getRejectedclaimspercentage() {
		return rejectedclaimspercentage;
	}
	public void setRejectedclaimspercentage(String rejectedclaimspercentage) {
		this.rejectedclaimspercentage = rejectedclaimspercentage;
	}
	@Override
	public String toString() {
		return "DashboardTotalClaimsModel [totalclaims=" + totalclaims + ", inprogressclaims=" + inprogressclaims
				+ ", completedclaims=" + completedclaims + ", rejectedclaims=" + rejectedclaims
				+ ", inprogressclaimspercentage=" + inprogressclaimspercentage + ", completedclaimspercentage="
				+ completedclaimspercentage + ", rejectedclaimspercentage=" + rejectedclaimspercentage
				+ ", getTotalclaims()=" + getTotalclaims() + ", getInprogressclaims()=" + getInprogressclaims()
				+ ", getCompletedclaims()=" + getCompletedclaims() + ", getRejectedclaims()=" + getRejectedclaims()
				+ ", getInprogressclaimspercentage()=" + getInprogressclaimspercentage()
				+ ", getCompletedclaimspercentage()=" + getCompletedclaimspercentage()
				+ ", getRejectedclaimspercentage()=" + getRejectedclaimspercentage() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	
	
}
