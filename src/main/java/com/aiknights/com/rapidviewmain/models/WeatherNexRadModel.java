package com.aiknights.com.rapidviewmain.models;

public class WeatherNexRadModel {
	
	private String event_date;
	private String maxsize;
	private String prob;
	private String sevprob;
	private String lat;
	private String lon;
	private String distance;
	
	public WeatherNexRadModel() {
		super();
	}

	public WeatherNexRadModel(String event_date, String maxsize, String prob, String sevprob, String lat, String lon,
			String distance) {
		super();
		this.event_date = event_date;
		this.maxsize = maxsize;
		this.prob = prob;
		this.sevprob = sevprob;
		this.lat = lat;
		this.lon = lon;
		this.distance = distance;
	}

	public String getEvent_date() {
		return event_date;
	}

	public void setEvent_date(String event_date) {
		this.event_date = event_date;
	}

	public String getMaxsize() {
		return maxsize;
	}

	public void setMaxsize(String maxsize) {
		this.maxsize = maxsize;
	}

	public String getProb() {
		return prob;
	}

	public void setProb(String prob) {
		this.prob = prob;
	}

	public String getSevprob() {
		return sevprob;
	}

	public void setSevprob(String sevprob) {
		this.sevprob = sevprob;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}
	
	
}
