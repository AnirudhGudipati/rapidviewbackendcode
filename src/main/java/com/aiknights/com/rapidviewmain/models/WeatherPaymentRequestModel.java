package com.aiknights.com.rapidviewmain.models;

public class WeatherPaymentRequestModel {

	WeatherAssessmentDataModel weatherInfo;

	PaypalPaymentModel paymentInfo;

	public WeatherAssessmentDataModel getWeatherInfo() {
		return weatherInfo;
	}

	public void setWeatherInfo(WeatherAssessmentDataModel weatherInfo) {
		this.weatherInfo = weatherInfo;
	}

	public PaypalPaymentModel getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(PaypalPaymentModel paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

}
