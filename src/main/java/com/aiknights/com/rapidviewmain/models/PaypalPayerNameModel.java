package com.aiknights.com.rapidviewmain.models;

import java.io.Serializable;

public class PaypalPayerNameModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4645061244063464178L;
	String given_name;
	String surname;

	public String getGiven_name() {
		return given_name;
	}

	public void setGiven_name(String given_name) {
		this.given_name = given_name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

}
