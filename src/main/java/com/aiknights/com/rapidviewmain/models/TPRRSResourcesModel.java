package com.aiknights.com.rapidviewmain.models;

public class TPRRSResourcesModel {
	String resourceName;
	String resourceStatus;
	public String getResourceName() {
		return resourceName;
	}
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
	public String getResourceStatus() {
		return resourceStatus;
	}
	public void setResourceStatus(String resourceStatus) {
		this.resourceStatus = resourceStatus;
	}
}
