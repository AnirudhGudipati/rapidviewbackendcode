package com.aiknights.com.rapidviewmain.models;

public class WeatherStageDatabaseModel implements Comparable<WeatherStageDatabaseModel>{

	String claimid;
	String eventdate;
	String hailsize;
	String latitude;
	String Longitude;
	String Distance;
	
	public String getClaimid() {
		return claimid;
	}

	public void setClaimid(String claimid) {
		this.claimid = claimid;
	}

	public String getEventdate() {
		return eventdate;
	}

	public void setEventdate(String eventdate) {
		this.eventdate = eventdate;
	}

	public String getHailsize() {
		return hailsize;
	}

	public void setHailsize(String hailsize) {
		this.hailsize = hailsize;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	public String getDistance() {
		return Distance;
	}

	public void setDistance(String distance) {
		Distance = distance;
	}

	
	@Override
	public String toString() {
		return "WeatherStageDatabaseModel [claimid=" + claimid + ", eventdate=" + eventdate + ", hailsize=" + hailsize
				+ ", latitude=" + latitude + ", Longitude=" + Longitude + ", Distance=" + Distance + ", getClaimid()="
				+ getClaimid() + ", getEventdate()=" + getEventdate() + ", getHailsize()=" + getHailsize()
				+ ", getLatitude()=" + getLatitude() + ", getLongitude()=" + getLongitude() + ", getDistance()="
				+ getDistance() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

	@Override
	public int compareTo(WeatherStageDatabaseModel row) {
		return this.eventdate.compareTo(row.getEventdate());

	}
	
}
