package com.aiknights.com.rapidviewmain.models;

public class StormEventsAlertModel {
	private String state;
	private String county;
	private String severity;
	
	public StormEventsAlertModel() {
		super();
	}
	public StormEventsAlertModel(String state,String county,String severity) {
		super();
		this.state=state;
		this.county=county;
		this.severity=severity;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
}
