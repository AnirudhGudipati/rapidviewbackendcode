package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class ThunderWindServiceModel {
	public List<ThunderWindServiceChildrenModel> weatherdata= new ArrayList<ThunderWindServiceChildrenModel>();

	public List<ThunderWindServiceChildrenModel> getWeatherdata() {
		return weatherdata;
	}

	public void setWeatherdata(List<ThunderWindServiceChildrenModel> weatherdata) {
		this.weatherdata = weatherdata;
	}


	
}
