package com.aiknights.com.rapidviewmain.models;

import java.io.Serializable;

public class PaypalPayerAddressModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7053457861103443791L;
	String country_code;

	public String getCountry_code() {
		return country_code;
	}

	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

}
