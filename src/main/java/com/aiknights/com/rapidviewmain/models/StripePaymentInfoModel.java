package com.aiknights.com.rapidviewmain.models;

import java.util.List;

public class StripePaymentInfoModel {

	String paymentIntent;
	String amount;
	String canceled_at;
	String cancellation_reason;
	String capture_method;
	String client_secret;
	String confirmation_method;
	String created;
	String currency;
	String description;
	String id;
	String last_payment_error;
	String livemode;
	String next_action;
	String object;
	String payment_method;
	String receipt_email;
	String setup_future_usage;
	String shipping;
	String source;
	String status;
	List<String> payment_method_types;

	public String getPaymentIntent() {
		return paymentIntent;
	}

	public void setPaymentIntent(String paymentIntent) {
		this.paymentIntent = paymentIntent;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCanceled_at() {
		return canceled_at;
	}

	public void setCanceled_at(String canceled_at) {
		this.canceled_at = canceled_at;
	}

	public String getCancellation_reason() {
		return cancellation_reason;
	}

	public void setCancellation_reason(String cancellation_reason) {
		this.cancellation_reason = cancellation_reason;
	}

	public String getCapture_method() {
		return capture_method;
	}

	public void setCapture_method(String capture_method) {
		this.capture_method = capture_method;
	}

	public String getClient_secret() {
		return client_secret;
	}

	public void setClient_secret(String client_secret) {
		this.client_secret = client_secret;
	}

	public String getConfirmation_method() {
		return confirmation_method;
	}

	public void setConfirmation_method(String confirmation_method) {
		this.confirmation_method = confirmation_method;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLast_payment_error() {
		return last_payment_error;
	}

	public void setLast_payment_error(String last_payment_error) {
		this.last_payment_error = last_payment_error;
	}

	public String getLivemode() {
		return livemode;
	}

	public void setLivemode(String livemode) {
		this.livemode = livemode;
	}

	public String getNext_action() {
		return next_action;
	}

	public void setNext_action(String next_action) {
		this.next_action = next_action;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public String getPayment_method() {
		return payment_method;
	}

	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}

	public String getReceipt_email() {
		return receipt_email;
	}

	public void setReceipt_email(String receipt_email) {
		this.receipt_email = receipt_email;
	}

	public String getSetup_future_usage() {
		return setup_future_usage;
	}

	public void setSetup_future_usage(String setup_future_usage) {
		this.setup_future_usage = setup_future_usage;
	}

	public String getShipping() {
		return shipping;
	}

	public void setShipping(String shipping) {
		this.shipping = shipping;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getPayment_method_types() {
		return payment_method_types;
	}

	public void setPayment_method_types(List<String> payment_method_types) {
		this.payment_method_types = payment_method_types;
	}

	@Override
	public String toString() {
		return "StripePaymentInfoModel [paymentIntent=" + paymentIntent + ", amount=" + amount + ", canceled_at="
				+ canceled_at + ", cancellation_reason=" + cancellation_reason + ", capture_method=" + capture_method
				+ ", client_secret=" + client_secret + ", confirmation_method=" + confirmation_method + ", created="
				+ created + ", currency=" + currency + ", description=" + description + ", id=" + id
				+ ", last_payment_error=" + last_payment_error + ", livemode=" + livemode + ", next_action="
				+ next_action + ", object=" + object + ", payment_method=" + payment_method + ", receipt_email="
				+ receipt_email + ", setup_future_usage=" + setup_future_usage + ", shipping=" + shipping + ", source="
				+ source + ", status=" + status + ", payment_method_types=" + payment_method_types + "]";
	}

}
