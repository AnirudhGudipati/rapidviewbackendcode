package com.aiknights.com.rapidviewmain.models;

public class UpdateWORequestModel {

	String workOrderID;
	String assignedTo;
	String woStartDate;
	String woEndDate;
	String notes;
	String assignmentType;
	
	public String getAssignmentType() {
		return assignmentType;
	}
	public void setAssignmentType(String assignmentType) {
		this.assignmentType = assignmentType;
	}
	public String getWorkOrderID() {
		return workOrderID;
	}
	public void setWorkOrderID(String workOrderID) {
		this.workOrderID = workOrderID;
	}
	public String getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	public String getWoStartDate() {
		return woStartDate;
	}
	public void setWoStartDate(String woStartDate) {
		this.woStartDate = woStartDate;
	}
	public String getWoEndDate() {
		return woEndDate;
	}
	public void setWoEndDate(String woEndDate) {
		this.woEndDate = woEndDate;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	@Override
	public String toString() {
		return "UpdateWORequestModel [workOrderID=" + workOrderID + ", assignedTo=" + assignedTo + ", woStartDate="
				+ woStartDate + ", woEndDate=" + woEndDate + ", notes=" + notes + ", assignmentType=" + assignmentType
				+ "]";
	}
	
	
}
