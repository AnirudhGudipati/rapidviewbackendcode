package com.aiknights.com.rapidviewmain.models;

public class PLSRWindServiceChildrenModel {

	String event;
	String event_date;
	String event_time;
	String magnitude;
	String lat;
	String lon;
	double distance;
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getEvent_date() {
		return event_date;
	}
	public void setEvent_date(String event_date) {
		this.event_date = event_date;
	}
	public String getEvent_time() {
		return event_time;
	}
	public void setEvent_time(String event_time) {
		this.event_time = event_time;
	}
	public String getMagnitude() {
		return magnitude;
	}
	public void setMagnitude(String magnitude) {
		this.magnitude = magnitude;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	@Override
	public String toString() {
		return "PLSRWindServiceChildrenModel [event_type="+event+", event_date=" + event_date +", event_time="+event_time+ ", magnitude=" + magnitude 
				+", lat=" + lat + ", lon=" + lon + ", distance=" + distance
				+", getEvent_type="+getEvent()+ ", getEvent_date()=" + getEvent_date() 
				+", getEvent_time="+getEvent()+ ", getMaxsize()=" + getMagnitude() 
				+", getLat()=" + getLat() + ", getLon()=" + getLon()
				+ ", getDistance()=" + getDistance() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	public PLSRWindServiceChildrenModel(String event,String event_date, String event_time,String magnitude,  String lat,
			String lon, double distance) {
		super();
		this.event=event;
		this.event_date = event_date;
		this.event_time=event_time;
		this.magnitude = magnitude;
		this.lat = lat;
		this.lon = lon;
		this.distance = distance;
	}
	public PLSRWindServiceChildrenModel() {
		super();
	}
	
}
