package com.aiknights.com.rapidviewmain.models;

public class KMLGenServiceRequestModel {
	String name;
	String desc;
	float lon;
	float lat;
	float tilt;
	float range;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public float getLon() {
		return lon;
	}
	public void setLon(float lon) {
		this.lon = lon;
	}
	public float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
	public float getTilt() {
		return tilt;
	}
	public void setTilt(float tilt) {
		this.tilt = tilt;
	}
	public float getRange() {
		return range;
	}
	public void setRange(float range) {
		this.range = range;
	}
	@Override
	public String toString() {
		return "KMLGenServiceRequestModel [name=" + name + ", desc=" + desc + ", lon=" + lon + ", lat=" + lat
				+ ", tilt=" + tilt + ", range=" + range + "]";
	}
	
}
