package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class PLSRWindServiceModel {
	public List<PLSRWindServiceChildrenModel> weatherdata= new ArrayList<PLSRWindServiceChildrenModel>();

	public List<PLSRWindServiceChildrenModel> getWeatherdata() {
		return weatherdata;
	}

	public void setWeatherdata(List<PLSRWindServiceChildrenModel> weatherdata) {
		this.weatherdata = weatherdata;
	}


}
