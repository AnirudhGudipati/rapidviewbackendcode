package com.aiknights.com.rapidviewmain.models;

public class WeatherPLSRWindModel {
	
	private String event;
	private String event_date;
	private String event_time;
	private String magnitude;
	private String lat;
	private String lon;
	private String distance;
	
	public WeatherPLSRWindModel() {
		super();
	}

	public WeatherPLSRWindModel(String event,String event_date, String event_time, String magnitude,  String lat, String lon, String distance) {
		super();
		this.event=event;
		this.event_date = event_date;
		this.event_time=event_time;
		this.magnitude = magnitude;
		this.lat = lat;
		this.lon = lon;
		this.distance = distance;
	}
	
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event=event;
	}
	public String getEvent_date() {
		return event_date;
	}

	public void setEvent_date(String event_date) {
		this.event_date = event_date;
	}
	
	public String getEvent_time() {
		return event_time;
	}

	public void setEvent_time(String event_time) {
		this.event_time = event_time;
	}
	
	public String getMagnitude() {
		return magnitude;
	}

	public void setMagnitude(String magnitude) {
		this.magnitude = magnitude;
	}
	
	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}
	
	
}