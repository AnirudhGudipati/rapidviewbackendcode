package com.aiknights.com.rapidviewmain.models;

import java.util.List;

public class SubscriptionPropertiesResponce {

	private List<WeatherSubscriptionPropertiesModel> weatherProductProperties = null;

	public List<WeatherSubscriptionPropertiesModel> getWeatherProperties() {
		return weatherProductProperties;
	}

	public void setWeatherProperties(List<WeatherSubscriptionPropertiesModel> weatherProductProperties) {
		this.weatherProductProperties = weatherProductProperties;
	}

}
