package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class PRRSManagementProductsModel {
	String productName;
	List<PRRSManagementRolesandResourcesModel> rolesAndResources = new ArrayList<PRRSManagementRolesandResourcesModel>();
	List<PRRSManagementSubscriptionPropertiesModel> subscriptionProperties = new ArrayList<PRRSManagementSubscriptionPropertiesModel>();

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public List<PRRSManagementRolesandResourcesModel> getRolesAndResources() {
		return rolesAndResources;
	}

	public void setRolesAndResources(List<PRRSManagementRolesandResourcesModel> rolesAndResources) {
		this.rolesAndResources = rolesAndResources;
	}

	public List<PRRSManagementSubscriptionPropertiesModel> getSubscriptionProperties() {
		return subscriptionProperties;
	}

	public void setSubscriptionProperties(List<PRRSManagementSubscriptionPropertiesModel> subscriptionProperties) {
		this.subscriptionProperties = subscriptionProperties;
	}

}
