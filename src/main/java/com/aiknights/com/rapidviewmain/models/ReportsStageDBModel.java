package com.aiknights.com.rapidviewmain.models;

public class ReportsStageDBModel implements Comparable<ReportsStageDBModel>{

	String reportid;
	String eventdate;
	String hailsize;
	String latitude;
	String Longitude;
	String Distance;
	public String getReportid() {
		return reportid;
	}
	public void setReportid(String reportid) {
		this.reportid = reportid;
	}
	public String getEventdate() {
		return eventdate;
	}
	public void setEventdate(String eventdate) {
		this.eventdate = eventdate;
	}
	public String getHailsize() {
		return hailsize;
	}
	public void setHailsize(String hailsize) {
		this.hailsize = hailsize;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return Longitude;
	}
	public void setLongitude(String longitude) {
		Longitude = longitude;
	}
	public String getDistance() {
		return Distance;
	}
	public void setDistance(String distance) {
		Distance = distance;
	}
	@Override
	public String toString() {
		return "ReportsStageDBModel [reportid=" + reportid + ", eventdate=" + eventdate + ", hailsize=" + hailsize
				+ ", latitude=" + latitude + ", Longitude=" + Longitude + ", Distance=" + Distance + "]";
	}
	
	@Override
	public int compareTo(ReportsStageDBModel row) {
		return this.eventdate.compareTo(row.getEventdate());

	}
	
}
