package com.aiknights.com.rapidviewmain.models;

import java.io.Serializable;

public class PaypalPurchaseUnitsShippingModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6520846699767590329L;
	PaypalPurchaseUnitsShippingNameModel name;
	PaypalPurchaseUnitsShippingAddressModel address;
	public PaypalPurchaseUnitsShippingNameModel getName() {
		return name;
	}
	public void setName(PaypalPurchaseUnitsShippingNameModel name) {
		this.name = name;
	}
	public PaypalPurchaseUnitsShippingAddressModel getAddress() {
		return address;
	}
	public void setAddress(PaypalPurchaseUnitsShippingAddressModel address) {
		this.address = address;
	}
	
}
