package com.aiknights.com.rapidviewmain.models;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class UserDetails extends LicensingModel {

	private int userId;
	private String userName;
	private String firstName;
	private String lastName;
	@Email(message = "Please provide a valid e-mail")
	@NotEmpty(message = "Please provide an e-mail")
	private String email;
	private String city;
	private String companyName;
	private String address;
	private String contactNumber;
	private String weatheSubscription;
	private String address1;
	private String address2;
	private String state;
	private String country;
	private String zipCode;
	private String isProfileCreated;
	private String isActive;
	private String isEmailVerified;
	private String forceChangePassword;

	public UserDetails() {
		super();
	}

	public UserDetails(int userId, String userName, String firstName, String lastName, String email, String city,
			String companyName, String address, String contactNumber) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.city = city;
		this.companyName = companyName;
		this.address = address;
		this.contactNumber = contactNumber;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getcompanyName() {
		return companyName;
	}

	public void setcompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getWeatheSubscription() {
		return weatheSubscription;
	}

	public void setWeatheSubscription(String weatheSubscription) {
		this.weatheSubscription = weatheSubscription;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getIsProfileCreated() {
		return isProfileCreated;
	}

	public void setIsProfileCreated(String isProfileCreated) {
		this.isProfileCreated = isProfileCreated;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsEmailVerified() {
		return isEmailVerified;
	}

	public void setIsEmailVerified(String isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public String getForceChangePassword() {
		return forceChangePassword;
	}

	public void setForceChangePassword(String forceChangePassword) {
		this.forceChangePassword = forceChangePassword;
	}

	@Override
	public String toString() {
		return "UserDetails [userId=" + userId + ", userName=" + userName + ", firstName=" + firstName + ", lastName="
				+ lastName + ", email=" + email + ", city=" + city + ", companyName=" + companyName + ", address="
				+ address + ", contactNumber=" + contactNumber + ", weatheSubscription=" + weatheSubscription
				+ ", address1=" + address1 + ", address2=" + address2 + ", state=" + state + ", country=" + country
				+ ", zipCode=" + zipCode + ", isProfileCreated=" + isProfileCreated + ", isActive=" + isActive
				+ ", isEmailVerified=" + isEmailVerified + ", forceChangePassword=" + forceChangePassword
				+ ", getContactNumber()=" + getContactNumber() + ", getUserId()=" + getUserId() + ", getUserName()="
				+ getUserName() + ", getFirstName()=" + getFirstName() + ", getLastName()=" + getLastName()
				+ ", getEmail()=" + getEmail() + ", getCity()=" + getCity() + ", getcompanyName()=" + getcompanyName()
				+ ", getAddress()=" + getAddress() + ", getWeatheSubscription()=" + getWeatheSubscription()
				+ ", getAddress1()=" + getAddress1() + ", getAddress2()=" + getAddress2() + ", getState()=" + getState()
				+ ", getCountry()=" + getCountry() + ", getZipCode()=" + getZipCode() + ", getIsProfileCreated()="
				+ getIsProfileCreated() + ", getIsActive()=" + getIsActive() + ", getIsEmailVerified()="
				+ getIsEmailVerified() + ", getForceChangePassword()=" + getForceChangePassword() + "]";
	}

}
