package com.aiknights.com.rapidviewmain.models;

import java.util.UUID;

public class TokenDBModel {

	String TokenID;
	String CreatedDateTime;
	String IsUsed;
	String UserID;

	public String getTokenID() {
		return TokenID;
	}

	public void setTokenID(String tokenID) {
		TokenID = tokenID;
	}

	public TokenDBModel() {
		super();
	}

	public String getCreatedDateTime() {
		return CreatedDateTime;
	}

	public TokenDBModel(String UserID) {
		super();
		TokenID = UUID.randomUUID().toString();
		UserID = this.UserID;
	}

	public void setCreatedDateTime(String createdDateTime) {
		CreatedDateTime = createdDateTime;
	}

	public String getIsUsed() {
		return IsUsed;
	}

	public void setIsUsed(String isUsed) {
		IsUsed = isUsed;
	}

	public String getUserID() {
		return UserID;
	}

	public void setUserID(String userID) {
		UserID = userID;
	}

	@Override
	public String toString() {
		return "TokenDBModel [TokenID=" + TokenID + ", CreatedDateTime=" + CreatedDateTime + ", IsUsed=" + IsUsed
				+ ", UserID=" + UserID + "]";
	}

}
