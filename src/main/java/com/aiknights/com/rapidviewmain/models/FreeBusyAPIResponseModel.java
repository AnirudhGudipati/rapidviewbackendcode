package com.aiknights.com.rapidviewmain.models;

public class FreeBusyAPIResponseModel {

	String slotIndex;
	String startTime;
	String endTime;
	String adjusterStatus;
	String pilotStatus;
	
	public String getSlotIndex() {
		return slotIndex;
	}
	public void setSlotIndex(String slotIndex) {
		this.slotIndex = slotIndex;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getAdjusterStatus() {
		return adjusterStatus;
	}
	public void setAdjusterStatus(String adjusterStatus) {
		this.adjusterStatus = adjusterStatus;
	}
	public String getPilotStatus() {
		return pilotStatus;
	}
	public void setPilotStatus(String pilotStatus) {
		this.pilotStatus = pilotStatus;
	}
	
}
