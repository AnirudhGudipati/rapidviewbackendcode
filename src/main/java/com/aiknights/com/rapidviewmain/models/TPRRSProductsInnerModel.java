package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class TPRRSProductsInnerModel {
	String productName;
	List<TPRRSRRModel> rolesAndResources = new ArrayList<TPRRSRRModel>();
	List<PRRSManagementSubscriptionPropertiesModel> subscriptionProperties = new ArrayList<PRRSManagementSubscriptionPropertiesModel>();
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public List<TPRRSRRModel> getRolesAndResources() {
		return rolesAndResources;
	}
	public void setRolesAndResources(List<TPRRSRRModel> rolesAndResources) {
		this.rolesAndResources = rolesAndResources;
	}
	public List<PRRSManagementSubscriptionPropertiesModel> getSubscriptionProperties() {
		return subscriptionProperties;
	}
	public void setSubscriptionProperties(List<PRRSManagementSubscriptionPropertiesModel> subscriptionProperties) {
		this.subscriptionProperties = subscriptionProperties;
	}
}
