package com.aiknights.com.rapidviewmain.models;

public class WeatherAlertsSubscriptionModel {

	private int userId;
	private String states;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getStates() {
		return states;
	}

	public void setStates(String states) {
		this.states = states;
	}

	@Override
	public String toString() {
		return "WeatherAlertsSubscriptionModel [userId=" + userId + ", states=" + states + ", getUserId()="
				+ getUserId() + ", getStates()=" + getStates() + "]";
	}

}
