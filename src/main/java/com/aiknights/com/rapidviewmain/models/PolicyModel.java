package com.aiknights.com.rapidviewmain.models;

public class PolicyModel {

	String policyNumber;
	String insuranceCompany;
	String 	startDate;
	String endDate;
	String policyType;
	String policyCoverage;
	String endorsementId;
	String deductibleAmount;
	String mortgageCompany;
	String claimId;
	String claimantFirstName;
	String claimantLastName;
	String email;
	String phoneNumber;
	String address;
	String city;
	String state;
	String zipCode;
	String incidentType;
	String incidentDate;
	String incidentTime;
	String claimDate;
	String claimAmount;
	String claimstatus;
	String overAllRisk;
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getInsuranceCompany() {
		return insuranceCompany;
	}
	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getPolicyType() {
		return policyType;
	}
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	public String getPolicyCoverage() {
		return policyCoverage;
	}
	public void setPolicyCoverage(String policyCoverage) {
		this.policyCoverage = policyCoverage;
	}
	public String getEndorsementId() {
		return endorsementId;
	}
	public void setEndorsementId(String endorsementId) {
		this.endorsementId = endorsementId;
	}
	public String getDeductibleAmount() {
		return deductibleAmount;
	}
	public void setDeductibleAmount(String deductibleAmount) {
		this.deductibleAmount = deductibleAmount;
	}
	public String getMortgageCompany() {
		return mortgageCompany;
	}
	public void setMortgageCompany(String mortgageCompany) {
		this.mortgageCompany = mortgageCompany;
	}
	public String getClaimId() {
		return claimId;
	}
	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}
	public String getClaimantFirstName() {
		return claimantFirstName;
	}
	public void setClaimantFirstName(String claimantFirstName) {
		this.claimantFirstName = claimantFirstName;
	}
	public String getClaimantLastName() {
		return claimantLastName;
	}
	public void setClaimantLastName(String claimantLastName) {
		this.claimantLastName = claimantLastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getIncidentType() {
		return incidentType;
	}
	public void setIncidentType(String incidentType) {
		this.incidentType = incidentType;
	}
	public String getIncidentDate() {
		return incidentDate;
	}
	public void setIncidentDate(String incidentDate) {
		this.incidentDate = incidentDate;
	}
	public String getIncidentTime() {
		return incidentTime;
	}
	public void setIncidentTime(String incidentTime) {
		this.incidentTime = incidentTime;
	}
	public String getClaimDate() {
		return claimDate;
	}
	public void setClaimDate(String claimDate) {
		this.claimDate = claimDate;
	}
	public String getClaimAmount() {
		return claimAmount;
	}
	public void setClaimAmount(String claimAmount) {
		this.claimAmount = claimAmount;
	}
	public String getClaimstatus() {
		return claimstatus;
	}
	public void setClaimstatus(String claimstatus) {
		this.claimstatus = claimstatus;
	}
	public String getOverAllRisk() {
		return overAllRisk;
	}
	public void setOverAllRisk(String overAllRisk) {
		this.overAllRisk = overAllRisk;
	}
}
