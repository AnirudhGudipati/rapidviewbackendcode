package com.aiknights.com.rapidviewmain.models;

public class CreateProfileRequestModel {

	String userName;
	String productType;
	String roleType;
	String subscriptionType;
	String addressLine1;
	String addressLine2;
	String city;
	String state;
	String zip;
	String weatherAlerts;
	String weatherStates;
	String country;
	StripePaymentInfoModel paymentInfo;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public String getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getWeatherAlerts() {
		return weatherAlerts;
	}

	public void setWeatherAlerts(String weatherAlerts) {
		this.weatherAlerts = weatherAlerts;
	}

	public String getWeatherStates() {
		return weatherStates;
	}

	public void setWeatherStates(String weatherStates) {
		this.weatherStates = weatherStates;
	}

	public StripePaymentInfoModel getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(StripePaymentInfoModel paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "CreateProfileRequestModel [userName=" + userName + ", productType=" + productType + ", roleType="
				+ roleType + ", subscriptionType=" + subscriptionType + ", addressLine1=" + addressLine1
				+ ", addressLine2=" + addressLine2 + ", city=" + city + ", state=" + state + ", zip=" + zip
				+ ", weatherAlerts=" + weatherAlerts + ", weatherStates=" + weatherStates + ", country=" + country
				+ ", paymentInfo=" + paymentInfo + ", getUserName()=" + getUserName() + ", getProductType()="
				+ getProductType() + ", getRoleType()=" + getRoleType() + ", getSubscriptionType()="
				+ getSubscriptionType() + ", getAddressLine1()=" + getAddressLine1() + ", getAddressLine2()="
				+ getAddressLine2() + ", getCity()=" + getCity() + ", getState()=" + getState() + ", getZip()="
				+ getZip() + ", getWeatherAlerts()=" + getWeatherAlerts() + ", getWeatherStates()=" + getWeatherStates()
				+ ", getPaymentInfo()=" + getPaymentInfo() + ", getCountry()=" + getCountry() + "]";
	}

}
