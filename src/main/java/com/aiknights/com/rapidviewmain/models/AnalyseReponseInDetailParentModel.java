package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class AnalyseReponseInDetailParentModel {
	List<AnalyseReponseInDetail> imageList = new ArrayList<AnalyseReponseInDetail>();

	public List<AnalyseReponseInDetail> getImageList() {
		return imageList;
	}

	public void setImageList(List<AnalyseReponseInDetail> imageList) {
		this.imageList = imageList;
	}
	
}
