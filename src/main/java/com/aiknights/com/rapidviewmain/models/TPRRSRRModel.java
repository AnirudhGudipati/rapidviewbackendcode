package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class TPRRSRRModel {
	List<TPRRSRolesModel> roles = new ArrayList<TPRRSRolesModel>();

	public List<TPRRSRolesModel> getRoles() {
		return roles;
	}

	public void setRoles(List<TPRRSRolesModel> roles) {
		this.roles = roles;
	}
	
}
