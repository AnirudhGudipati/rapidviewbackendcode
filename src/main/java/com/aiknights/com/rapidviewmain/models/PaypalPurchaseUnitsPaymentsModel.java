package com.aiknights.com.rapidviewmain.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PaypalPurchaseUnitsPaymentsModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5345581122314365851L;
	List<PaypalPurchaseUnitsPaymentsCapturesModel> captures = new ArrayList<>();

	public List<PaypalPurchaseUnitsPaymentsCapturesModel> getCaptures() {
		return captures;
	}

	public void setCaptures(List<PaypalPurchaseUnitsPaymentsCapturesModel> captures) {
		this.captures = captures;
	}

}
