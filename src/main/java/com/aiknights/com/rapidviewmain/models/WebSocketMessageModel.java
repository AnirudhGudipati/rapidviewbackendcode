package com.aiknights.com.rapidviewmain.models;

public class WebSocketMessageModel {

	private String Notification_Object_ID;
	private String Notification_Topic;
	private String Notification_Type_ID;
	private String Notification_Claim_ID;
	private String Created_on;
	private String Notification_Subject;
	private String Message_content;
	private String Read_Status;
	private String IsDeleted;
	private String SenderUserID;
	private String Sender_Read_Status;
	private String Sender_IsDeleted;
	private String ReceiverUserID;
	private String Receiver_Read_Status;
	private String Receiver_IsDeleted;
	public String getNotification_Object_ID() {
		return Notification_Object_ID;
	}
	public void setNotification_Object_ID(String notification_Object_ID) {
		Notification_Object_ID = notification_Object_ID;
	}
	public String getNotification_Topic() {
		return Notification_Topic;
	}
	public void setNotification_Topic(String notification_Topic) {
		Notification_Topic = notification_Topic;
	}
	public String getNotification_Type_ID() {
		return Notification_Type_ID;
	}
	public void setNotification_Type_ID(String notification_Type_ID) {
		Notification_Type_ID = notification_Type_ID;
	}
	public String getNotification_Claim_ID() {
		return Notification_Claim_ID;
	}
	public void setNotification_Claim_ID(String notification_Claim_ID) {
		Notification_Claim_ID = notification_Claim_ID;
	}
	public String getCreated_on() {
		return Created_on;
	}
	public void setCreated_on(String created_on) {
		Created_on = created_on;
	}
	public String getNotification_Subject() {
		return Notification_Subject;
	}
	public void setNotification_Subject(String notification_Subject) {
		Notification_Subject = notification_Subject;
	}
	public String getMessage_content() {
		return Message_content;
	}
	public void setMessage_content(String message_content) {
		Message_content = message_content;
	}
	public String getRead_Status() {
		return Read_Status;
	}
	public void setRead_Status(String read_Status) {
		Read_Status = read_Status;
	}
	public String getIsDeleted() {
		return IsDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		IsDeleted = isDeleted;
	}
	public String getSenderUserID() {
		return SenderUserID;
	}
	public void setSenderUserID(String senderUserID) {
		SenderUserID = senderUserID;
	}
	public String getSender_Read_Status() {
		return Sender_Read_Status;
	}
	public void setSender_Read_Status(String sender_Read_Status) {
		Sender_Read_Status = sender_Read_Status;
	}
	public String getSender_IsDeleted() {
		return Sender_IsDeleted;
	}
	public void setSender_IsDeleted(String sender_IsDeleted) {
		Sender_IsDeleted = sender_IsDeleted;
	}
	public String getReceiverUserID() {
		return ReceiverUserID;
	}
	public void setReceiverUserID(String receiverUserID) {
		ReceiverUserID = receiverUserID;
	}
	public String getReceiver_Read_Status() {
		return Receiver_Read_Status;
	}
	public void setReceiver_Read_Status(String receiver_Read_Status) {
		Receiver_Read_Status = receiver_Read_Status;
	}
	public String getReceiver_IsDeleted() {
		return Receiver_IsDeleted;
	}
	public void setReceiver_IsDeleted(String receiver_IsDeleted) {
		Receiver_IsDeleted = receiver_IsDeleted;
	}
	@Override
	public String toString() {
		return "WebSocketMessageModel [Notification_Object_ID=" + Notification_Object_ID + ", Notification_Topic="
				+ Notification_Topic + ", Notification_Type_ID=" + Notification_Type_ID + ", Notification_Claim_ID="
				+ Notification_Claim_ID + ", Created_on=" + Created_on + ", Notification_Subject="
				+ Notification_Subject + ", Message_content=" + Message_content + ", Read_Status=" + Read_Status
				+ ", IsDeleted=" + IsDeleted + ", SenderUserID=" + SenderUserID + ", Sender_Read_Status="
				+ Sender_Read_Status + ", Sender_IsDeleted=" + Sender_IsDeleted + ", ReceiverUserID=" + ReceiverUserID
				+ ", Receiver_Read_Status=" + Receiver_Read_Status + ", Receiver_IsDeleted=" + Receiver_IsDeleted + "]";
	}
	
}
