package com.aiknights.com.rapidviewmain.models;

import java.util.Comparator;

public class DetectionScoreTypeComparator implements Comparator<Detection>{

	@Override
	public int compare(Detection o1, Detection o2) {
		return (int) ((o2.getScore()*100) - (o1.getScore()*100));
	}

}
