package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class KMLGenServiceRequestModelParent {
	List<KMLGenServiceRequestModel> features = new ArrayList<KMLGenServiceRequestModel>();

	public List<KMLGenServiceRequestModel> getFeatures() {
		return features;
	}

	public void setFeatures(List<KMLGenServiceRequestModel> features) {
		this.features = features;
	}

	@Override
	public String toString() {
		return "KMLGenServiceRequestModelParent [features=" + features + "]";
	}
	
}
