package com.aiknights.com.rapidviewmain.models;

public class PropertyCsvModel {
	
	private String  property_id;
	private String  propertyType; 
	private String propertyPurchaseDate;
	private String streetAddress;
	private String city;
	private String state; 
	private String zipcode;
	private String policy_id;
	
	
	
	public String getProperty_id() {
		return property_id;
	}
	public void setProperty_id(String property_id) {
		this.property_id = property_id;
	}
	public String getPropertyType() {
		return propertyType;
	}
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}
	public String getPropertyPurchaseDate() {
		return propertyPurchaseDate;
	}
	public void setPropertyPurchaseDate(String propertyPurchaseDate) {
		this.propertyPurchaseDate = propertyPurchaseDate;
	}
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	
	public String getPolicy_id() {
		return policy_id;
	}
	public void setPolicy_id(String policy_id) {
		this.policy_id = policy_id;
	}
	@Override
	public String toString() {
		return "PropertyCsvModel [property_id=" + property_id + ", propertyType=" + propertyType
				+ ", propertyPurchaseDate=" + propertyPurchaseDate + ", streetAddress=" + streetAddress + ", city="
				+ city + ", state=" + state + ", zipcode=" + zipcode + ", policy_id=" + policy_id + "]";
	}
	
	
	


}
