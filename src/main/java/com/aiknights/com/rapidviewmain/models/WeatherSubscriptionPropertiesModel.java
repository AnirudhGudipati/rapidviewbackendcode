package com.aiknights.com.rapidviewmain.models;

public class WeatherSubscriptionPropertiesModel {
	String Subscription_Type;
	String Weather_Download_MaxCount;
	String AmountToBe_Paid;
	String Subscription_Type_For_UI;

	public String getSubscription_Type() {
		return Subscription_Type;
	}

	public void setSubscription_Type(String subscription_Type) {
		Subscription_Type = subscription_Type;
	}

	public String getWeather_Download_MaxCount() {
		return Weather_Download_MaxCount;
	}

	public void setWeather_Download_MaxCount(String weather_Download_MaxCount) {
		Weather_Download_MaxCount = weather_Download_MaxCount;
	}

	public String getAmountToBe_Paid() {
		return AmountToBe_Paid;
	}

	public void setAmountToBe_Paid(String amountToBe_Paid) {
		AmountToBe_Paid = amountToBe_Paid;
	}

	public String getSubscription_Type_For_UI() {
		return Subscription_Type_For_UI;
	}

	public void setSubscription_Type_For_UI(String subscription_Type_For_UI) {
		Subscription_Type_For_UI = subscription_Type_For_UI;
	}

	@Override
	public String toString() {
		return "WeatherSubscriptionPropertiesModel [Subscription_Type=" + Subscription_Type
				+ ", Weather_Download_MaxCount=" + Weather_Download_MaxCount + ", AmountToBe_Paid=" + AmountToBe_Paid
				+ ", Subscription_Type_For_UI=" + Subscription_Type_For_UI + ", getSubscription_Type()="
				+ getSubscription_Type() + ", getWeather_Download_MaxCount()=" + getWeather_Download_MaxCount()
				+ ", getAmountToBe_Paid()=" + getAmountToBe_Paid() + "]";
	}

}
