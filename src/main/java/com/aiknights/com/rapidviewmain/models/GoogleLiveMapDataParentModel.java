package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class GoogleLiveMapDataParentModel {

	public List<GoogleLiveMapDataModel> GMapMarkerData = new ArrayList<GoogleLiveMapDataModel>();

	public List<GoogleLiveMapDataModel> getGMapMarkerData() {
		return GMapMarkerData;
	}

	public void setGMapMarkerData(List<GoogleLiveMapDataModel> gMapMarkerData) {
		GMapMarkerData = gMapMarkerData;
	}


	
	
}
