package com.aiknights.com.rapidviewmain.models;

public class ProductCertainResourceDBTableModel {
	String RoleName;
	String ResourceName;
	String IsActive;
	public String getRoleName() {
		return RoleName;
	}
	public void setRoleName(String roleName) {
		RoleName = roleName;
	}
	public String getResourceName() {
		return ResourceName;
	}
	public void setResourceName(String resourceName) {
		ResourceName = resourceName;
	}
	public String getIsActive() {
		return IsActive;
	}
	public void setIsActive(String isActive) {
		IsActive = isActive;
	}
	@Override
	public String toString() {
		return "ProductCertainResourceDBTableModel [RoleName=" + RoleName + ", ResourceName=" + ResourceName
				+ ", IsActive=" + IsActive + "]";
	}
}
