package com.aiknights.com.rapidviewmain.models;

import java.util.Date;

public class PolicyCheckNotification {

	private String policyNumber;
	private String insuranceCompany;
	private Date startDate;
	private Date endDate;
	private String policyType;
	private Integer policyCoverage;
	private Integer endorsementId;
	private Integer deductibleAmount;
	private String mortgageCompany;
	private Integer priorClaims;
	private Date policyLastUpdateDate;
	private String policyStatus;
	private String policyLimit;
	private Integer coverageAmt;

	public PolicyCheckNotification() {
		super();
	}

	public PolicyCheckNotification(String policyNumber, String insuranceCompany, Date startDate, Date endDate,
			String policyType, Integer policyCoverage, Integer endorsementId, Integer deductibleAmount,
			String mortgageCompany) {
		super();
		this.policyNumber = policyNumber;
		this.insuranceCompany = insuranceCompany;
		this.startDate = startDate;
		this.endDate = endDate;
		this.policyType = policyType;
		this.policyCoverage = policyCoverage;
		this.endorsementId = endorsementId;
		this.deductibleAmount = deductibleAmount;
		this.mortgageCompany = mortgageCompany;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public Integer getPolicyCoverage() {
		return policyCoverage;
	}

	public void setPolicyCoverage(Integer policyCoverage) {
		this.policyCoverage = policyCoverage;
	}

	public Integer getEndorsementId() {
		return endorsementId;
	}

	public void setEndorsementId(Integer endorsementId) {
		this.endorsementId = endorsementId;
	}

	public Integer getDeductibleAmount() {
		return deductibleAmount;
	}

	public void setDeductibleAmount(Integer deductibleAmount) {
		this.deductibleAmount = deductibleAmount;
	}

	public String getMortgageCompany() {
		return mortgageCompany;
	}

	public void setMortgageCompany(String mortgageCompany) {
		this.mortgageCompany = mortgageCompany;
	}

	public Integer getPriorClaims() {
		return priorClaims;
	}

	public void setPriorClaims(Integer priorClaims) {
		this.priorClaims = priorClaims;
	}

	public Date getPolicyLastUpdateDate() {
		return policyLastUpdateDate;
	}

	public void setPolicyLastUpdateDate(Date policyLastUpdateDate) {
		this.policyLastUpdateDate = policyLastUpdateDate;
	}

	public String getPolicyStatus() {
		return policyStatus;
	}

	public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}

	public String getPolicyLimit() {
		return policyLimit;
	}

	public void setPolicyLimit(String policyLimit) {
		this.policyLimit = policyLimit;
	}

	public Integer getCoverageAmt() {
		return coverageAmt;
	}

	public void setCoverageAmt(Integer coverageAmt) {
		this.coverageAmt = coverageAmt;
	}

	@Override
	public String toString() {
		return "PolicyCheckNotification [policyNumber=" + policyNumber + ", insuranceCompany=" + insuranceCompany
				+ ", startDate=" + startDate + ", endDate=" + endDate + ", policyType=" + policyType
				+ ", policyCoverage=" + policyCoverage + ", endorsementId=" + endorsementId + ", deductibleAmount="
				+ deductibleAmount + ", mortgageCompany=" + mortgageCompany + ", priorClaims=" + priorClaims
				+ ", policyLastUpdateDate=" + policyLastUpdateDate + " ,policyStatus=" + policyStatus + ",policyLimit="
				+ policyLimit + ",coverageAmt=" + coverageAmt + "]";
	}

}
