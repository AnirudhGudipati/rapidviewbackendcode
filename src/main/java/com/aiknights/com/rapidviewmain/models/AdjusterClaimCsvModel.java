package com.aiknights.com.rapidviewmain.models;

public class AdjusterClaimCsvModel {

	private String adjusterId;
	private String claimId;
	private String adjusterCompany;
	private String adjusterAddress;
	private String expertise;
	private String assignedDate;
	private String adjuster_FirstName;
	private String adjuster_LastName;
	private String adjuster_PhoneNumber;
	private String adjuster_Email;

	public String getAdjusterId() {
		return adjusterId;
	}

	public void setAdjusterId(String adjusterId) {
		this.adjusterId = adjusterId;
	}

	public String getClaimId() {
		return claimId;
	}

	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}

	public String getAdjusterCompany() {
		return adjusterCompany;
	}

	public void setAdjusterCompany(String adjusterCompany) {
		this.adjusterCompany = adjusterCompany;
	}

	public String getAdjusterAddress() {
		return adjusterAddress;
	}

	public void setAdjusterAddress(String adjusterAddress) {
		this.adjusterAddress = adjusterAddress;
	}

	public String getExpertise() {
		return expertise;
	}

	public void setExpertise(String expertise) {
		this.expertise = expertise;
	}

	public String getAssignedDate() {
		return assignedDate;
	}

	public void setAssignedDate(String assignedDate) {
		this.assignedDate = assignedDate;
	}

	public String getAdjuster_FirstName() {
		return adjuster_FirstName;
	}

	public void setAdjuster_FirstName(String adjuster_FirstName) {
		this.adjuster_FirstName = adjuster_FirstName;
	}

	public String getAdjuster_LastName() {
		return adjuster_LastName;
	}

	public void setAdjuster_LastName(String adjuster_LastName) {
		this.adjuster_LastName = adjuster_LastName;
	}

	public String getAdjuster_PhoneNumber() {
		return adjuster_PhoneNumber;
	}

	public void setAdjuster_PhoneNumber(String adjuster_PhoneNumber) {
		this.adjuster_PhoneNumber = adjuster_PhoneNumber;
	}

	public String getAdjuster_Email() {
		return adjuster_Email;
	}

	public void setAdjuster_Email(String adjuster_Email) {
		this.adjuster_Email = adjuster_Email;
	}

	@Override
	public String toString() {
		return "AdjusterClaimModel [adjusterId=" + adjusterId + ", claimId=" + claimId + ", adjusterCompany="
				+ adjusterCompany + ", adjusterAddress=" + adjusterAddress + ", expertise=" + expertise
				+ ", assignedDate=" + assignedDate + ", adjuster_FirstName=" + adjuster_FirstName
				+ ", adjuster_LastName=" + adjuster_LastName + ", adjuster_PhoneNumber=" + adjuster_PhoneNumber
				+ ", adjuster_Email=" + adjuster_Email + "]";
	}

}
