package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class TPRRSProductsModel {
List<TPRRSProductsInnerModel> products = new ArrayList<TPRRSProductsInnerModel>();

public List<TPRRSProductsInnerModel> getProducts() {
	return products;
}

public void setProducts(List<TPRRSProductsInnerModel> products) {
	this.products = products;
}

}
