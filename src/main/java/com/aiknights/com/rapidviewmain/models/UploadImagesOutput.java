package com.aiknights.com.rapidviewmain.models;

public class UploadImagesOutput {
	
	private long uniqueIdForImage;
	private String imageId;
	private String imageUrl;
	private String shingletype;
	private String sourceImageName;
	
	public UploadImagesOutput() {
		super();
	}
	
	public long getUniqueIdForImage() {
		return uniqueIdForImage;
	}

	public void setUniqueIdForImage(long uniqueIdForImage) {
		this.uniqueIdForImage = uniqueIdForImage;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getShingletype() {
		return shingletype;
	}

	public void setShingletype(String shingletype) {
		this.shingletype = shingletype;
	}

	public String getSourceImageName() {
		return sourceImageName;
	}

	public void setSourceImageName(String sourceImageName) {
		this.sourceImageName = sourceImageName;
	}

	@Override
	public String toString() {
		return "UploadImagesOutput [uniqueIdForImage=" + uniqueIdForImage + ", imageId=" + imageId + ", imageUrl="
				+ imageUrl + ", shingletype=" + shingletype + ", sourceImageName=" + sourceImageName + "]";
	}

	





	
	

}
