package com.aiknights.com.rapidviewmain.models;

public class PolicyCheckCsvModel {

	private String policy_id;
	
	private String policyNumber;

	private String insuranceCompany;

	private String startDate;

	private String endDate;

	private String policyType;

	private String policyCoverage;

	// need to add mandatory
	private String endorsementId;

	//private String deductibleAmount;

	private String mortgageCompany;

	private String policyLimit;

	private String CoverageAmt;

	private String policyStatus;

	private String PolicyLastUpdateD;

	private String priorClaims;

	private String deductible;
	
	
	

	public String getPolicy_id() {
		return policy_id;
	}

	public void setPolicy_id(String policy_id) {
		this.policy_id = policy_id;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public String getPolicyCoverage() {
		return policyCoverage;
	}

	public void setPolicyCoverage(String policyCoverage) {
		this.policyCoverage = policyCoverage;
	}

	public String getEndorsementId() {
		return endorsementId;
	}

	public void setEndorsementId(String endorsementId) {
		this.endorsementId = endorsementId;
	}

	

	public String getMortgageCompany() {
		return mortgageCompany;
	}

	public void setMortgageCompany(String mortgageCompany) {
		this.mortgageCompany = mortgageCompany;
	}

	public String getPolicyLimit() {
		return policyLimit;
	}

	public void setPolicyLimit(String policyLimit) {
		this.policyLimit = policyLimit;
	}

	public String getCoverageAmt() {
		return CoverageAmt;
	}

	public void setCoverageAmt(String coverageAmt) {
		CoverageAmt = coverageAmt;
	}

	public String getPolicyStatus() {
		return policyStatus;
	}

	public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}

	public String getPolicyLastUpdateD() {
		return PolicyLastUpdateD;
	}

	public void setPolicyLastUpdateD(String policyLastUpdateD) {
		PolicyLastUpdateD = policyLastUpdateD;
	}

	public String getPriorClaims() {
		return priorClaims;
	}

	public void setPriorClaims(String priorClaims) {
		this.priorClaims = priorClaims;
	}

	public String getDeductible() {
		return deductible;
	}

	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}
	

	@Override
	public String toString() {
		return "PolicyCheckCsvModel [policy_id=" + policy_id + ", policyNumber=" + policyNumber + ", insuranceCompany="
				+ insuranceCompany + ", startDate=" + startDate + ", endDate=" + endDate + ", policyType=" + policyType
				+ ", policyCoverage=" + policyCoverage + ", endorsementId=" + endorsementId + ", mortgageCompany="
				+ mortgageCompany + ", policyLimit=" + policyLimit + ", CoverageAmt=" + CoverageAmt + ", policyStatus="
				+ policyStatus + ", PolicyLastUpdateD=" + PolicyLastUpdateD + ", priorClaims=" + priorClaims
				+ ", deductible=" + deductible + "]";
	}
	
	
	

}
