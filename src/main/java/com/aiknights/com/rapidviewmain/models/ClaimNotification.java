package com.aiknights.com.rapidviewmain.models;

public class ClaimNotification extends PolicyCheckNotification {

	private String claimId;
	private String claimantFirstName;
	private String claimantLastName;
	private String email;
	private String phoneNumber;
	private String address;
	private String city;
	private String state;
	private int zipCode;
	private String policyNumber;
	private String incidentType;
	private String incidentDate;
	private String incidentTime;
	private String claimDate;
	private int claimAmount;
	private ClaimStatus claimstatus;
	private String insurance_company;
	// need to be removed, Just placed for UI representation
	private int overAllRisk;
	private String claimDescription;

	public String getClaimId() {
		return claimId;
	}

	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}

	public String getClaimantFirstName() {
		return claimantFirstName;
	}

	public void setClaimantFirstName(String claimantFirstName) {
		this.claimantFirstName = claimantFirstName;
	}

	public String getClaimantLastName() {
		return claimantLastName;
	}

	public void setClaimantLastName(String claimantLastName) {
		this.claimantLastName = claimantLastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getIncidentType() {
		return incidentType;
	}

	public void setIncidentType(String incidentType) {
		this.incidentType = incidentType;
	}

	public String getIncidentDate() {
		return incidentDate;
	}

	public void setIncidentDate(String incidentDate) {
		this.incidentDate = incidentDate;
	}

	public String getIncidentTime() {
		return incidentTime;
	}

	public void setIncidentTime(String incidentTime) {
		this.incidentTime = incidentTime;
	}

	public String getClaimDate() {
		return claimDate;
	}

	public void setClaimDate(String claimDate) {
		this.claimDate = claimDate;
	}

	public int getClaimAmount() {
		return claimAmount;
	}

	public void setClaimAmount(int claimAmount) {
		this.claimAmount = claimAmount;
	}

	public ClaimStatus getClaimstatus() {
		return claimstatus;
	}

	public void setClaimstatus(ClaimStatus claimstatus) {
		this.claimstatus = claimstatus;
	}

	public String getInsurance_company() {
		return insurance_company;
	}

	public void setInsurance_company(String insurance_company) {
		this.insurance_company = insurance_company;
	}

	public int getOverAllRisk() {
		return overAllRisk;
	}

	public void setOverAllRisk(int overAllRisk) {
		this.overAllRisk = overAllRisk;
	}

	public String getClaimDescription() {
		return claimDescription;
	}

	public void setClaimDescription(String claimDescription) {
		this.claimDescription = claimDescription;
	}

	@Override
	public String toString() {
		return "ClaimNotification [claimId=" + claimId + ", claimantFirstName=" + claimantFirstName
				+ ", claimantLastName=" + claimantLastName + ", email=" + email + ", phoneNumber=" + phoneNumber
				+ ", address=" + address + ", city=" + city + ", state=" + state + ", zipCode=" + zipCode
				+ ", policyNumber=" + policyNumber + ", incidentType=" + incidentType + ", incidentDate=" + incidentDate
				+ ", incidentTime=" + incidentTime + ", claimDate=" + claimDate + ", claimAmount=" + claimAmount
				+ ", claimstatus=" + claimstatus + ", insurance_company=" + insurance_company + ", overAllRisk="
				+ overAllRisk + ", getClaimId()=" + getClaimId() + ", getClaimantFirstName()=" + getClaimantFirstName()
				+ ", getClaimantLastName()=" + getClaimantLastName() + ", getEmail()=" + getEmail()
				+ ", getPhoneNumber()=" + getPhoneNumber() + ", getAddress()=" + getAddress() + ", getCity()="
				+ getCity() + ", getState()=" + getState() + ", getZipCode()=" + getZipCode() + ", getPolicyNumber()="
				+ getPolicyNumber() + ", getIncidentType()=" + getIncidentType() + ", getIncidentDate()="
				+ getIncidentDate() + ", getIncidentTime()=" + getIncidentTime() + ", getClaimDate()=" + getClaimDate()
				+ ", getClaimAmount()=" + getClaimAmount() + ", getClaimstatus()=" + getClaimstatus()
				+ ", getInsurance_company()=" + getInsurance_company() + ", getOverAllRisk()=" + getOverAllRisk()
				+ ", getInsuranceCompany()=" + getInsuranceCompany() + ", getStartDate()=" + getStartDate()
				+ ", getEndDate()=" + getEndDate() + ", getPolicyType()=" + getPolicyType() + ", getPolicyCoverage()="
				+ getPolicyCoverage() + ", getEndorsementId()=" + getEndorsementId() + ", getDeductibleAmount()="
				+ getDeductibleAmount() + ", getMortgageCompany()=" + getMortgageCompany() + ",getCoverageAmt()="
				+ getCoverageAmt() + ", getPolicyLimit()=" + getPolicyLimit() + ", getPolicyStatus()="
				+ getPolicyStatus() + ", getPolicyLastUpdateDate()=" + getPolicyLastUpdateDate() + ",getPriorClaims()="
				+ getPriorClaims() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + "]";
	}

}
