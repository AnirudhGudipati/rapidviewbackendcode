package com.aiknights.com.rapidviewmain.models;

import com.opencsv.bean.CsvBindByName;

/**
 * @author
 *
 */
public class ClaimInjectorCsv {

	@CsvBindByName(column = "claimId")
	private String claimId;

	@CsvBindByName(column = "claim_Status")
	private String claim_Status;

	@CsvBindByName(column = "claimantFirstName")
	private String claimantFirstName;

	@CsvBindByName(column = "claimantLastName")
	private String claimantLastName;

	@CsvBindByName(column = "email")
	private String email;
	@CsvBindByName(column = "phoneNumber")
	private String phoneNumber;

	@CsvBindByName(column = "propertyAddress")
	private String propertyAddress;

	@CsvBindByName(column = "City")
	private String city;

	@CsvBindByName(column = "State")
	private String state;

	@CsvBindByName(column = "zipCode")
	private String zipCode;

	@CsvBindByName(column = "incidentType")
	private String incidentType;

	@CsvBindByName(column = "incidentDate")
	private String incidentDate;

	@CsvBindByName(column = "incidentTime")
	private String incidentTime;

	@CsvBindByName(column = "claimDate")
	private String claimDate;

	@CsvBindByName(column = "policyNumber")
	private String policyNumber;

	@CsvBindByName(column = "insuranceCompany")
	private String insuranceCompany;

	@CsvBindByName(column = "policyStartDate")
	private String policyStartDate;

	@CsvBindByName(column = "policyEndDate")
	private String policyEndDate;

	@CsvBindByName(column = "policyCoverage")
	private String policyCoverage;

	@CsvBindByName(column = "deductibleAmount")
	private String deductibleAmount;

	@CsvBindByName(column = "policyLimitAmount")
	private String policyLimitAmount;

	@CsvBindByName(column = "CoverageAmt")
	private String CoverageAmt;

	@CsvBindByName(column = "policyStatus")
	private String policyStatus;

	@CsvBindByName(column = "policyLastUpdateDate")
	private String policyLastUpdateDate;

	@CsvBindByName(column = "priorClaims")
	private String priorClaims;

	@CsvBindByName(column = "endorsement_type")
	private String endorsement_type;

	@CsvBindByName(column = "Endorsment_Type_Desc")
	private String endorsement_type_desc;

	@CsvBindByName(column = "Property_Type")
	private String Property_Type;

	@CsvBindByName(column = "Property_Purchase_Date")
	private String Property_Purchase_Date;

	@CsvBindByName(column = "Adjuster_FName")
	private String Adjuster_FName;

	@CsvBindByName(column = "Adjuster_ID")
	private String Adjuster_ID;

	@CsvBindByName(column = "Adjuster_LName")
	private String Adjuster_LName;

	@CsvBindByName(column = "Adjuster_phone")
	private String Adjuster_phone;

	@CsvBindByName(column = "Adjuster_email")
	private String Adjuster_email;

	@CsvBindByName(column = "claimDescription")
	private String claimDescription;

	private String InjectorStatus;

	private String Reason;

	public String getClaimId() {
		return claimId;
	}

	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}

	public String getClaim_Status() {
		return claim_Status;
	}

	public void setClaim_Status(String claim_Status) {
		this.claim_Status = claim_Status;
	}

	public String getClaimantFirstName() {
		return claimantFirstName;
	}

	public void setClaimantFirstName(String claimantFirstName) {
		this.claimantFirstName = claimantFirstName;
	}

	public String getClaimantLastName() {
		return claimantLastName;
	}

	public void setClaimantLastName(String claimantLastName) {
		this.claimantLastName = claimantLastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPropertyAddress() {
		return propertyAddress;
	}

	public void setPropertyAddress(String propertyAddress) {
		this.propertyAddress = propertyAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getIncidentType() {
		return incidentType;
	}

	public void setIncidentType(String incidentType) {
		this.incidentType = incidentType;
	}

	public String getIncidentDate() {
		return incidentDate;
	}

	public void setIncidentDate(String incidentDate) {
		this.incidentDate = incidentDate;
	}

	public String getIncidentTime() {
		return incidentTime;
	}

	public void setIncidentTime(String incidentTime) {
		this.incidentTime = incidentTime;
	}

	public String getClaimDate() {
		return claimDate;
	}

	public void setClaimDate(String claimDate) {
		this.claimDate = claimDate;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public String getPolicyStartDate() {
		return policyStartDate;
	}

	public void setPolicyStartDate(String policyStartDate) {
		this.policyStartDate = policyStartDate;
	}

	public String getPolicyEndDate() {
		return policyEndDate;
	}

	public void setPolicyEndDate(String policyEndDate) {
		this.policyEndDate = policyEndDate;
	}

	

	public String getPolicyCoverage() {
		return policyCoverage;
	}

	public void setPolicyCoverage(String policyCoverage) {
		this.policyCoverage = policyCoverage;
	}

	public String getDeductibleAmount() {
		return deductibleAmount;
	}

	public void setDeductibleAmount(String deductibleAmount) {
		this.deductibleAmount = deductibleAmount;
	}

	public String getPolicyLimitAmount() {
		return policyLimitAmount;
	}

	public void setPolicyLimitAmount(String policyLimitAmount) {
		this.policyLimitAmount = policyLimitAmount;
	}

	public String getCoverageAmt() {
		return CoverageAmt;
	}

	public void setCoverageAmt(String coverageAmt) {
		CoverageAmt = coverageAmt;
	}

	public String getPolicyStatus() {
		return policyStatus;
	}

	public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}

	public String getPolicyLastUpdateDate() {
		return policyLastUpdateDate;
	}

	public void setPolicyLastUpdateDate(String policyLastUpdateDate) {
		this.policyLastUpdateDate = policyLastUpdateDate;
	}

	public String getPriorClaims() {
		return priorClaims;
	}

	public void setPriorClaims(String priorClaims) {
		this.priorClaims = priorClaims;
	}

	public String getEndorsement_type() {
		return endorsement_type;
	}

	public void setEndorsement_type(String endorsement_type) {
		this.endorsement_type = endorsement_type;
	}

	public String getEndorsement_type_desc() {
		return endorsement_type_desc;
	}

	public void setEndorsement_type_desc(String endorsement_type_desc) {
		this.endorsement_type_desc = endorsement_type_desc;
	}

	public String getProperty_Type() {
		return Property_Type;
	}

	public void setProperty_Type(String property_Type) {
		Property_Type = property_Type;
	}

	public String getProperty_Purchase_Date() {
		return Property_Purchase_Date;
	}

	public void setProperty_Purchase_Date(String property_Purchase_Date) {
		Property_Purchase_Date = property_Purchase_Date;
	}

	public String getAdjuster_FName() {
		return Adjuster_FName;
	}

	public void setAdjuster_FName(String adjuster_FName) {
		Adjuster_FName = adjuster_FName;
	}

	public String getAdjuster_ID() {
		return Adjuster_ID;
	}

	public void setAdjuster_ID(String adjuster_ID) {
		Adjuster_ID = adjuster_ID;
	}

	public String getAdjuster_LName() {
		return Adjuster_LName;
	}

	public void setAdjuster_LName(String adjuster_LName) {
		Adjuster_LName = adjuster_LName;
	}

	public String getAdjuster_phone() {
		return Adjuster_phone;
	}

	public void setAdjuster_phone(String adjuster_phone) {
		Adjuster_phone = adjuster_phone;
	}

	public String getAdjuster_email() {
		return Adjuster_email;
	}

	public void setAdjuster_email(String adjuster_email) {
		Adjuster_email = adjuster_email;
	}

	public String getClaimDescription() {
		return claimDescription;
	}

	public void setClaimDescription(String claimDescription) {
		this.claimDescription = claimDescription;
	}

	public String getInjectorStatus() {
		return InjectorStatus;
	}

	public void setInjectorStatus(String injectorStatus) {
		InjectorStatus = injectorStatus;
	}

	public String getReason() {
		return Reason;
	}

	public void setReason(String reason) {
		Reason = reason;
	}

	@Override
	public String toString() {
		return "ClaimInjectorCsv [claimId=" + claimId + ", claim_Status=" + claim_Status + ", claimantFirstName="
				+ claimantFirstName + ", claimantLastName=" + claimantLastName + ", email=" + email + ", phoneNumber="
				+ phoneNumber + ", propertyAddress=" + propertyAddress + ", city=" + city + ", state=" + state
				+ ", zipCode=" + zipCode + ", incidentType=" + incidentType + ", incidentDate=" + incidentDate
				+ ", incidentTime=" + incidentTime + ", claimDate=" + claimDate + ", policyNumber=" + policyNumber
				+ ", insuranceCompany=" + insuranceCompany + ", policyStartDate=" + policyStartDate + ", policyEndDate="
				+ policyEndDate + ", policyCoverage=" + policyCoverage + ", deductibleAmount=" + deductibleAmount
				+ ", policyLimitAmount=" + policyLimitAmount + ", CoverageAmt=" + CoverageAmt + ", policyStatus="
				+ policyStatus + ", policyLastUpdateDate=" + policyLastUpdateDate + ", priorClaims=" + priorClaims
				+ ", endorsement_type=" + endorsement_type + ", endorsement_type_desc=" + endorsement_type_desc
				+ ", Property_Type=" + Property_Type + ", Property_Purchase_Date=" + Property_Purchase_Date
				+ ", Adjuster_FName=" + Adjuster_FName + ", Adjuster_ID=" + Adjuster_ID + ", Adjuster_LName="
				+ Adjuster_LName + ", Adjuster_phone=" + Adjuster_phone + ", Adjuster_email=" + Adjuster_email
				+ ", claimDescription=" + claimDescription + ", InjectorStatus=" + InjectorStatus + ", Reason=" + Reason
				+ "]";
	}

}
