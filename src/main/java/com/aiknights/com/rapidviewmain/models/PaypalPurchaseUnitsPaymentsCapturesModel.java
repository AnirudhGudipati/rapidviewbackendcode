package com.aiknights.com.rapidviewmain.models;

import java.io.Serializable;

public class PaypalPurchaseUnitsPaymentsCapturesModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2534319859322860987L;

	String status;

	String id;
	String final_capture;
	String create_time;
	String update_time;
	PaypalPurchaseUnitsAmountModel amount;
	// PaypalPurchaseUnitsPaymentsCapturesSellerProtectionModel seller_protection;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFinal_capture() {
		return final_capture;
	}

	public void setFinal_capture(String final_capture) {
		this.final_capture = final_capture;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public String getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

	public PaypalPurchaseUnitsAmountModel getAmount() {
		return amount;
	}

	public void setAmount(PaypalPurchaseUnitsAmountModel amount) {
		this.amount = amount;
	}

}
