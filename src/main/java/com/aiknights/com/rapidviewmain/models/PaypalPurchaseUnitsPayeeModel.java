package com.aiknights.com.rapidviewmain.models;

import java.io.Serializable;

public class PaypalPurchaseUnitsPayeeModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7340840566140111850L;
	String email_address;
	String merchant_id;

	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public String getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

}
