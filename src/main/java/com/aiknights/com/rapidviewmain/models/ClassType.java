package com.aiknights.com.rapidviewmain.models;

public enum ClassType {
	
	HAIL(3),WIND(2),NON_STORM(1),NON_CONCLUDED(-1);
	
	int priority;
	
   ClassType(int priority) {
	   this.priority = priority;
		
	}

   public int getPriority() {
	   return priority;
   }
}
