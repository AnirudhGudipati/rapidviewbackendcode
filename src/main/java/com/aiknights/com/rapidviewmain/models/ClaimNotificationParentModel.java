package com.aiknights.com.rapidviewmain.models;

public class ClaimNotificationParentModel {

	ClaimNotification claim;

	public ClaimNotification getClaim() {
		return claim;
	}

	public void setClaim(ClaimNotification claim) {
		this.claim = claim;
	}
	
}
