package com.aiknights.com.rapidviewmain.models;

public class NotificationAlterInputPayloadModel {

	String 	userId;
	String notification_Object_ID;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getNotification_Object_ID() {
		return notification_Object_ID;
	}
	public void setNotification_Object_ID(String notification_Object_ID) {
		this.notification_Object_ID = notification_Object_ID;
	}
	@Override
	public String toString() {
		return "NotificationAcknowledgementModel [userId=" + userId + ", notification_Object_ID="
				+ notification_Object_ID + "]";
	}
	
}
