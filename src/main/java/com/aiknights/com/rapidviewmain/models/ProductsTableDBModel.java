package com.aiknights.com.rapidviewmain.models;

public class ProductsTableDBModel {
	String ProductID;
	String ProductName;
	String ProductTableName;
	String SubscriptionPropertiesTableName;

	public String getProductID() {
		return ProductID;
	}

	public void setProductID(String productID) {
		ProductID = productID;
	}

	public String getProductName() {
		return ProductName;
	}

	public void setProductName(String productName) {
		ProductName = productName;
	}

	public String getProductTableName() {
		return ProductTableName;
	}

	public void setProductTableName(String productTableName) {
		ProductTableName = productTableName;
	}

	public String getSubscriptionPropertiesTableName() {
		return SubscriptionPropertiesTableName;
	}

	public void setSubscriptionPropertiesTableName(String subscriptionPropertiesTableName) {
		SubscriptionPropertiesTableName = subscriptionPropertiesTableName;
	}

	@Override
	public String toString() {
		return "ProductsTableDBModel [ProductID=" + ProductID + ", ProductName=" + ProductName + ", ProductTableName="
				+ ProductTableName + ", SubscriptionPropertiesTableName=" + SubscriptionPropertiesTableName + "]";
	}

}
