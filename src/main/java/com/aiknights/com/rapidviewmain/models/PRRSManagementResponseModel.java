package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class PRRSManagementResponseModel {
	public List<PRRSManagementProductsModel> products = new ArrayList<PRRSManagementProductsModel>();

	public List<PRRSManagementProductsModel> getProducts() {
		return products;
	}

	public void setProducts(List<PRRSManagementProductsModel> products) {
		this.products = products;
	}

}
