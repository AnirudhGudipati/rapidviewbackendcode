package com.aiknights.com.rapidviewmain.models;

import java.io.Serializable;

public class PaypalPurchaseUnitsModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5681066125614308078L;
	String reference_id;
	String soft_descriptor;
	PaypalPurchaseUnitsAmountModel amount;
	PaypalPurchaseUnitsPayeeModel payee;
	PaypalPurchaseUnitsShippingModel shipping;
	PaypalPurchaseUnitsPaymentsModel payments;

	public String getReference_id() {
		return reference_id;
	}

	public void setReference_id(String reference_id) {
		this.reference_id = reference_id;
	}

	public String getSoft_descriptor() {
		return soft_descriptor;
	}

	public void setSoft_descriptor(String soft_descriptor) {
		this.soft_descriptor = soft_descriptor;
	}

	public PaypalPurchaseUnitsAmountModel getAmount() {
		return amount;
	}

	public void setAmount(PaypalPurchaseUnitsAmountModel amount) {
		this.amount = amount;
	}

	public PaypalPurchaseUnitsPayeeModel getPayee() {
		return payee;
	}

	public void setPayee(PaypalPurchaseUnitsPayeeModel payee) {
		this.payee = payee;
	}

	public PaypalPurchaseUnitsShippingModel getShipping() {
		return shipping;
	}

	public void setShipping(PaypalPurchaseUnitsShippingModel shipping) {
		this.shipping = shipping;
	}

	public PaypalPurchaseUnitsPaymentsModel getPayments() {
		return payments;
	}

	public void setPayments(PaypalPurchaseUnitsPaymentsModel payments) {
		this.payments = payments;
	}

}
