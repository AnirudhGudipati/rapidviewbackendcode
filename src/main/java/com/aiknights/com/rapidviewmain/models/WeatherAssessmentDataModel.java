package com.aiknights.com.rapidviewmain.models;

public class WeatherAssessmentDataModel {
	String reportId;
	String userid;
	String policystartdate;
	String claimdate;
	String claimid;
	String carriername;
	String ownername;
	String address;
	String Report_Received_Date;
	String reportProcessedDate;
	String stagingstatus;
	String reportType;
	String latitude;
	String longitude;

	public String getReportId() {
		return reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getPolicystartdate() {
		return policystartdate;
	}

	public void setPolicystartdate(String policystartdate) {
		this.policystartdate = policystartdate;
	}

	public String getClaimdate() {
		return claimdate;
	}

	public void setClaimdate(String claimdate) {
		this.claimdate = claimdate;
	}

	public String getClaimid() {
		return claimid;
	}

	public void setClaimid(String claimid) {
		this.claimid = claimid;
	}

	public String getCarriername() {
		return carriername;
	}

	public void setCarriername(String carriername) {
		this.carriername = carriername;
	}

	public String getOwnername() {
		return ownername;
	}

	public void setOwnername(String ownername) {
		this.ownername = ownername;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getReport_Received_Date() {
		return Report_Received_Date;
	}

	public void setReport_Received_Date(String report_Received_Date) {
		Report_Received_Date = report_Received_Date;
	}

	public String getReportProcessedDate() {
		return reportProcessedDate;
	}

	public void setReportProcessedDate(String reportProcessedDate) {
		this.reportProcessedDate = reportProcessedDate;
	}

	public String getStagingstatus() {
		return stagingstatus;
	}

	public void setStagingstatus(String stagingstatus) {
		this.stagingstatus = stagingstatus;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

}
