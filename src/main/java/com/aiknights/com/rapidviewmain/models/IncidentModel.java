package com.aiknights.com.rapidviewmain.models;

public class IncidentModel {

	String sourceImageId;
	String sourceImageUrl;
	String sourceImageName;
	String analysedImageUrl;
	String analysedImageId;
	String assessmentBlurrinessError;
	String imageName;
	String assessmentScaleError;
	String assessmentStatus;
	String classType;
	String detectionScore;
	String damageSize;
	String shingleType;
	public String getSourceImageId() {
		return sourceImageId;
	}
	public void setSourceImageId(String sourceImageId) {
		this.sourceImageId = sourceImageId;
	}
	public String getSourceImageUrl() {
		return sourceImageUrl;
	}
	public void setSourceImageUrl(String sourceImageUrl) {
		this.sourceImageUrl = sourceImageUrl;
	}
	public String getSourceImageName() {
		return sourceImageName;
	}
	public void setSourceImageName(String sourceImageName) {
		this.sourceImageName = sourceImageName;
	}
	public String getAnalysedImageUrl() {
		return analysedImageUrl;
	}
	public void setAnalysedImageUrl(String analysedImageUrl) {
		this.analysedImageUrl = analysedImageUrl;
	}
	public String getAnalysedImageId() {
		return analysedImageId;
	}
	public void setAnalysedImageId(String analysedImageId) {
		this.analysedImageId = analysedImageId;
	}
	public String getAssessmentBlurrinessError() {
		return assessmentBlurrinessError;
	}
	public void setAssessmentBlurrinessError(String assessmentBlurrinessError) {
		this.assessmentBlurrinessError = assessmentBlurrinessError;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getAssessmentScaleError() {
		return assessmentScaleError;
	}
	public void setAssessmentScaleError(String assessmentScaleError) {
		this.assessmentScaleError = assessmentScaleError;
	}
	public String getAssessmentStatus() {
		return assessmentStatus;
	}
	public void setAssessmentStatus(String assessmentStatus) {
		this.assessmentStatus = assessmentStatus;
	}
	public String getClassType() {
		return classType;
	}
	public void setClassType(String classType) {
		this.classType = classType;
	}
	public String getDetectionScore() {
		return detectionScore;
	}
	public void setDetectionScore(String detectionScore) {
		this.detectionScore = detectionScore;
	}
	public String getDamageSize() {
		return damageSize;
	}
	public void setDamageSize(String damageSize) {
		this.damageSize = damageSize;
	}
	public String getShingleType() {
		return shingleType;
	}
	public void setShingleType(String shingleType) {
		this.shingleType = shingleType;
	}
	@Override
	public String toString() {
		return "{\"damageSize\":\"" + damageSize + "\"}";
	}
	
}