package com.aiknights.com.rapidviewmain.models;

public class WorkOrderDetailsResponseModel implements Comparable<WorkOrderDetailsResponseModel> {
	String workOrderID;
	String orderStatus;
	String assignedTo;
	String assignedBy;
	String claimID;
	String adjusterEmail;
	String claimantMobile;
	String propertyAddress;
	String flightPlanID;
	String notes;
	String flightPlanName;
	String expectedStartDate;
	String expectedEndDate;
	String creationDate;
	String workOrderApprovalStatus;
	String wOReopenComment;
	String wOReopenCount;
	String Claimant_FN;
	String Claimant_LN;
	String Claimant_Email;
	String Meeting_link;
	String PilotEmail;
	
	
	public String getPilotEmail() {
		return PilotEmail;
	}

	public void setPilotEmail(String pilotEmail) {
		PilotEmail = pilotEmail;
	}

	public String getWorkOrderID() {
		return workOrderID;
	}

	public void setWorkOrderID(String workOrderID) {
		this.workOrderID = workOrderID;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getAssignedBy() {
		return assignedBy;
	}

	public void setAssignedBy(String assignedBy) {
		this.assignedBy = assignedBy;
	}

	public String getClaimID() {
		return claimID;
	}

	public void setClaimID(String claimID) {
		this.claimID = claimID;
	}

	public String getAdjusterEmail() {
		return adjusterEmail;
	}

	public void setAdjusterEmail(String adjusterEmail) {
		this.adjusterEmail = adjusterEmail;
	}

	public String getClaimantMobile() {
		return claimantMobile;
	}

	public void setClaimantMobile(String claimantMobile) {
		this.claimantMobile = claimantMobile;
	}

	public String getPropertyAddress() {
		return propertyAddress;
	}

	public void setPropertyAddress(String propertyAddress) {
		this.propertyAddress = propertyAddress;
	}

	public String getFlightPlanID() {
		return flightPlanID;
	}

	public void setFlightPlanID(String flightPlanID) {
		this.flightPlanID = flightPlanID;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getFlightPlanName() {
		return flightPlanName;
	}

	public void setFlightPlanName(String flightPlanName) {
		this.flightPlanName = flightPlanName;
	}

	public String getExpectedStartDate() {
		return expectedStartDate;
	}

	public void setExpectedStartDate(String expectedStartDate) {
		this.expectedStartDate = expectedStartDate;
	}

	public String getExpectedEndDate() {
		return expectedEndDate;
	}

	public void setExpectedEndDate(String expectedEndDate) {
		this.expectedEndDate = expectedEndDate;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getWorkOrderApprovalStatus() {
		return workOrderApprovalStatus;
	}

	public void setWorkOrderApprovalStatus(String workOrderApprovalStatus) {
		this.workOrderApprovalStatus = workOrderApprovalStatus;
	}

	public String getwOReopenComment() {
		return wOReopenComment;
	}

	public void setwOReopenComment(String wOReopenComment) {
		this.wOReopenComment = wOReopenComment;
	}

	public String getwOReopenCount() {
		return wOReopenCount;
	}

	public void setwOReopenCount(String wOReopenCount) {
		this.wOReopenCount = wOReopenCount;
	}

	public String getClaimant_FN() {
		return Claimant_FN;
	}

	public void setClaimant_FN(String claimant_FN) {
		Claimant_FN = claimant_FN;
	}

	public String getClaimant_LN() {
		return Claimant_LN;
	}

	public void setClaimant_LN(String claimant_LN) {
		Claimant_LN = claimant_LN;
	}

	public String getClaimant_Email() {
		return Claimant_Email;
	}

	public void setClaimant_Email(String claimant_Email) {
		Claimant_Email = claimant_Email;
	}

	public String getMeeting_link() {
		return Meeting_link;
	}

	public void setMeeting_link(String meeting_link) {
		Meeting_link = meeting_link;
	}

	

	@Override
	public String toString() {
		return "WorkOrderDetailsResponseModel [workOrderID=" + workOrderID + ", orderStatus=" + orderStatus
				+ ", assignedTo=" + assignedTo + ", assignedBy=" + assignedBy + ", claimID=" + claimID
				+ ", adjusterEmail=" + adjusterEmail + ", claimantMobile=" + claimantMobile + ", propertyAddress="
				+ propertyAddress + ", flightPlanID=" + flightPlanID + ", notes=" + notes + ", flightPlanName="
				+ flightPlanName + ", expectedStartDate=" + expectedStartDate + ", expectedEndDate=" + expectedEndDate
				+ ", creationDate=" + creationDate + ", workOrderApprovalStatus=" + workOrderApprovalStatus
				+ ", wOReopenComment=" + wOReopenComment + ", wOReopenCount=" + wOReopenCount + ", Claimant_FN="
				+ Claimant_FN + ", Claimant_LN=" + Claimant_LN + ", Claimant_Email=" + Claimant_Email
				+ ", Meeting_link=" + Meeting_link + ", PilotEmail=" + PilotEmail + "]";
	}

	@Override
	public int compareTo(WorkOrderDetailsResponseModel row) {
		return this.expectedEndDate.compareTo(row.getExpectedEndDate());
	}
	
	
}
