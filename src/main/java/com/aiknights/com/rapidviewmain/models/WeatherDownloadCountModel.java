/**
 * 
 */
package com.aiknights.com.rapidviewmain.models;

/**
 * @author ARUN
 *
 */
public class WeatherDownloadCountModel {

	private int WeatherDownloadID;
	private String username;
	private String Weather_Download_MaxCount;
	private String AmountToBe_Paid;

	public int getWeatherDownloadID() {
		return WeatherDownloadID;
	}

	public void setWeatherDownloadID(int weatherDownloadID) {
		WeatherDownloadID = weatherDownloadID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getWeather_Download_MaxCount() {
		return Weather_Download_MaxCount;
	}

	public void setWeather_Download_MaxCount(String weather_Download_MaxCount) {
		Weather_Download_MaxCount = weather_Download_MaxCount;
	}

	public String getAmountToBe_Paid() {
		return AmountToBe_Paid;
	}

	public void setAmountToBe_Paid(String amountToBe_Paid) {
		AmountToBe_Paid = amountToBe_Paid;
	}

	@Override
	public String toString() {
		return "WeatherDownloadCountModel [WeatherDownloadID=" + WeatherDownloadID + ", username=" + username
				+ ", Weather_Download_MaxCount=" + Weather_Download_MaxCount + ", AmountToBe_Paid=" + AmountToBe_Paid
				+ "]";
	}

}
