package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class TPRRSRolesModel {
	String rolename;
	List<TPRRSResourcesModel> resources = new ArrayList<TPRRSResourcesModel>();
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public List<TPRRSResourcesModel> getResources() {
		return resources;
	}
	public void setResources(List<TPRRSResourcesModel> resources) {
		this.resources = resources;
	}
	
}
