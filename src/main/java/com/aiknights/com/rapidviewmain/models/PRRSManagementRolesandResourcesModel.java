package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class PRRSManagementRolesandResourcesModel {
	String roleName;
	List<String> resources = new ArrayList<String>();

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<String> getResources() {
		return resources;
	}

	public void setResources(List<String> resources) {
		this.resources = resources;
	}

}
