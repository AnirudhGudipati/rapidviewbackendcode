package com.aiknights.com.rapidviewmain.models;

public class SelectedPropertiesModel {

	String selectedOption;
	String selectedValue;
	public String getSelectedOption() {
		return selectedOption;
	}
	public void setSelectedOption(String selectedOption) {
		this.selectedOption = selectedOption;
	}
	public String getSelectedValue() {
		return selectedValue;
	}
	public void setSelectedValue(String selectedValue) {
		this.selectedValue = selectedValue;
	}
	@Override
	public String toString() {
		return "SelectedPropertiesModel [selectedOption=" + selectedOption + ", selectedValue=" + selectedValue + "]";
	}
	
}
