package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class WayPointDataModel {
	String wayPointKey;
	String latitude;
	String longitude;
	String altitude;
	String aboveGround;
	String speed;
	String curvesize;
	String heading;
	String pointOfInterest;
	SelectedPropertiesModel gimbal;
	SelectedPropertiesModel interval;
	List<FPActionsModel> actions = new ArrayList<FPActionsModel>();
	
	public List<FPActionsModel> getActions() {
		return actions;
	}
	public void setActions(List<FPActionsModel> actions) {
		this.actions = actions;
	}
	public String getPointOfInterest() {
		return pointOfInterest;
	}
	public void setPointOfInterest(String pointOfInterest) {
		this.pointOfInterest = pointOfInterest;
	}
	public String getWayPointKey() {
		return wayPointKey;
	}
	public void setWayPointKey(String wayPointKey) {
		this.wayPointKey = wayPointKey;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getAltitude() {
		return altitude;
	}
	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}
	public String getAboveGround() {
		return aboveGround;
	}
	public void setAboveGround(String aboveGround) {
		this.aboveGround = aboveGround;
	}
	public String getSpeed() {
		return speed;
	}
	public void setSpeed(String speed) {
		this.speed = speed;
	}
	public String getCurvesize() {
		return curvesize;
	}
	public void setCurvesize(String curvesize) {
		this.curvesize = curvesize;
	}
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public SelectedPropertiesModel getGimbal() {
		return gimbal;
	}
	public void setGimbal(SelectedPropertiesModel gimbal) {
		this.gimbal = gimbal;
	}
	public SelectedPropertiesModel getInterval() {
		return interval;
	}
	public void setInterval(SelectedPropertiesModel interval) {
		this.interval = interval;
	}
	@Override
	public String toString() {
		return "WayPointDataModel [wayPointKey=" + wayPointKey + ", latitude=" + latitude + ", longitude=" + longitude
				+ ", altitude=" + altitude + ", aboveGround=" + aboveGround + ", speed=" + speed + ", curvesize="
				+ curvesize + ", heading=" + heading + ", pointOfInterest=" + pointOfInterest + ", gimbal=" + gimbal
				+ ", interval=" + interval + ", actions=" + actions + "]";
	}

	

	
}
