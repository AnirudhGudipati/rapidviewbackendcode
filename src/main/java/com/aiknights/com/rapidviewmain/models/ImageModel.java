package com.aiknights.com.rapidviewmain.models;

import java.util.Arrays;

public class ImageModel {

	private String name;
	private String type;
	private int userId;
	private byte[] picByte;

	public ImageModel(String name, String type, byte[] picByte) {
		super();
		this.name = name;
		this.type = type;
		this.picByte = picByte;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public byte[] getPicByte() {
		return picByte;
	}

	public void setPicByte(byte[] picByte) {
		this.picByte = picByte;
	}

	@Override
	public String toString() {
		return "ImageModel [name=" + name + ", type=" + type + ", userId=" + userId + ", picByte="
				+ Arrays.toString(picByte) + "]";
	}

}
