package com.aiknights.com.rapidviewmain.models;

public class WayPointActionDBModel {
	String Flight_Action_ID;
	String Way_Point_ID;
	String Action_Number;
	String Action_Option;
	String Action_Value;
	public String getFlight_Action_ID() {
		return Flight_Action_ID;
	}
	public void setFlight_Action_ID(String flight_Action_ID) {
		Flight_Action_ID = flight_Action_ID;
	}
	public String getWay_Point_ID() {
		return Way_Point_ID;
	}
	public void setWay_Point_ID(String way_Point_ID) {
		Way_Point_ID = way_Point_ID;
	}
	public String getAction_Number() {
		return Action_Number;
	}
	public void setAction_Number(String action_Number) {
		Action_Number = action_Number;
	}
	public String getAction_Option() {
		return Action_Option;
	}
	public void setAction_Option(String action_Option) {
		Action_Option = action_Option;
	}
	public String getAction_Value() {
		return Action_Value;
	}
	public void setAction_Value(String action_Value) {
		Action_Value = action_Value;
	}
	
}
