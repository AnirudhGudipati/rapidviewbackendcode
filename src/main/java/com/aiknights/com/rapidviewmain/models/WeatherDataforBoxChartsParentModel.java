package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class WeatherDataforBoxChartsParentModel {

	public List<WeatherDataforBoxChartsModel> weatherData = new ArrayList<WeatherDataforBoxChartsModel>();

	public List<WeatherDataforBoxChartsModel> getWeatherData() {
		return weatherData;
	}

	public void setWeatherData(List<WeatherDataforBoxChartsModel> weatherData) {
		this.weatherData = weatherData;
	}
	
}
