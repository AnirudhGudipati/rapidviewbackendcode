package com.aiknights.com.rapidviewmain.models;

public class EndorsementCsvModel {

	private String endorsement_id;
	private String hail;
	private String wind;
	private String water;
	private String hurricane;
	private String endorsement_type;
	private String endorsement_type_desc;

	public String getEndorsement_id() {
		return endorsement_id;
	}

	public void setEndorsement_id(String endorsement_id) {
		this.endorsement_id = endorsement_id;
	}

	public String getHail() {
		return hail;
	}

	public void setHail(String hail) {
		this.hail = hail;
	}

	public String getWind() {
		return wind;
	}

	public void setWind(String wind) {
		this.wind = wind;
	}

	public String getWater() {
		return water;
	}

	public void setWater(String water) {
		this.water = water;
	}

	public String getHurricane() {
		return hurricane;
	}

	public void setHurricane(String hurricane) {
		this.hurricane = hurricane;
	}

	public String getEndorsement_type() {
		return endorsement_type;
	}

	public void setEndorsement_type(String endorsement_type) {
		this.endorsement_type = endorsement_type;
	}

	public String getEndorsement_type_desc() {
		return endorsement_type_desc;
	}

	public void setEndorsement_type_desc(String endorsement_type_desc) {
		this.endorsement_type_desc = endorsement_type_desc;
	}

	@Override
	public String toString() {
		return "EndorsementCsvModel [endorsement_id=" + endorsement_id + ", hail=" + hail + ", wind=" + wind
				+ ", water=" + water + ", hurricane=" + hurricane + ", endorsement_type=" + endorsement_type
				+ ", endorsement_type_desc=" + endorsement_type_desc + "]";
	}

}
