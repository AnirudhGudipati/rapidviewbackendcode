package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class IncidentParent {
	
	public List<IncidentModel> imageList= new ArrayList<IncidentModel>();
	//public IncidentModel imageList;

	public List<IncidentModel> getImageList() {
		return imageList;
	}

	public void setImageList(List<IncidentModel> imageList) {
		this.imageList = imageList;
	}
	

	
}
