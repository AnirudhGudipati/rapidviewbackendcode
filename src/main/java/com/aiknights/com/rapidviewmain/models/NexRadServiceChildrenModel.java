package com.aiknights.com.rapidviewmain.models;

public class NexRadServiceChildrenModel {

	String event_date;
	String maxsize;
	String prob;
	String sevprob;
	String lat;
	String lon;
	double distance;
	public String getEvent_date() {
		return event_date;
	}
	public void setEvent_date(String event_date) {
		this.event_date = event_date;
	}
	public String getMaxsize() {
		return maxsize;
	}
	public void setMaxsize(String maxsize) {
		this.maxsize = maxsize;
	}
	public String getProb() {
		return prob;
	}
	public void setProb(String prob) {
		this.prob = prob;
	}
	public String getSevprob() {
		return sevprob;
	}
	public void setSevprob(String sevprob) {
		this.sevprob = sevprob;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	@Override
	public String toString() {
		return "NexRadServiceChildrenModel [event_date=" + event_date + ", maxsize=" + maxsize + ", prob=" + prob
				+ ", sevprob=" + sevprob + ", lat=" + lat + ", lon=" + lon + ", distance=" + distance
				+ ", getEvent_date()=" + getEvent_date() + ", getMaxsize()=" + getMaxsize() + ", getProb()=" + getProb()
				+ ", getSevprob()=" + getSevprob() + ", getLat()=" + getLat() + ", getLon()=" + getLon()
				+ ", getDistance()=" + getDistance() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	public NexRadServiceChildrenModel(String event_date, String maxsize, String prob, String sevprob, String lat,
			String lon, double distance) {
		super();
		this.event_date = event_date;
		this.maxsize = maxsize;
		this.prob = prob;
		this.sevprob = sevprob;
		this.lat = lat;
		this.lon = lon;
		this.distance = distance;
	}
	public NexRadServiceChildrenModel() {
		super();
	}
	
}
