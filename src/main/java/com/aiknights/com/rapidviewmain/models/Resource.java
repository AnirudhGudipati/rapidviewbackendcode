package com.aiknights.com.rapidviewmain.models;

public class Resource {

	private int resouceId;
	private String resourceName;
	private String userName;

	public Resource() {
		super();
	}

	public Resource(int resouceId, String resourceName) {
		super();
		this.resouceId = resouceId;
		this.resourceName = resourceName;
	}

	public int getResouceId() {
		return resouceId;
	}

	public void setResouceId(int resouceId) {
		this.resouceId = resouceId;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "Resource [resouceId=" + resouceId + ", resourceName=" + resourceName + ", userName=" + userName + "]";
	}

}
