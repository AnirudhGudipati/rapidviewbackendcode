package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class FlaskServiceModel {
	public List<FlaskServiceChildrenModel> weatherdata= new ArrayList<FlaskServiceChildrenModel>();

	public List<FlaskServiceChildrenModel> getWeatherdata() {
		return weatherdata;
	}

	public void setWeatherdata(List<FlaskServiceChildrenModel> weatherdata) {
		this.weatherdata = weatherdata;
	}


	
}
