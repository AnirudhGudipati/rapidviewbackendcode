package com.aiknights.com.rapidviewmain.models;

public class GenericSubscriptionDetailsModel {
	String Subscription_Type;
	String Weather_Download_MaxCount;
	String AmountToBe_Paid;
	String Number_of_Days;
	String Subscription_Type_For_UI;

	public String getSubscription_Type() {
		return Subscription_Type;
	}

	public void setSubscription_Type(String subscription_Type) {
		Subscription_Type = subscription_Type;
	}

	public String getWeather_Download_MaxCount() {
		return Weather_Download_MaxCount;
	}

	public void setWeather_Download_MaxCount(String weather_Download_MaxCount) {
		Weather_Download_MaxCount = weather_Download_MaxCount;
	}

	public String getAmountToBe_Paid() {
		return AmountToBe_Paid;
	}

	public void setAmountToBe_Paid(String amountToBe_Paid) {
		AmountToBe_Paid = amountToBe_Paid;
	}

	public String getNumber_of_Days() {
		return Number_of_Days;
	}

	public void setNumber_of_Days(String number_of_Days) {
		Number_of_Days = number_of_Days;
	}

	public String getSubscription_Type_For_UI() {
		return Subscription_Type_For_UI;
	}

	public void setSubscription_Type_For_UI(String subscription_Type_For_UI) {
		Subscription_Type_For_UI = subscription_Type_For_UI;
	}

	@Override
	public String toString() {
		return "GenericSubscriptionDetailsModel [Subscription_Type=" + Subscription_Type
				+ ", Weather_Download_MaxCount=" + Weather_Download_MaxCount + ", AmountToBe_Paid=" + AmountToBe_Paid
				+ ", Number_of_Days=" + Number_of_Days + ", Subscription_Type_For_UI=" + Subscription_Type_For_UI + "]";
	}

}
