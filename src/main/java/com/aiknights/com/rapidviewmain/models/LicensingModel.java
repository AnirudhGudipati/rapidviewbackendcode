package com.aiknights.com.rapidviewmain.models;

import java.time.LocalDate;

public class LicensingModel extends Role {

	private String License_ID;
	private String License_Type;
	private LocalDate License_StartDate;
	private LocalDate License_EndDate;
	private String Product_ID;
	private String ProductType;
	private String username;
	private String Membership_Level;

	public String getLicense_ID() {
		return License_ID;
	}

	public void setLicense_ID(String license_ID) {
		License_ID = license_ID;
	}

	public String getLicense_Type() {
		return License_Type;
	}

	public void setLicense_Type(String license_Type) {
		License_Type = license_Type;
	}

	public LocalDate getLicense_StartDate() {
		return License_StartDate;
	}

	public void setLicense_StartDate(LocalDate license_StartDate) {
		License_StartDate = license_StartDate;
	}

	public LocalDate getLicense_EndDate() {
		return License_EndDate;
	}

	public void setLicense_EndDate(LocalDate license_EndDate) {
		License_EndDate = license_EndDate;
	}

	public String getProduct_ID() {
		return Product_ID;
	}

	public void setProduct_ID(String product_ID) {
		Product_ID = product_ID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMembership_Level() {
		return Membership_Level;
	}

	public void setMembership_Level(String membership_Level) {
		Membership_Level = membership_Level;
	}

	public String getProductType() {
		return ProductType;
	}

	public void setProductType(String productType) {
		ProductType = productType;
	}

	@Override
	public String toString() {
		return "LicensingModel [License_ID=" + License_ID + ", License_Type=" + License_Type + ", License_StartDate="
				+ License_StartDate + ", License_EndDate=" + License_EndDate + ", Product_ID=" + Product_ID
				+ ", ProductType=" + ProductType + ", username=" + username + ", Membership_Level=" + Membership_Level
				+ "]";
	}

}
