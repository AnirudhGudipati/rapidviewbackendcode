package com.aiknights.com.rapidviewmain.models;

public enum ShingleType {
	
	THREE_TAB(5.625),ARCHITECTURAL(5.875),HIGH_DEFINATION(5.625);
	
	double shingleValue;
	
	
	 ShingleType(double singleValue) {
		this.shingleValue = singleValue;
	}
	
	public double getShingleValue() {
		return shingleValue;
	}

}
