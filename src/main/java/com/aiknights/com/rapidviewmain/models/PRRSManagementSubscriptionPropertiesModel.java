package com.aiknights.com.rapidviewmain.models;

public class PRRSManagementSubscriptionPropertiesModel {
	String subscriptionType;
	String subscriptionPrice;
	String subscriptionPeriod;
	String DownloadCount;
	String subscriptionName;
	public String getSubscriptionType() {
		return subscriptionType;
	}
	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}
	public String getSubscriptionPrice() {
		return subscriptionPrice;
	}
	public void setSubscriptionPrice(String subscriptionPrice) {
		this.subscriptionPrice = subscriptionPrice;
	}
	public String getSubscriptionPeriod() {
		return subscriptionPeriod;
	}
	public void setSubscriptionPeriod(String subscriptionPeriod) {
		this.subscriptionPeriod = subscriptionPeriod;
	}
	public String getDownloadCount() {
		return DownloadCount;
	}
	public void setDownloadCount(String downloadCount) {
		DownloadCount = downloadCount;
	}
	public String getSubscriptionName() {
		return subscriptionName;
	}
	public void setSubscriptionName(String subscriptionName) {
		this.subscriptionName = subscriptionName;
	}
	@Override
	public String toString() {
		return "PRRSManagementSubscriptionPropertiesModel [subscriptionType=" + subscriptionType
				+ ", subscriptionPrice=" + subscriptionPrice + ", subscriptionPeriod=" + subscriptionPeriod
				+ ", DownloadCount=" + DownloadCount + ", subscriptionName=" + subscriptionName + "]";
	}
}
