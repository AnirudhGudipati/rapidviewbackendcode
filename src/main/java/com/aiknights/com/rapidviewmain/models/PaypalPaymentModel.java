package com.aiknights.com.rapidviewmain.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PaypalPaymentModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5170849423832204161L;
	String create_time;
	String update_time;
	String id;
	String intent;
	String status;
	PaypalPayerModel payer;

	List<PaypalPurchaseUnitsModel> purchase_units = new ArrayList<>();

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public String getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIntent() {
		return intent;
	}

	public void setIntent(String intent) {
		this.intent = intent;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public PaypalPayerModel getPayer() {
		return payer;
	}

	public void setPayer(PaypalPayerModel payer) {
		this.payer = payer;
	}

	public List<PaypalPurchaseUnitsModel> getPurchase_units() {
		return purchase_units;
	}

	public void setPurchase_units(List<PaypalPurchaseUnitsModel> purchase_units) {
		this.purchase_units = purchase_units;
	}

}
