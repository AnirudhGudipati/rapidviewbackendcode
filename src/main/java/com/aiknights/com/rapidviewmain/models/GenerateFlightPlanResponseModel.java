package com.aiknights.com.rapidviewmain.models;

public class GenerateFlightPlanResponseModel {
	String Status;
	String FlightPlanID;
	String ClaimID;
	String FlightPlanName;
	String defaultWorkOrderID;
	String userID;
	
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getDefaultWorkOrderID() {
		return defaultWorkOrderID;
	}
	public void setDefaultWorkOrderID(String defaultWorkOrderID) {
		this.defaultWorkOrderID = defaultWorkOrderID;
	}
	public String getFlightPlanName() {
		return FlightPlanName;
	}
	public void setFlightPlanName(String flightPlanName) {
		FlightPlanName = flightPlanName;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getFlightPlanID() {
		return FlightPlanID;
	}
	public void setFlightPlanID(String flightPlanID) {
		FlightPlanID = flightPlanID;
	}
	public String getClaimID() {
		return ClaimID;
	}
	public void setClaimID(String claimID) {
		ClaimID = claimID;
	}
	
}
