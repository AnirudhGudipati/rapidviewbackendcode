package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class FlightPlanDataModel {
	String claimID;
	String address;
	String flightPlanName;
	String format;
	String userID;
	

	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	List<WayPointDataModel> wayPoints = new ArrayList<WayPointDataModel>();
	public String getClaimID() {
		return claimID;
	}
	public void setClaimID(String claimID) {
		this.claimID = claimID;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getFlightPlanName() {
		return flightPlanName;
	}
	public void setFlightPlanName(String flightPlanName) {
		this.flightPlanName = flightPlanName;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public List<WayPointDataModel> getWayPoints() {
		return wayPoints;
	}
	public void setWayPoints(List<WayPointDataModel> wayPoints) {
		this.wayPoints = wayPoints;
	}
	@Override
	public String toString() {
		return "FlightPlanDataModel [claimID=" + claimID + ", address=" + address + ", flightPlanName=" + flightPlanName
				+ ", format=" + format + ", userID=" + userID + ", wayPoints=" + wayPoints + "]";
	}
	
	
	
}
