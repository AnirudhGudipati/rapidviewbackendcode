package com.aiknights.com.rapidviewmain.models;

public class FlightPlanPropertiesDBModel {
	String Flight_Plan_ID;
	String User_ID;
	String Flight_Plan_Name;
	String Flight_Plan_Version;
	String Flight_Plan_Format;
	public String getFlight_Plan_ID() {
		return Flight_Plan_ID;
	}
	public void setFlight_Plan_ID(String flight_Plan_ID) {
		Flight_Plan_ID = flight_Plan_ID;
	}
	public String getUser_ID() {
		return User_ID;
	}
	public void setUser_ID(String user_ID) {
		User_ID = user_ID;
	}
	public String getFlight_Plan_Name() {
		return Flight_Plan_Name;
	}
	public void setFlight_Plan_Name(String flight_Plan_Name) {
		Flight_Plan_Name = flight_Plan_Name;
	}
	public String getFlight_Plan_Version() {
		return Flight_Plan_Version;
	}
	public void setFlight_Plan_Version(String flight_Plan_Version) {
		Flight_Plan_Version = flight_Plan_Version;
	}
	public String getFlight_Plan_Format() {
		return Flight_Plan_Format;
	}
	public void setFlight_Plan_Format(String flight_Plan_Format) {
		Flight_Plan_Format = flight_Plan_Format;
	}
	
}
