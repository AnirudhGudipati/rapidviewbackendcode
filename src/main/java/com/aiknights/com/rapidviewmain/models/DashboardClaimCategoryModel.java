package com.aiknights.com.rapidviewmain.models;

public class DashboardClaimCategoryModel {

	private String totalclaims;
	private String waterclaims;
	private String hailclaims;
	private String windclaims;
	private String waterclaimspercentage;
	private String hailclaimspercentage;
	private String windclaimspercentage;
	public String getTotalclaims() {
		return totalclaims;
	}
	public void setTotalclaims(String totalclaims) {
		this.totalclaims = totalclaims;
	}
	public String getWaterclaims() {
		return waterclaims;
	}
	public void setWaterclaims(String waterclaims) {
		this.waterclaims = waterclaims;
	}
	public String getHailclaims() {
		return hailclaims;
	}
	public void setHailclaims(String hailclaims) {
		this.hailclaims = hailclaims;
	}
	public String getWindclaims() {
		return windclaims;
	}
	public void setWindclaims(String windclaims) {
		this.windclaims = windclaims;
	}
	public String getWaterclaimspercentage() {
		return waterclaimspercentage;
	}
	public void setWaterclaimspercentage(String waterclaimspercentage) {
		this.waterclaimspercentage = waterclaimspercentage;
	}
	public String getHailclaimspercentage() {
		return hailclaimspercentage;
	}
	public void setHailclaimspercentage(String hailclaimspercentage) {
		this.hailclaimspercentage = hailclaimspercentage;
	}
	public String getWindclaimspercentage() {
		return windclaimspercentage;
	}
	public void setWindclaimspercentage(String windclaimspercentage) {
		this.windclaimspercentage = windclaimspercentage;
	}
	@Override
	public String toString() {
		return "DashboardClaimCategoryModel [totalclaims=" + totalclaims + ", waterclaims=" + waterclaims
				+ ", hailclaims=" + hailclaims + ", windclaims=" + windclaims + ", waterclaimspercentage="
				+ waterclaimspercentage + ", hailclaimspercentage=" + hailclaimspercentage + ", windclaimspercentage="
				+ windclaimspercentage + ", getTotalclaims()=" + getTotalclaims() + ", getWaterclaims()="
				+ getWaterclaims() + ", getHailclaims()=" + getHailclaims() + ", getWindclaims()=" + getWindclaims()
				+ ", getWaterclaimspercentage()=" + getWaterclaimspercentage() + ", getHailclaimspercentage()="
				+ getHailclaimspercentage() + ", getWindclaimspercentage()=" + getWindclaimspercentage()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
	
	
}
