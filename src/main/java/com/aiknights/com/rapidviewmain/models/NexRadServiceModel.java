package com.aiknights.com.rapidviewmain.models;

import java.util.ArrayList;
import java.util.List;

public class NexRadServiceModel {
	public List<NexRadServiceChildrenModel> weatherdata= new ArrayList<NexRadServiceChildrenModel>();

	public List<NexRadServiceChildrenModel> getWeatherdata() {
		return weatherdata;
	}

	public void setWeatherdata(List<NexRadServiceChildrenModel> weatherdata) {
		this.weatherdata = weatherdata;
	}


}
