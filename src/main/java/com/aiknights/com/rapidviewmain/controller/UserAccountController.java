package com.aiknights.com.rapidviewmain.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.aiknights.com.rapidviewmain.DAO.CreateAnAccountRepository;
import com.aiknights.com.rapidviewmain.constants.RapidViewMainConstants;
import com.aiknights.com.rapidviewmain.dto.RoleResultsDTO;
import com.aiknights.com.rapidviewmain.dto.UserDetailsResultsDTO;
import com.aiknights.com.rapidviewmain.models.CreateProfileRequestModel;
import com.aiknights.com.rapidviewmain.models.TokenDBModel;
import com.aiknights.com.rapidviewmain.models.UserDetails;
import com.aiknights.com.rapidviewmain.service.CreateAnAccountService;
import com.aiknights.com.rapidviewmain.service.EmailSenderService;
import com.aiknights.com.rapidviewmain.service.RoleService;
import com.aiknights.com.rapidviewmain.service.TokenDBService;
import com.aiknights.com.rapidviewmain.service.UserDetailsService;

//Added Test 2 CreateAnAccountController.
@RequestMapping("/api")
@RestController
public class UserAccountController {

	@Value("${rapid.super.Company.Name}")
	private String superCompanyName;
	@Value("${rapid.UI.address.URL}")
	private String uiAddressURL;
	@Value("${rapid.BE.address.URL}")
	private String beAddressURL;
	@Value("${spring.mail.username}")
	String frommail;
	@Autowired
	private RoleService roleService;
	@Autowired
	private CreateAnAccountService createAnAccountService;
	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	@Autowired
	UserDetailsService userDetailsService;
	@Autowired
	TokenDBService tokenDBService;
	@Autowired
	CreateAnAccountRepository createAnAccountRepository;

	@Autowired
	private EmailSenderService emailSenderService;
	String Timezone = "GMT";

	@RequestMapping(value = "/createanaccount", method = RequestMethod.POST)
	public ResponseEntity<Map<String, String>> createAnAccount(@RequestBody final Map<String, String> accountDetails) {
		System.out.println("createanaccount");
		final Map<String, String> responseMap = new HashMap<>();
		String emailId = accountDetails.get("emailOfAccount");
		boolean isEmailIdExists = createAnAccountService.findByEmail(accountDetails, namedParameterJdbcTemplate);
		boolean isUserExists = createAnAccountService.findByUserName(accountDetails);

		if (isEmailIdExists) {
			responseMap.put("isEmailIdExists", "This email already exists");
		}
		if (isUserExists) {
			responseMap.put("isUserExists", "This User Name already exists");
		}

		if (isEmailIdExists == false && isUserExists == false) {
			boolean userInsertionStatus = createAnAccountService.insertIntoUsersSetting(accountDetails,
					namedParameterJdbcTemplate);
			UserDetailsResultsDTO userDetailsInsertionStatus = createAnAccountService
					.insertIntoUserDetailsSetting(accountDetails, namedParameterJdbcTemplate);
			RoleResultsDTO roleInsertionStatus = createAnAccountService.insertIntoRoleSetting(accountDetails,
					namedParameterJdbcTemplate);
			if (userInsertionStatus && userDetailsInsertionStatus.getInsertion_status()
					&& roleInsertionStatus.getInsertion_status()) {
				emailVerification(userDetailsInsertionStatus.getUserid_pk(), emailId);
			}

			responseMap.put("accountCreationStatus", "Created");
		}
		return new ResponseEntity<>(responseMap, HttpStatus.OK);
	}

	private void emailVerification(int UserId, String emailId) {

		TokenDBModel confirmationToken = new TokenDBModel(Integer.toString(UserId));
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone(Timezone));
		Date dateo = new Date();
		String CreatedDateTime = df.format(dateo);

		boolean confirmationTokenStatus = tokenDBService.insertNewTokenDetailsToDB(confirmationToken.getTokenID(),
				CreatedDateTime, "0", Integer.toString(UserId));
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(emailId);
		mailMessage.setSubject("Complete Registration!");
		mailMessage.setFrom(frommail);
		mailMessage.setText("To confirm your account, please click here : "
				+ beAddressURL +"/api/confirm-account?token="
				+ confirmationToken.getTokenID());
		emailSenderService.sendEmail(mailMessage);

	}

	@RequestMapping(value = "/createprofile", method = RequestMethod.POST)
	public ResponseEntity<Map<String, String>> createProfile(
			@RequestBody final CreateProfileRequestModel profileDetails) {
		boolean status = false;
		final Map<String, String> responseMap = new HashMap<>();
		String UserName = profileDetails.getUserName();
		UserDetails userdetails = userDetailsService.getUserDetailsByUserName(UserName);
		String emailId = createAnAccountService.findEmailByUserName(UserName);
		// to update/set user details
		status = createAnAccountService.updateUserDetailsByID(userdetails.getUserId(), profileDetails);
		RoleResultsDTO roleInsertionStatus = createAnAccountService.insertIntoRoleSettingProfile(profileDetails,
				namedParameterJdbcTemplate);

		// boolean profileStatus =
		// createAnAccountService.insertIntoResourcesSetting(profileDetails,namedParameterJdbcTemplate);
		boolean licenseStatus = createAnAccountService.insertIntolicenseDetails(profileDetails,
				namedParameterJdbcTemplate);
		boolean weatherAlerts = createAnAccountService.insertIntoweatherAlerts(profileDetails, userdetails.getUserId());
		profileDetails.getPaymentInfo();
		

		if (licenseStatus) {
			responseMap.put("ProfileCreationStatus", "Created");
		} else {
			responseMap.put("ProfileCreationStatus", "Not Created");
		}

		return new ResponseEntity<>(responseMap, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/editProfileAsAdmin", method = RequestMethod.POST)
	public ResponseEntity<Map<String, String>> editProfileAsAdmin(@RequestBody final Map<String, String> data) {
		final Map<String, String> responseMap = new HashMap<>();
		String userID = data.get("userID");
		String adminID = data.get("adminID");
		String ticket_ID = data.get("ticketID");
		String ticket_Notes = data.get("ticketNotes");
		String ticket_Context = RapidViewMainConstants.ADMIN_CONTEXT_EDIT_PROFILE;
		String product = data.get("product");
		String role = data.get("role");
		String subscription = data.get("subscription");
		UserDetails adminData = userDetailsService.getUserDetailsByUserID(adminID);
		UserDetails userData = userDetailsService.getUserDetailsByUserID(userID);
		String adminName = adminData.getUserName();
		String userCompany = userData.getcompanyName();
		String adminCompany = adminData.getcompanyName();
		String userName = userData.getUserName();
		String productID = createAnAccountRepository.findProductIDWithProductType(product);
		List<String> adminRole = roleService.getRolesByUser(adminName);
		if (adminCompany.toLowerCase().equals(userCompany.toLowerCase()) || adminCompany.equals(superCompanyName)) {
			if (adminRole.get(0).equals("ADMIN")) 
				{
					boolean editProductStatus = createAnAccountService.editProductAsAdmin(userName, productID);
					boolean editSubscriptionStatus = createAnAccountService.editSubscriptionAsAdmin(userName, subscription);
					boolean editRoleStatus = createAnAccountService.editRoleAsAdmin(userName, role);
					if(editProductStatus==true && editSubscriptionStatus==true && editRoleStatus==true) {
						if(ticket_ID!=null && ticket_ID!="") {
							userDetailsService.InsertAdminTicketIntoDB(ticket_ID, ticket_Notes, ticket_Context, userID, adminID);
						}
						responseMap.put("Status", "Success");
						return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
					}else {
						responseMap.put("Status", "Failed");
						responseMap.put("ErrorMessage", "Failed to Edit User Profile");
						return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
					}
				}else {
					responseMap.put("Status", "Failed");
					responseMap.put("ErrorMessage", "You should be an Admin to perform this Operation");
					return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
				}
			}else {
				responseMap.put("Status", "Failed");
				responseMap.put("ErrorMessage", "Comp Diff");
				return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
			}
	}

	@RequestMapping(value = "/confirm-account")
	public ModelAndView confirmUserAccount(ModelAndView modelAndView, @RequestParam("token") String confirmationToken) {
		List<TokenDBModel> tokendata = tokenDBService.getTokenDetailsBasedOnTokenID(confirmationToken);
		modelAndView.setViewName("EmailVerification");
		if (tokendata.size() == 0) {
			modelAndView.addObject("message", "The link is invalid or broken!");
		} else {

			if (tokendata.size() != 0 && confirmationToken != null) {

				String isUsed = tokendata.get(0).getIsUsed();
				if (isUsed.equalsIgnoreCase("1")) {
					modelAndView.addObject("message", "Email already verified ! Thank you ");
					modelAndView.addObject("url", uiAddressURL);
				} else {

					UserDetails userdetails = userDetailsService.getUserDetailsByUserID(tokendata.get(0).getUserID());
					boolean isEmailVerified = createAnAccountService.markisEmailVerified(userdetails.getUserId());
					//boolean user = createAnAccountService.enableUserByID(userdetails.getUserName().toString());
					boolean status = tokenDBService.markTokenAsUsedByToken(confirmationToken);
					modelAndView.addObject("url", uiAddressURL);
					modelAndView.addObject("message",
							"Email Verification has been done ! Thank you for verifying your email address ");
				}

			} else {
				modelAndView.addObject("message", "The link is invalid or broken!");
				modelAndView.addObject("url", uiAddressURL);
			}
		}
		return modelAndView;
	}

}
