package com.aiknights.com.rapidviewmain.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aiknights.com.rapidviewmain.models.AuthenticationRequest;
import com.aiknights.com.rapidviewmain.models.AuthenticationResponse;
import com.aiknights.com.rapidviewmain.service.CustomUserDetailsService;
import com.aiknights.com.rapidviewmain.utils.JwtUtil;

@Component
@RestController
public class Authentication {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	private CustomUserDetailsService custUserDetailsService;
	
	@Autowired
	private JwtUtil jwtutil;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authReq) throws Exception {
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authReq.getUsername(), authReq.getPassword()));
		} catch (BadCredentialsException e) {
			System.out.println("Incorrect username or password");
			throw new Exception("Incorrect username or password", e);
		}
		final UserDetails userDetails = custUserDetailsService.loadUserByUsername(authReq.getUsername());
		final String jwt = jwtutil.generateToken(userDetails);
		
		return ResponseEntity.ok(new AuthenticationResponse(jwt));
	}
}
