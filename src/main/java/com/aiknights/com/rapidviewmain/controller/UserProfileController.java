package com.aiknights.com.rapidviewmain.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aiknights.com.rapidviewmain.DAO.UserRepository;
import com.aiknights.com.rapidviewmain.models.UserDetails;
import com.aiknights.com.rapidviewmain.service.RoleService;
import com.aiknights.com.rapidviewmain.service.UserProfileService;

@RequestMapping("/api")
@RestController
public class UserProfileController {

	@Autowired
	private UserProfileService userProfileService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private UserRepository userRepository;

	@SuppressWarnings("unused")
	@RequestMapping(value = "/getProfileDetails", method = RequestMethod.POST)

	public ResponseEntity<Map<String, UserDetails>> GetProfileDetails(@RequestBody final Map<String, String> user) {
		// final Map<String, List<UserDetails>> responseMap = new HashMap<>();
		Map<String, UserDetails> outputHashMap = new HashMap<>();

		String userName = user.get("userName");

		final UserDetails userProfileDetails = userProfileService.getProfileDetailsByUserId(userName);
		final List<String> rolesList = roleService.getRolesByUser(userName);
		String ProductType = userRepository.findProductTypeById(userProfileDetails.getProduct_ID());
		userProfileDetails.setRolename(rolesList.get(0));
		userProfileDetails.setProductType(ProductType);
		if (userProfileDetails == null) {
			outputHashMap.put("userProfileDetails", null);
			return new ResponseEntity<>(outputHashMap, HttpStatus.NOT_FOUND);
		} else {
			outputHashMap.put("userProfileDetails", userProfileDetails);

			return new ResponseEntity<>(outputHashMap, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/updateProfileDetails", method = RequestMethod.POST)

	public ResponseEntity<Map<String, String>> UpdateProfileDetails(@RequestBody final Map<String, String> user) {
		// final Map<String, List<UserDetails>> responseMap = new HashMap<>();
		Map<String, String> outputHashMap = new HashMap<>();

		String userName = user.get("userName");
		String address = user.get("address");
		String contactNumber = user.get("contactNumber");

		final boolean updateProfileDetails = userProfileService.updateProfileDetailsByUserName(userName, address,
				contactNumber);

		if (updateProfileDetails) {
			outputHashMap.put("Status", "Success");
			return new ResponseEntity<>(outputHashMap, HttpStatus.OK);
		} else {
			outputHashMap.put("Status", "Error");
			outputHashMap.put("Error", "Error while updating the Details. Please try again later");
			return new ResponseEntity<>(outputHashMap, HttpStatus.NOT_FOUND);
		}

	}

}
