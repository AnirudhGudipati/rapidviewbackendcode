package com.aiknights.com.rapidviewmain.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aiknights.com.rapidviewmain.models.WeatherCheck;
import com.aiknights.com.rapidviewmain.service.WeatherCheckService;

@RequestMapping("/api")
@RestController
public class WeatherController {

	@Autowired
	private WeatherCheckService weatherService;
	
	@GetMapping("/getWeatherData/{claimId}")
	public ResponseEntity<Map<String, WeatherCheck>> getWeatherDataForClaim(@PathVariable final String claimId) {
		final Map<String,WeatherCheck> claimWeatherMap = new HashMap<>();
		final WeatherCheck weatherData = weatherService.getWeatherCheckDataByClaimId(claimId);
		if(weatherData == null) {
			claimWeatherMap.put("weatherdata",null);
			return new ResponseEntity<>(claimWeatherMap,HttpStatus.NOT_FOUND);
		} else {
			claimWeatherMap.put("weatherdata",weatherData);
			return new ResponseEntity<>(claimWeatherMap,HttpStatus.OK);
		}

		
	}
	
	@GetMapping("/weatherassesment/{claimId}")
	public ResponseEntity<Map<String, Float>> getWeatherRiskAssesmentScore(@PathVariable final String claimId) {
		Random random = new Random();
		final Map<String,Float> riskMap = new HashMap<>();
		riskMap.put("score",random.nextFloat());
		return new ResponseEntity<>(riskMap,HttpStatus.OK);
		
	}
	
}
