package com.aiknights.com.rapidviewmain.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.aiknights.com.rapidviewmain.DAO.SubscriptionDBRepository;
import com.aiknights.com.rapidviewmain.DAO.UserRepository;
import com.aiknights.com.rapidviewmain.models.GenericSubscriptionDetailsModel;
import com.aiknights.com.rapidviewmain.models.PRRSManagementProductsModel;
import com.aiknights.com.rapidviewmain.models.PRRSManagementResponseModel;
import com.aiknights.com.rapidviewmain.models.PRRSManagementRolesandResourcesModel;
import com.aiknights.com.rapidviewmain.models.PRRSManagementSubscriptionPropertiesModel;
import com.aiknights.com.rapidviewmain.models.ProductCertainResourceDBTableModel;
import com.aiknights.com.rapidviewmain.models.ProductsTableDBModel;
import com.aiknights.com.rapidviewmain.models.TPRRSProductsInnerModel;
import com.aiknights.com.rapidviewmain.models.TPRRSProductsModel;
import com.aiknights.com.rapidviewmain.models.TPRRSRRModel;
import com.aiknights.com.rapidviewmain.models.TPRRSResourcesModel;
import com.aiknights.com.rapidviewmain.models.TPRRSRolesModel;
import com.aiknights.com.rapidviewmain.models.UserDetails;
import com.aiknights.com.rapidviewmain.service.RoleService;
import com.aiknights.com.rapidviewmain.service.UserDetailsService;

@RequestMapping("/api")
@Controller
public class SuperUserController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	private RoleService roleService;
	
	@Value("${rapid.super.Company.Name}")
	private String superCompanyName;

	@Autowired
	private UserDetailsService userdetailsservice;

	@Autowired
	SubscriptionDBRepository subscriptionDBRepository;

	@RequestMapping("/getProductRoleResourceSubscriptionData/{adminId}")
	public ResponseEntity<PRRSManagementResponseModel> getProductLicensingData(@PathVariable final String adminId) {
		UserDetails data = userdetailsservice.getUserDetailsByUserID(adminId);
		if (data.getcompanyName().equals(superCompanyName)) {
			PRRSManagementResponseModel response = new PRRSManagementResponseModel();
			List<ProductsTableDBModel> productDetails = userRepository.getAllProductsData();
			List<PRRSManagementProductsModel> products = new ArrayList<PRRSManagementProductsModel>();
			for (int i = 0; i < productDetails.size(); i++) {
				if (!(productDetails.get(i).getProductTableName().equals("RAPID_View_User_Table"))) {
					PRRSManagementProductsModel product = new PRRSManagementProductsModel();
					product.setProductName(productDetails.get(i).getProductName());
					List<String> RolesForProduct = userRepository.getDistinctAvailableRolesFromGivenProductTable(
							productDetails.get(i).getProductTableName());
					List<PRRSManagementRolesandResourcesModel> RolesandResourcesAll = new ArrayList<PRRSManagementRolesandResourcesModel>();
					for (int j = 0; j < RolesForProduct.size(); j++) {
						PRRSManagementRolesandResourcesModel rolesAndResources = new PRRSManagementRolesandResourcesModel();
						String currentrole = RolesForProduct.get(j).toString();
						rolesAndResources.setRoleName(currentrole);
						List<String> resourcesforRoleofProduct = userRepository
								.getResourcesAvailableOfGivenRoleFromGivenProductTable(
										productDetails.get(i).getProductTableName(), currentrole);
						rolesAndResources.setResources(resourcesforRoleofProduct);
						RolesandResourcesAll.add(rolesAndResources);
					}
					String SubscriptionTable = productDetails.get(i).getSubscriptionPropertiesTableName();
					if (SubscriptionTable != null) {
						List<PRRSManagementSubscriptionPropertiesModel> subscriptionsAll = new ArrayList<PRRSManagementSubscriptionPropertiesModel>();
						List<GenericSubscriptionDetailsModel> subdetails = subscriptionDBRepository
								.getAllSubscriptionDetailsForGivenSubscriptionTable(SubscriptionTable);
						for (int l = 0; l < subdetails.size(); l++) {
							PRRSManagementSubscriptionPropertiesModel subscription = new PRRSManagementSubscriptionPropertiesModel();
							subscription.setDownloadCount(subdetails.get(l).getWeather_Download_MaxCount());
							subscription.setSubscriptionPeriod(subdetails.get(l).getNumber_of_Days());
							subscription.setSubscriptionPrice(subdetails.get(l).getAmountToBe_Paid());
							subscription.setSubscriptionType(subdetails.get(l).getSubscription_Type());
							subscriptionsAll.add(subscription);
						}
						product.setSubscriptionProperties(subscriptionsAll);
					}
					product.setRolesAndResources(RolesandResourcesAll);
					products.add(product);
				}
				response.setProducts(products);
			}
			return new ResponseEntity<>(response, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.OK);
		}
	}

	@RequestMapping("/getProductRoleResourceSubscriptionProperties/{adminId}")
	public ResponseEntity<TPRRSProductsModel> getProductRoleResourceSubscriptionProperties(
			@PathVariable final String adminId) {
		UserDetails data = userdetailsservice.getUserDetailsByUserID(adminId);
		if (data.getcompanyName().equals(superCompanyName)) {
			TPRRSProductsModel response = new TPRRSProductsModel();
			List<ProductsTableDBModel> productDetails = userRepository.getAllProductsData();
			List<TPRRSProductsInnerModel> products = new ArrayList<TPRRSProductsInnerModel>();
			for (int i = 0; i < productDetails.size(); i++) {
				if (!(productDetails.get(i).getProductTableName().equals("RAPID_View_User_Table"))) {
					TPRRSProductsInnerModel product = new TPRRSProductsInnerModel();
					product.setProductName(productDetails.get(i).getProductName());
					List<String> RolesForProduct = userRepository.getDistinctAvailableRolesFromGivenProductTable(
							productDetails.get(i).getProductTableName());
					List<TPRRSRRModel> RolesandResourcesAll = new ArrayList<TPRRSRRModel>();
					List<TPRRSRolesModel> rolesdata = new ArrayList<TPRRSRolesModel>();
					TPRRSRRModel RandR = new TPRRSRRModel();
					for (int j = 0; j < RolesForProduct.size(); j++) {
						TPRRSRolesModel roles = new TPRRSRolesModel();
						String currentrole = RolesForProduct.get(j).toString();
						roles.setRolename(currentrole);
						List<ProductCertainResourceDBTableModel> resources = userRepository
								.getResourcesDataAvailableOfGivenRoleFromGivenProductTable(
										productDetails.get(i).getProductTableName(), currentrole);
						List<TPRRSResourcesModel> resourcesdata = new ArrayList<TPRRSResourcesModel>();
						for (int k = 0; k < resources.size(); k++) {
							TPRRSResourcesModel resource = new TPRRSResourcesModel();
							resource.setResourceName(resources.get(k).getResourceName());
							resource.setResourceStatus(resources.get(k).getIsActive());
							resourcesdata.add(resource);
						}
						roles.setResources(resourcesdata);
						rolesdata.add(roles);
					}
					String SubscriptionTable = productDetails.get(i).getSubscriptionPropertiesTableName();
					if (SubscriptionTable != null) {
						List<PRRSManagementSubscriptionPropertiesModel> subscriptionsAll = new ArrayList<PRRSManagementSubscriptionPropertiesModel>();
						List<GenericSubscriptionDetailsModel> subdetails = subscriptionDBRepository
								.getAllSubscriptionDetailsForGivenSubscriptionTable(SubscriptionTable);
						for (int l = 0; l < subdetails.size(); l++) {
							PRRSManagementSubscriptionPropertiesModel subscription = new PRRSManagementSubscriptionPropertiesModel();
							subscription.setDownloadCount(subdetails.get(l).getWeather_Download_MaxCount());
							subscription.setSubscriptionPeriod(subdetails.get(l).getNumber_of_Days());
							subscription.setSubscriptionPrice(subdetails.get(l).getAmountToBe_Paid());
							subscription.setSubscriptionType(subdetails.get(l).getSubscription_Type());
							subscription.setSubscriptionName(subdetails.get(l).getSubscription_Type_For_UI());
							subscriptionsAll.add(subscription);
						}
						product.setSubscriptionProperties(subscriptionsAll);
					}
					product.setRolesAndResources(RolesandResourcesAll);
					RandR.setRoles(rolesdata);
					RolesandResourcesAll.add(RandR);
					product.setRolesAndResources(RolesandResourcesAll);
					products.add(product);
				}
				response.setProducts(products);
			}
			return new ResponseEntity<>(response, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.OK);
		}
	}

	@PostMapping(value = "/postProductRolesResources/{adminId}", consumes = "application/json")
	public ResponseEntity<Map<String, String>> postProductRolesResources(@RequestBody TPRRSProductsInnerModel reqData,
			@PathVariable final String adminId) {
		final Map<String, String> responseMap = new HashMap<>();
		UserDetails data = userdetailsservice.getUserDetailsByUserID(adminId);
		String CompanyName = data.getcompanyName();
		String userName = data.getUserName();
		List<String> role = roleService.getRolesByUser(userName);
		if(role.get(0).equals("ADMIN")) {
			boolean status = false;
			String CurrentProductTableName = null;
			List<ProductsTableDBModel> productDetails = userRepository.getAllProductsData();
			for (int p = 0; p < productDetails.size(); p++) {
				if (productDetails.get(p).getProductName().equals(reqData.getProductName())) {
					CurrentProductTableName = productDetails.get(p).getProductTableName();
				}
			}
			if (CurrentProductTableName != null) {
				boolean totalUpdateStatus = true;
				for (int i = 0; i < reqData.getRolesAndResources().get(0).getRoles().size(); i++) {
					String CurrentRoleName = reqData.getRolesAndResources().get(0).getRoles().get(i).getRolename();
					for (int j = 0; j < reqData.getRolesAndResources().get(0).getRoles().get(i).getResources()
							.size(); j++) {
						String CurrentResourceName = reqData.getRolesAndResources().get(0).getRoles().get(i)
								.getResources().get(j).getResourceName();
						String CurrentResourceStatus = reqData.getRolesAndResources().get(0).getRoles().get(i)
								.getResources().get(j).getResourceStatus();
						boolean currentUpdateStatus = userRepository
								.updateRoleResourceActiveStatusForGivenRoleResourceProductTableName(
										CurrentProductTableName, CurrentRoleName, CurrentResourceName,
										CurrentResourceStatus);
						if (currentUpdateStatus == false) {
							totalUpdateStatus = false;
						}
					}
				}
				if (totalUpdateStatus == false) {
					status = false;
				} else {
					status = true;
				}
			}
			if (status == true) {
				responseMap.put("Status", "Success");
			} else {
				responseMap.put("Status", "Failure");
			}
		} else {
			responseMap.put("Status", "Failure");
		}
		return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
	}

	@PostMapping(value = "/postSubscriptionProperties/{adminId}", consumes = "application/json")
	public ResponseEntity<Map<String, String>> postSubscriptionProperties(@RequestBody TPRRSProductsInnerModel reqData,
			@PathVariable final String adminId) {
		final Map<String, String> responseMap = new HashMap<>();
		UserDetails data = userdetailsservice.getUserDetailsByUserID(adminId);
		String CompanyName = data.getcompanyName();
		String userName = data.getUserName();
		List<String> role = roleService.getRolesByUser(userName);
		if(role.get(0).equals("ADMIN") || CompanyName.equals(superCompanyName)) {
			boolean status = false;
			String CurrentSubscriptionTableName = null;
			List<ProductsTableDBModel> productDetails = userRepository.getAllProductsData();
			for (int p = 0; p < productDetails.size(); p++) {
				if (productDetails.get(p).getProductName().equals(reqData.getProductName())) {
					CurrentSubscriptionTableName = productDetails.get(p).getSubscriptionPropertiesTableName();
				}
			}
			if (CurrentSubscriptionTableName != null) {
				boolean totalUpdateStatus = true;
				for (int i = 0; i < reqData.getSubscriptionProperties().size(); i++) {
					String subscriptionType = reqData.getSubscriptionProperties().get(i).getSubscriptionType();
					String subscriptionPrice = reqData.getSubscriptionProperties().get(i).getSubscriptionPrice();
					String downloadCount = reqData.getSubscriptionProperties().get(i).getDownloadCount();
					boolean currentUpdateStatus = userRepository
							.updateSubscriptionPropertiesForGivenTypeSubscriptionTableName(CurrentSubscriptionTableName,
									subscriptionType, subscriptionPrice, downloadCount);
					String subscriptionPeriod = reqData.getSubscriptionProperties().get(i).getSubscriptionPeriod();
					userRepository.updateCommonSubscriptionProperties(subscriptionType, subscriptionPeriod);
					if (currentUpdateStatus == false) {
						totalUpdateStatus = false;
					}
				}
				if (totalUpdateStatus == false) {
					status = false;
				} else {
					status = true;
				}
			}
			if (status == true) {
				responseMap.put("Status", "Success");
			} else {
				responseMap.put("Status", "Failure");
			}
		}else {
			responseMap.put("Status", "Failure");
		}
		return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
	}
}
