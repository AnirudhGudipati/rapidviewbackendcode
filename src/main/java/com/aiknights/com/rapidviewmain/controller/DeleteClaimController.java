package com.aiknights.com.rapidviewmain.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aiknights.com.rapidviewmain.dto.DeleteDTO;
import com.aiknights.com.rapidviewmain.service.DeleteClaimService;

@RequestMapping("/api")
@RestController
public class DeleteClaimController {

	@Autowired
	private DeleteClaimService claimDeleteService;

	List<String> valueList;

	@RequestMapping(value = "/deleteClaim", method = RequestMethod.POST)
	public ResponseEntity<Map<String, /* List<DeleteDTO> */Integer>> deleteClaimId(
			@RequestBody final Map<String, List<String>> claimDetails) {

		Map<String, Integer> outputHashMap = new HashMap<>();
		Map<String, List<String>> deleteClaimDetails = new HashMap<String, List<String>>();

		int delete_status = 0;
		DeleteDTO deleteStatusDTO = new DeleteDTO();

		List<DeleteDTO> deleteStatusDTOList = new ArrayList<DeleteDTO>();

		deleteClaimDetails.putAll(claimDetails);
		List<String> claim_idList = new ArrayList<>();

		for (Entry<String, List<String>> entry : deleteClaimDetails.entrySet()) {
			String key = entry.getKey();
			for (String value : entry.getValue()) {
				claim_idList.add(value);

			}
		}

		for (int i = 0; i < claim_idList.size(); i++) {

			String Claim_id = claim_idList.get(i);
			/*
			 * // weather_check table boolean isClaimIdPresentWeatherCheck =
			 * claimDeleteService.checkClaimIDInWeatherCheck(Claim_id); if
			 * (isClaimIdPresentWeatherCheck) {
			 * System.out.println("Claimid present in incident_evidence"); boolean
			 * weather_check_status =
			 * claimDeleteService.deleteWeatherCheckClaimByDataByClaimID(Claim_id); } else {
			 * 
			 * }
			 * 
			 * // create_report table boolean isClaimIdPresentCreateReport =
			 * claimDeleteService.checkClaimIDInCreateReport(Claim_id); if
			 * (isClaimIdPresentCreateReport) {
			 * System.out.println("Claimid present in incident_evidence"); boolean
			 * create_report_status =
			 * claimDeleteService.deleteCreateReportClaimDataByClaimID(Claim_id); } else {
			 * 
			 * }
			 * 
			 * // incident_evidence table boolean isClaimIdPresentInidentEvidence =
			 * claimDeleteService.checkClaimIDInIncidentEvidence(Claim_id); if
			 * (isClaimIdPresentInidentEvidence) {
			 * System.out.println("Claimid present in incident_evidence"); boolean
			 * incident_evidence_status =
			 * claimDeleteService.deleteIncidentEvdenceClaimDataByClaimID(Claim_id); } else
			 * {
			 * 
			 * }
			 * 
			 * // risk_assesment table boolean isClaimIdPresentInRiskAssesment =
			 * claimDeleteService.checkClaimIDInRiskAssesment(Claim_id); if
			 * (isClaimIdPresentInRiskAssesment) {
			 * System.out.println("Claimid present in risk_assesment"); boolean
			 * incident_evidence_status =
			 * claimDeleteService.deleteRiskAssesmentClaimDataByClaimID(Claim_id); } else {
			 * 
			 * }
			 */

			// Currently we are deleting from adjuster table only from dashboard
			boolean djusterClaimId_delete_status = claimDeleteService.deleteAdjusterClaimDataByClaimID(Claim_id);
			/*
			 * String policyNumber = claimDeleteService.getPOlicyNumberByClaimId(Claim_id);
			 * boolean claimId_delete_status =
			 * claimDeleteService.deleteClaimByClaimID(Claim_id);
			 */

			// boolean policy_delete_status =
			// claimDeleteService.deletePolicyByClaimID(policyNumber);

			if (djusterClaimId_delete_status) {
				/*
				 * deleteStatusDTO.setClaim_ID(Claim_id); deleteStatusDTO.setStatus(true);
				 */
				delete_status = 1;

			} else {
				/*
				 * deleteStatusDTO.setClaim_ID(Claim_id); deleteStatusDTO.setStatus(false);
				 */
				delete_status = 0;
			}
			deleteStatusDTOList.add(deleteStatusDTO);
		}

		outputHashMap.put("DeleteStatus", delete_status);

		System.out.println("Delete");

		return new ResponseEntity<>(outputHashMap, HttpStatus.OK);
	}

}
