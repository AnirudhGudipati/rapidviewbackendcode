package com.aiknights.com.rapidviewmain.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.aiknights.com.rapidviewmain.models.FlightPlanDataModel;
import com.aiknights.com.rapidviewmain.models.FlightPlanObjectDBModel;
import com.aiknights.com.rapidviewmain.models.FlightPlanPropertiesDBModel;
import com.aiknights.com.rapidviewmain.models.GenerateFlightPlanResponseModel;
import com.aiknights.com.rapidviewmain.models.KMLGenServiceRequestModel;
import com.aiknights.com.rapidviewmain.models.KMLGenServiceRequestModelParent;
import com.aiknights.com.rapidviewmain.models.WayPointDataDBModel;
import com.aiknights.com.rapidviewmain.service.FlightPlanDBService;
import com.aiknights.com.rapidviewmain.service.GetIDService;
import com.aiknights.com.rapidviewmain.service.WorkOrderDBService;

import org.springframework.http.MediaType;

@RequestMapping("/api")
@RestController
public class SiteInspectionController {

	@Autowired
	FlightPlanDBService dbservice;

	@Autowired
	WorkOrderDBService wodbservice;

	String Timezone = "GMT";
	
	@Value("${flight.plan.files-path}")
	private String flightplanfilesdirectory;

	@Value("${kml.service.url}")
	private String kmlServiceURL;

	private static final Logger LOGGER = LoggerFactory.getLogger(SiteInspectionController.class);

	@RequestMapping(value="/downloadFlightPlan",method = RequestMethod.POST)
	public ResponseEntity<Object> DownloadFlightPlan(@RequestBody FlightPlanDataModel flightPlanData) throws Exception
	{
		boolean flightPlanDBInsertionOutput=false;
		boolean flightPlanPropertiesDBInsertionOutput=false;
		boolean flightPlanWayPointsDBInsertionOutput=false;
		boolean WayPointsActionsDBInsertionOutput=false;
		boolean FPOverallResponse=true;
		String flight_Plan_ID = "FP"+Long.toString(GetIDService.getID());
		String claim_ID = flightPlanData.getClaimID();
		String claim_Address = flightPlanData.getAddress();
		String user_ID=flightPlanData.getUserID();
		flightPlanDBInsertionOutput = dbservice.putFlighPlanDataToDB(flight_Plan_ID, claim_ID, claim_Address);
		if(flightPlanDBInsertionOutput==true) {
			String flight_Plan_Name = flightPlanData.getFlightPlanName();
			String flight_Plan_Version ="";
			String flight_Plan_Format =flightPlanData.getFormat();
			flightPlanPropertiesDBInsertionOutput = dbservice.putFlightPlanPropertiesToDB(flight_Plan_ID,user_ID, flight_Plan_Name, flight_Plan_Version, flight_Plan_Format);
			System.out.println("Flight Plan DB Object Created");
			if(flightPlanPropertiesDBInsertionOutput==true) {
				for(int i=0;i<flightPlanData.getWayPoints().size();i++) {
					String way_Point_ID="WP"+Long.toString(GetIDService.getID());
					String way_Point_Key=flightPlanData.getWayPoints().get(i).getWayPointKey();
					String latitude = flightPlanData.getWayPoints().get(i).getLatitude();
					String longitude= flightPlanData.getWayPoints().get(i).getLongitude();
					String altitude= flightPlanData.getWayPoints().get(i).getAltitude();
					String above_Ground= flightPlanData.getWayPoints().get(i).getAboveGround();
					String speed= flightPlanData.getWayPoints().get(i).getSpeed();
					String curve_size= flightPlanData.getWayPoints().get(i).getCurvesize();
					String heading= flightPlanData.getWayPoints().get(i).getHeading();
					String pOI= flightPlanData.getWayPoints().get(i).getPointOfInterest();
					String gimbal_Option= flightPlanData.getWayPoints().get(i).getGimbal().getSelectedOption();
					String gimbal_Value= flightPlanData.getWayPoints().get(i).getGimbal().getSelectedValue();
					String interval_Option= flightPlanData.getWayPoints().get(i).getInterval().getSelectedOption();
					String interval_Value= flightPlanData.getWayPoints().get(i).getInterval().getSelectedValue();
					flightPlanWayPointsDBInsertionOutput=dbservice.putWayPointDataToDB(flight_Plan_ID, way_Point_ID ,way_Point_Key,latitude, longitude, 
							altitude, above_Ground, speed, curve_size, heading, pOI, gimbal_Option, gimbal_Value, interval_Option, interval_Value);
					System.out.println("WayPointInsert : "+flightPlanWayPointsDBInsertionOutput);
					if(flightPlanWayPointsDBInsertionOutput==true) {
						for(int j=0;j<flightPlanData.getWayPoints().get(i).getActions().size();j++) {
							String flight_Action_ID="FA"+Long.toString(GetIDService.getID());
							String action_Number = flightPlanData.getWayPoints().get(i).getActions().get(j).getAction();
							String selected_Action_Option = flightPlanData.getWayPoints().get(i).getActions().get(j).getSelectedOption();
							String selected_Action_Value = flightPlanData.getWayPoints().get(i).getActions().get(j).getSelectedValue();
							WayPointsActionsDBInsertionOutput = dbservice.putActionDatatoDB(flight_Action_ID,way_Point_ID,action_Number,selected_Action_Option,selected_Action_Value);
							if(WayPointsActionsDBInsertionOutput==true) {
								System.out.println("ActionInsert : "+WayPointsActionsDBInsertionOutput);
							}
							else {
								System.out.println("ActionInsert failed");
								FPOverallResponse=false;
							}
						}
					}
					else {
						System.out.println("WayPointInsertion failed");
						FPOverallResponse=false;
					}
				}
				System.out.println("Flight Plan DB Properties Created");
			}else {
				FPOverallResponse=false;
				System.out.println("Flight Plan DB Properties Creation Failed");
			}
		}else {
			FPOverallResponse=false;
			System.out.println("Flight Plan DB Object Creation Failed");
		}

		if(FPOverallResponse==true) {
			if(flightPlanData.getFormat().equals("plan")) {
				KMLGenServiceRequestModelParent features = buildFeatureList(flightPlanData);
				RestTemplate rt = new RestTemplate();
				ResponseEntity<String> kmloutput = rt.postForEntity(kmlServiceURL+flightPlanData.getFlightPlanName(), features, String.class);
				System.out.println(kmloutput);
				String filename = flightPlanData.getFlightPlanName()+".plan";
				String path = flightplanfilesdirectory+filename;
				File file = new File(path);
				file.createNewFile();
				file.setWritable(true);
				file.setExecutable(true);
				FileWriter filewriter = new FileWriter(path);
				String content = kmloutput.getBody();
				filewriter.write(content);
				filewriter.close();
				InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
				HttpHeaders headers = new HttpHeaders();
				headers.add("Content-Disposition", String.format(file.getName()));
				headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
				headers.add("Pragma", "no-cache");
				headers.add("Expires", "0");
				headers.add("Status", "Success");
				ResponseEntity<Object> 
				responseEntity = ResponseEntity.ok().headers(headers).contentLength(
						file.length()).contentType(MediaType.APPLICATION_OCTET_STREAM).body(resource);

				return responseEntity;
			}
			else if(flightPlanData.getFormat().equals("csv")) {
				String content = "\"WayPointKey\",\"Latitude\",\"Longitude\",\"Altitude\",\"AboveGround\",\"Speed\",\"CurveSize\",\"Heading\",\"PointOfInterest\",\"GimbalOption\",\"GimbalValue\",\"IntervalOption\",\"IntervalValue\"\n";
				for(int i=0;i<flightPlanData.getWayPoints().size();i++) {
					String way_Point_Key=flightPlanData.getWayPoints().get(i).getWayPointKey();
					String latitude = flightPlanData.getWayPoints().get(i).getLatitude();
					String longitude= flightPlanData.getWayPoints().get(i).getLongitude();
					String altitude= flightPlanData.getWayPoints().get(i).getAltitude();
					String above_Ground= flightPlanData.getWayPoints().get(i).getAboveGround();
					String speed= flightPlanData.getWayPoints().get(i).getSpeed();
					String curve_size= flightPlanData.getWayPoints().get(i).getCurvesize();
					String heading= flightPlanData.getWayPoints().get(i).getHeading();
					String pOI= flightPlanData.getWayPoints().get(i).getPointOfInterest();
					String gimbal_Option= flightPlanData.getWayPoints().get(i).getGimbal().getSelectedOption();
					String gimbal_Value= flightPlanData.getWayPoints().get(i).getGimbal().getSelectedValue();
					String interval_Option= flightPlanData.getWayPoints().get(i).getInterval().getSelectedOption();
					String interval_Value= flightPlanData.getWayPoints().get(i).getInterval().getSelectedValue();
					String delimiter="\",\"";
					content=content+"\""+way_Point_Key+delimiter+latitude+delimiter+longitude+delimiter+altitude+delimiter+above_Ground+delimiter+speed+delimiter+curve_size+delimiter+heading+delimiter+pOI+delimiter+gimbal_Option+delimiter+gimbal_Value+delimiter+interval_Option+delimiter+interval_Value+"\"\n";
				}
				String filename = flightPlanData.getFlightPlanName()+".csv";
				String path = flightplanfilesdirectory+filename;
				File file = new File(path);
				file.getAbsoluteFile().exists();
				file.setWritable(true);
				file.setExecutable(true);
				FileWriter filewriter = new FileWriter(path);
				filewriter.write(content);
				filewriter.close();
				InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
				HttpHeaders headers = new HttpHeaders();
				headers.add("Content-Disposition", String.format(file.getName()));
				headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
				headers.add("Pragma", "no-cache");
				headers.add("Expires", "0");
				headers.add("Status", "Success");
				ResponseEntity<Object> 
				responseEntity = ResponseEntity.ok().headers(headers).contentLength(
						file.length()).contentType(MediaType.APPLICATION_OCTET_STREAM).body(resource);

				return responseEntity;
			}
			else {
				HttpHeaders headers = new HttpHeaders();
				headers.add("Content-Disposition", "");
				headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
				headers.add("Pragma", "no-cache");
				headers.add("Expires", "0");
				headers.add("Status", "Failure");
				ResponseEntity<Object> responseEntity = ResponseEntity.ok().headers(headers).contentLength(0).contentType(MediaType.TEXT_PLAIN).body(null);
				return responseEntity;
			}
		}
		else {
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Disposition", "");
			headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
			headers.add("Pragma", "no-cache");
			headers.add("Expires", "0");
			headers.add("Status", "Failure");
			ResponseEntity<Object> responseEntity = ResponseEntity.ok().headers(headers).contentLength(0).contentType(MediaType.TEXT_PLAIN).body(null);
			return responseEntity;
		}
	}


	@RequestMapping(value="/generateFlightPlan",method = RequestMethod.POST)
	public ResponseEntity<Map<String,GenerateFlightPlanResponseModel>> GenerateFlightPlan(@RequestBody FlightPlanDataModel flightPlanData) throws IOException
	{
		final Map<String,GenerateFlightPlanResponseModel> responseMap = new HashMap<>();
		GenerateFlightPlanResponseModel responsedata = new GenerateFlightPlanResponseModel();
		boolean flightPlanDBInsertionOutput=false;
		boolean flightPlanPropertiesDBInsertionOutput=false;
		boolean flightPlanWayPointsDBInsertionOutput=false;
		boolean WayPointsActionsDBInsertionOutput=false;
		boolean FPOverallResponse=true;
		boolean OverallResponse=false;
		String flight_Plan_ID = "FP"+Long.toString(GetIDService.getID());
		String claim_ID = flightPlanData.getClaimID();
		String claim_Address = flightPlanData.getAddress();
		String user_ID=flightPlanData.getUserID();
		flightPlanDBInsertionOutput = dbservice.putFlighPlanDataToDB(flight_Plan_ID, claim_ID, claim_Address);
		if(flightPlanDBInsertionOutput==true) {
			String flight_Plan_Name = flightPlanData.getFlightPlanName();
			String flight_Plan_Version ="";
			String flight_Plan_Format ="";
			flightPlanPropertiesDBInsertionOutput = dbservice.putFlightPlanPropertiesToDB(flight_Plan_ID,user_ID, flight_Plan_Name, flight_Plan_Version, flight_Plan_Format);
			System.out.println("Flight Plan DB Object Created");
			if(flightPlanPropertiesDBInsertionOutput==true) {
				for(int i=0;i<flightPlanData.getWayPoints().size();i++) {
					String way_Point_ID="WP"+Long.toString(GetIDService.getID());
					String way_Point_Key=flightPlanData.getWayPoints().get(i).getWayPointKey();
					String latitude = flightPlanData.getWayPoints().get(i).getLatitude();
					String longitude= flightPlanData.getWayPoints().get(i).getLongitude();
					String altitude= flightPlanData.getWayPoints().get(i).getAltitude();
					String above_Ground= flightPlanData.getWayPoints().get(i).getAboveGround();
					String speed= flightPlanData.getWayPoints().get(i).getSpeed();
					String curve_size= flightPlanData.getWayPoints().get(i).getCurvesize();
					String heading= flightPlanData.getWayPoints().get(i).getHeading();
					String pOI= flightPlanData.getWayPoints().get(i).getPointOfInterest();
					String gimbal_Option= flightPlanData.getWayPoints().get(i).getGimbal().getSelectedOption();
					String gimbal_Value= flightPlanData.getWayPoints().get(i).getGimbal().getSelectedValue();
					String interval_Option= flightPlanData.getWayPoints().get(i).getInterval().getSelectedOption();
					String interval_Value= flightPlanData.getWayPoints().get(i).getInterval().getSelectedValue();
					flightPlanWayPointsDBInsertionOutput=dbservice.putWayPointDataToDB(flight_Plan_ID, way_Point_ID ,way_Point_Key,latitude, longitude, 
							altitude, above_Ground, speed, curve_size, heading, pOI, gimbal_Option, gimbal_Value, interval_Option, interval_Value);
					System.out.println("WayPointInsert : "+flightPlanWayPointsDBInsertionOutput);
					if(flightPlanWayPointsDBInsertionOutput==true) {
						for(int j=0;j<flightPlanData.getWayPoints().get(i).getActions().size();j++) {
							String flight_Action_ID="FA"+Long.toString(GetIDService.getID());
							String action_Number = flightPlanData.getWayPoints().get(i).getActions().get(j).getAction();
							String selected_Action_Option = flightPlanData.getWayPoints().get(i).getActions().get(j).getSelectedOption();
							String selected_Action_Value = flightPlanData.getWayPoints().get(i).getActions().get(j).getSelectedValue();
							WayPointsActionsDBInsertionOutput = dbservice.putActionDatatoDB(flight_Action_ID,way_Point_ID,action_Number,selected_Action_Option,selected_Action_Value);
							if(WayPointsActionsDBInsertionOutput==true) {
								System.out.println("ActionInsert : "+WayPointsActionsDBInsertionOutput);
							}else {
								System.out.println("ActionInsert failed");
								FPOverallResponse=false;
							}
						}
					}else {
						System.out.println("WayPointInsertion failed");
						FPOverallResponse=false;
					}
				}

				System.out.println("Flight Plan DB Properties Created");
			}else {
				FPOverallResponse=false;
				System.out.println("Flight Plan DB Properties Creation Failed");
			}
		}else {
			FPOverallResponse=false;
			System.out.println("Flight Plan DB Object Creation Failed");
		}
		String work_Order_ID = "WO"+Long.toString(GetIDService.getID());
		String work_Order_Status="Pending";
		String assigned_To=user_ID;
		String assigned_By=user_ID;
		String expected_Start_Date="";
		String expected_End_Date="";
		String notes="";
		String work_Order_Approval_Status="NotSubmitted";
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		df.setTimeZone(TimeZone.getTimeZone(Timezone));
		Date dateo = new Date();
		String created_Date = df.format(dateo);
		if(FPOverallResponse==true)
		{
			boolean workordercreationstatus=wodbservice.putWorkOrderToDB(work_Order_ID,flight_Plan_ID,work_Order_Status,assigned_To,assigned_By,expected_Start_Date,expected_End_Date,created_Date,notes,work_Order_Approval_Status);
			if(workordercreationstatus==true) {
				System.out.println("Default WorkOrder Created");
				OverallResponse=true;
			}else
			{
				System.out.println("Error Creating Default WorkOrder");
				OverallResponse=false;
			}
		}
		if(OverallResponse==true)
		{
			responsedata.setUserID(user_ID);
			responsedata.setDefaultWorkOrderID(work_Order_ID);
			responsedata.setClaimID(claim_ID);
			responsedata.setFlightPlanID(flight_Plan_ID);
			responsedata.setFlightPlanName(flightPlanData.getFlightPlanName());
			responsedata.setStatus("Success");
			responseMap.put("Status", responsedata);
		}else {
			responsedata.setStatus("Failed");
			responseMap.put("Status", responsedata);
		}
		return new ResponseEntity<Map<String,GenerateFlightPlanResponseModel>>(responseMap,HttpStatus.OK);
	}

	@RequestMapping(value="/getFlightPlan",method = RequestMethod.POST)
	public ResponseEntity<Object> GetFlightPlan(@RequestBody final Map<String,String> data) throws IOException
	{
		String FlightPlanID=data.get("flightPlanID");
		String FlightPlanFormat=data.get("flightPlanFormat");
		List<FlightPlanObjectDBModel> flightPlanObjectData = dbservice.getFlightPlanObjectByFlightPlanID(FlightPlanID);
		List<FlightPlanPropertiesDBModel> flightPlanPropertiesData = dbservice.getFlightPlanPropertiesByFlightPlanID(FlightPlanID);
		List<WayPointDataDBModel> wayPointData = dbservice.getWayPointsByFlightPlanID(FlightPlanID);
		String FlightPlanName = flightPlanPropertiesData.get(0).getFlight_Plan_Name();
		if(FlightPlanFormat.equals("plan")) 
		{
			KMLGenServiceRequestModelParent features = getFeatureList(wayPointData);
			RestTemplate rt = new RestTemplate();
			ResponseEntity<String> kmloutput = rt.postForEntity(kmlServiceURL+FlightPlanName, features, String.class);
			System.out.println(kmloutput);
			String filename = FlightPlanName+".plan";
			String path = flightplanfilesdirectory+filename;
			File file = new File(path);
			file.createNewFile();
			file.setWritable(true);
			file.setExecutable(true);
			FileWriter filewriter = new FileWriter(path);
			String content = kmloutput.getBody();
			filewriter.write(content);
			filewriter.close();
			InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Disposition", String.format(file.getName()));
			headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
			headers.add("Pragma", "no-cache");
			headers.add("Expires", "0");
			headers.add("Status", "Success");
			ResponseEntity<Object> 
			responseEntity = ResponseEntity.ok().headers(headers).contentLength(
					file.length()).contentType(MediaType.APPLICATION_OCTET_STREAM).body(resource);

			return responseEntity;
		}else if(FlightPlanFormat.equals("csv"))
		{
			String content = "\"WayPointKey\",\"Latitude\",\"Longitude\",\"Altitude\",\"AboveGround\",\"Speed\",\"CurveSize\",\"Heading\",\"PointOfInterest\",\"GimbalOption\",\"GimbalValue\",\"IntervalOption\",\"IntervalValue\"\n";
			for(int i=0;i<wayPointData.size();i++) {
				String way_Point_Key=wayPointData.get(i).getWay_Point_Key();
				String latitude = wayPointData.get(i).getLatitude();
				String longitude= wayPointData.get(i).getLongitude();
				String altitude= wayPointData.get(i).getAltitude();
				String above_Ground= wayPointData.get(i).getAbove_Ground();
				String speed= wayPointData.get(i).getSpeed();
				String curve_size= wayPointData.get(i).getCurve_size();
				String heading= wayPointData.get(i).getHeading();
				String pOI= wayPointData.get(i).getPOI();
				String gimbal_Option= wayPointData.get(i).getGimbal_Option();
				String gimbal_Value= wayPointData.get(i).getGimbal_Value();
				String interval_Option= wayPointData.get(i).getInterval_Option();
				String interval_Value= wayPointData.get(i).getInterval_Value();
				String delimiter="\",\"";
				content=content+"\""+way_Point_Key+delimiter+latitude+delimiter+longitude+delimiter+altitude+delimiter+above_Ground+delimiter+speed+delimiter+curve_size+delimiter+heading+delimiter+pOI+delimiter+gimbal_Option+delimiter+gimbal_Value+delimiter+interval_Option+delimiter+interval_Value+"\"\n";
			}
			System.out.println(content);
			String filename = FlightPlanName+".csv";
			String path = flightplanfilesdirectory+filename;
			File file = new File(path);
			file.getAbsoluteFile().exists();
			file.setWritable(true);
			file.setExecutable(true);
			FileWriter filewriter = new FileWriter(path);
			filewriter.write(content);
			LOGGER.info("Written to File CSV");
			filewriter.close();
			LOGGER.info("Closing File CSV");
			InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Disposition", String.format(file.getName()));
			headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
			headers.add("Pragma", "no-cache");
			headers.add("Expires", "0");
			headers.add("Status", "Success");
			ResponseEntity<Object> 
			responseEntity = ResponseEntity.ok().headers(headers).contentLength(
					file.length()).contentType(MediaType.APPLICATION_OCTET_STREAM).body(resource);

			return responseEntity;	
		}else
		{
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Disposition", "");
			headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
			headers.add("Pragma", "no-cache");
			headers.add("Expires", "0");
			headers.add("Status", "Failure");
			ResponseEntity<Object> responseEntity = ResponseEntity.ok().headers(headers).contentLength(0).contentType(MediaType.TEXT_PLAIN).body(null);
			return responseEntity;
		}
	}

	public KMLGenServiceRequestModelParent buildFeatureList(FlightPlanDataModel flightplandata) {
		KMLGenServiceRequestModelParent features = new KMLGenServiceRequestModelParent();
		List<KMLGenServiceRequestModel> featurelist = new ArrayList<KMLGenServiceRequestModel>();
		for(int i=0;i<flightplandata.getWayPoints().size();i++) {
			KMLGenServiceRequestModel feature = new KMLGenServiceRequestModel();
			feature.setName("WayPoint Key - "+flightplandata.getWayPoints().get(i).getWayPointKey());
			feature.setDesc("WayPoint Key - "+flightplandata.getWayPoints().get(i).getWayPointKey());
			feature.setLat(Float.parseFloat(flightplandata.getWayPoints().get(i).getLatitude()));
			feature.setLon(Float.parseFloat(flightplandata.getWayPoints().get(i).getLongitude()));
			feature.setTilt(45);
			feature.setRange(3000);
			featurelist.add(feature);
		}
		features.setFeatures(featurelist);
		return features;
	}

	public KMLGenServiceRequestModelParent getFeatureList(List<WayPointDataDBModel> wayPointData) {
		KMLGenServiceRequestModelParent features = new KMLGenServiceRequestModelParent();
		List<KMLGenServiceRequestModel> featurelist = new ArrayList<KMLGenServiceRequestModel>();
		for(int i=0;i<wayPointData.size();i++) {
			KMLGenServiceRequestModel feature = new KMLGenServiceRequestModel();
			feature.setName("WayPoint Key - "+wayPointData.get(i).getWay_Point_Key());
			feature.setDesc("WayPoint Key - "+wayPointData.get(i).getWay_Point_Key());
			feature.setLat(Float.parseFloat(wayPointData.get(i).getLatitude()));
			feature.setLon(Float.parseFloat(wayPointData.get(i).getLongitude()));
			feature.setTilt(45);
			feature.setRange(3000);
			featurelist.add(feature);
		}
		features.setFeatures(featurelist);
		return features;
	}
}
