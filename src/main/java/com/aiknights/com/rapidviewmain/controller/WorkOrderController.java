package com.aiknights.com.rapidviewmain.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.aiknights.com.rapidviewmain.models.FreeBusyAPIResponseModel;
import com.aiknights.com.rapidviewmain.models.UpdateWORequestModel;
import com.aiknights.com.rapidviewmain.models.WorkOrderDetailsResponseModel;
import com.aiknights.com.rapidviewmain.models.WorkOrderEventSlotsModel;
import com.aiknights.com.rapidviewmain.service.GetIDService;
import com.aiknights.com.rapidviewmain.service.WebSocketService;
import com.aiknights.com.rapidviewmain.service.WorkOrderDBService;

@RequestMapping("/api")
@RestController
public class WorkOrderController {

	@Autowired
	WebSocketService wsservice;

	@Autowired
	WorkOrderDBService dbservice;

	@PostMapping(value = "/createWorkOrder", consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Map<String, String>> UpdateDefaultWorkOrder(@RequestBody UpdateWORequestModel reqdata) {
		final Map<String, String> responseMap = new HashMap<>();
		boolean status = false;
		String workOrderStatus;
		String workOrderID = reqdata.getWorkOrderID();
		String assignmentType = reqdata.getAssignmentType();
		String assignedTo;
		if (assignmentType.equals("MarketPlace")) {
			assignedTo = null;
			workOrderStatus = "Marketplace";
		} else if (assignmentType.equals("Internal")) {
			assignedTo = reqdata.getAssignedTo();
			workOrderStatus = "Assigned";
		} else {
			assignedTo = null;
			workOrderStatus = "Marketplace";
		}
		String woStartDate = reqdata.getWoStartDate();
		String woEndDate = reqdata.getWoEndDate();
		String notes = reqdata.getNotes();
		status = dbservice.updateDefaultWorkOrderByID(workOrderStatus, workOrderID, assignedTo, woStartDate, woEndDate,
				notes);
		if (status == true) {
			responseMap.put("Status", "Success");
		} else {
			responseMap.put("Status", "Failure");
		}
		return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
	}

	@PostMapping(value = "/updateWorkOrder", consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Map<String, String>> editWorkOrderDetails(@RequestBody UpdateWORequestModel reqdata) {
		final Map<String, String> responseMap = new HashMap<>();
		boolean status = false;
		String workOrderStatus;
		String workOrderID = reqdata.getWorkOrderID();
		String assignmentType = reqdata.getAssignmentType();
		String assignedTo;
		if (assignmentType.equals("MarketPlace")) {
			assignedTo = null;
			workOrderStatus = "Marketplace";
		} else if (assignmentType.equals("Internal")) {
			assignedTo = reqdata.getAssignedTo();
			workOrderStatus = "Assigned";
		} else {
			assignedTo = null;
			workOrderStatus = "Marketplace";
		}
		String woStartDate = reqdata.getWoStartDate();
		String woEndDate = reqdata.getWoEndDate();
		String notes = reqdata.getNotes();
		status = dbservice.updateDefaultWorkOrderByID(workOrderStatus, workOrderID, assignedTo, woStartDate, woEndDate,
				notes);
		if (status == true) {
			responseMap.put("Status", "Success");
		} else {
			responseMap.put("Status", "Failure");
		}
		return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
	}

	@RequestMapping("/updateWOApproval")
	public ResponseEntity<Map<String, String>> updateWorkOrderApproval(@RequestBody final Map<String, String> data) {
		final Map<String, String> responseMap = new HashMap<>();
		boolean processStatus = false;
		boolean ApprovalprocessStatus = false;
		String Context = data.get("context");
		if (Context.equals("SignOff")) {
			String WorkOrderID = data.get("workOrderID");
			String ApprovalStatus = "Approved";
			String WorkOrderStatus = "Approved";
			processStatus = dbservice.updateWorkOrderStatusByID(WorkOrderID, WorkOrderStatus, ApprovalStatus);
			ApprovalprocessStatus = dbservice.updateWorkOrderApprovalStatus(WorkOrderID, ApprovalStatus);
			if (processStatus == true && ApprovalprocessStatus == true) {
				responseMap.put("Status", "Success");
			} else {
				responseMap.put("Status", "Failure");
			}
			return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
		} else if (Context.equals("Reopen")) {
			String WorkOrderID = data.get("workOrderID");
			String ApprovalStatus = "ReOpened";
			String WorkOrderStatus = "Re-Assigned";
			String ReopenComment = data.get("reOpenComment");
			boolean reopencountupdate;
			List<WorkOrderDetailsResponseModel> wodata = dbservice.getOrderByWOID(WorkOrderID);
			int OldCount = Integer.parseInt(wodata.get(0).getwOReopenCount());
			String NewCount = Integer.toString(OldCount + 1);
			processStatus = dbservice.updateWorkOrderStatusByID(WorkOrderID, WorkOrderStatus, ApprovalStatus);
			reopencountupdate = dbservice.updateWorkOrderReopenCount(WorkOrderID, NewCount);
			ApprovalprocessStatus = dbservice.reOpenWorkOrderUpdateMethod(WorkOrderID, ApprovalStatus, ReopenComment);
			if (processStatus == true && ApprovalprocessStatus == true && reopencountupdate == true) {
				responseMap.put("Status", "Success");
			} else {
				responseMap.put("Status", "Failure");
			}
			return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
		} else if (Context.equals("PaymentRequest")) {
			String WorkOrderID = data.get("workOrderID");
			String ApprovalStatus = "PaymentRequested";
			String WorkOrderStatus = "PaymentRequested";
			processStatus = dbservice.updateWorkOrderStatusByID(WorkOrderID, WorkOrderStatus, ApprovalStatus);
			ApprovalprocessStatus = dbservice.updateWorkOrderApprovalStatus(WorkOrderID, ApprovalStatus);
			if (processStatus == true && ApprovalprocessStatus == true) {
				responseMap.put("Status", "Success");
			} else {
				responseMap.put("Status", "Failure");
			}
			return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
		} else {
			responseMap.put("Status", "Failure");
			return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
		}
	}

	@RequestMapping("/updateWOStatus")
	public ResponseEntity<Map<String, String>> updateWorkOrderStatus(@RequestBody final Map<String, String> data) {
		final Map<String, String> responseMap = new HashMap<>();
		boolean processStatus = false;
		String Context = data.get("context");
		if (Context.equals("MarketPlace")) {
			String WorkOrderID = data.get("workOrderID");
			String WorkOrderStatus = "Assigned";
			String AssignedTo = data.get("assignedTo");
			processStatus = dbservice.updateWorkOrderMarketPlaceAssignment(WorkOrderID, WorkOrderStatus, AssignedTo);
			if (processStatus == true) {
				responseMap.put("Status", "Success");
			} else {
				responseMap.put("Status", "Failure");
			}
			return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
		} else if (Context.equals("WorkOrders")) {
			String WorkOrderStatus = data.get("workOrderStatus");
			String WorkOrderApprovalStatus;
			if (WorkOrderStatus.equals("Completed")) {
				WorkOrderApprovalStatus = "PendingApproval";
				String WorkOrderID = data.get("workOrderID");
				processStatus = dbservice.updateWorkOrderStatusByID(WorkOrderID, WorkOrderStatus,
						WorkOrderApprovalStatus);
				if (processStatus == true) {
					List<WorkOrderDetailsResponseModel> wodata = dbservice.getOrderByWOID(WorkOrderID);
					responseMap.put("Status", "Success");
					String SenderUserID = "0";
					String ReceiverUserID = wodata.get(0).getAssignedBy();
					String ReceiverUserGroup = "";
					String Notification_Type_ID = "SPECIFIC_USER";
					String Notification_Claim_ID = wodata.get(0).getClaimID();
					String Notification_Subject = "Action Needed! WorkOrder Approval";
					String Message_content = "Workorder - \'" + WorkOrderID + "\' for Claim - \'"
							+ wodata.get(0).getClaimID()
							+ "\' has been marked as Completed and is waiting for Approval";
					Boolean message = wsservice.sendMessageService(SenderUserID, ReceiverUserID, ReceiverUserGroup,
							Notification_Type_ID, Notification_Claim_ID, Notification_Subject, Message_content);
					System.out.println("Notification sent? :" + message);
				} else {
					responseMap.put("Status", "Failure");
				}
				return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);

			} else {
				responseMap.put("Status", "Failure");
				return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
			}
		} else {
			responseMap.put("Status", "Failure");
			return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
		}
	}

	@RequestMapping("/getWorkOrders")
	public ResponseEntity<Map<String, List<WorkOrderDetailsResponseModel>>> getWorkOrders(
			@RequestBody final Map<String, String> data) {
		Map<String, List<WorkOrderDetailsResponseModel>> responsemap = new HashMap<>();
		String userType = data.get("userType");
		String userID = data.get("userID");
		List<WorkOrderDetailsResponseModel> responselist = new ArrayList<>();
		if (userType.equals("ADJUSTER")) {
			responselist = dbservice.getOrdersAsAdjuster(userID);
			Collections.sort(responselist);
		} else if (userType.equals("PILOT")) {
			responselist = dbservice.getOrdersAsPilot(userID);
			Collections.sort(responselist);
		} else if (userType.equals("MarketPlace")) {
			responselist = dbservice.getMarketPlaceOrders();
			Collections.sort(responselist);
		} else {
			responselist = null;
		}
		responsemap.put("Orders", responselist);
		return new ResponseEntity<>(responsemap, HttpStatus.OK);
	}

	@RequestMapping("/freeBusyApi")
	public ResponseEntity<Map<String, List<FreeBusyAPIResponseModel>>> checkAvailability(
			@RequestBody final Map<String, String> data) throws ParseException {
		String givenDate = data.get("date");
		String adjusterID = data.get("adjusterid");
		String pilotID = data.get("pilotid");
		int duration = Integer.parseInt(data.get("duration"));
		String defaultStartTime = "09:00:00";
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Date date = null;
		try {
			date = sdf.parse(defaultStartTime);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Date rawtime = date;
		String defaultformattedStartTime = sdf.format(date);
		List<WorkOrderEventSlotsModel> slotlist = new ArrayList<WorkOrderEventSlotsModel>();
		for (int i = 0; i < 6; i++) {
			WorkOrderEventSlotsModel slot = new WorkOrderEventSlotsModel();
			slot.setStartTime(defaultformattedStartTime);
			Calendar cal = Calendar.getInstance();
			Calendar cal1 = Calendar.getInstance();
			cal.setTime(rawtime);
			cal1.setTime(rawtime);
			cal.add(Calendar.MINUTE, duration);
			String newTime = sdf.format(cal.getTime());
			cal1.add(Calendar.MINUTE, 60);
			String newTime1 = sdf.format(cal1.getTime());
			slot.setEndTime(newTime);
			slotlist.add(slot);
			defaultformattedStartTime = newTime1;
			rawtime = sdf.parse(newTime1);
		}
		List<FreeBusyAPIResponseModel> responselist = new ArrayList<FreeBusyAPIResponseModel>();
		Map<String, List<FreeBusyAPIResponseModel>> responsemap = new HashMap<>();
		for (int i = 0; i < slotlist.size(); i++) {
			FreeBusyAPIResponseModel response = new FreeBusyAPIResponseModel();
			String starttime = slotlist.get(i).getStartTime();
			String endtime = slotlist.get(i).getEndTime();
			response.setStartTime(starttime);
			response.setEndTime(endtime);
			response.setSlotIndex(Integer.toString(i + 1));
			Calendar cal = Calendar.getInstance();
			Date endtimed = sdf.parse(endtime);
			cal.setTime(endtimed);
			cal.add(Calendar.SECOND, -1);
			endtime = sdf.format(cal.getTime());
			Calendar cal1 = Calendar.getInstance();
			Date starttimed = sdf.parse(starttime);
			cal1.setTime(starttimed);
			cal1.add(Calendar.SECOND, 1);
			starttime = sdf.format(cal1.getTime());
			String adjusterStatus = dbservice.checkAdjusterEventAvailability(adjusterID, givenDate, starttime, endtime);
			String pilotStatus = dbservice.checkPilotEventAvailability(pilotID, givenDate, starttime, endtime);
			response.setAdjusterStatus(adjusterStatus);
			response.setPilotStatus(pilotStatus);
			responselist.add(response);
		}
		responsemap.put("Schedule", responselist);
		return new ResponseEntity<>(responsemap, HttpStatus.OK);
	}
	
	@RequestMapping("/createEvent")
	public ResponseEntity<Map<String, String>> createEvent(@RequestBody final Map<String, String> data) {
		final Map<String, String> responseMap = new HashMap<>();
		boolean insertStatus;
		String eventDate = data.get("date");
		String adjuster_ID = data.get("adjusterid");
		String pilot_ID = data.get("pilotid");
		String timePeriod = data.get("duration");
		String slotIndex = data.get("slotIndex");
		String startTime = data.get("startTime");
		String endTime = data.get("endTime");
		String workOrderID = data.get("workOrderID");
		String eventLink = data.get("eventLink");
		String eventSystem="Google";
		String eventID= Long.toString(GetIDService.getID());
		insertStatus = dbservice.createEvent(workOrderID,eventID,eventLink,adjuster_ID,pilot_ID,eventSystem,startTime,endTime,eventDate,timePeriod);
		if (insertStatus == true) {
			responseMap.put("Status", "Success");
		} else {
			responseMap.put("Status", "Failure");
		}
		return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
	}
}
