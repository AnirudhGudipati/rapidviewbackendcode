package com.aiknights.com.rapidviewmain.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aiknights.com.rapidviewmain.models.SubscriptionPropertiesModel;
import com.aiknights.com.rapidviewmain.models.SubscriptionPropertiesResponce;
import com.aiknights.com.rapidviewmain.service.SubscriptionService;

@RestController
public class SubscriptionController {

	@Autowired
	SubscriptionService subscriptionservice;

	@RequestMapping(value = "/subscriptionproperties/{subscriptionProperties}")
	public ResponseEntity<Map<String, List<SubscriptionPropertiesModel>>> subscriptionProperties(
			@PathVariable final String subscriptionProperties) {
		final Map<String, List<SubscriptionPropertiesModel>> responseMap = new HashMap<>();
		List<SubscriptionPropertiesModel> SubscriptionProperties = subscriptionservice.getSubscriptionProperties();
		responseMap.put("SubscriptionProperties", SubscriptionProperties);

		return new ResponseEntity<Map<String, List<SubscriptionPropertiesModel>>>(responseMap, HttpStatus.OK);
	}

	@RequestMapping(value = "/subscriptiondetails/{subscriptionDetails}")
	public ResponseEntity<Map<String, List<SubscriptionPropertiesResponce>>> subscriptionDetails(
			@PathVariable final String subscriptionDetails) {
		final Map<String, List<SubscriptionPropertiesResponce>> responseMap = new HashMap<>();
		List<SubscriptionPropertiesResponce> SubscriptionDetails = subscriptionservice.getSubscriptionDetails();
		responseMap.put("SubscriptionDeatils", SubscriptionDetails);

		return new ResponseEntity<Map<String, List<SubscriptionPropertiesResponce>>>(responseMap, HttpStatus.OK);
	}
}
