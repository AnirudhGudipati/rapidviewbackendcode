package com.aiknights.com.rapidviewmain.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aiknights.com.rapidviewmain.models.NotificationAlterInputPayloadModel;
import com.aiknights.com.rapidviewmain.models.WebSocketMessageModel;
import com.aiknights.com.rapidviewmain.service.NotificationDBService;
import com.aiknights.com.rapidviewmain.service.WebSocketService;
@RequestMapping("/api")
@EnableScheduling
@Controller
public class NotificationController {

	@Autowired
	SimpMessagingTemplate template;
	@Autowired
	WebSocketService wsservice;
	@Autowired
	NotificationDBService dbservice;
	@RequestMapping("/wsexample")
	public String wsexample() {
		return "index";
	}

	@RequestMapping("/getMessagesForUser/{UserID}")
	public ResponseEntity<Map<String, List<WebSocketMessageModel>>> getMessagesForUser(@PathVariable("UserID") String UserID){
		final Map<String,List<WebSocketMessageModel>> responseMap = new HashMap<>();
		List<WebSocketMessageModel> messagelist = dbservice.getMessagesOfUser(UserID);
		responseMap.put("messageList", messagelist);
		return new ResponseEntity<>(responseMap,HttpStatus.OK);
	}
	
	@PostMapping(value = "/sendMessage",  consumes="application/json")
	@ResponseBody
	public ResponseEntity<Map<String, String>> broadcast(@RequestBody final Map<String, String> messageDetails) {
		final Map<String,String> responseMap = new HashMap<>();
		String SenderUserID = messageDetails.get("SenderUserID");
		String ReceiverUserID = messageDetails.get("ReceiverUserID");
		String ReceiverUserGroup = messageDetails.get("ReceiverUserGroup");
		String Notification_Type_ID = messageDetails.get("Notification_Type_ID");
		String Notification_Claim_ID = messageDetails.get("notification_Claim_ID");
		String Notification_Subject = messageDetails.get("Notification_Subject");
		String Message_content = messageDetails.get("Message_content");
		if(SenderUserID!="" && Notification_Type_ID!="" && Notification_Subject!="" && Message_content!="") {
			Boolean messageoutput = wsservice.sendMessageService(SenderUserID, ReceiverUserID, ReceiverUserGroup, Notification_Type_ID, Notification_Claim_ID, Notification_Subject, Message_content);
			System.out.println("messageoutput: "+messageoutput);
			if(messageoutput == true) {
				responseMap.put("Status", "Success");
			}else{
				responseMap.put("Status", "Failure");
			}
		}else{
			responseMap.put("Status", "Failure");
		}
		return new ResponseEntity<Map<String,String>>(responseMap,HttpStatus.OK);
	}
	
	@MessageMapping("/notificationRead")
	public String markNotificationReadAsReceiver(NotificationAlterInputPayloadModel data) {
		System.out.println(data.getUserId()+"  "+data.getNotification_Object_ID());
		String UserID = data.getUserId();
		String Notification_Object_ID = data.getNotification_Object_ID();
		boolean output = dbservice.markNotificationAsRead(UserID,Notification_Object_ID);
		System.out.println(output);
		if(output = true) {
			return "Message marked as read";
		}else{
			return "Error marking message as read";
		}
	}
	
	@RequestMapping("/deleteNotification")
	public ResponseEntity<Map<String, String>> notificationDeleted(@RequestBody final Map<String, String> data) {
		final Map<String,String> responseMap = new HashMap<>();
		System.out.println(data.get("userId")+"  "+data.get("notification_Object_ID"));
		String UserID = data.get("userId");
		String Notification_Object_ID = data.get("notification_Object_ID");
		boolean output = dbservice.deletedNotificationAsReceiver(UserID,Notification_Object_ID);
		System.out.println(output);
		if(output == true) {
			responseMap.put("Status", "Success");
			String Notification_Topic ="/topic/"+UserID+"/NotificationRefresh";
			template.convertAndSend(Notification_Topic,"refresh");
		}else{
			responseMap.put("Status", "Failure");
		}
		return new ResponseEntity<Map<String,String>>(responseMap,HttpStatus.OK);
	}
}
