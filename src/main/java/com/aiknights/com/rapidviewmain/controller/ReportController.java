package com.aiknights.com.rapidviewmain.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.aiknights.com.rapidviewmain.models.AnalyseReponseInDetail;
import com.aiknights.com.rapidviewmain.models.AnalyseReponseInDetailParentModel;
import com.aiknights.com.rapidviewmain.models.ClaimNotification;
import com.aiknights.com.rapidviewmain.models.ClaimNotificationParentModel;
import com.aiknights.com.rapidviewmain.models.FlaskServiceChildrenModel;
import com.aiknights.com.rapidviewmain.models.FlaskServiceModel;
import com.aiknights.com.rapidviewmain.models.GMapOutputParentModel;
import com.aiknights.com.rapidviewmain.models.GoogleLiveMapDataModel;
import com.aiknights.com.rapidviewmain.models.GoogleLiveMapDataParentModel;
import com.aiknights.com.rapidviewmain.models.NexRadServiceChildrenModel;
import com.aiknights.com.rapidviewmain.models.NexRadServiceModel;
import com.aiknights.com.rapidviewmain.models.ReportsStageDBModel;
import com.aiknights.com.rapidviewmain.models.WeatherAssessmentDataModel;
import com.aiknights.com.rapidviewmain.models.WeatherDataforBoxChartsModel;
import com.aiknights.com.rapidviewmain.models.WeatherDataforBoxChartsParentModel;
import com.aiknights.com.rapidviewmain.models.WeatherEDBModel;
import com.aiknights.com.rapidviewmain.models.WeatherNexRadModel;
import com.aiknights.com.rapidviewmain.models.WeatherOutputModel;
import com.aiknights.com.rapidviewmain.models.WeatherStageDatabaseModel;
import com.aiknights.com.rapidviewmain.service.ClaimNotificationService;
import com.aiknights.com.rapidviewmain.service.GetIDService;
import com.aiknights.com.rapidviewmain.service.GoogleMapAPIService;
import com.aiknights.com.rapidviewmain.service.UploadImageService;
import com.aiknights.com.rapidviewmain.service.UploadWeatherAssessmentDetailstoDBService;
import com.aiknights.com.rapidviewmain.service.UserDetailsService;
import com.aiknights.com.rapidviewmain.service.WeatherDBService;
import com.aiknights.com.rapidviewmain.service.WeatherDataConversionService;
import com.aiknights.com.rapidviewmain.service.WeatherReportifyService;

@RequestMapping("/report")
@Controller
public class ReportController {
	// Upload Report - Branch Testing
	// Upload Report - Branch Testing 2

	@Autowired
	private UploadWeatherAssessmentDetailstoDBService uploadWeatherDataService;

	@Autowired
	private WeatherDBService weatherDBService;

	@Autowired
	private WeatherDBController weatherDBController;

	@Autowired
	private UploadImageService uploadService;

	@Autowired
	private ClaimNotificationService claimService;

	String Timezone = "GMT";

	@RequestMapping("/reporthome")
	public String home() {
		return "home";
	}

	@RequestMapping("/reporthomelocal")
	public String homelocal() {
		return "homelocal";
	}

	@Autowired
	UserDetailsService userservice;

	@RequestMapping("/viewreport/{claimid}")
	public ModelAndView viewreport(@PathVariable("claimid") String claimid) {
		ModelAndView viewmv = new ModelAndView();
		ClaimNotificationParentModel cm = new ClaimNotificationParentModel();
		ClaimNotification claimdata = claimService.getClaimByClaimId(claimid);
		cm.setClaim(claimdata);
		String address = cm.getClaim().getAddress() + " " + cm.getClaim().getCity() + " " + cm.getClaim().getState()
				+ " " + cm.getClaim().getZipCode();
		System.out.println(
				"Claim Report Viewed for Claim ID : " + cm.getClaim().getClaimId() + " with address : " + address);
		System.out.println("Requesting GoogleMap API for Coordinates of the Address");
		GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
		System.out.println("GoogleMap API Output Status : " + wm.getStatus());
		new GetIDService();
		long UID = GetIDService.getID();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone(Timezone));
		Date dateo = new Date();
		String dateobj = df.format(dateo) + " " + Timezone;
		String latitude = "";
		String longitude = "";
		for (int i = 0; i < wm.getResults().size(); i++) {
			latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
			longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
		}
		System.out.println("GoogleMap Coordinate Output");
		System.out.println("Latitude : " + latitude);
		System.out.println("Longitude : " + longitude);
		ClaimNotificationParentModel pm = new ClaimNotificationParentModel();
		ClaimNotification claim = claimService.getClaimWithPolicyDataByClaimId(claimid);
		pm.setClaim(claim);
		List<AnalyseReponseInDetail> imageList = uploadService.getIncidentEvidenceForClaim(claimid);
		AnalyseReponseInDetailParentModel lim = new AnalyseReponseInDetailParentModel();
		lim.setImageList(imageList);
		List<WeatherOutputModel> wt = new ArrayList<WeatherOutputModel>();
		List<WeatherStageDatabaseModel> stagelist = weatherDBService.getWeatherDataFromStageByClaimID(claimid);
		wt = new WeatherDataConversionService().ConvertToWeatherOutputModelFromStage(stagelist);
		Collections.sort(wt);
		Collections.reverse(wt);
		List<WeatherOutputModel> xt = new ArrayList<WeatherOutputModel>();
		xt = new WeatherReportifyService().Reportify(wt);
		List<WeatherDataforBoxChartsModel> wl = weatherDBController.getWeatherDataFromStageByClaimID(claimid);
		WeatherDataforBoxChartsParentModel weatherdataforboxplot = new WeatherDataforBoxChartsParentModel();
		weatherdataforboxplot.setWeatherData(wl);
		List<GoogleLiveMapDataModel> gl = weatherDBController.getGMapPlotWeatherDataFromStageByClaimID(claimid);
		GoogleLiveMapDataParentModel dataforgmap = new GoogleLiveMapDataParentModel();
		dataforgmap.setGMapMarkerData(gl);
		System.out.println("Sending Data to Report View");
		List<WeatherOutputModel> wd = xt;
		viewmv.addObject("boxplotdata", weatherdataforboxplot.getWeatherData());
		viewmv.addObject("gmapdata", dataforgmap.getGMapMarkerData());
		viewmv.addObject("UID", UID);
		viewmv.addObject("cd", cm.getClaim());
		viewmv.addObject("pd", pm.getClaim());
		viewmv.addObject("id", lim.getImageList());
		viewmv.addObject("ReportGenDateTime", dateobj);
		viewmv.addObject("latitude", latitude);
		viewmv.addObject("longitude", longitude);
		viewmv.addObject("wd", wd);
		viewmv.addObject("weathereventscount", new WeatherReportifyService().getCount());
		viewmv.setViewName("ReportView");
		return viewmv;
	}

	@RequestMapping("/downloadreport/{claimid}")
	public ModelAndView downloadreport(@PathVariable("claimid") String claimid) {
		ModelAndView downmv = new ModelAndView();
		ClaimNotificationParentModel cm = new ClaimNotificationParentModel();
		ClaimNotification claimdata = claimService.getClaimByClaimId(claimid);
		cm.setClaim(claimdata);
		String address = cm.getClaim().getAddress() + " " + cm.getClaim().getCity() + " " + cm.getClaim().getState()
				+ " " + cm.getClaim().getZipCode();
		System.out.println(
				"Claim Report Downloaded for Claim ID : " + cm.getClaim().getClaimId() + " with address : " + address);
		System.out.println("Requesting GoogleMap API for Coordinates of the Address");
		GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
		System.out.println("GoogleMap API Output Status : " + wm.getStatus());
		new GetIDService();
		long UID = GetIDService.getID();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone(Timezone));
		Date dateo = new Date();
		String dateobj = df.format(dateo) + " " + Timezone;
		String latitude = "";
		String longitude = "";
		for (int i = 0; i < wm.getResults().size(); i++) {
			latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
			longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
		}
		System.out.println("GoogleMap Coordinate Output");
		System.out.println("Latitude : " + latitude);
		System.out.println("Longitude : " + longitude);
		ClaimNotificationParentModel pm = new ClaimNotificationParentModel();
		ClaimNotification claim = claimService.getClaimWithPolicyDataByClaimId(claimid);
		pm.setClaim(claim);
		List<AnalyseReponseInDetail> imageList = uploadService.getIncidentEvidenceForClaim(claimid);
		AnalyseReponseInDetailParentModel lim = new AnalyseReponseInDetailParentModel();
		lim.setImageList(imageList);
		List<WeatherOutputModel> wt = new ArrayList<WeatherOutputModel>();
		List<WeatherStageDatabaseModel> stagelist = weatherDBService.getWeatherDataFromStageByClaimID(claimid);
		wt = new WeatherDataConversionService().ConvertToWeatherOutputModelFromStage(stagelist);
		Collections.sort(wt);
		Collections.reverse(wt);
		List<WeatherOutputModel> xt = new ArrayList<WeatherOutputModel>();
		xt = new WeatherReportifyService().Reportify(wt);
		List<WeatherDataforBoxChartsModel> wl = weatherDBController.getWeatherDataFromStageByClaimID(claimid);
		WeatherDataforBoxChartsParentModel weatherdataforboxplot = new WeatherDataforBoxChartsParentModel();
		weatherdataforboxplot.setWeatherData(wl);
		List<GoogleLiveMapDataModel> gl = weatherDBController.getGMapPlotWeatherDataFromStageByClaimID(claimid);
		GoogleLiveMapDataParentModel dataforgmap = new GoogleLiveMapDataParentModel();
		dataforgmap.setGMapMarkerData(gl);
		System.out.println("Sending Data to Report Download View");
		List<WeatherOutputModel> wd = xt;
		downmv.addObject("boxplotdata", weatherdataforboxplot.getWeatherData());
		downmv.addObject("gmapdata", dataforgmap.getGMapMarkerData());
		downmv.addObject("UID", UID);
		downmv.addObject("cd", cm.getClaim());
		downmv.addObject("pd", pm.getClaim());
		downmv.addObject("id", lim.getImageList());
		downmv.addObject("ReportGenDateTime", dateobj);
		downmv.addObject("latitude", latitude);
		downmv.addObject("longitude", longitude);
		downmv.addObject("wd", wd);
		downmv.addObject("weathereventscount", new WeatherReportifyService().getCount());
		downmv.setViewName("ReportDownload");
		return downmv;
	}

	@RequestMapping("/downloadweatherreport/{claimid}")
	public ModelAndView downloadweatherreport(@PathVariable("claimid") String claimid) {
		ModelAndView downmv = new ModelAndView();
		ClaimNotificationParentModel cm = new ClaimNotificationParentModel();
		ClaimNotification claimdata = claimService.getClaimByClaimId(claimid);
		cm.setClaim(claimdata);
		String address = cm.getClaim().getAddress() + " " + cm.getClaim().getCity() + " " + cm.getClaim().getState()
				+ " " + cm.getClaim().getZipCode();
		System.out.println("Claim Weather Report Downloaded for Claim ID : " + cm.getClaim().getClaimId()
				+ " with address : " + address);
		System.out.println("Requesting GoogleMap API for Coordinates of the Address");
		GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
		System.out.println("GoogleMap API Output Status : " + wm.getStatus());
		new GetIDService();
		long UID = GetIDService.getID();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone(Timezone));
		Date dateo = new Date();
		String dateobj = df.format(dateo) + " " + Timezone;
		String latitude = "";
		String longitude = "";
		for (int i = 0; i < wm.getResults().size(); i++) {
			latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
			longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
		}
		System.out.println("GoogleMap Coordinate Output");
		System.out.println("Latitude : " + latitude);
		System.out.println("Longitude : " + longitude);
		ClaimNotificationParentModel pm = new ClaimNotificationParentModel();
		ClaimNotification claim = claimService.getClaimWithPolicyDataByClaimId(claimid);
		pm.setClaim(claim);
		List<WeatherOutputModel> wt = new ArrayList<WeatherOutputModel>();
		List<WeatherStageDatabaseModel> stagelist = weatherDBService.getWeatherDataFromStageByClaimID(claimid);
		wt = new WeatherDataConversionService().ConvertToWeatherOutputModelFromStage(stagelist);
		Collections.sort(wt);
		Collections.reverse(wt);
		List<WeatherDataforBoxChartsModel> wl = weatherDBController.getWeatherDataFromStageByClaimID(claimid);
		WeatherDataforBoxChartsParentModel weatherdataforboxplot = new WeatherDataforBoxChartsParentModel();
		weatherdataforboxplot.setWeatherData(wl);
		List<GoogleLiveMapDataModel> gl = weatherDBController.getGMapPlotWeatherDataFromStageByClaimID(claimid);
		GoogleLiveMapDataParentModel dataforgmap = new GoogleLiveMapDataParentModel();
		dataforgmap.setGMapMarkerData(gl);
		List<WeatherOutputModel> xt = new ArrayList<WeatherOutputModel>();
		xt = new WeatherReportifyService().Reportify(wt);
		System.out.println("Sending Data to Weather Report Download View");
		List<WeatherOutputModel> wd = xt;
		downmv.addObject("boxplotdata", weatherdataforboxplot.getWeatherData());
		downmv.addObject("gmapdata", dataforgmap.getGMapMarkerData());
		downmv.addObject("UID", UID);
		downmv.addObject("cd", cm.getClaim());
		downmv.addObject("pd", pm.getClaim());
		downmv.addObject("ReportGenDateTime", dateobj);
		downmv.addObject("latitude", latitude);
		downmv.addObject("longitude", longitude);
		downmv.addObject("wd", wd);
		downmv.addObject("weathereventscount", new WeatherReportifyService().getCount());
		downmv.setViewName("WeatherReportDownload");
		return downmv;

	}

	@RequestMapping("/viewweatherreport/{claimid}")
	public ModelAndView viewweatherreport(@PathVariable("claimid") String claimid) {
		ModelAndView viewmv = new ModelAndView();
		ClaimNotificationParentModel cm = new ClaimNotificationParentModel();
		ClaimNotification claimdata = claimService.getClaimByClaimId(claimid);
		cm.setClaim(claimdata);
		String address = cm.getClaim().getAddress() + " " + cm.getClaim().getCity() + " " + cm.getClaim().getState()
				+ " " + cm.getClaim().getZipCode();
		System.out.println("Claim Weather Report Viewed for Claim ID : " + cm.getClaim().getClaimId()
				+ " with address : " + address);
		System.out.println("Requesting GoogleMap API for Coordinates of the Address");
		GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
		System.out.println("GoogleMap API Output Status : " + wm.getStatus());
		new GetIDService();
		long UID = GetIDService.getID();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone(Timezone));
		Date dateo = new Date();
		String dateobj = df.format(dateo) + " " + Timezone;
		String latitude = "";
		String longitude = "";
		for (int i = 0; i < wm.getResults().size(); i++) {
			latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
			longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
		}
		System.out.println("GoogleMap Coordinate Output");
		System.out.println("Latitude : " + latitude);
		System.out.println("Longitude : " + longitude);
		ClaimNotificationParentModel pm = new ClaimNotificationParentModel();
		ClaimNotification claim = claimService.getClaimWithPolicyDataByClaimId(claimid);
		pm.setClaim(claim);
		List<WeatherOutputModel> wt = new ArrayList<WeatherOutputModel>();
		List<WeatherStageDatabaseModel> stagelist = weatherDBService.getWeatherDataFromStageByClaimID(claimid);
		wt = new WeatherDataConversionService().ConvertToWeatherOutputModelFromStage(stagelist);
		Collections.sort(wt);
		Collections.reverse(wt);
		List<WeatherDataforBoxChartsModel> wl = weatherDBController.getWeatherDataFromStageByClaimID(claimid);
		WeatherDataforBoxChartsParentModel weatherdataforboxplot = new WeatherDataforBoxChartsParentModel();
		weatherdataforboxplot.setWeatherData(wl);
		List<GoogleLiveMapDataModel> gl = weatherDBController.getGMapPlotWeatherDataFromStageByClaimID(claimid);
		GoogleLiveMapDataParentModel dataforgmap = new GoogleLiveMapDataParentModel();
		dataforgmap.setGMapMarkerData(gl);
		List<WeatherOutputModel> xt = new ArrayList<WeatherOutputModel>();
		xt = new WeatherReportifyService().Reportify(wt);
		System.out.println("Sending Data to Weather Report View");
		List<WeatherOutputModel> wd = xt;
		viewmv.addObject("boxplotdata", weatherdataforboxplot.getWeatherData());
		viewmv.addObject("gmapdata", dataforgmap.getGMapMarkerData());
		viewmv.addObject("UID", UID);
		viewmv.addObject("cd", cm.getClaim());
		viewmv.addObject("pd", pm.getClaim());
		viewmv.addObject("ReportGenDateTime", dateobj);
		viewmv.addObject("latitude", latitude);
		viewmv.addObject("longitude", longitude);
		viewmv.addObject("wd", wd);
		viewmv.addObject("weathereventscount", new WeatherReportifyService().getCount());
		viewmv.setViewName("WeatherReportView");
		return viewmv;

	}

	@RequestMapping("/weatherassessmentview/{claimid}&{carrier}&{address}&{policystartdate}&{claimdate}&{ownername}")
	public ModelAndView weatherassessmentview(@PathVariable("address") String address,
			@PathVariable("policystartdate") String policystartdate, @PathVariable("claimdate") String claimdate,
			@PathVariable("claimid") String claimid, @PathVariable("carrier") String carrier,
			@PathVariable("ownername") String ownername) {
		ModelAndView viewmv = new ModelAndView();
		String givenaddress = address;
		String givenpolicydate = policystartdate;
		String givenclaimdate = claimdate;
		System.out.println("Weather Assessment Viewed for address : " + givenaddress);
		System.out.println("from policy start date : " + givenpolicydate);
		System.out.println("to claim date : " + givenclaimdate);
		System.out.println("Requesting GoogleMap API for Coordinates of the Address");
		GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
		System.out.println("GoogleMap API Output Status : " + wm.getStatus());
		new GetIDService();
		long UID = GetIDService.getID();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone(Timezone));
		Date dateo = new Date();
		String dateobj = df.format(dateo) + " " + Timezone;
		String latitude = "";
		String longitude = "";
		for (int i = 0; i < wm.getResults().size(); i++) {
			latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
			longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
		}
		System.out.println("GoogleMap Coordinate Output");
		System.out.println("Latitude : " + latitude);
		System.out.println("Longitude : " + longitude);
		System.out.println("Searching Storm Event Database for Events");
		System.out.println("Searching Nex-Rad Database for Events");
		FlaskServiceModel fm = new FlaskServiceModel();
		List<FlaskServiceChildrenModel> FL = new ArrayList<FlaskServiceChildrenModel>();
		List<WeatherEDBModel> weatherData = weatherDBService.getWeatherDBDataByClaimId(givenpolicydate, givenclaimdate,
				latitude, longitude, "0.0", "10.0");
		for (int i = 0; i < weatherData.size(); i++) {
			FlaskServiceChildrenModel fmc = new FlaskServiceChildrenModel();
			fmc.setBegin_date(weatherData.get(i).getBegin_date());
			fmc.setBegin_lat(weatherData.get(i).getBegin_lat());
			fmc.setBegin_location(weatherData.get(i).getBegin_location());
			fmc.setBegin_lon(weatherData.get(i).getBegin_lon());
			fmc.setBegin_time(weatherData.get(i).getBegin_time());
			fmc.setDistance(Double.parseDouble(weatherData.get(i).getDistance()));
			fmc.setEvent_narrative(weatherData.get(i).getEvent_narrative());
			fmc.setEvent_type(weatherData.get(i).getEvent_type());
			fmc.setMagnitude(weatherData.get(i).getMagnitude());
			FL.add(fmc);
		}
		fm.setWeatherdata(FL);
		NexRadServiceModel nm = new NexRadServiceModel();
		List<NexRadServiceChildrenModel> NL = new ArrayList<NexRadServiceChildrenModel>();
		List<WeatherNexRadModel> weatherNexRadData = weatherDBService.getWeatherNexRadDataByClaimId(givenpolicydate,
				givenclaimdate, latitude, longitude, "0.0", "10.0");
		for (int i = 0; i < weatherNexRadData.size(); i++) {
			NexRadServiceChildrenModel nmc = new NexRadServiceChildrenModel();
			nmc.setDistance(Double.parseDouble(weatherNexRadData.get(i).getDistance()));
			nmc.setEvent_date(weatherNexRadData.get(i).getEvent_date());
			nmc.setLat(weatherNexRadData.get(i).getLat());
			nmc.setLon(weatherNexRadData.get(i).getLon());
			nmc.setMaxsize(weatherNexRadData.get(i).getMaxsize());
			nmc.setProb(weatherNexRadData.get(i).getProb());
			nmc.setSevprob(weatherNexRadData.get(i).getSevprob());
			NL.add(nmc);
		}
		nm.setWeatherdata(NL);
		List<WeatherOutputModel> wt = new ArrayList<WeatherOutputModel>();
		List<WeatherDataforBoxChartsModel> bt = new ArrayList<WeatherDataforBoxChartsModel>();
		List<GoogleLiveMapDataModel> gt = new ArrayList<GoogleLiveMapDataModel>();
		List<FlaskServiceChildrenModel> flist = new ArrayList<FlaskServiceChildrenModel>();
		List<NexRadServiceChildrenModel> nlist = new ArrayList<NexRadServiceChildrenModel>();
		flist.addAll(fm.getWeatherdata());
		nlist.addAll(nm.getWeatherdata());
		wt = new WeatherDataConversionService().ConvertToWeatherOutputModel(flist, nlist);
		bt = new WeatherDataConversionService().ConvertToBoxPlotDataModel(flist, nlist);
		Collections.sort(bt);
		Collections.reverse(bt);
		gt = new WeatherDataConversionService().ConvertToGmapDataModel(flist, nlist, latitude, longitude);
		Collections.sort(wt);
		Collections.reverse(wt);
		List<WeatherOutputModel> xt = new ArrayList<WeatherOutputModel>();
		xt = new WeatherReportifyService().Reportify(wt);
		System.out.println("Sending Data to Weather Assessment View");
		List<WeatherOutputModel> wd = xt;
		viewmv.addObject("UID", UID);
		viewmv.addObject("boxplotdata", bt);
		viewmv.addObject("gmapdata", gt);
		viewmv.addObject("address", givenaddress);
		viewmv.addObject("policystartDate", givenpolicydate);
		viewmv.addObject("claimDate", givenclaimdate);
		viewmv.addObject("ReportGenDateTime", dateobj);
		viewmv.addObject("carrier", carrier);
		viewmv.addObject("ownername", ownername);
		viewmv.addObject("claimid", claimid);
		viewmv.addObject("latitude", latitude);
		viewmv.addObject("longitude", longitude);
		viewmv.addObject("wd", wd);
		viewmv.addObject("weathereventscount", new WeatherReportifyService().getCount());
		viewmv.setViewName("WeatherAssessmentView");
		return viewmv;
	}

	@RequestMapping("/weatherassessmentdownload/{claimid}&{carrier}&{address}&{policystartdate}&{claimdate}&{ownername}")
	public ModelAndView weatherassessmentdownload(@PathVariable("address") String address,
			@PathVariable("policystartdate") String policystartdate, @PathVariable("claimdate") String claimdate,
			@PathVariable("claimid") String claimid, @PathVariable("carrier") String carrier,
			@PathVariable("ownername") String ownername) {
		ModelAndView downmv = new ModelAndView();
		String givenaddress = address;
		String givenpolicydate = policystartdate;
		String givenclaimdate = claimdate;
		System.out.println("Weather Assessment Downloaded for address : " + givenaddress);
		System.out.println("from policy start date : " + givenpolicydate);
		System.out.println("to claim date : " + givenclaimdate);
		System.out.println("Requesting GoogleMap API for Coordinates of the Address");
		GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
		System.out.println("GoogleMap API Output Status : " + wm.getStatus());
		new GetIDService();
		long UID = GetIDService.getID();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone(Timezone));
		Date dateo = new Date();
		String dateobj = df.format(dateo) + " " + Timezone;
		String latitude = "";
		String longitude = "";
		for (int i = 0; i < wm.getResults().size(); i++) {
			latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
			longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
		}
		System.out.println("GoogleMap Coordinate Output");
		System.out.println("Latitude : " + latitude);
		System.out.println("Longitude : " + longitude);
		System.out.println("Searching Storm Event Database for Events");
		System.out.println("Searching Nex-Rad Database for Events");
		FlaskServiceModel fm = new FlaskServiceModel();
		List<FlaskServiceChildrenModel> FL = new ArrayList<FlaskServiceChildrenModel>();
		List<WeatherEDBModel> weatherData = weatherDBService.getWeatherDBDataByClaimId(givenpolicydate, givenclaimdate,
				latitude, longitude, "0.0", "10.0");
		for (int i = 0; i < weatherData.size(); i++) {
			FlaskServiceChildrenModel fmc = new FlaskServiceChildrenModel();
			fmc.setBegin_date(weatherData.get(i).getBegin_date());
			fmc.setBegin_lat(weatherData.get(i).getBegin_lat());
			fmc.setBegin_location(weatherData.get(i).getBegin_location());
			fmc.setBegin_lon(weatherData.get(i).getBegin_lon());
			fmc.setBegin_time(weatherData.get(i).getBegin_time());
			fmc.setDistance(Double.parseDouble(weatherData.get(i).getDistance()));
			fmc.setEvent_narrative(weatherData.get(i).getEvent_narrative());
			fmc.setEvent_type(weatherData.get(i).getEvent_type());
			fmc.setMagnitude(weatherData.get(i).getMagnitude());
			FL.add(fmc);
		}
		fm.setWeatherdata(FL);
		NexRadServiceModel nm = new NexRadServiceModel();
		List<NexRadServiceChildrenModel> NL = new ArrayList<NexRadServiceChildrenModel>();
		List<WeatherNexRadModel> weatherNexRadData = weatherDBService.getWeatherNexRadDataByClaimId(givenpolicydate,
				givenclaimdate, latitude, longitude, "0.0", "10.0");
		for (int i = 0; i < weatherNexRadData.size(); i++) {
			NexRadServiceChildrenModel nmc = new NexRadServiceChildrenModel();
			nmc.setDistance(Double.parseDouble(weatherNexRadData.get(i).getDistance()));
			nmc.setEvent_date(weatherNexRadData.get(i).getEvent_date());
			nmc.setLat(weatherNexRadData.get(i).getLat());
			nmc.setLon(weatherNexRadData.get(i).getLon());
			nmc.setMaxsize(weatherNexRadData.get(i).getMaxsize());
			nmc.setProb(weatherNexRadData.get(i).getProb());
			nmc.setSevprob(weatherNexRadData.get(i).getSevprob());
			NL.add(nmc);
		}
		nm.setWeatherdata(NL);
		List<WeatherOutputModel> wt = new ArrayList<WeatherOutputModel>();
		List<WeatherDataforBoxChartsModel> bt = new ArrayList<WeatherDataforBoxChartsModel>();
		List<GoogleLiveMapDataModel> gt = new ArrayList<GoogleLiveMapDataModel>();
		List<FlaskServiceChildrenModel> flist = new ArrayList<FlaskServiceChildrenModel>();
		List<NexRadServiceChildrenModel> nlist = new ArrayList<NexRadServiceChildrenModel>();
		flist.addAll(fm.getWeatherdata());
		nlist.addAll(nm.getWeatherdata());
		wt = new WeatherDataConversionService().ConvertToWeatherOutputModel(flist, nlist);
		bt = new WeatherDataConversionService().ConvertToBoxPlotDataModel(flist, nlist);
		Collections.sort(bt);
		Collections.reverse(bt);
		gt = new WeatherDataConversionService().ConvertToGmapDataModel(flist, nlist, latitude, longitude);
		Collections.sort(wt);
		Collections.reverse(wt);
		List<WeatherOutputModel> xt = new ArrayList<WeatherOutputModel>();
		xt = new WeatherReportifyService().Reportify(wt);
		System.out.println("Sending Data to Weather Assessment Download View");
		List<WeatherOutputModel> wd = xt;
		downmv.addObject("UID", UID);
		downmv.addObject("boxplotdata", bt);
		downmv.addObject("gmapdata", gt);
		downmv.addObject("address", givenaddress);
		downmv.addObject("policystartDate", givenpolicydate);
		downmv.addObject("claimDate", givenclaimdate);
		downmv.addObject("ReportGenDateTime", dateobj);
		downmv.addObject("latitude", latitude);
		downmv.addObject("longitude", longitude);
		downmv.addObject("carrier", carrier);
		downmv.addObject("ownername", ownername);
		downmv.addObject("claimid", claimid);
		downmv.addObject("wd", wd);
		downmv.addObject("weathereventscount", new WeatherReportifyService().getCount());
		downmv.setViewName("WeatherAssessmentDownload");
		return downmv;
	}

	@RequestMapping("/viewweatherassessment/{reportid}")
	public ModelAndView viewweatherassessment(@PathVariable("reportid") String reportid) {
		ModelAndView viewmv = new ModelAndView();
		final WeatherAssessmentDataModel weatherAssessmentData = uploadWeatherDataService
				.getWeatherReportById(reportid);
		String address = weatherAssessmentData.getAddress();
		String policydate = weatherAssessmentData.getPolicystartdate();
		String claimdate = weatherAssessmentData.getClaimdate();
		String carrier = weatherAssessmentData.getCarriername();
		String claimid = weatherAssessmentData.getClaimid();
		String ownername = weatherAssessmentData.getOwnername();
		String reportType = weatherAssessmentData.getReportType();
		String report = null;
		if (reportType.equals("Hail")) {
			report = "WeatherAssessmentView";
		} else {
			report = "WeatherAssessmentViewWind";
		}
		System.out.println("Weather Assessment Viewed for address : " + address);
		System.out.println("from policy start date : " + policydate);
		System.out.println("to claim date : " + claimdate);
		System.out.println("Requesting GoogleMap API for Coordinates of the Address");
		GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
		System.out.println("GoogleMap API Output Status : " + wm.getStatus());
		new GetIDService();
		long UID = GetIDService.getID();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone(Timezone));
		Date dateo = new Date();
		String dateobj = df.format(dateo) + " " + Timezone;
		String latitude = "";
		String longitude = "";
		for (int i = 0; i < wm.getResults().size(); i++) {
			latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
			longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
		}
		System.out.println("GoogleMap Coordinate Output");
		System.out.println("Latitude : " + latitude);
		System.out.println("Longitude : " + longitude);
		List<WeatherOutputModel> wt = new ArrayList<WeatherOutputModel>();
		List<ReportsStageDBModel> stagelist = weatherDBService.getWeatherDataFromStageByReportID(reportid);
		wt = new WeatherDataConversionService().ConvertToWeatherOutputModelFromReportsStage(stagelist);
		Collections.sort(wt);
		Collections.reverse(wt);
		List<WeatherDataforBoxChartsModel> wl = weatherDBController.getWeatherDataFromReportsStageByReportID(reportid);
		WeatherDataforBoxChartsParentModel weatherdataforboxplot = new WeatherDataforBoxChartsParentModel();
		weatherdataforboxplot.setWeatherData(wl);
		List<GoogleLiveMapDataModel> gl = weatherDBController.getGMapPlotWeatherDataFromStageByReportID(reportid);
		GoogleLiveMapDataParentModel dataforgmap = new GoogleLiveMapDataParentModel();
		dataforgmap.setGMapMarkerData(gl);
		List<WeatherOutputModel> xt = new ArrayList<WeatherOutputModel>();
		xt = new WeatherReportifyService().Reportify(wt);
		System.out.println("Sending Data to Weather Assessment View");
		List<WeatherOutputModel> wd = xt;
		viewmv.addObject("UID", UID);
		viewmv.addObject("boxplotdata", weatherdataforboxplot.getWeatherData());
		viewmv.addObject("gmapdata", dataforgmap.getGMapMarkerData());
		viewmv.addObject("address", address);
		viewmv.addObject("policystartDate", policydate);
		viewmv.addObject("claimDate", claimdate);
		viewmv.addObject("ReportGenDateTime", dateobj);
		viewmv.addObject("carrier", carrier);
		viewmv.addObject("ownername", ownername);
		viewmv.addObject("claimid", claimid);
		viewmv.addObject("latitude", latitude);
		viewmv.addObject("longitude", longitude);
		viewmv.addObject("wd", wd);
		viewmv.addObject("weathereventscount", new WeatherReportifyService().getCount());
		viewmv.setViewName(report);
		return viewmv;
	}

	@RequestMapping("/downloadweatherassessment/{reportid}")
	public ModelAndView downloadweatherassessment(@PathVariable("reportid") String reportid) {
		ModelAndView downmv = new ModelAndView();
		final WeatherAssessmentDataModel weatherAssessmentData = uploadWeatherDataService
				.getWeatherReportById(reportid);
		String address = weatherAssessmentData.getAddress();
		String policydate = weatherAssessmentData.getPolicystartdate();
		String claimdate = weatherAssessmentData.getClaimdate();
		String carrier = weatherAssessmentData.getCarriername();
		String claimid = weatherAssessmentData.getClaimid();
		String ownername = weatherAssessmentData.getOwnername();
		String reportType = weatherAssessmentData.getReportType();
		String report = null;
		if (reportType.equals("Hail")) {
			report = "WeatherAssessmentDownload";
		} else {
			report = "WeatherAssessmentDownloadWind";
		}
		System.out.println("Weather Assessment Viewed for address : " + address);
		System.out.println("from policy start date : " + policydate);
		System.out.println("to claim date : " + claimdate);
		System.out.println("Requesting GoogleMap API for Coordinates of the Address");
		GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
		System.out.println("GoogleMap API Output Status : " + wm.getStatus());
		new GetIDService();
		long UID = GetIDService.getID();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone(Timezone));
		Date dateo = new Date();
		String dateobj = df.format(dateo) + " " + Timezone;
		String latitude = "";
		String longitude = "";
		for (int i = 0; i < wm.getResults().size(); i++) {
			latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
			longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
		}
		System.out.println("GoogleMap Coordinate Output");
		System.out.println("Latitude : " + latitude);
		System.out.println("Longitude : " + longitude);
		List<WeatherOutputModel> wt = new ArrayList<WeatherOutputModel>();
		List<ReportsStageDBModel> stagelist = weatherDBService.getWeatherDataFromStageByReportID(reportid);
		wt = new WeatherDataConversionService().ConvertToWeatherOutputModelFromReportsStage(stagelist);
		Collections.sort(wt);
		Collections.reverse(wt);
		List<WeatherDataforBoxChartsModel> wl = weatherDBController.getWeatherDataFromReportsStageByReportID(reportid);
		WeatherDataforBoxChartsParentModel weatherdataforboxplot = new WeatherDataforBoxChartsParentModel();
		weatherdataforboxplot.setWeatherData(wl);
		List<GoogleLiveMapDataModel> gl = weatherDBController.getGMapPlotWeatherDataFromStageByReportID(reportid);
		GoogleLiveMapDataParentModel dataforgmap = new GoogleLiveMapDataParentModel();
		dataforgmap.setGMapMarkerData(gl);
		List<WeatherOutputModel> xt = new ArrayList<WeatherOutputModel>();
		xt = new WeatherReportifyService().Reportify(wt);
		System.out.println("Sending Data to Weather Assessment Download");
		List<WeatherOutputModel> wd = xt;
		downmv.addObject("UID", UID);
		downmv.addObject("boxplotdata", weatherdataforboxplot.getWeatherData());
		downmv.addObject("gmapdata", dataforgmap.getGMapMarkerData());
		downmv.addObject("address", address);
		downmv.addObject("policystartDate", policydate);
		downmv.addObject("claimDate", claimdate);
		downmv.addObject("ReportGenDateTime", dateobj);
		downmv.addObject("carrier", carrier);
		downmv.addObject("ownername", ownername);
		downmv.addObject("claimid", claimid);
		downmv.addObject("latitude", latitude);
		downmv.addObject("longitude", longitude);
		downmv.addObject("wd", wd);
		downmv.addObject("weathereventscount", new WeatherReportifyService().getCount());
		downmv.setViewName(report);
		return downmv;
	}

	@RequestMapping("/emailweatherassessment/{reportid}&{userid}")
	public ModelAndView emailweatherassessment(@PathVariable("reportid") String reportid,
			@PathVariable("userid") String userid) {
		ModelAndView emailmv = new ModelAndView();
		final WeatherAssessmentDataModel weatherAssessmentData = uploadWeatherDataService
				.getWeatherReportById(reportid);
		String address = weatherAssessmentData.getAddress();
		String policydate = weatherAssessmentData.getPolicystartdate();
		String claimdate = weatherAssessmentData.getClaimdate();
		String carrier = weatherAssessmentData.getCarriername();
		String claimid = weatherAssessmentData.getClaimid();
		String ownername = weatherAssessmentData.getOwnername();
		String reportType = weatherAssessmentData.getReportType();
		String report = null;
		if (reportType.equals("Hail")) {
			report = "WeatherAssessmentEmailReport";
		} else {
			report = "WeatherAssessmentEmailReportWind";
		}
		System.out.println("Weather Assessment Viewed for address : " + address);
		System.out.println("from policy start date : " + policydate);
		System.out.println("to claim date : " + claimdate);
		System.out.println("Requesting GoogleMap API for Coordinates of the Address");
		GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
		System.out.println("GoogleMap API Output Status : " + wm.getStatus());
		new GetIDService();
		long UID = GetIDService.getID();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone(Timezone));
		Date dateo = new Date();
		String dateobj = df.format(dateo) + " " + Timezone;
		String latitude = "";
		String longitude = "";
		for (int i = 0; i < wm.getResults().size(); i++) {
			latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
			longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
		}
		System.out.println("GoogleMap Coordinate Output");
		System.out.println("Latitude : " + latitude);
		System.out.println("Longitude : " + longitude);
		List<WeatherOutputModel> wt = new ArrayList<WeatherOutputModel>();
		List<ReportsStageDBModel> stagelist = weatherDBService.getWeatherDataFromStageByReportID(reportid);
		wt = new WeatherDataConversionService().ConvertToWeatherOutputModelFromReportsStage(stagelist);
		Collections.sort(wt);
		Collections.reverse(wt);
		List<WeatherDataforBoxChartsModel> wl = weatherDBController.getWeatherDataFromReportsStageByReportID(reportid);
		WeatherDataforBoxChartsParentModel weatherdataforboxplot = new WeatherDataforBoxChartsParentModel();
		weatherdataforboxplot.setWeatherData(wl);
		List<GoogleLiveMapDataModel> gl = weatherDBController.getGMapPlotWeatherDataFromStageByReportID(reportid);
		GoogleLiveMapDataParentModel dataforgmap = new GoogleLiveMapDataParentModel();
		dataforgmap.setGMapMarkerData(gl);
		List<WeatherOutputModel> xt = new ArrayList<WeatherOutputModel>();
		xt = new WeatherReportifyService().Reportify(wt);
		System.out.println("Sending Data to Weather Assessment Download");
		List<WeatherOutputModel> wd = xt;
		emailmv.addObject("UID", UID);
		emailmv.addObject("boxplotdata", weatherdataforboxplot.getWeatherData());
		emailmv.addObject("gmapdata", dataforgmap.getGMapMarkerData());
		emailmv.addObject("address", address);
		emailmv.addObject("policystartDate", policydate);
		emailmv.addObject("claimDate", claimdate);
		emailmv.addObject("ReportGenDateTime", dateobj);
		emailmv.addObject("carrier", carrier);
		emailmv.addObject("ownername", ownername);
		emailmv.addObject("claimid", claimid.toString());
		emailmv.addObject("latitude", latitude);
		emailmv.addObject("longitude", longitude);
		emailmv.addObject("wd", wd);
		emailmv.addObject("weathereventscount", new WeatherReportifyService().getCount());
		emailmv.addObject("userid", userid);
		emailmv.setViewName(report);
		return emailmv;
	}

}
