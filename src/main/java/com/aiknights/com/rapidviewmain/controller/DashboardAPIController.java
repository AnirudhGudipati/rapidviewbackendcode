package com.aiknights.com.rapidviewmain.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aiknights.com.rapidviewmain.models.DashboardClaimCategoryModel;
import com.aiknights.com.rapidviewmain.models.DashboardTotalClaimsModel;
import com.aiknights.com.rapidviewmain.service.DashboardAPIService;

@RequestMapping("/api")
@RestController
public class DashboardAPIController {

	@Autowired
	private DashboardAPIService DashboardService;
	
	@GetMapping("/getTotalClaimsData/{userid}")
	public ResponseEntity<Map<String, DashboardTotalClaimsModel>> getTotalClaimsData(@PathVariable String userid) {
		Map<String,DashboardTotalClaimsModel> totalclaimsmap = new HashMap<>();
		DashboardTotalClaimsModel totalclaimsdata = DashboardService.getTotalClaimsData(userid);
		if (totalclaimsdata==null) {
			totalclaimsmap.put("data", null);
			return new ResponseEntity<>(totalclaimsmap,HttpStatus.NOT_FOUND);
		}else{
			if(Integer.parseInt(totalclaimsdata.getTotalclaims())==0) {
				totalclaimsmap.put("No Claim Assigned for the User", null);
				return new ResponseEntity<>(totalclaimsmap,HttpStatus.NOT_FOUND); 
			}
			else {
				int inprogresscent= (Integer.parseInt(totalclaimsdata.getInprogressclaims())*100)/Integer.parseInt(totalclaimsdata.getTotalclaims());
				totalclaimsdata.setInprogressclaimspercentage(Integer.toString(inprogresscent)+"%");
				int completedcent= (Integer.parseInt(totalclaimsdata.getCompletedclaims())*100)/Integer.parseInt(totalclaimsdata.getTotalclaims());
				totalclaimsdata.setCompletedclaimspercentage(Integer.toString(completedcent)+"%");
				int rejectedcent= (Integer.parseInt(totalclaimsdata.getRejectedclaims())*100)/Integer.parseInt(totalclaimsdata.getTotalclaims());
				totalclaimsdata.setRejectedclaimspercentage(Integer.toString(rejectedcent)+"%");
				totalclaimsmap.put("data", totalclaimsdata);
				return new ResponseEntity<>(totalclaimsmap,HttpStatus.OK);
			}
		}
			
	}
	
	@GetMapping("/getClaimCategoryData/{userid}")
	public ResponseEntity<Map<String, DashboardClaimCategoryModel>> getClaimCategoryData(@PathVariable String userid){
		Map<String,DashboardClaimCategoryModel> claimcategorymap = new HashMap<>();
		DashboardClaimCategoryModel claimcategorydata = DashboardService.getClaimCategoryData(userid);
		if (claimcategorydata==null) {
			claimcategorymap.put("data", null);
			return new ResponseEntity<>(claimcategorymap,HttpStatus.NOT_FOUND);
		}else {
			if(Integer.parseInt(claimcategorydata.getTotalclaims())==0) {
				claimcategorymap.put("No Claim Assigned for the User", null);
				return new ResponseEntity<>(claimcategorymap,HttpStatus.NOT_FOUND); 
			}else {
				int watercent = (Integer.parseInt(claimcategorydata.getWaterclaims())*100)/Integer.parseInt(claimcategorydata.getTotalclaims());
				claimcategorydata.setWaterclaimspercentage(Integer.toString(watercent)+"%");
				int hailcent = (Integer.parseInt(claimcategorydata.getHailclaims())*100)/Integer.parseInt(claimcategorydata.getTotalclaims());
				claimcategorydata.setHailclaimspercentage(Integer.toString(hailcent)+"%");
				int windcent = (Integer.parseInt(claimcategorydata.getWindclaims())*100)/Integer.parseInt(claimcategorydata.getTotalclaims());
				claimcategorydata.setWindclaimspercentage(Integer.toString(windcent)+"%");
				claimcategorymap.put("data", claimcategorydata);
				return new ResponseEntity<>(claimcategorymap,HttpStatus.OK);
			}
		}
	}
	
}
