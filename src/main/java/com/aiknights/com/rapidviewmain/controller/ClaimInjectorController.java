package com.aiknights.com.rapidviewmain.controller;

//import java.io.BufferedReader;
//import java.io.InputStreamReader;
//import java.io.Reader;
//import java.text.SimpleDateFormat;
//import java.time.LocalDate;
//import java.time.LocalDateTime;
//import java.time.LocalTime;
//import java.time.format.DateTimeFormatter;
//import java.time.format.DateTimeParseException;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Locale;
//import java.util.Map;
//import java.util.TimeZone;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
//
//import com.aiknights.com.rapidviewmain.dto.EndorsementResultDTO;
//import com.aiknights.com.rapidviewmain.dto.PolicyResultDTO;
//import com.aiknights.com.rapidviewmain.models.ClaimInjectorCsv;
//import com.aiknights.com.rapidviewmain.service.ClaimInjectorCsvService;
//import com.fasterxml.jackson.databind.exc.InvalidFormatException;
//import com.opencsv.bean.CsvToBean;
//import com.opencsv.bean.CsvToBeanBuilder;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClaimInjectorController {
	/*
	 * 
	 * @Autowired private ClaimInjectorCsvService claimCsvService;
	 * 
	 * @Autowired NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	 * 
	 * private String claimNotification_table = "claim_notification"; private String
	 * policy_check_table = "policy_check"; private String property = "property";
	 * private String adjuster_claim_table = "adjuster_claim"; private String
	 * endorsement_table = "endorsement_list";
	 * 
	 * ClaimInjectorCsv successCsvData; List<ClaimInjectorCsv> successCsvDataList =
	 * new ArrayList<ClaimInjectorCsv>(); List<ClaimInjectorCsv> faliuerCsvDataList
	 * = new ArrayList<ClaimInjectorCsv>();;
	 * 
	 * ClaimInjectorCsv faliuerCsvData = new ClaimInjectorCsv();
	 * 
	 * @PostMapping("/upload-csv-file") public ResponseEntity<Map<String,
	 * List<ClaimInjectorCsv>>> uploadCSVFile(@RequestParam("file") MultipartFile
	 * file, Model model) throws InvalidFormatException {
	 * successCsvDataList.clear(); faliuerCsvDataList.clear();
	 * 
	 * Map<String, List<ClaimInjectorCsv>> outputHashMap = new HashMap<>();
	 * 
	 * if (file.isEmpty()) { outputHashMap.put("Please select a CSV file to upload",
	 * null); return new ResponseEntity<>(outputHashMap, HttpStatus.NOT_FOUND); }
	 * else { try (Reader reader = new BufferedReader(new
	 * InputStreamReader(file.getInputStream()))) { CsvToBean<ClaimInjectorCsv>
	 * csvToBean_claim_injector = new CsvToBeanBuilder(reader)
	 * .withType(ClaimInjectorCsv.class).withIgnoreLeadingWhiteSpace(true).
	 * withSkipLines(1).build(); List<ClaimInjectorCsv> csvDataList =
	 * csvToBean_claim_injector.parse();
	 * 
	 * for (int i = 0; i < csvDataList.size(); i++) { if
	 * ((csvDataList.get(i).getPolicyNumber()) == null ||
	 * csvDataList.get(i).getClaimantFirstName() == null ||
	 * csvDataList.get(i).getClaimantLastName() == null ||
	 * csvDataList.get(i).getClaimId() == null || csvDataList.get(i).getCity() ==
	 * null || csvDataList.get(i).getState() == null ||
	 * csvDataList.get(i).getPhoneNumber() == null || csvDataList.get(i).getEmail()
	 * == null || csvDataList.get(i).getZipCode() == null ||
	 * csvDataList.get(i).getPropertyAddress() == null ||
	 * csvDataList.get(i).getProperty_Type() == null ||
	 * csvDataList.get(i).getInsuranceCompany() == null ||
	 * csvDataList.get(i).getIncidentType() == null ||
	 * csvDataList.get(i).getClaimDate() == null ||
	 * csvDataList.get(i).getClaimDescription() == null) {
	 * 
	 * csvDataList.get(i).setInjectorStatus("Fail");
	 * csvDataList.get(i).setReason("Mandatory Fields Are Missing ");
	 * 
	 * faliuerCsvDataList.add(csvDataList.get(i));
	 * 
	 * } else { successCsvDataList.add(csvDataList.get(i)); } }
	 * 
	 * for (int i = 0; i < successCsvDataList.size(); i++) {
	 * 
	 * // 2025-08-24 yyy-MM-DD String policyStartDate =
	 * successCsvDataList.get(i).getPolicyStartDate(); String policyEndDate =
	 * successCsvDataList.get(i).getPolicyEndDate(); String policyClaimDate =
	 * successCsvDataList.get(i).getClaimDate(); String policyIncidentDate =
	 * successCsvDataList.get(i).getIncidentDate(); String propertyPurchaseDate =
	 * successCsvDataList.get(i).getProperty_Purchase_Date(); String
	 * policyLastUpdateDate = successCsvDataList.get(i).getProperty_Purchase_Date();
	 * boolean IsCalimExist =
	 * claimCsvService.chcekClaimIdIsDuplicate(successCsvDataList.get(i));
	 * 
	 * boolean isValidpolicyStartDate = isValidFormat("yyyy-MM-dd", policyStartDate,
	 * Locale.ENGLISH); boolean isValidpolicyEndDate = isValidFormat("yyyy-MM-dd",
	 * policyEndDate, Locale.ENGLISH); boolean isValidpolicyClaimDate =
	 * isValidFormat("yyyy-MM-dd", policyClaimDate, Locale.ENGLISH); boolean
	 * isValidpolicyIncidentDate = isValidFormat("yyyy-MM-dd", policyIncidentDate,
	 * Locale.ENGLISH); boolean isValidpropertyPurchaseDate =
	 * isValidFormat("yyyy-MM-dd", propertyPurchaseDate, Locale.ENGLISH); boolean
	 * isValidpolicyLastUpdateDate = isValidFormat("yyyy-MM-dd",
	 * policyLastUpdateDate, Locale.ENGLISH);
	 * 
	 * if (!isValidpolicyStartDate) {
	 * successCsvDataList.get(i).setInjectorStatus("Fail");
	 * successCsvDataList.get(i).
	 * setReason("Please provide the valid Date format YYYY-MM-DD");
	 * 
	 * } else if (!isValidpolicyEndDate) {
	 * successCsvDataList.get(i).setInjectorStatus("Fail");
	 * successCsvDataList.get(i).
	 * setReason("Please provide the valid Date format YYYY-MM-DD");
	 * 
	 * } else if (!isValidpolicyLastUpdateDate) {
	 * successCsvDataList.get(i).setInjectorStatus("Fail");
	 * successCsvDataList.get(i).
	 * setReason("Please provide the valid Date format YYYY-MM-DD");
	 * 
	 * } else if (!isValidpropertyPurchaseDate) {
	 * successCsvDataList.get(i).setInjectorStatus("Fail");
	 * successCsvDataList.get(i).
	 * setReason("Please provide the valid Date format YYYY-MM-DD");
	 * 
	 * } else if (!isValidpolicyClaimDate) {
	 * successCsvDataList.get(i).setInjectorStatus("Fail");
	 * successCsvDataList.get(i).
	 * setReason("Please provide the valid Date format YYYY-MM-DD");
	 * 
	 * } else if (!isValidpolicyIncidentDate) {
	 * successCsvDataList.get(i).setInjectorStatus("Fail");
	 * successCsvDataList.get(i).
	 * setReason("Please provide the valid Date format YYYY-MM-DD");
	 * 
	 * } else if (IsCalimExist) {
	 * successCsvDataList.get(i).setInjectorStatus("Fail");
	 * successCsvDataList.get(i).setReason("Duplicate Claim ID ");
	 * 
	 * } else { EndorsementResultDTO output_Endorsement =
	 * claimCsvService.insertIntoEndorsementTableSetting(successCsvDataList.get(i),
	 * namedParameterJdbcTemplate); if (output_Endorsement.getinsertion_status() ==
	 * true) {
	 * 
	 * claimCsvService.getByPolicyNumber(successCsvDataList.get(i).getPolicyNumber()
	 * );
	 * 
	 * // PolicyTable PolicyResultDTO output_policy =
	 * claimCsvService.insertIntoPolicyTableSetting( successCsvDataList.get(i),
	 * namedParameterJdbcTemplate, output_Endorsement); if
	 * (output_policy.getinsertion_status() == true) { // ClaimTable boolean
	 * output_claim = claimCsvService.insertIntoClaimTableSetting(
	 * successCsvDataList.get(i), namedParameterJdbcTemplate, output_policy);
	 * 
	 * if (output_claim == true) {
	 * 
	 * // AdjusterClaim Table boolean output_Adjuster =
	 * claimCsvService.insertIntoAdjusterClaimTableSetting(
	 * successCsvDataList.get(i), namedParameterJdbcTemplate); if (output_Adjuster
	 * == true) { // property boolean output_property =
	 * claimCsvService.insertIntoPropertyTableSetting( successCsvDataList.get(i),
	 * namedParameterJdbcTemplate, output_policy); if (output_property == true) {
	 * 
	 * successCsvDataList.get(i).setInjectorStatus("Success"); } else {
	 * 
	 * claimCsvService.deleteUltil(adjuster_claim_table, "claim_id",
	 * successCsvDataList.get(i).getClaimId());
	 * claimCsvService.deleteUltil(claimNotification_table, "claim_id",
	 * successCsvDataList.get(i).getClaimId());
	 * claimCsvService.deleteUltil(policy_check_table, "policy_number",
	 * successCsvDataList.get(i).getPolicyNumber());
	 * claimCsvService.deleteUltil(endorsement_table, "endorsement_id",
	 * Integer.toString(output_Endorsement.getEndorsement_id_pk()));
	 * successCsvDataList.get(i).setInjectorStatus("Fail");
	 * 
	 * }
	 * 
	 * } else { claimCsvService.deleteUltil(claimNotification_table, "claim_id",
	 * successCsvDataList.get(i).getClaimId());
	 * claimCsvService.deleteUltil(policy_check_table, "policy_number",
	 * successCsvDataList.get(i).getPolicyNumber());
	 * claimCsvService.deleteUltil(endorsement_table, "endorsement_id",
	 * Integer.toString(output_Endorsement.getEndorsement_id_pk()));
	 * successCsvDataList.get(i).setInjectorStatus("Fail"); }
	 * 
	 * } else {
	 * 
	 * claimCsvService.deleteUltil(policy_check_table, "policy_number",
	 * successCsvDataList.get(i).getPolicyNumber());
	 * claimCsvService.deleteUltil(endorsement_table, "endorsement_id",
	 * Integer.toString(output_Endorsement.getEndorsement_id_pk()));
	 * successCsvDataList.get(i).setInjectorStatus("Fail");
	 * 
	 * }
	 * 
	 * } else {
	 * 
	 * claimCsvService.deleteUltil(endorsement_table, "endorsement_id",
	 * Integer.toString(output_Endorsement.getEndorsement_id_pk()));
	 * successCsvDataList.get(i).setInjectorStatus("Fail"); }
	 * 
	 * } else {
	 * 
	 * successCsvDataList.get(i).setReason("Mandatory Fields Are Missing ");
	 * successCsvDataList.get(i).setInjectorStatus("Fail");
	 * 
	 * }
	 * 
	 * }
	 * 
	 * } claimCsvService.CompleteWeatherStagging(successCsvDataList);
	 * successCsvDataList.addAll(faliuerCsvDataList); outputHashMap.put("Data",
	 * successCsvDataList);
	 * 
	 * } catch (Exception ex) { ex.printStackTrace(); }
	 * 
	 * }
	 * 
	 * return new ResponseEntity<>(outputHashMap, HttpStatus.OK); }
	 * 
	 * public static boolean isValidFormat(String format, String value, Locale
	 * locale) { LocalDateTime ldt = null; DateTimeFormatter fomatter =
	 * DateTimeFormatter.ofPattern(format, locale);
	 * 
	 * try { ldt = LocalDateTime.parse(value, fomatter); String result =
	 * ldt.format(fomatter); return result.equals(value); } catch
	 * (DateTimeParseException e) { try { LocalDate ld = LocalDate.parse(value,
	 * fomatter); String result = ld.format(fomatter); return result.equals(value);
	 * } catch (DateTimeParseException exp) { try { LocalTime lt =
	 * LocalTime.parse(value, fomatter); String result = lt.format(fomatter); return
	 * result.equals(value); } catch (DateTimeParseException e2) { // Debugging
	 * purposes // e2.printStackTrace(); } } }
	 * 
	 * return false; }
	 */}