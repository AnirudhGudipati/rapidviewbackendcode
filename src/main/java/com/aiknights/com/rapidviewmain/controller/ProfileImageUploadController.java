package com.aiknights.com.rapidviewmain.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aiknights.com.rapidviewmain.models.UploadImagesOutput;
import com.aiknights.com.rapidviewmain.service.ProfileImageUploadService;
import com.aiknights.com.rapidviewmain.service.UploadImageService;

@RequestMapping("/api")
@RestController
public class ProfileImageUploadController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UploadImageService.class);
	@Autowired
	ProfileImageUploadService profileImageUploadService;

	@Value("${upload.profileimages.base-path}")
	private String profileimagesdirectory;

	@Value("${upload.images.server-url}")
	private String imageServerUrl;

	@Value("${upload.images.replace-path}")
	private String replacepath;

	@RequestMapping(value = "/uploadProfileImage", method = RequestMethod.POST)
	public ResponseEntity<Map<String, String>> uplaodImage(@RequestParam("file") MultipartFile file,
			@RequestParam("userId") String userId) throws IOException {
		Map<String, String> outputHashMap = new HashMap<>();

		boolean isUserExist = profileImageUploadService.isUserExist(userId);
		if (isUserExist) {
			boolean deleteStatus = profileImageUploadService.deleteOldProfileUrl(userId);
			uploadImageFiles(file, userId);
		} else {
			uploadImageFiles(file, userId);
		}
		outputHashMap.put("Status", "Success");
		return new ResponseEntity<>(outputHashMap, HttpStatus.OK);
	}

	@RequestMapping(value = "/getProfileImage", method = RequestMethod.POST)

	public ResponseEntity<Map<String, String>> GetProfileImageById(@RequestBody final Map<String, String> user) {

		Map<String, String> outputHashMap = new HashMap<>();

		String userId = user.get("userId");

		String url = profileImageUploadService.getProfileImageById(userId);

		if (url==null) {
			outputHashMap.put("profileImageUrl", "NotFound");
			return new ResponseEntity<>(outputHashMap, HttpStatus.NOT_FOUND);

		} else {
			outputHashMap.put("profileImageUrl", url);
			return new ResponseEntity<>(outputHashMap, HttpStatus.OK);
		}

	}

	public UploadImagesOutput uploadImageFiles(final MultipartFile file, final String userId) {
		String s = createDirectories(Arrays.asList(userId));
		UploadImagesOutput uploadedResponse = uploadFile(file, s);
		boolean UploadStatus = profileImageUploadService.uploadProfileImageFile(
				uploadedResponse.getSourceImageName().toString(), userId, uploadedResponse.getImageUrl().toString());
		return uploadedResponse;
	}

	private UploadImagesOutput uploadFile(MultipartFile file, String folder) {
		UploadImagesOutput outPut = new UploadImagesOutput();
		long uniqueIdForImage = System.currentTimeMillis();
		String uniqueId = String.valueOf(uniqueIdForImage) + '_' + file.getOriginalFilename();
		String finalPath = folder + uniqueId;
		try {
			byte[] bytes = file.getBytes();
			Path path = Paths.get(finalPath);
			Files.write(path, bytes);
			outPut.setUniqueIdForImage(uniqueIdForImage);
			outPut.setImageId(uniqueId);
			outPut.setImageUrl(prepareUrl(finalPath));
			outPut.setSourceImageName(file.getOriginalFilename());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(outPut);
		return outPut;
	}

	private String createDirectories(final List<String> names) {
		System.out.println(profileimagesdirectory);

		String basePath = profileimagesdirectory;
		for (String s : names) {
			String folder = basePath + s;
			File directory = new File(folder);
			if (!directory.exists()) {
				directory.mkdir();
			}
			basePath = folder + File.separator;
		}
		LOGGER.info("Upload Images Service Base Path Log :" + basePath);
		return basePath;
	}

	private String prepareUrl(final String path) {
		if (path != null) {
			return path.replace(replacepath, imageServerUrl);
		}
		return null;
	}
}