package com.aiknights.com.rapidviewmain.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aiknights.com.rapidviewmain.models.AnalyseReponseInDetail;
import com.aiknights.com.rapidviewmain.models.BulkAnalyseImageRequestModel;
import com.aiknights.com.rapidviewmain.models.ClaimNotification;
import com.aiknights.com.rapidviewmain.models.ClaimNotificationParentModel;
import com.aiknights.com.rapidviewmain.models.DeleteIncidentImagesInput;
import com.aiknights.com.rapidviewmain.models.UploadImagesOutput;
import com.aiknights.com.rapidviewmain.models.UserDetails;
import com.aiknights.com.rapidviewmain.service.ClaimNotificationService;
import com.aiknights.com.rapidviewmain.service.EmailNotificationService;
import com.aiknights.com.rapidviewmain.service.UploadImageService;
import com.aiknights.com.rapidviewmain.service.UserDetailsService;
import com.aiknights.com.rapidviewmain.service.WebSocketService;

@RequestMapping("/api")
@RestController
public class UploadImagesController {
	
	@Autowired
	private UploadImageService uploadService;
	
	@Autowired
	private EmailNotificationService emailservice;
	
	@Autowired
	private UserDetailsService userservice;
	
	@Autowired
	WebSocketService wsservice;
	
	@Autowired
	private ClaimNotificationService claimService;
	
	@PostMapping("/uploadImageFiles")
	public ResponseEntity<Map<String, List<UploadImagesOutput>>> uploadFiles(@RequestParam("files") MultipartFile[] files, 
																			 @RequestParam("claimId") String claimId,
																			 @RequestParam("category") String category,
																			 @RequestParam("insuranceCompany") String insuranceCompany,
																			 @RequestParam("shingleType") String shingleType) {
		List<UploadImagesOutput> uploadedList = uploadService.uploadImageFiles(files, claimId, category, insuranceCompany,shingleType);
		Map<String,List<UploadImagesOutput>> uploadedMap = new HashMap<>();
		uploadedMap.put("uploadedMap",uploadedList);
		return new ResponseEntity<>(uploadedMap,HttpStatus.OK);
	}
	
	@PostMapping(value = "/analyseImage"/* ,produces = MediaType.IMAGE_JPEG_VALUE */)
	public ResponseEntity<?> analyseImage(@RequestParam("imageId") final String imageId,
										  @RequestParam("url") final String url,
										  @RequestParam("claimId") final String claimId,
										  @RequestParam("category") final String category,
										  @RequestParam("insuranceCompany") final String insuranceCompany,
										  @RequestParam("shingleType") final String shingleType) {
		uploadService.changeAnalysisStatetoZero(imageId);
		AnalyseReponseInDetail analysedOutput = uploadService.analyseImage(imageId,url, claimId, category, insuranceCompany,shingleType);
		Map<String,AnalyseReponseInDetail> analysedMap = new HashMap<>();
		analysedMap.put("analysedMap",analysedOutput);
		if(analysedOutput!=null) {
			return new ResponseEntity<Map<String, AnalyseReponseInDetail>>(analysedMap,HttpStatus.OK);
		}else {
			return new ResponseEntity<>("Analysis Rest call failed. It may be due to special characters andspaces in image name or AI service not responding.",HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@PostMapping(value = "/analyzeBulkImages",  consumes="application/json")
	public ResponseEntity<Map<String, List<AnalyseReponseInDetail>>> analyzeBulkImages(@RequestBody List<BulkAnalyseImageRequestModel> bulkdata){
		for(int i=0; i<bulkdata.size(); i++) {
			String imageId = bulkdata.get(i).getImageId();
			uploadService.changeAnalysisStatetoZero(imageId);
		}
		for(int i=0; i<bulkdata.size(); i++) {
			String imageId = bulkdata.get(i).getImageId();
			String claimId = bulkdata.get(i).getClaimId();
			String url = bulkdata.get(i).getUrl();
			String category = bulkdata.get(i).getCategory();
			String insuranceCompany = bulkdata.get(i).getInsuranceCompany();
			String shingleType = bulkdata.get(i).getShingleType();
			AnalyseReponseInDetail analysedOutput = uploadService.analyseImage(imageId,url, claimId, category, insuranceCompany,shingleType);
		}
		UserDetails userdetails = userservice.getUserDetailsByUserID(bulkdata.get(1).getUserId());
		String useremail= userdetails.getEmail();
		String claimId = bulkdata.get(0).getClaimId();
		ClaimNotificationParentModel pm = new ClaimNotificationParentModel();
		ClaimNotification  claim = claimService.getClaimWithPolicyDataByClaimId(claimId);
		pm.setClaim(claim);
		String InsCompany= pm.getClaim().getInsuranceCompany();
		String EmailAddress=useremail;
		String Subject=InsCompany+" - "+claimId+" - Images Analysis Completed";
		String Body = bulkdata.size()+" images submitted for Damage Detection Analysis have completed processing.\r\n\r\nView the Claim(ClaimID- \""+claimId+"\") to see the analysed Images.\r\n\r\nThank you\nRapidView";
		String email = emailservice.sendemailcontroller(EmailAddress, Subject, Body); 
		String SenderUserID = "0";
		String ReceiverUserID = bulkdata.get(1).getUserId();
		String ReceiverUserGroup = "";
		String Notification_Type_ID = "SPECIFIC_USER";
		String Notification_Claim_ID = claimId;
		String Notification_Subject = InsCompany+" - "+claimId+" - Images Analysis Completed";
		String Message_content = bulkdata.size()+" images submitted for Damage Detection Analysis have completed processing. View the Claim(ClaimID- \""+claimId+"\") to see the analysed Images. - RapidView";
		Boolean message = wsservice.sendMessageService(SenderUserID, ReceiverUserID, ReceiverUserGroup, Notification_Type_ID, Notification_Claim_ID, Notification_Subject, Message_content);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping(value="/getIncidentEvidenceForClaim/{claimId}")
	public  ResponseEntity<Map<String, List<AnalyseReponseInDetail>>> getIncidentEvidenceForClaim(@PathVariable final String claimId) {
		List<AnalyseReponseInDetail> imageList = uploadService.getIncidentEvidenceForClaim(claimId);
		Map<String,List<AnalyseReponseInDetail>> imagesMap = new HashMap<>();
		imagesMap.put("imageList",imageList);
		return new ResponseEntity<>(imagesMap,HttpStatus.OK);
		
	}
	
	@PostMapping(value = "/deleteIncidentEvidenceImages")
	public ResponseEntity<int[]> deleteIncidentEvidenceImages(@RequestBody List<DeleteIncidentImagesInput> deleteIncidentImages){
		int [] deletedArray = uploadService.deleteIncidentEvidenceImages(deleteIncidentImages);
		//return  uploadService.deleteIncidentEvidenceImages(deleteIncidentImages);
		return new ResponseEntity<>(deletedArray, HttpStatus.OK);
		
	}
	
	/*@RequestMapping("/sendtestemail")
	public String testmailsender() {
	String claimId = "100";
	RestTemplate rt1 = new RestTemplate();
	PolicyParent pm = rt1.getForObject(
			"http://
			//PUTIPHERE:8080/rapidviewmain/api/getClaimPolicy/" + claimId, PolicyParent.class);
	String InsCompany= pm.getClaim().getInsuranceCompany();
	String EmailAddress="anirudhg@aiknights.com,anirudh635@gmail.com";
	String Subject=InsCompany+" - "+claimId+" - Images Analysis Completed";
	String Body =" images submitted for Damage Detection Analysis have completed processing.\r\n\r\n"+"Analysed Images : \n"+""+" \r\n\r\nView the claim(ClaimID- \""+claimId+"\") to see the analysed Images.\r\n\r\nThank you\nRapidView";
	String email = emailservice.sendemailcontroller(EmailAddress, Subject, Body);
	return email;
	}*/
}