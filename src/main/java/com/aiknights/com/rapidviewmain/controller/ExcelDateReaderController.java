package com.aiknights.com.rapidviewmain.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aiknights.com.rapidviewmain.constants.RapidViewMainConstants;
import com.aiknights.com.rapidviewmain.dto.EndorsementResultDTO;
import com.aiknights.com.rapidviewmain.dto.PolicyResultDTO;
import com.aiknights.com.rapidviewmain.models.ClaimInjectorExcelModel;
import com.aiknights.com.rapidviewmain.service.ClaimInjectorCsvService;

@RestController
public class ExcelDateReaderController {

	@Autowired
	private ClaimInjectorCsvService claimCsvService;
	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private String claimNotification_table = "claim_notification";
	private String policy_check_table = "policy_check";
	@SuppressWarnings("unused")
	private String property = "property";
	private String adjuster_claim_table = "adjuster_claim";
	private String endorsement_table = "endorsement_list";
	List<ClaimInjectorExcelModel> tempCsvDataList = new ArrayList<ClaimInjectorExcelModel>();
	List<ClaimInjectorExcelModel> tempCsvDataList1 = new ArrayList<ClaimInjectorExcelModel>();

	ClaimInjectorExcelModel successCsvData;
	List<ClaimInjectorExcelModel> successCsvDataList = new ArrayList<ClaimInjectorExcelModel>();
	List<ClaimInjectorExcelModel> faliuerCsvDataList = new ArrayList<ClaimInjectorExcelModel>();
	ClaimInjectorExcelModel faliuerCsvData = new ClaimInjectorExcelModel();
	boolean policyValidation = true;

	@SuppressWarnings("resource")
	@PostMapping("/upload-csv-file")
	public ResponseEntity<Map<String, List<ClaimInjectorExcelModel>>> uploadCSVFile(
			@RequestParam("file") MultipartFile reapExcelDataFile, Model model) throws IOException, ParseException {
		successCsvDataList.clear();
		faliuerCsvDataList.clear();
		String filename = reapExcelDataFile.getOriginalFilename();
		boolean filenamevalidation = filename.endsWith(".xlsx");
		Map<String, List<ClaimInjectorExcelModel>> outputHashMap = new HashMap<>();
		if (!filenamevalidation) {
			outputHashMap.put("invalidFileFormat", null);
			return new ResponseEntity<>(outputHashMap, HttpStatus.OK);
		} else {
			try {

				FileInputStream excelFile = (FileInputStream) reapExcelDataFile.getInputStream();
				Workbook workbook = new XSSFWorkbook(excelFile);
				Sheet datatypeSheet = workbook.getSheetAt(0);
				int rows_count = datatypeSheet.getPhysicalNumberOfRows();
				if (rows_count > 1) {
					List<ClaimInjectorExcelModel> tempClaimListfinal = null;
					Iterator<Row> iterator = datatypeSheet.iterator();

					tempClaimListfinal = new ArrayList<ClaimInjectorExcelModel>();

					for (int i = -1; iterator.hasNext(); i++) {
						Row row = iterator.next();
						row.getRowNum();
						// display row number in the console.
						System.out.println("Row No.: " + row.getRowNum());
						if (row.getRowNum() == 0) {
							continue; // just skip the rows if row number is 0 or 1
						} else {

							// Row currentRow = iterator.next();
							Iterator<Cell> cellIterator = row.iterator();
							ClaimInjectorExcelModel tempClaim = new ClaimInjectorExcelModel();
							ClaimInjectorExcelModel tempCsvData = new ClaimInjectorExcelModel();
							for (int j = 0; cellIterator.hasNext(); j++) {

								Cell currentCell = cellIterator.next();
								tempClaim = prepareCsvObject(currentCell, tempCsvData);

							}
							tempClaimListfinal.add(i, tempClaim);

							System.out.println();
						}
					}
					outputHashMap = claimInjector(tempClaimListfinal);
				} else {
					outputHashMap.put("noClaimsToImport", null);
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		return new ResponseEntity<>(outputHashMap, HttpStatus.OK);

	}

	public Map<String, List<ClaimInjectorExcelModel>> claimInjector(List<ClaimInjectorExcelModel> tempClaimList)
			throws ParseException {

		Map<String, List<ClaimInjectorExcelModel>> outputHashMap = new HashMap<>();
		List<ClaimInjectorExcelModel> csvDataList = tempClaimList;

		for (int i = 0; i < csvDataList.size(); i++) {
			if ((csvDataList.get(i).getPolicyNumber()) == null || csvDataList.get(i).getClaimantFirstName() == null
					|| csvDataList.get(i).getClaimantLastName() == null || csvDataList.get(i).getClaimId() == null
					|| csvDataList.get(i).getCity() == null || csvDataList.get(i).getState() == null
					|| csvDataList.get(i).getPhoneNumber() == null || csvDataList.get(i).getEmail() == null
					|| csvDataList.get(i).getZipCode() == null || csvDataList.get(i).getPropertyAddress() == null
					|| csvDataList.get(i).getProperty_Type() == null || csvDataList.get(i).getInsuranceCompany() == null
					|| csvDataList.get(i).getIncidentType() == null || csvDataList.get(i).getClaimDate() == null
					|| csvDataList.get(i).getClaimDescription() == null || csvDataList.get(i).getPolicyEndDate() == null
					|| csvDataList.get(i).getPolicyStartDate() == null || csvDataList.get(i).getIncidentDate() == null
					|| !(csvDataList.get(i).getReason() == null)) {

				csvDataList.get(i).setInjectorStatus("Fail");
				if (csvDataList.get(i).getReason() == null) {
					csvDataList.get(i).setReason("Mandatory Fields Are Missing ");
				}

				faliuerCsvDataList.add(csvDataList.get(i));

			} else {
				successCsvDataList.add(csvDataList.get(i));
			}
		}

		for (int i = 0; i < successCsvDataList.size(); i++) {

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date PolicyStartDate = format.parse(successCsvDataList.get(i).getPolicyStartDate().toString());
			Date PolicyEndDate = format.parse(successCsvDataList.get(i).getPolicyEndDate().toString());
			// policy Date validation
			boolean policyValidation = PolicyStartDate.before(PolicyEndDate);
			// incident date validation
			Date IncidentDate = format.parse(successCsvDataList.get(i).getIncidentDate().toString());

			boolean IsCalimExist = claimCsvService.chcekClaimIdIsDuplicate(successCsvDataList.get(i));

			if (IsCalimExist) {
				successCsvDataList.get(i).setInjectorStatus("Fail");
				successCsvDataList.get(i).setReason("Duplicate Claim ID ");
			} else if (!policyValidation) {
				successCsvDataList.get(i).setInjectorStatus("Fail");
				successCsvDataList.get(i).setReason(RapidViewMainConstants.POLICY_DATE_VALIDATION);
			} else if (!(IncidentDate.after(PolicyStartDate) && IncidentDate.before(PolicyEndDate))) {
				successCsvDataList.get(i).setInjectorStatus("Fail");
				successCsvDataList.get(i).setReason(RapidViewMainConstants.INCIDENT_DATE_VALIDATION);
			} else {

				EndorsementResultDTO output_Endorsement = claimCsvService
						.insertIntoEndorsementTableSetting(successCsvDataList.get(i), namedParameterJdbcTemplate);
				if (output_Endorsement.getinsertion_status() == true) {

					claimCsvService.getByPolicyNumber(successCsvDataList.get(i).getPolicyNumber());

					// PolicyTable
					PolicyResultDTO output_policy = claimCsvService.insertIntoPolicyTableSetting(
							successCsvDataList.get(i), namedParameterJdbcTemplate, output_Endorsement,
							successCsvDataList.get(i).getPolicyStartDate().toString(),
							successCsvDataList.get(i).getPolicyEndDate().toString(),
							successCsvDataList.get(i).getPolicyLastUpdateDate());
					if (output_policy.getinsertion_status() == true) {
						// ClaimTable
						boolean output_claim = claimCsvService.insertIntoClaimTableSetting(successCsvDataList.get(i),
								namedParameterJdbcTemplate, output_policy, successCsvDataList.get(i).getIncidentDate(),
								successCsvDataList.get(i).getClaimDate());

						if (output_claim == true) {

							// AdjusterClaim Table
							boolean output_Adjuster = claimCsvService.insertIntoAdjusterClaimTableSetting(
									successCsvDataList.get(i), namedParameterJdbcTemplate);
							if (output_Adjuster == true) {
								// property
								boolean output_property = claimCsvService.insertIntoPropertyTableSetting(
										successCsvDataList.get(i), namedParameterJdbcTemplate, output_policy,
										successCsvDataList.get(i).getProperty_Purchase_Date());
								if (output_property == true) {

									successCsvDataList.get(i).setInjectorStatus("Success");
								} else {

									claimCsvService.deleteUltil(adjuster_claim_table, "claim_id",
											successCsvDataList.get(i).getClaimId());
									claimCsvService.deleteUltil(claimNotification_table, "claim_id",
											successCsvDataList.get(i).getClaimId());
									claimCsvService.deleteUltil(policy_check_table, "policy_number",
											successCsvDataList.get(i).getPolicyNumber());
									claimCsvService.deleteUltil(endorsement_table, "endorsement_id",
											Integer.toString(output_Endorsement.getEndorsement_id_pk()));
									successCsvDataList.get(i).setInjectorStatus("Fail");

								}

							} else {
								claimCsvService.deleteUltil(claimNotification_table, "claim_id",
										successCsvDataList.get(i).getClaimId());
								claimCsvService.deleteUltil(policy_check_table, "policy_number",
										successCsvDataList.get(i).getPolicyNumber());
								claimCsvService.deleteUltil(endorsement_table, "endorsement_id",
										Integer.toString(output_Endorsement.getEndorsement_id_pk()));
								successCsvDataList.get(i).setInjectorStatus("Fail");
							}

						} else {

							claimCsvService.deleteUltil(policy_check_table, "policy_number",
									successCsvDataList.get(i).getPolicyNumber());
							claimCsvService.deleteUltil(endorsement_table, "endorsement_id",
									Integer.toString(output_Endorsement.getEndorsement_id_pk()));
							successCsvDataList.get(i).setInjectorStatus("Fail");

						}

					} else {

						claimCsvService.deleteUltil(endorsement_table, "endorsement_id",
								Integer.toString(output_Endorsement.getEndorsement_id_pk()));
						successCsvDataList.get(i).setInjectorStatus("Fail");
					}

				} else {

					successCsvDataList.get(i).setReason("Mandatory Fields Are Missing ");
					successCsvDataList.get(i).setInjectorStatus("Fail");

				}

			}

		}
		claimCsvService.CompleteWeatherStagging(successCsvDataList);
		successCsvDataList.addAll(faliuerCsvDataList);
		outputHashMap.put("Data", successCsvDataList);
		return outputHashMap;

	}

	private String formatDate(String dateTobeformat) throws ParseException {
		// String dateStr = "Mon Jun 18 00:00:00 IST 2012";
		DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		Date date = (Date) formatter.parse(dateTobeformat);
		System.out.println(date);

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		String month = String.valueOf(cal.get(Calendar.MONTH) + 1);

		if (Integer.valueOf(month) < 10) {
			month = "0" + month;
		}

		String date1 = String.valueOf(cal.get(Calendar.DATE));

		if (Integer.valueOf(date1) < 10) {
			date1 = "0" + date1;
		}

		// String formatedDate = cal.get(Calendar.YEAR) + "-" + month + "-" +
		// cal.get(Calendar.DATE);
		String formatedDate = cal.get(Calendar.YEAR) + "-" + month + "-" + date1;
		System.out.println("formatedDate : " + formatedDate);

		return formatedDate;

	}

	private ClaimInjectorExcelModel prepareCsvObject(Cell currentCell, ClaimInjectorExcelModel tempCsvDataList2) {

		try {
			int ColumnInde = currentCell.getColumnIndex();
			if (ColumnInde == 0) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setClaimId(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 1) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setClaimantFirstName(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 2) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setClaimantLastName(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			}

			else if (ColumnInde == 3) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setPropertyAddress(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 4) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setCity(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 5) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setState(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 6) {
				int cellType = currentCell.getCellType();
				if ((cellType == 1)) {
					tempCsvDataList2.setZipCode(currentCell.getStringCellValue());
				} else if ((cellType == 0)) {
					int ZipCode = (int) currentCell.getNumericCellValue();
					tempCsvDataList2.setZipCode(String.valueOf(ZipCode));
				} else if ((cellType == 3)) {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 7) {
				int cellType = currentCell.getCellType();
				if ((cellType == 1)) {
					tempCsvDataList2.setPhoneNumber(currentCell.getStringCellValue());
				} else if ((cellType == 0)) {
					int PhoneNumber = (int) currentCell.getNumericCellValue();
					tempCsvDataList2.setPhoneNumber(String.valueOf(PhoneNumber));
				} else if ((cellType == 3)) {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 8) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setEmail(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 9) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setEndorsement_type(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 10) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setEndorsement_type_desc(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setEndorsement_type_desc(currentCell.getStringCellValue());
				}
			} else if (ColumnInde == 11) {

				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setProperty_Type(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 12) {

				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setProperty_Purchase_Date(formatDate(currentCell.getDateCellValue().toString()));
				} else {
					tempCsvDataList2.setProperty_Purchase_Date(null);

				}
			} else if (ColumnInde == 13) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setInsuranceCompany(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 14) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setPolicyNumber(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 15) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setPolicyStartDate(formatDate(currentCell.getDateCellValue().toString()));
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}

			} else if (ColumnInde == 16) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setPolicyEndDate(formatDate(currentCell.getDateCellValue().toString()));
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 17) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setPolicyStatus(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setPolicyStatus(currentCell.getStringCellValue());
				}
			} else if (ColumnInde == 18) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setPolicyLastUpdateDate(formatDate(currentCell.getDateCellValue().toString()));
				} else {
					tempCsvDataList2.setPolicyLastUpdateDate(null);
				}
			} else if (ColumnInde == 19) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setIncidentType(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 20) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setIncidentDate(formatDate(currentCell.getDateCellValue().toString()));
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 21) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setIncidentTime(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setIncidentTime(currentCell.getStringCellValue());
				}
			} else if (ColumnInde == 22) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setClaimDate(formatDate(currentCell.getDateCellValue().toString()));
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			} else if (ColumnInde == 23) {
				int cellType = currentCell.getCellType();
				if ((cellType == 1)) {

					tempCsvDataList2.setPriorClaims(currentCell.getStringCellValue());
				} else if ((cellType == 0)) {
					int PriorClaims = (int) currentCell.getNumericCellValue();
					tempCsvDataList2.setPriorClaims(String.valueOf(PriorClaims));
				} else if ((cellType == 3)) {
					tempCsvDataList2.setPriorClaims(null);
				}

			} else if (ColumnInde == 24) {
				int cellType = currentCell.getCellType();
				if ((cellType == 1)) {

					tempCsvDataList2.setDeductibleAmount(currentCell.getStringCellValue());
				} else if ((cellType == 0)) {
					int DeductibleAmount = (int) currentCell.getNumericCellValue();
					tempCsvDataList2.setDeductibleAmount(String.valueOf(DeductibleAmount));
				} else if ((cellType == 3)) {
					tempCsvDataList2.setDeductibleAmount(null);
				}

			} else if (ColumnInde == 25) {
				int cellType = currentCell.getCellType();
				if ((cellType == 1)) {
					tempCsvDataList2.setPolicyLimitAmount(currentCell.getStringCellValue());
				} else if ((cellType == 0)) {
					int PolicyLimitAmount = (int) currentCell.getNumericCellValue();
					tempCsvDataList2.setPolicyLimitAmount(String.valueOf(PolicyLimitAmount));
				} else if ((cellType == 3)) {
					tempCsvDataList2.setPolicyLimitAmount(null);
				}

			} else if (ColumnInde == 26) {
				int cellType = currentCell.getCellType();
				if ((cellType == 1)) {
					tempCsvDataList2.setCoverageAmt(currentCell.getStringCellValue());
				} else if ((cellType == 0)) {
					int CoverageAmt = (int) currentCell.getNumericCellValue();
					tempCsvDataList2.setCoverageAmt(String.valueOf(CoverageAmt));
				} else if ((cellType == 3)) {
					tempCsvDataList2.setCoverageAmt(null);
				}

			} else if (ColumnInde == 27) {
				int cellType = currentCell.getCellType();
				if ((cellType == 1)) {
					tempCsvDataList2.setPolicyCoverage(currentCell.getStringCellValue());
				} else if ((cellType == 0)) {
					int CoverageAmt = (int) currentCell.getNumericCellValue();
					tempCsvDataList2.setPolicyCoverage(String.valueOf(CoverageAmt));
				} else if ((cellType == 3)) {
					tempCsvDataList2.setPolicyCoverage(null);
				}

			} else if (ColumnInde == 28) {
				int cellType = currentCell.getCellType();
				if ((cellType == 1)) {
					tempCsvDataList2.setAdjuster_ID(currentCell.getStringCellValue());
				} else if ((cellType == 0)) {
					int AdjusterId = (int) currentCell.getNumericCellValue();
					tempCsvDataList2.setAdjuster_ID(String.valueOf(AdjusterId));
				} else if ((cellType == 3)) {
					tempCsvDataList2.setReason("One of the field value is blank please reimport the claim");
				}
			} else if (ColumnInde == 29) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setAdjuster_FName(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setAdjuster_FName(currentCell.getStringCellValue());
				}
			} else if (ColumnInde == 30) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setAdjuster_LName(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setAdjuster_LName(currentCell.getStringCellValue());
				}
			} else if (ColumnInde == 31) {
				int cellType = currentCell.getCellType();
				if ((cellType == 1)) {
					tempCsvDataList2.setAdjuster_phone(currentCell.getStringCellValue());
				} else if ((cellType == 0)) {
					int Adjuster_phone = (int) currentCell.getNumericCellValue();
					tempCsvDataList2.setAdjuster_phone(String.valueOf(Adjuster_phone));
				} else if ((cellType == 3)) {
					tempCsvDataList2.setAdjuster_phone(null);
				}
			} else if (ColumnInde == 32) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setAdjuster_email(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setAdjuster_email(currentCell.getStringCellValue());
				}
			} else if (ColumnInde == 33) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setClaim_Status(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setClaim_Status(currentCell.getStringCellValue());
				}
			} else if (ColumnInde == 34) {
				int cellType = currentCell.getCellType();
				if (!(cellType == 3)) {
					tempCsvDataList2.setClaimDescription(currentCell.getStringCellValue());
				} else {
					tempCsvDataList2.setReason("Mandatory Fields Are Missing");
				}
			}
		} catch (Exception e) {
			tempCsvDataList2.setReason("Not able to parse the Excel Please chcek the Excel");
		}

		return tempCsvDataList2;

	}

	public static boolean isValidFormat(String format, String value, Locale locale) {
		LocalDateTime ldt = null;
		DateTimeFormatter fomatter = DateTimeFormatter.ofPattern(format, locale);

		try {
			ldt = LocalDateTime.parse(value, fomatter);
			String result = ldt.format(fomatter);
			return result.equals(value);
		} catch (DateTimeParseException e) {
			try {
				LocalDate ld = LocalDate.parse(value, fomatter);
				String result = ld.format(fomatter);
				return result.equals(value);
			} catch (DateTimeParseException exp) {
				try {
					LocalTime lt = LocalTime.parse(value, fomatter);
					String result = lt.format(fomatter);
					return result.equals(value);
				} catch (DateTimeParseException e2) {
					// Debugging purposes
					// e2.printStackTrace();
				}
			}
		}

		return false;
	}

	public static boolean isRowEmpty(Row row) {
		for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
			Cell cell = row.getCell(c);
			if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK)
				return false;
		}
		return true;
	}
}