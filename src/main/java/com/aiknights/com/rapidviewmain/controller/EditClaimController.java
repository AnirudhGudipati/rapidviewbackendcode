package com.aiknights.com.rapidviewmain.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aiknights.com.rapidviewmain.constants.RapidViewMainConstants;
import com.aiknights.com.rapidviewmain.dto.EndorsementResultDTO;
import com.aiknights.com.rapidviewmain.dto.PolicyResultDTO;
import com.aiknights.com.rapidviewmain.models.ClaimInjectorCsv;
import com.aiknights.com.rapidviewmain.service.EditClaimService;

@RequestMapping("/api")
@RestController
public class EditClaimController {

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	ClaimInjectorCsv editclaimData = new ClaimInjectorCsv();
	private String claimNotification_table = "claim_notification";
	private String policy_check_table = "policy_check";
	@SuppressWarnings("unused")
	private String property = "property";
	private String adjuster_claim_table = "adjuster_claim";
	private String endorsement_table = "endorsement_list";

	@Autowired
	private EditClaimService claimCsvService;

	@RequestMapping(value = "/editclaim", method = RequestMethod.POST)

	// @RequestMapping("/editclaim")
	public ResponseEntity<Map<String, ClaimInjectorCsv>> editClaim(
			@RequestBody final Map<String, String> editClaimDEtails) throws ParseException {
		List<ClaimInjectorCsv> successDataList = new ArrayList<ClaimInjectorCsv>();
		successDataList.clear();

		editclaimData = claimCsvService.editcalimModelSetting(editClaimDEtails);

		Map<String, ClaimInjectorCsv> outputHashMap = new HashMap<>();

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date PolicyStartDate = format.parse(editClaimDEtails.get("policyStartDate"));
		Date PolicyEndDate = format.parse(editClaimDEtails.get("policyEndDate"));
		// policy Date validation
		boolean policyValidation = PolicyStartDate.before(PolicyEndDate);

		// incident date validation
		Date IncidentDate = format.parse(editClaimDEtails.get("incidentDate"));

		boolean ISCalimExist = claimCsvService.chcekClaimIdIsDuplicate(editclaimData);
		if (ISCalimExist) {
			editclaimData.setInjectorStatus("Fail");
			editclaimData.setReason("Duplicate Claim id");

		} else if (!policyValidation) {
			editclaimData.setInjectorStatus("Fail");
			editclaimData.setReason(RapidViewMainConstants.POLICY_DATE_VALIDATION);
		} else if (!PolicyEndDate.before(IncidentDate) && !IncidentDate.after(PolicyStartDate)) {
			editclaimData.setInjectorStatus("Fail");
			editclaimData.setReason(RapidViewMainConstants.INCIDENT_DATE_VALIDATION);
		} else {
			EndorsementResultDTO output_Endorsement = claimCsvService.insertIntoEndorsementTableSetting(editclaimData,
					namedParameterJdbcTemplate);
			if (output_Endorsement.getinsertion_status() == true) {

				// PolicyTable
				PolicyResultDTO output_policy = claimCsvService.insertIntoPolicyTableSetting(editclaimData,
						namedParameterJdbcTemplate, output_Endorsement);
				if (output_policy.getinsertion_status() == true) {
					// ClaimTable
					boolean output_claim = claimCsvService.insertIntoClaimTableSetting(editclaimData,
							namedParameterJdbcTemplate, output_policy);

					if (output_claim == true) {

						// AdjusterClaim Table
						boolean output_Adjuster = claimCsvService.insertIntoAdjusterClaimTableSetting(editclaimData,
								namedParameterJdbcTemplate);
						if (output_Adjuster == true) { // property
							boolean output_property = claimCsvService.insertIntoPropertyTableSetting(editclaimData,
									namedParameterJdbcTemplate, output_policy);
							if (output_property == true) {

								editclaimData.setInjectorStatus("Success");
							} else {

								claimCsvService.deleteUltil(adjuster_claim_table, "claim_id",
										editclaimData.getClaimId());
								claimCsvService.deleteUltil(claimNotification_table, "claim_id",
										editclaimData.getClaimId());
								claimCsvService.deleteUltil(policy_check_table, "policy_number",
										editclaimData.getPolicyNumber());
								claimCsvService.deleteUltil(endorsement_table, "endorsement_id",
										Integer.toString(output_Endorsement.getEndorsement_id_pk()));
								editclaimData.setInjectorStatus("Fail");

							}
						} else {
							claimCsvService.deleteUltil(claimNotification_table, "claim_id",
									editclaimData.getClaimId());
							claimCsvService.deleteUltil(policy_check_table, "policy_number",
									editclaimData.getPolicyNumber());
							claimCsvService.deleteUltil(endorsement_table, "endorsement_id",
									Integer.toString(output_Endorsement.getEndorsement_id_pk()));
							editclaimData.setInjectorStatus("Fail");
						}

					} else {

						claimCsvService.deleteUltil(policy_check_table, "policy_number",
								editclaimData.getPolicyNumber());
						claimCsvService.deleteUltil(endorsement_table, "endorsement_id",
								Integer.toString(output_Endorsement.getEndorsement_id_pk()));
						editclaimData.setInjectorStatus("Fail");

					}

				} else {

					claimCsvService.deleteUltil(endorsement_table, "endorsement_id",
							Integer.toString(output_Endorsement.getEndorsement_id_pk()));
					editclaimData.setInjectorStatus("Fail");
				}

			} else {

				editclaimData.setInjectorStatus("Fail");

			}
		}

		successDataList.add(editclaimData);
		claimCsvService.CompleteWeatherStagging(successDataList);
		outputHashMap.put("Data", editclaimData);

		return new ResponseEntity<>(outputHashMap, HttpStatus.OK);

	}

}
