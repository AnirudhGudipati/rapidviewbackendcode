package com.aiknights.com.rapidviewmain.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aiknights.com.rapidviewmain.service.GetIDService;
import com.aiknights.com.rapidviewmain.service.WeatherReportDownloadService;

@RequestMapping("/api")
@RestController
public class WeatherReportDownloadController {

	@Autowired

	WeatherReportDownloadService weatherReportDownloadService;
	private static final Logger logger = LoggerFactory.getLogger(WeatherReportDownloadController.class);

	@RequestMapping(value = "/weatherreportdownloadcount", method = RequestMethod.POST)
	public ResponseEntity<Map<String, String>> weatherreportdownloadcount(
			@RequestBody final Map<String, String> reportDownloadCount) {
		logger.info("WeatherReportDownloadController");
		final Map<String, String> responseMap = new HashMap<>();
		String downloadCountui = reportDownloadCount.get("downloadCount");
		String userName = reportDownloadCount.get("username");
		int downloadCount = Integer.valueOf(downloadCountui);

		String weatherReportDownloadCount = weatherReportDownloadService
				.getweatherReportDownloadCountByUserName(userName);

		if (weatherReportDownloadCount != null && !weatherReportDownloadCount.isEmpty()) {
			int downloadCountDb = Integer.valueOf(weatherReportDownloadCount);

			if (downloadCountDb == 0) {
				responseMap.put("WeatherReportDownloadStatus", "invalid");
				responseMap.put("RemainingDownloadsCount", "0");
				logger.info("RemainingDownloadsCount" + "0");
			} else {
				int latestDwonloadCount = downloadCountDb - 1;

				boolean updatestatus = weatherReportDownloadService.updateLatestDownloadCount(userName,
						latestDwonloadCount);
				if (updatestatus) {
					responseMap.put("WeatherReportDownloadStatus", "valid");
					responseMap.put("RemainingDownloadsCount", String.valueOf(latestDwonloadCount));
				} else {

				}

			}

		}
		return new ResponseEntity<>(responseMap, HttpStatus.OK);
	}

	@RequestMapping(value = "/availableweatherdownloadcount", method = RequestMethod.POST)
	public ResponseEntity<Map<String, String>> availableDownloadCount(
			@RequestBody final Map<String, String> availableDownloadCount) {
		logger.info("WeatherReportDownloadController");
		final Map<String, String> responseMap = new HashMap<>();

		String userName = availableDownloadCount.get("username");

		Map<String, Object> availableDownloadCountforUser = weatherReportDownloadService
				.getAvailableCountByUserName(userName);

		String AmountToBe_Paid = (String) availableDownloadCountforUser.get("AmountToBe_Paid");
		int Weather_Download_MaxCount = (int) availableDownloadCountforUser.get("Weather_Download_MaxCount");

		String reportId = Long.toString(GetIDService.getID());

		responseMap.put("reportId", reportId);
		responseMap.put("AmountToBePaid", AmountToBe_Paid);
		responseMap.put("RemainingDownloadsCount", String.valueOf(Weather_Download_MaxCount));
		return new ResponseEntity<>(responseMap, HttpStatus.OK);
	}

}
