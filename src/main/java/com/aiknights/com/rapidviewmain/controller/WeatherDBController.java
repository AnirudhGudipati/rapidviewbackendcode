package com.aiknights.com.rapidviewmain.controller;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aiknights.com.rapidviewmain.DAOImpl.ClaimNotificationDAOImpl;
import com.aiknights.com.rapidviewmain.models.ClaimNotification;
import com.aiknights.com.rapidviewmain.models.ClaimNotificationParentModel;
import com.aiknights.com.rapidviewmain.models.FlaskServiceChildrenModel;
import com.aiknights.com.rapidviewmain.models.FlaskServiceModel;
import com.aiknights.com.rapidviewmain.models.GMapOutputParentModel;
import com.aiknights.com.rapidviewmain.models.GoogleLiveMapDataModel;
import com.aiknights.com.rapidviewmain.models.NexRadServiceChildrenModel;
import com.aiknights.com.rapidviewmain.models.NexRadServiceModel;
import com.aiknights.com.rapidviewmain.models.PLSRWindServiceChildrenModel;
import com.aiknights.com.rapidviewmain.models.PLSRWindServiceModel;
import com.aiknights.com.rapidviewmain.models.ReportsStageDBModel;
import com.aiknights.com.rapidviewmain.models.StormEventsAlertModel;
import com.aiknights.com.rapidviewmain.models.ThunderWindServiceChildrenModel;
import com.aiknights.com.rapidviewmain.models.ThunderWindServiceModel;
import com.aiknights.com.rapidviewmain.models.UserDetails;
import com.aiknights.com.rapidviewmain.models.WeatherAssessmentDataModel;
import com.aiknights.com.rapidviewmain.models.WeatherDataforBoxChartsModel;
import com.aiknights.com.rapidviewmain.models.WeatherEDBModel;
import com.aiknights.com.rapidviewmain.models.WeatherNexRadModel;
import com.aiknights.com.rapidviewmain.models.WeatherPLSRWindModel;
import com.aiknights.com.rapidviewmain.models.WeatherStageDatabaseModel;
import com.aiknights.com.rapidviewmain.models.WeatherThunderWindModel;
import com.aiknights.com.rapidviewmain.service.ClaimNotificationService;
import com.aiknights.com.rapidviewmain.service.EmailNotificationService;
import com.aiknights.com.rapidviewmain.service.GoogleMapAPIService;
import com.aiknights.com.rapidviewmain.service.UploadWeatherAssessmentDetailstoDBService;
import com.aiknights.com.rapidviewmain.service.UserDetailsService;
import com.aiknights.com.rapidviewmain.service.WeatherDBService;
import com.aiknights.com.rapidviewmain.service.WeatherDataConversionService;
import com.aiknights.com.rapidviewmain.service.WebSocketService;

@RequestMapping("/api")
@RestController
public class WeatherDBController {

	@Autowired
	private UploadWeatherAssessmentDetailstoDBService uploadWeatherDataService;

	@Autowired
	private WeatherDBService weatherDBService;

	@Autowired
	private ClaimNotificationService claimService;

	@Autowired
	private ClaimNotificationService claimnotificationservice;

	String Timezone = "GMT";

	@Autowired
	WebSocketService wsservice;

	@Autowired
	UserDetailsService userservice;

	@Autowired
	private EmailNotificationService emailservice;

	private static final Logger LOGGER = LoggerFactory.getLogger(ClaimNotificationDAOImpl.class);

	private static DecimalFormat df2 = new DecimalFormat("#.##");

	@GetMapping("/getWeatherDataFromDetails/{startdate}&{enddate}&{latitude}&{longitude}&{startdist}&{enddist}")
	public ResponseEntity<Map<String, List<WeatherEDBModel>>> getWeatherDBDataFromClaim(
			@PathVariable final String startdate, @PathVariable final String enddate,
			@PathVariable final String latitude, @PathVariable final String longitude,
			@PathVariable final String startdist, @PathVariable final String enddist) {

		List<WeatherEDBModel> weatherData = weatherDBService.getWeatherDBDataByClaimId(startdate, enddate, latitude,
				longitude, startdist, enddist);
		Map<String, List<WeatherEDBModel>> claimWeatherDBMap = new HashMap<>();
		if (weatherData == null) {
			claimWeatherDBMap.put("weatherdata", null);
			return new ResponseEntity<>(claimWeatherDBMap, HttpStatus.NOT_FOUND);
		} else {
			claimWeatherDBMap.put("weatherdata", weatherData);
			return new ResponseEntity<>(claimWeatherDBMap, HttpStatus.OK);
		}
	}

	@GetMapping("/getWeathernexradFromDetails/{startdate}&{enddate}&{latitude}&{longitude}&{startdist}&{enddist}")
	public ResponseEntity<Map<String, List<WeatherNexRadModel>>> getWeatherNexRadDataFromClaim(
			@PathVariable final String startdate, @PathVariable final String enddate,
			@PathVariable final String latitude, @PathVariable final String longitude,
			@PathVariable final String startdist, @PathVariable final String enddist) {

		List<WeatherNexRadModel> weatherNexRadData = weatherDBService.getWeatherNexRadDataByClaimId(startdate, enddate,
				latitude, longitude, startdist, enddist);
		Map<String, List<WeatherNexRadModel>> claimWeatherNexRadMap = new HashMap<>();
		if (weatherNexRadData == null) {
			claimWeatherNexRadMap.put("weatherdata", null);
			return new ResponseEntity<>(claimWeatherNexRadMap, HttpStatus.NOT_FOUND);
		} else {
			claimWeatherNexRadMap.put("weatherdata", weatherNexRadData);
			return new ResponseEntity<>(claimWeatherNexRadMap, HttpStatus.OK);
		}
	}

	@GetMapping("/getWeatherDataFromStageByClaimID/{claimid}")
	public ResponseEntity<Map<String, List<WeatherDataforBoxChartsModel>>> getWeatherDataFromStageByClaimIDAPI(
			@PathVariable String claimid) {
		List<WeatherStageDatabaseModel> weatherdata = weatherDBService.getWeatherDataFromStageByClaimID(claimid);
		List<WeatherDataforBoxChartsModel> outputdata = new ArrayList<>();
		for (int i = 0; i < weatherdata.size(); i++) {
			WeatherDataforBoxChartsModel wom = new WeatherDataforBoxChartsModel();
			wom.setBeginDate(weatherdata.get(i).getEventdate());
			wom.setDistance(Double.parseDouble(df2.format(Double.parseDouble(weatherdata.get(i).getDistance()))));
			wom.setMagnitude(Double.parseDouble(df2.format(Double.parseDouble(weatherdata.get(i).getHailsize()))));
			outputdata.add(wom);
		}
		Map<String, List<WeatherDataforBoxChartsModel>> weatherdatamap = new HashMap<>();
		weatherdatamap.put("weatherData", outputdata);
		return new ResponseEntity<>(weatherdatamap, HttpStatus.OK);

	}

	public List<WeatherDataforBoxChartsModel> getWeatherDataFromStageByClaimID(String claimid) {
		List<WeatherStageDatabaseModel> weatherdata = weatherDBService.getWeatherDataFromStageByClaimID(claimid);
		List<WeatherDataforBoxChartsModel> outputdata = new ArrayList<>();
		for (int i = 0; i < weatherdata.size(); i++) {
			WeatherDataforBoxChartsModel wom = new WeatherDataforBoxChartsModel();
			wom.setBeginDate(weatherdata.get(i).getEventdate());
			wom.setDistance(Double.parseDouble(df2.format(Double.parseDouble(weatherdata.get(i).getDistance()))));
			wom.setMagnitude(Double.parseDouble(df2.format(Double.parseDouble(weatherdata.get(i).getHailsize()))));
			outputdata.add(wom);
		}
		return outputdata;

	}

	public List<WeatherDataforBoxChartsModel> getWeatherDataFromReportsStageByReportID(String reportid) {
		List<ReportsStageDBModel> weatherdata = weatherDBService.getWeatherDataFromStageByReportID(reportid);
		List<WeatherDataforBoxChartsModel> outputdata = new ArrayList<>();
		for (int i = 0; i < weatherdata.size(); i++) {
			WeatherDataforBoxChartsModel wom = new WeatherDataforBoxChartsModel();
			wom.setBeginDate(weatherdata.get(i).getEventdate());
			wom.setDistance(Double.parseDouble(df2.format(Double.parseDouble(weatherdata.get(i).getDistance()))));
			wom.setMagnitude(Double.parseDouble(df2.format(Double.parseDouble(weatherdata.get(i).getHailsize()))));
			outputdata.add(wom);
		}
		return outputdata;

	}

	@GetMapping("/getGMapPlotWeatherDataFromStageByClaimID/{claimid}")
	public ResponseEntity<Map<String, List<GoogleLiveMapDataModel>>> getGMapPlotWeatherDataFromStageByClaimIDAPI(
			@PathVariable String claimid) {
		List<WeatherStageDatabaseModel> weatherdata = weatherDBService.getWeatherDataFromStageByClaimID(claimid);
		List<GoogleLiveMapDataModel> outputdata = new ArrayList<>();
		int count = 0;
		ClaimNotificationParentModel cm = new ClaimNotificationParentModel();
		ClaimNotification claimdata = claimService.getClaimByClaimId(claimid);
		cm.setClaim(claimdata);
		String address = cm.getClaim().getAddress() + " " + cm.getClaim().getCity() + " " + cm.getClaim().getState()
				+ " " + cm.getClaim().getZipCode();
		GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
		String latitude = "";
		String longitude = "";
		for (int i = 0; i < wm.getResults().size(); i++) {
			latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
			longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
		}
		for (int i = 0; i < weatherdata.size(); i++) {
			if (Double.parseDouble((weatherdata.get(i).getDistance())) <= 5) {
				count = count + 1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(weatherdata.get(i).getLatitude()));
				wom.setLongitude(Double.parseDouble(weatherdata.get(i).getLongitude()));
				wom.setLabel(Integer.toString(count));
				wom.setDraggable("true");
				wom.setDistance(Double.parseDouble(df2.format(Double.parseDouble(weatherdata.get(i).getDistance()))));
				wom.setHailsize(Double.parseDouble(weatherdata.get(i).getHailsize()));
				wom.setEventDate(weatherdata.get(i).getEventdate());
				outputdata.add(wom);
			}
		}
		if (count == 0) {
			for (int i = 0; i < weatherdata.size(); i++) {
				count = count + 1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(weatherdata.get(i).getLatitude()));
				wom.setLongitude(Double.parseDouble(weatherdata.get(i).getLongitude()));
				wom.setLabel(Integer.toString(count));
				wom.setDraggable("true");
				wom.setDistance(Double.parseDouble(df2.format(Double.parseDouble(weatherdata.get(i).getDistance()))));
				wom.setHailsize(Double.parseDouble(weatherdata.get(i).getHailsize()));
				wom.setEventDate(weatherdata.get(i).getEventdate());
				outputdata.add(wom);
			}
		}
		Map<String, List<GoogleLiveMapDataModel>> weatherdatamap = new HashMap<>();
		weatherdatamap.put("GMapMarkerData", outputdata);
		return new ResponseEntity<>(weatherdatamap, HttpStatus.OK);

	}

	public List<GoogleLiveMapDataModel> getGMapPlotWeatherDataFromStageByClaimID(String claimid) {
		List<WeatherStageDatabaseModel> weatherdata = weatherDBService.getWeatherDataFromStageByClaimID(claimid);
		List<GoogleLiveMapDataModel> outputdata = new ArrayList<>();
		int count = 0;
		ClaimNotificationParentModel cm = new ClaimNotificationParentModel();
		ClaimNotification claimdata = claimService.getClaimByClaimId(claimid);
		cm.setClaim(claimdata);
		String address = cm.getClaim().getAddress() + " " + cm.getClaim().getCity() + " " + cm.getClaim().getState()
				+ " " + cm.getClaim().getZipCode();
		GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
		String latitude = "";
		String longitude = "";
		for (int i = 0; i < wm.getResults().size(); i++) {
			latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
			longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
		}
		for (int i = 0; i < weatherdata.size(); i++) {
			if (Double.parseDouble((weatherdata.get(i).getDistance())) <= 5) {
				count = count + 1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(weatherdata.get(i).getLatitude()));
				wom.setLongitude(Double.parseDouble(weatherdata.get(i).getLongitude()));
				wom.setLabel(Integer.toString(count));
				wom.setDraggable("true");
				wom.setDistance(Double.parseDouble(df2.format(Double.parseDouble(weatherdata.get(i).getDistance()))));
				wom.setHailsize(Double.parseDouble(weatherdata.get(i).getHailsize()));
				wom.setEventDate(weatherdata.get(i).getEventdate());
				outputdata.add(wom);
			}
		}
		if (count == 0) {
			for (int i = 0; i < weatherdata.size(); i++) {
				count = count + 1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(weatherdata.get(i).getLatitude()));
				wom.setLongitude(Double.parseDouble(weatherdata.get(i).getLongitude()));
				wom.setLabel(Integer.toString(count));
				wom.setDraggable("true");
				wom.setDistance(Double.parseDouble(df2.format(Double.parseDouble(weatherdata.get(i).getDistance()))));
				wom.setHailsize(Double.parseDouble(weatherdata.get(i).getHailsize()));
				wom.setEventDate(weatherdata.get(i).getEventdate());
				outputdata.add(wom);
			}
		}
		return outputdata;

	}

	public List<GoogleLiveMapDataModel> getGMapPlotWeatherDataFromStageByReportID(String reportid) {
		List<ReportsStageDBModel> weatherdata = weatherDBService.getWeatherDataFromStageByReportID(reportid);
		List<GoogleLiveMapDataModel> outputdata = new ArrayList<>();
		int count = 0;
		final WeatherAssessmentDataModel weatherAssessmentData = uploadWeatherDataService
				.getWeatherReportById(reportid);
		String address = weatherAssessmentData.getAddress();
		GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
		String latitude = "";
		String longitude = "";
		for (int i = 0; i < wm.getResults().size(); i++) {
			latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
			longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
		}
		for (int i = 0; i < weatherdata.size(); i++) {
			if (Double.parseDouble((weatherdata.get(i).getDistance())) <= 5) {
				count = count + 1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(weatherdata.get(i).getLatitude()));
				wom.setLongitude(Double.parseDouble(weatherdata.get(i).getLongitude()));
				wom.setLabel(Integer.toString(count));
				wom.setDraggable("true");
				wom.setDistance(Double.parseDouble(df2.format(Double.parseDouble(weatherdata.get(i).getDistance()))));
				wom.setHailsize(Double.parseDouble(weatherdata.get(i).getHailsize()));
				wom.setEventDate(weatherdata.get(i).getEventdate());
				outputdata.add(wom);
			}
		}
		if (count == 0) {
			for (int i = 0; i < weatherdata.size(); i++) {
				count = count + 1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(weatherdata.get(i).getLatitude()));
				wom.setLongitude(Double.parseDouble(weatherdata.get(i).getLongitude()));
				wom.setLabel(Integer.toString(count));
				wom.setDraggable("true");
				wom.setDistance(Double.parseDouble(df2.format(Double.parseDouble(weatherdata.get(i).getDistance()))));
				wom.setHailsize(Double.parseDouble(weatherdata.get(i).getHailsize()));
				wom.setEventDate(weatherdata.get(i).getEventdate());
				outputdata.add(wom);
			}
		}
		return outputdata;

	}

	@GetMapping("/getGMapPlotWeatherDataFromStageByReportID/{reportid}")
	public ResponseEntity<Map<String, List<GoogleLiveMapDataModel>>> getGMapPlotWeatherDataFromStageByReportIdAPI(
			@PathVariable String reportid) {

		List<ReportsStageDBModel> weatherdata = weatherDBService.getWeatherDataFromStageByReportID(reportid);
		List<GoogleLiveMapDataModel> outputdata = new ArrayList<>();
		int count = 0;
		final WeatherAssessmentDataModel weatherAssessmentData = uploadWeatherDataService
				.getWeatherReportById(reportid);
		String address = weatherAssessmentData.getAddress();
		GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
		String latitude = "";
		String longitude = "";
		for (int i = 0; i < wm.getResults().size(); i++) {
			latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
			longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
		}
		for (int i = 0; i < weatherdata.size(); i++) {
			if (Double.parseDouble((weatherdata.get(i).getDistance())) <= 5) {
				count = count + 1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(weatherdata.get(i).getLatitude()));
				wom.setLongitude(Double.parseDouble(weatherdata.get(i).getLongitude()));
				wom.setLabel(Integer.toString(count));
				wom.setDraggable("true");
				wom.setDistance(Double.parseDouble(df2.format(Double.parseDouble(weatherdata.get(i).getDistance()))));
				wom.setHailsize(Double.parseDouble(weatherdata.get(i).getHailsize()));
				wom.setEventDate(weatherdata.get(i).getEventdate());
				outputdata.add(wom);
			}
		}
		if (count == 0) {
			for (int i = 0; i < weatherdata.size(); i++) {
				count = count + 1;
				GoogleLiveMapDataModel wom = new GoogleLiveMapDataModel();
				wom.setHomeLatitude(Double.parseDouble(latitude));
				wom.setHomeLongitude(Double.parseDouble(longitude));
				wom.setLatitude(Double.parseDouble(weatherdata.get(i).getLatitude()));
				wom.setLongitude(Double.parseDouble(weatherdata.get(i).getLongitude()));
				wom.setLabel(Integer.toString(count));
				wom.setDraggable("true");
				wom.setDistance(Double.parseDouble(df2.format(Double.parseDouble(weatherdata.get(i).getDistance()))));
				wom.setHailsize(Double.parseDouble(weatherdata.get(i).getHailsize()));
				wom.setEventDate(weatherdata.get(i).getEventdate());
				outputdata.add(wom);
			}
		}
		Map<String, List<GoogleLiveMapDataModel>> weatherdatamap = new HashMap<>();
		weatherdatamap.put("GMapMarkerData", outputdata);
		return new ResponseEntity<>(weatherdatamap, HttpStatus.OK);
	}

	@GetMapping("/stageWeatherDataByClaimID/{claimid}")
	public ResponseEntity<String> stageWeatherDataByClaimIDAPI(@PathVariable String claimid) {
		boolean stagecheck = weatherDBService.stageWeatherDataCheckbyClaimID(claimid);
		if (stagecheck == true) {
			ClaimNotificationParentModel cm = new ClaimNotificationParentModel();
			ClaimNotification claimdata = claimService.getClaimByClaimId(claimid);
			cm.setClaim(claimdata);
			String address = cm.getClaim().getAddress() + " " + cm.getClaim().getCity() + " " + cm.getClaim().getState()
					+ " " + cm.getClaim().getZipCode();
			GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
			String latitude = "";
			String longitude = "";
			for (int i = 0; i < wm.getResults().size(); i++) {
				latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
				longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
			}
			ClaimNotificationParentModel pm = new ClaimNotificationParentModel();
			ClaimNotification claim = claimService.getClaimWithPolicyDataByClaimId(claimid);
			pm.setClaim(claim);
			if (pm.getClaim().getStartDate() != null || pm.getClaim().getClaimDate() != null) {
				FlaskServiceModel fm = new FlaskServiceModel();
				List<FlaskServiceChildrenModel> FL = new ArrayList<FlaskServiceChildrenModel>();
				List<WeatherEDBModel> weatherData = weatherDBService.getWeatherDBDataByClaimId(
						pm.getClaim().getStartDate().toString(), pm.getClaim().getClaimDate(), latitude, longitude,
						"0.0", "10.0");
				for (int i = 0; i < weatherData.size(); i++) {
					FlaskServiceChildrenModel fmc = new FlaskServiceChildrenModel();
					fmc.setBegin_date(weatherData.get(i).getBegin_date());
					fmc.setBegin_lat(weatherData.get(i).getBegin_lat());
					fmc.setBegin_location(weatherData.get(i).getBegin_location());
					fmc.setBegin_lon(weatherData.get(i).getBegin_lon());
					fmc.setBegin_time(weatherData.get(i).getBegin_time());
					fmc.setDistance(Double.parseDouble(weatherData.get(i).getDistance()));
					fmc.setEvent_narrative(weatherData.get(i).getEvent_narrative());
					fmc.setEvent_type(weatherData.get(i).getEvent_type());
					fmc.setMagnitude(weatherData.get(i).getMagnitude());
					FL.add(fmc);
				}
				fm.setWeatherdata(FL);
				NexRadServiceModel nm = new NexRadServiceModel();
				List<NexRadServiceChildrenModel> NL = new ArrayList<NexRadServiceChildrenModel>();
				List<WeatherNexRadModel> weatherNexRadData = weatherDBService.getWeatherNexRadDataByClaimId(
						pm.getClaim().getStartDate().toString(), pm.getClaim().getClaimDate(), latitude, longitude,
						"0.0", "10.0");
				for (int i = 0; i < weatherNexRadData.size(); i++) {
					NexRadServiceChildrenModel nmc = new NexRadServiceChildrenModel();
					nmc.setDistance(Double.parseDouble(weatherNexRadData.get(i).getDistance()));
					nmc.setEvent_date(weatherNexRadData.get(i).getEvent_date());
					nmc.setLat(weatherNexRadData.get(i).getLat());
					nmc.setLon(weatherNexRadData.get(i).getLon());
					nmc.setMaxsize(weatherNexRadData.get(i).getMaxsize());
					nmc.setProb(weatherNexRadData.get(i).getProb());
					nmc.setSevprob(weatherNexRadData.get(i).getSevprob());
					NL.add(nmc);
				}
				nm.setWeatherdata(NL);
				List<FlaskServiceChildrenModel> flist = new ArrayList<FlaskServiceChildrenModel>();
				List<NexRadServiceChildrenModel> nlist = new ArrayList<NexRadServiceChildrenModel>();
				flist.addAll(fm.getWeatherdata());
				nlist.addAll(nm.getWeatherdata());
				List<WeatherStageDatabaseModel> wt = new ArrayList<WeatherStageDatabaseModel>();
				wt = new WeatherDataConversionService().ConvertToWeatherStageModel(claimid, flist, nlist);
				Collections.sort(wt);
				Collections.reverse(wt);
				boolean insertstatus = weatherDBService.stageWeatherDataByClaimID(wt);
				if (insertstatus == true) {
					System.out.println("Staged Weather data for claim : " + claimid);
					return new ResponseEntity<>("Staged Weather data for claim : " + claimid, HttpStatus.OK);
				} else {
					System.out.println("Error Staging Weather data for claim : " + claimid);
					return new ResponseEntity<>("Error Staging Weather data for claim : " + claimid, HttpStatus.OK);
				}
			} else {
				System.out.println("Policy Start Date or Claim Date is Null : " + claimid);
				return new ResponseEntity<>("Policy Start Date or Claim Date is Null for claim : " + claimid,
						HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>("Claim " + claimid + "'s Weather Data Already Staged", HttpStatus.OK);
		}
	}

	public String stageWeatherDataByClaimID(@PathVariable String claimid) {
		boolean stagecheck = weatherDBService.stageWeatherDataCheckbyClaimID(claimid);
		if (stagecheck == true) {
			ClaimNotificationParentModel cm = new ClaimNotificationParentModel();
			ClaimNotification claimdata = claimService.getClaimByClaimId(claimid);
			cm.setClaim(claimdata);
			String address = cm.getClaim().getAddress() + " " + cm.getClaim().getCity() + " " + cm.getClaim().getState()
					+ " " + cm.getClaim().getZipCode();
			GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
			String latitude = "";
			String longitude = "";
			for (int i = 0; i < wm.getResults().size(); i++) {
				latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
				longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
			}
			ClaimNotificationParentModel pm = new ClaimNotificationParentModel();
			ClaimNotification claim = claimService.getClaimWithPolicyDataByClaimId(claimid);
			pm.setClaim(claim);
			if (pm.getClaim().getStartDate() != null || pm.getClaim().getClaimDate() != null) {
				FlaskServiceModel fm = new FlaskServiceModel();
				List<FlaskServiceChildrenModel> FL = new ArrayList<FlaskServiceChildrenModel>();
				List<WeatherEDBModel> weatherData = weatherDBService.getWeatherDBDataByClaimId(
						pm.getClaim().getStartDate().toString(), pm.getClaim().getClaimDate(), latitude, longitude,
						"0.0", "10.0");
				for (int i = 0; i < weatherData.size(); i++) {
					FlaskServiceChildrenModel fmc = new FlaskServiceChildrenModel();
					fmc.setBegin_date(weatherData.get(i).getBegin_date());
					fmc.setBegin_lat(weatherData.get(i).getBegin_lat());
					fmc.setBegin_location(weatherData.get(i).getBegin_location());
					fmc.setBegin_lon(weatherData.get(i).getBegin_lon());
					fmc.setBegin_time(weatherData.get(i).getBegin_time());
					fmc.setDistance(Double.parseDouble(weatherData.get(i).getDistance()));
					fmc.setEvent_narrative(weatherData.get(i).getEvent_narrative());
					fmc.setEvent_type(weatherData.get(i).getEvent_type());
					fmc.setMagnitude(weatherData.get(i).getMagnitude());
					FL.add(fmc);
				}
				fm.setWeatherdata(FL);
				NexRadServiceModel nm = new NexRadServiceModel();
				List<NexRadServiceChildrenModel> NL = new ArrayList<NexRadServiceChildrenModel>();
				List<WeatherNexRadModel> weatherNexRadData = weatherDBService.getWeatherNexRadDataByClaimId(
						pm.getClaim().getStartDate().toString(), pm.getClaim().getClaimDate(), latitude, longitude,
						"0.0", "10.0");
				for (int i = 0; i < weatherNexRadData.size(); i++) {
					NexRadServiceChildrenModel nmc = new NexRadServiceChildrenModel();
					nmc.setDistance(Double.parseDouble(weatherNexRadData.get(i).getDistance()));
					nmc.setEvent_date(weatherNexRadData.get(i).getEvent_date());
					nmc.setLat(weatherNexRadData.get(i).getLat());
					nmc.setLon(weatherNexRadData.get(i).getLon());
					nmc.setMaxsize(weatherNexRadData.get(i).getMaxsize());
					nmc.setProb(weatherNexRadData.get(i).getProb());
					nmc.setSevprob(weatherNexRadData.get(i).getSevprob());
					NL.add(nmc);
				}
				nm.setWeatherdata(NL);
				List<FlaskServiceChildrenModel> flist = new ArrayList<FlaskServiceChildrenModel>();
				List<NexRadServiceChildrenModel> nlist = new ArrayList<NexRadServiceChildrenModel>();
				flist.addAll(fm.getWeatherdata());
				nlist.addAll(nm.getWeatherdata());
				List<WeatherStageDatabaseModel> wt = new ArrayList<WeatherStageDatabaseModel>();
				wt = new WeatherDataConversionService().ConvertToWeatherStageModel(claimid, flist, nlist);
				Collections.sort(wt);
				Collections.reverse(wt);
				boolean insertstatus = weatherDBService.stageWeatherDataByClaimID(wt);
				if (insertstatus == true) {
					System.out.println("Staged Weather data for claim : " + claimid);
					return "Staged Weather data for claim : " + claimid;
				} else {
					System.out.println("Error Staging Weather data for claim : " + claimid);
					return "Error Staging Weather data for claim : " + claimid;
				}
			} else {
				System.out.println("Policy Start Date or Claim Date is Null : " + claimid);
				return "Policy Start Date or Claim Date is Null for claim : " + claimid;
			}
		} else {
			return "Claim " + claimid + "'s Weather Data Already Staged";
		}
	}

	public boolean stageWeatherDataByReportID(String reportID) {
		boolean stagecheck = weatherDBService.stageWeatherDataCheckbyReportID(reportID);
		if (stagecheck == true) {
			final WeatherAssessmentDataModel weatherAssessmentData = uploadWeatherDataService
					.getWeatherReportById(reportID);
			String address = weatherAssessmentData.getAddress();
			String policydate = weatherAssessmentData.getPolicystartdate();
			String claimdate = weatherAssessmentData.getClaimdate();
			String reportType = weatherAssessmentData.getReportType();
			GMapOutputParentModel wm = new GoogleMapAPIService().GetLatLongService(address);
			String latitude = "";
			String longitude = "";
			for (int i = 0; i < wm.getResults().size(); i++) {
				latitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLat());
				longitude = Double.toString(wm.getResults().get(0).getGeometry().getLocation().getLng());
			}
			if (reportType.equals("Hail")) {
				if (policydate != null || claimdate != null) {
					FlaskServiceModel fm = new FlaskServiceModel();
					List<FlaskServiceChildrenModel> FL = new ArrayList<FlaskServiceChildrenModel>();
					List<WeatherEDBModel> weatherData = weatherDBService.getWeatherDBDataByClaimId(policydate,
							claimdate, latitude, longitude, "0.0", "10.0");
					for (int i = 0; i < weatherData.size(); i++) {
						FlaskServiceChildrenModel fmc = new FlaskServiceChildrenModel();
						fmc.setBegin_date(weatherData.get(i).getBegin_date());
						fmc.setBegin_lat(weatherData.get(i).getBegin_lat());
						fmc.setBegin_location(weatherData.get(i).getBegin_location());
						fmc.setBegin_lon(weatherData.get(i).getBegin_lon());
						fmc.setBegin_time(weatherData.get(i).getBegin_time());
						fmc.setDistance(Double.parseDouble(weatherData.get(i).getDistance()));
						fmc.setEvent_narrative(weatherData.get(i).getEvent_narrative());
						fmc.setEvent_type(weatherData.get(i).getEvent_type());
						fmc.setMagnitude(weatherData.get(i).getMagnitude());
						FL.add(fmc);
					}
					fm.setWeatherdata(FL);
					NexRadServiceModel nm = new NexRadServiceModel();
					List<NexRadServiceChildrenModel> NL = new ArrayList<NexRadServiceChildrenModel>();
					List<WeatherNexRadModel> weatherNexRadData = weatherDBService
							.getWeatherNexRadDataByClaimId(policydate, claimdate, latitude, longitude, "0.0", "10.0");
					for (int i = 0; i < weatherNexRadData.size(); i++) {
						NexRadServiceChildrenModel nmc = new NexRadServiceChildrenModel();
						nmc.setDistance(Double.parseDouble(weatherNexRadData.get(i).getDistance()));
						nmc.setEvent_date(weatherNexRadData.get(i).getEvent_date());
						nmc.setLat(weatherNexRadData.get(i).getLat());
						nmc.setLon(weatherNexRadData.get(i).getLon());
						nmc.setMaxsize(weatherNexRadData.get(i).getMaxsize());
						nmc.setProb(weatherNexRadData.get(i).getProb());
						nmc.setSevprob(weatherNexRadData.get(i).getSevprob());
						NL.add(nmc);
					}
					nm.setWeatherdata(NL);
					List<FlaskServiceChildrenModel> flist = new ArrayList<FlaskServiceChildrenModel>();
					List<NexRadServiceChildrenModel> nlist = new ArrayList<NexRadServiceChildrenModel>();
					flist.addAll(fm.getWeatherdata());
					nlist.addAll(nm.getWeatherdata());
					List<ReportsStageDBModel> wt = new ArrayList<ReportsStageDBModel>();
					wt = new WeatherDataConversionService().ConvertToReportsWeatherStageModel(reportID, flist, nlist);
					Collections.sort(wt);
					Collections.reverse(wt);
					boolean insertstatus = weatherDBService.stageWeatherDataByReportID(wt);
					if (insertstatus == true) {
						System.out.println("Staged Weather data for Report : " + reportID);
						return true;
					} else {
						System.out.println("Error Staging Weather data for Report : " + reportID);
						return false;
					}
				} else {
					System.out.println("Policy Start Date or Claim Date is Null : " + reportID);
					return false;
				}
			} else {
				if (policydate != null || claimdate != null) {
					ThunderWindServiceModel fm = new ThunderWindServiceModel();
					List<ThunderWindServiceChildrenModel> FL = new ArrayList<ThunderWindServiceChildrenModel>();
					List<WeatherThunderWindModel> weatherData = weatherDBService.getWeatherThunderWindDataByClaimid(
							policydate, claimdate, latitude, longitude, "0.0", "10.0");
					for (int i = 0; i < weatherData.size(); i++) {
						ThunderWindServiceChildrenModel fmc = new ThunderWindServiceChildrenModel();
						fmc.setBegin_date(weatherData.get(i).getBegin_date());
						fmc.setBegin_lat(weatherData.get(i).getBegin_lat());
						fmc.setBegin_location(weatherData.get(i).getBegin_location());
						fmc.setBegin_lon(weatherData.get(i).getBegin_lon());
						fmc.setBegin_time(weatherData.get(i).getBegin_time());
						fmc.setDistance(Double.parseDouble(weatherData.get(i).getDistance()));
						fmc.setEvent_narrative(weatherData.get(i).getEvent_narrative());
						fmc.setEvent_type(weatherData.get(i).getEvent_type());
						fmc.setMagnitude(weatherData.get(i).getMagnitude());
						FL.add(fmc);
					}
					fm.setWeatherdata(FL);
					PLSRWindServiceModel nm = new PLSRWindServiceModel();
					List<PLSRWindServiceChildrenModel> NL = new ArrayList<PLSRWindServiceChildrenModel>();
					List<WeatherPLSRWindModel> weatherPLSRWindData = weatherDBService
							.getWeatherPLSRWindDataByClaimid(policydate, claimdate, latitude, longitude, "0.0", "10.0");
					for (int i = 0; i < weatherPLSRWindData.size(); i++) {
						PLSRWindServiceChildrenModel nmc = new PLSRWindServiceChildrenModel();
						nmc.setDistance(Double.parseDouble(weatherPLSRWindData.get(i).getDistance()));
						nmc.setEvent_date(weatherPLSRWindData.get(i).getEvent_date());
						nmc.setLat(weatherPLSRWindData.get(i).getLat());
						nmc.setLon(weatherPLSRWindData.get(i).getLon());
						nmc.setEvent(weatherPLSRWindData.get(i).getEvent());
						nmc.setEvent_time(weatherPLSRWindData.get(i).getEvent_time());
						nmc.setMagnitude(weatherPLSRWindData.get(i).getMagnitude());
						NL.add(nmc);
					}
					nm.setWeatherdata(NL);
					List<ThunderWindServiceChildrenModel> flist = new ArrayList<ThunderWindServiceChildrenModel>();
					List<PLSRWindServiceChildrenModel> nlist = new ArrayList<PLSRWindServiceChildrenModel>();
					flist.addAll(fm.getWeatherdata());
					nlist.addAll(nm.getWeatherdata());
					List<ReportsStageDBModel> wt = new ArrayList<ReportsStageDBModel>();
					wt = new WeatherDataConversionService().ConvertToReportsWeatherWindStageModel(reportID, flist,
							nlist);
					Collections.sort(wt);
					Collections.reverse(wt);
					boolean insertstatus = weatherDBService.stageWeatherDataByReportID(wt);
					if (insertstatus == true) {
						System.out.println("Staged Weather data for Report : " + reportID);
						return true;
					} else {
						System.out.println("Error Staging Weather data for Report : " + reportID);
						return false;
					}
				} else {
					System.out.println("Policy Start Date or Claim Date is Null : " + reportID);
					return false;
				}
			}
		} else {
			return true;
		}
	}

	@Scheduled(initialDelay = 10 * 60 * 1000, fixedRate = 60 * 60 * 1000)
	public String scheduledWeatherStageUtility() {
		List<ClaimNotification> allclaims = claimnotificationservice.getAllClaims();
		for (int i = 0; i < allclaims.size(); i++) {
			String stageoutput = stageWeatherDataByClaimID(allclaims.get(i).getClaimId());
			LOGGER.info("Auto Weather Stage Functionality says, " + stageoutput);
		}
		return null;
	}

	@Scheduled(cron = "0 0 8 * * ?")
	public void getEventsForState() {
		List<String> users = weatherDBService.getWeatherSubscriptionUserList();
		for (String u : users) {
			List<StormEventsAlertModel> eventsForUser = new ArrayList<StormEventsAlertModel>();
			List<String> states = weatherDBService.getStatesForSubscribedUser(u);
			List<String> s = new ArrayList<String>();
			for (String state : states) {
				List<StormEventsAlertModel> events = weatherDBService.getEventSeverity(state);
				if (events.size() > 0) {
					eventsForUser.addAll(events);
					s.add(state);
				}
			}
			if (s.size() > 0) {
				String message = weatherDBService.convertToAlertNotificationMessage(s, eventsForUser);
				String emailMessage = weatherDBService.convertToAlertNotificationMessageEmail(s, eventsForUser);
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				df.setTimeZone(TimeZone.getTimeZone(Timezone));
				Date dateo = new Date();
				String dateobj = df.format(dateo);
				String notificationSubject = "RAPIDView Weather Verification Services Alerts for " + dateobj.toString();
				boolean messageStatus = wsservice.sendMessageService("0", u, "", "SPECIFIC_USER", "   -",
						notificationSubject, message);
				UserDetails userDetails = userservice.getUserDetailsByUserID(u);
				String EmailAddress = userDetails.getEmail();
				emailservice.sendMailWithAttachment(EmailAddress, notificationSubject, emailMessage, "", "");
			}
		}
	}
	
	@PostMapping("/emailPdf")
	public void emailPdf(@RequestParam("files") MultipartFile file,@RequestParam("claimid") String claimid,@RequestParam("userid") String userid,@RequestParam("reportType") String reportType) throws IOException {
		LOGGER.info("email api rest call triggered");
		UserDetails userDetails=userservice.getUserDetailsByUserID(userid);
		String EmailAddress=userDetails.getEmail();
		String notificationSubject="Weather Assessment Report("+reportType+") for claim id-"+claimid;
		String username=userDetails.getFirstName()+" "+userDetails.getLastName();
		String message="Hello <b>"+username+"</b>,<br><br>Please find your Weather Assessment Report("+reportType+") in attachment for claim id-"+claimid;
		File directory = new File("/tmp/");
		File temp = File.createTempFile("tmp", ".pdf",directory);
		file.transferTo(temp);
		LOGGER.info("email pdf file path:"+temp.getAbsolutePath());
		emailservice.sendMailWithAttachment(EmailAddress, notificationSubject, message, temp.getAbsolutePath(),file.getOriginalFilename());
	}

}
