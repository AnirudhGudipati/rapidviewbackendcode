package com.aiknights.com.rapidviewmain.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aiknights.com.rapidviewmain.DAO.UserRepository;
import com.aiknights.com.rapidviewmain.constants.RapidViewMainConstants;
import com.aiknights.com.rapidviewmain.models.ManageUsersModel;
import com.aiknights.com.rapidviewmain.models.TokenDBModel;
import com.aiknights.com.rapidviewmain.models.User;
import com.aiknights.com.rapidviewmain.models.UserAndRoleDetailsModel;
import com.aiknights.com.rapidviewmain.models.UserDetails;
import com.aiknights.com.rapidviewmain.service.EmailNotificationService;
import com.aiknights.com.rapidviewmain.service.LoginService;
import com.aiknights.com.rapidviewmain.service.ResourceService;
import com.aiknights.com.rapidviewmain.service.RoleService;
import com.aiknights.com.rapidviewmain.service.TokenDBService;
import com.aiknights.com.rapidviewmain.service.UserDetailsService;

@RequestMapping("/api")
@RestController
public class UserController {

	@Value("${rapid.super.Company.Name}")
	private String superCompanyName;
	
	@Autowired
	private ResourceService resourceService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private RoleService roleService;

	@Autowired
	private LoginService loginService;

	@Autowired
	private UserDetailsService userdetailsservice;

	@Autowired
	private TokenDBService tokenService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private EmailNotificationService emailService;

	String Timezone = "GMT";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

//	@GetMapping("/hello")
//	public String securedAl() {
//		return "Hello World";
//	}

	@GetMapping(value = "/resourcesForUser/{userName}")
	public ResponseEntity<Map<String, List<String>>> getResourcesByUser(@PathVariable final String userName) {
		final Map<String, List<String>> resourcesMap = new HashMap<>();
		final List<String> resourceList = resourceService.getResourceByUser(userName);
		resourcesMap.put("resources", resourceList);
		return new ResponseEntity<>(resourcesMap, HttpStatus.OK);
	}

	@GetMapping(value = "/rolesForUser/{userName}")
	public ResponseEntity<Map<String, List<String>>> getRolesByUser(@PathVariable final String userName) {
		final Map<String, List<String>> rolesMap = new HashMap<>();
		final List<String> rolesList = roleService.getRolesByUser(userName);
		rolesMap.put(RapidViewMainConstants.ROLE, rolesList);
		return new ResponseEntity<>(rolesMap, HttpStatus.OK);
	}

	@RequestMapping(value = "/loginUser", method = RequestMethod.POST)
	public ResponseEntity<Map<String, String>> validteUser(@RequestBody final Map<String, String> userDetails) {
		final String userName = userDetails.get("userName");
		final String password = userDetails.get("password");
		return loginService.isValidUser(userName, password);
	}

	@RequestMapping("/getAllUserDetails")
	public ResponseEntity<Map<String, List<UserAndRoleDetailsModel>>> getAllUserDetails() {
		final Map<String, List<UserAndRoleDetailsModel>> UserMap = new HashMap<>();
		final List<UserAndRoleDetailsModel> userdetails = userdetailsservice.getAllUsers("");
		UserMap.put("UserDetails", userdetails);
		return new ResponseEntity<>(UserMap, HttpStatus.OK);
	}
	
	@RequestMapping("/manageUsers/{adminId}")
	public ResponseEntity<Map<String, List<ManageUsersModel>>> manageUsersAsAdmin(@PathVariable final String adminId) {
		final Map<String, List<ManageUsersModel>> UserMap = new HashMap<>(); 
		UserDetails data = userdetailsservice.getUserDetailsByUserID(adminId);
		String CompanyName = data.getcompanyName();
		String userName = data.getUserName();
		List<String> role = roleService.getRolesByUser(userName);
		if(role.get(0).equals("ADMIN")) {
			final List<ManageUsersModel> userdetails = userdetailsservice.manageUsersAsAdmin(CompanyName);
			UserMap.put("UserDetails", userdetails);
			return new ResponseEntity<>(UserMap, HttpStatus.OK);
		}else {
			UserMap.put("You should be an Admin to view this Data", null);
			return new ResponseEntity<>(UserMap, HttpStatus.OK);
		}
		
	}
	
	@RequestMapping("/userActiveStatusChange")
	public ResponseEntity<Map<String, String>> userActiveStatusChangeAsAdmin(@RequestBody final Map<String, String> data) {
		final Map<String, String> responseMap = new HashMap<>();
		String userID = data.get("userID");
		String adminID = data.get("adminID");
		String ticket_ID = data.get("ticketID");
		String ticket_Notes = data.get("ticketNotes");
		String ticket_Context_Activate = RapidViewMainConstants.ADMIN_CONTEXT_ACTIVATE_USER;
		String ticket_Context_DeActivate = RapidViewMainConstants.ADMIN_CONTEXT_DEACTIVATE_USER;
		String changeStatusTo = data.get("changeStatusTo");
		UserDetails adminData = userdetailsservice.getUserDetailsByUserID(adminID);
		UserDetails userData = userdetailsservice.getUserDetailsByUserID(userID);
		String adminName = adminData.getUserName();
		String userCompany = userData.getcompanyName();
		String adminCompany = adminData.getcompanyName();
		List<String> adminRole = roleService.getRolesByUser(adminName);
		if (adminCompany.toLowerCase().equals(userCompany.toLowerCase()) || adminCompany.equals(superCompanyName)) {
			if (adminRole.get(0).equals("ADMIN")) {
				if (changeStatusTo.equals("ACTIVATED")) {
					boolean result = userdetailsservice.activateUserAsAdmin(userID);
					if (result == true) {
						if(ticket_ID!=null && ticket_ID!="") {
							userdetailsservice.InsertAdminTicketIntoDB(ticket_ID, ticket_Notes, ticket_Context_Activate, userID, adminID);
						}
						responseMap.put("Status", "Success");
						return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
					} else {
						responseMap.put("Status", "Failed");
						responseMap.put("ErrorMessage", "Couldn't Deactivate User");
						return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
					}
				} else if (changeStatusTo.equals("DEACTIVATED")) {
					boolean result = userdetailsservice.deactivateUserAsAdmin(userID);
					if (result == true) {
						if(ticket_ID!=null && ticket_ID!="") {
							userdetailsservice.InsertAdminTicketIntoDB(ticket_ID, ticket_Notes, ticket_Context_DeActivate, userID, adminID);
						}
						responseMap.put("Status", "Success");
						return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
					} else {
						responseMap.put("Status", "Failed");
						responseMap.put("ErrorMessage", "Couldn't Deactivate User");
						return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
					}
				} else {
					responseMap.put("Status", "Failed");
					responseMap.put("ErrorMessage", "Bad Request");
					return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
				}
			} else {
				responseMap.put("Status", "Failed");
				responseMap.put("ErrorMessage", "You should be an Admin to perform this Operation");
				return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
			}
		} else {
			responseMap.put("Status", "Failed");
			responseMap.put("ErrorMessage", "Comp Diff");
			return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
		}
	}
	
	@RequestMapping("/resetPasswordAsAdmin")
	public ResponseEntity<Map<String, String>> resetPasswordAsAdmin(@RequestBody final Map<String, String> data) {
		final Map<String, String> responseMap = new HashMap<>();
		String userID = data.get("userID");
		String adminID = data.get("adminID");
		String ticket_ID = data.get("ticketID");
		String ticket_Notes = data.get("ticketNotes");
		String ticket_Context = RapidViewMainConstants.ADMIN_CONTEXT_RESETPASSWORD;
		UserDetails adminData = userdetailsservice.getUserDetailsByUserID(adminID);
		UserDetails userData = userdetailsservice.getUserDetailsByUserID(userID);
		String adminName = adminData.getUserName();
		String userCompany = userData.getcompanyName();
		String adminCompany = adminData.getcompanyName();
		List<String> adminRole = roleService.getRolesByUser(adminName);
		if (adminCompany.toLowerCase().equals(userCompany.toLowerCase()) || adminCompany.equals(superCompanyName)) {
			if (adminRole.get(0).equals("ADMIN")) {
				Map<String,String> result = userdetailsservice.resetPasswordAsAdmin(userID); 
				if (result.get("Status").equals("Success")) {
					if(ticket_ID!=null && ticket_ID!="") {
						userdetailsservice.InsertAdminTicketIntoDB(ticket_ID, ticket_Notes, ticket_Context, userID, adminID);
					}
					responseMap.put("Status", "Success");
					return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
				}
				else {
					responseMap.put("Status", "Failed");
					responseMap.put("ErrorMessage", result.get("Error"));
					return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
				}
			} else {
				responseMap.put("Status", "Failed");
				responseMap.put("ErrorMessage", "You should be an Admin to perform this Operation");
				return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
			}
		} else {
			responseMap.put("Status", "Failed");
			responseMap.put("ErrorMessage", "Comp Diff");
			return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
		}
	}
	
	@RequestMapping("/changePassword")
	public ResponseEntity<Map<String, String>> changePassword(@RequestBody final Map<String, String> reqdata) {
		final Map<String, String> responseMap = new HashMap<>();
		boolean status = false;
		String ErrorMessage = null;
		String guserid = reqdata.get("userid");
		String goldpass = reqdata.get("oldPassword");
		String gnewpass = passwordEncoder.encode(reqdata.get("newPassword"));
		UserDetails userdetails = userdetailsservice.getUserDetailsByUserID(guserid);
		String vusername = userdetails.getUserName();
		User olddetails = userRepository.findUserByUserName(vusername);
		String voldpass = olddetails.getPassword();
		if (passwordEncoder.matches(goldpass, voldpass)) {
			boolean passwordResetStatus = userdetailsservice.updatePasswordByUserName(vusername, gnewpass);
			if (passwordResetStatus == true) {
				status = true;
			} else {
				status = false;
				ErrorMessage = "Error Changing Password. Please try again later";
			}
		} else {
			status = false;
			ErrorMessage = "Incorrect old password";
		}
		if (status == true) {
			responseMap.put("Status", "Success");
		} else {
			responseMap.put("Status", "Failure");
			responseMap.put("Error", ErrorMessage);
		}
		return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
	}

	@RequestMapping("/forgotPassword")
	public ResponseEntity<Map<String, String>> forgotPassword(@RequestBody final Map<String, String> reqdata) {
		final Map<String, String> responseMap = new HashMap<>();
		boolean status = false;
		String ErrorMessage = "";
		String context = reqdata.get("context");
		if (context.equals("ACCOUNTVERIFICATION")) {
			String gEmail = reqdata.get("emailID");
			UserDetails userdetails = userdetailsservice.getUserDetailsByEmailID(gEmail);
			if (userdetails != null) {
				String Name = userdetails.getFirstName() + " " + userdetails.getLastName();
				String VEmailID = userdetails.getEmail();
				String UserID = Integer.toString(userdetails.getUserId());
				Random rng = new Random();
				String token = Integer.toString(rng.nextInt(900000) + 100000);
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				df.setTimeZone(TimeZone.getTimeZone(Timezone));
				Date dateo = new Date();
				String CreatedDateTime = df.format(dateo);
				boolean tokeninsert = tokenService.insertNewTokenDetailsToDB(token, CreatedDateTime, "-1", UserID);
				if (tokeninsert == true) {
					String Subject = "RapidView Username/Password Reset Request";
					String Body = "Hi " + Name + ","
							+ "\r\n\r\nWe’ve received a change request for your RapidView account. "
							+ "To complete your request, enter the following verification code into the security field when asked.\r\n\r\n"
							+ token + "\r\n\r\n"
							+ "If this request did not come from you, change your account password immediately to prevent further unauthorized access.";
					emailService.sendemailcontroller(VEmailID, Subject, Body);
					status = true;
				} else {
					status = false;
					ErrorMessage = "Failed to Generate Token";
				}
			} else {
				status = false;
				ErrorMessage = "Email entered is not valid";
			}
		} else if (context.equals("TOKENVERIFICATION")) {
			String gEmail = reqdata.get("emailID");
			String gToken = reqdata.get("token");
			UserDetails userdetails = userdetailsservice.getUserDetailsByEmailID(gEmail);
			if (userdetails != null) {
				String VUserID = Integer.toString(userdetails.getUserId());
				List<TokenDBModel> tokendata = tokenService.getTokenDetailsBasedOnTokenID(gToken);
				if (tokendata.size() != 0) {
					if (tokendata.get(0).getUserID().equals(VUserID)) {
						boolean validatesession = tokenService.validateSessionByToken(tokendata.get(0).getTokenID());
						if (validatesession == true) {
							status = true;
						} else {
							status = false;
							ErrorMessage = "Failed to Validate Session";
						}
					}
				} else {
					status = false;
					ErrorMessage = "Entered Token is Invalid";
				}
			} else {
				status = false;
				ErrorMessage = "Email entered is not valid";
			}
		} else if (context.equals("FORGOTUSERNAME")) {
			String gToken = reqdata.get("token");
			List<TokenDBModel> tokendata = tokenService.getTokenDetailsBasedOnTokenID(gToken);
			if (tokendata.size() != 0) {
				if (tokendata.get(0).getIsUsed().equals("0")) {
					String VUserid = tokendata.get(0).getUserID();
					UserDetails userdetails = userdetailsservice.getUserDetailsByUserID(VUserid);
					String VName = userdetails.getFirstName() + " " + userdetails.getLastName();
					String VEmailID = userdetails.getEmail();
					String VUserName = userdetails.getUserName();
					String Subject = "RapidView Account Information Request";
					String Body = "Hi " + VName + "," + "\r\n\r\nWe’ve received a request for your RapidView account. "
							+ "Below is the Username that was set by you while creating the RapidView Account.\r\n\r\n"
							+ VUserName + "\r\n\r\n"
							+ "If this request did not come from you, change your account password immediately to prevent further unauthorized access.";
					emailService.sendemailcontroller(VEmailID, Subject, Body);
					boolean markTokenAsUsed = tokenService.markTokenAsUsedByToken(gToken);
					if (markTokenAsUsed == true) {
						status = true;
					} else {
						status = false;
						ErrorMessage = "Failed to process request, Please try again.";
					}

				} else {
					status = false;
					if (tokendata.get(0).getIsUsed().equals("-1")) {
						ErrorMessage = "Invalid Session, Please try again later.";
					} else {
						ErrorMessage = "Session Timed Out, Please try again.";
					}
				}

			} else {
				status = false;
				ErrorMessage = "Invalid Session";
			}
		} else if (context.equals("FORGOTPASSWORD")) {
			String gToken = reqdata.get("token");
			String gpassword = reqdata.get("password");
			String gpasswordencoded = passwordEncoder.encode(gpassword);
			List<TokenDBModel> tokendata = tokenService.getTokenDetailsBasedOnTokenID(gToken);
			if (tokendata.size() != 0) {
				if (tokendata.get(0).getIsUsed().equals("0")) {
					String VUserid = tokendata.get(0).getUserID();
					UserDetails userdetails = userdetailsservice.getUserDetailsByUserID(VUserid);
					String vusername = userdetails.getUserName();
					User olddetails = userRepository.findUserByUserName(vusername);
					String voldpass = olddetails.getPassword();
					if (passwordEncoder.matches(gpassword, voldpass)) {
						status = false;
						ErrorMessage = "The entered password is currently in use, please enter a new password.";
					} else {
						boolean passwordreset = userdetailsservice.updatePasswordByUserName(vusername,
								gpasswordencoded);
						if (passwordreset == true) {
							tokenService.markTokenAsUsedByToken(gToken);
							String VName = userdetails.getFirstName() + " " + userdetails.getLastName();
							String VEmailID = userdetails.getEmail();
							String Subject = "RapidView Password Change Alert";
							String Body = "Hi " + VName + ","
									+ "\r\n\r\nYour RapidView Password has been changed Successfully " + "\r\n\r\n"
									+ "If this was not you, change your account password immediately to prevent further unauthorized access.";
							emailService.sendemailcontroller(VEmailID, Subject, Body);
							boolean markTokenAsUsed = tokenService.markTokenAsUsedByToken(gToken);
							if (markTokenAsUsed == true) {
								status = true;
							} else {
								status = false;
								ErrorMessage = "Failed to process request, Please try again.";
							}
						} else {
							status = false;
							ErrorMessage = "Failed to reset Password, Try again Later";
							tokenService.markTokenAsUsedByToken(gToken);
						}
					}
				} else {
					if (tokendata.get(0).getIsUsed().equals("-1")) {
						ErrorMessage = "Invalid Session, Please try again later.";
					} else {
						ErrorMessage = "Session Timed Out, Please try again.";
					}
					status = false;
				}

			} else {
				status = false;
				ErrorMessage = "Invalid Session";
			}
		}
		if (status == true) {
			responseMap.put("Status", "Success");
		} else {
			responseMap.put("Status", "Failure");
			responseMap.put("Error", ErrorMessage);
		}
		return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
	}

	@Scheduled(initialDelay = 1000, fixedRate = 1 * 60 * 1000)
	public String invalidateTokens() throws ParseException {
		LOGGER.info("Running Invalidate Old Tokens Service");
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone(Timezone));
		Date dateo = new Date();
		String CurrentDateTime = df.format(dateo);
		Date datetimenow =df.parse(CurrentDateTime);
		List<TokenDBModel> opentokens = tokenService.getAllOpenTokens();
		for(int i=0; i<opentokens.size(); i++) {
			Date Tokendatetime = df.parse(opentokens.get(i).getCreatedDateTime());
			long difference_In_Time = datetimenow.getTime() - Tokendatetime.getTime();
			long difference_In_Time_In_Minutes = difference_In_Time/60000;
			String Token = opentokens.get(i).getTokenID();
			if(difference_In_Time_In_Minutes>30) {
				if(Token.length()<8) {
					tokenService.markTokenAsUsedByToken(opentokens.get(i).getTokenID());
				}
			}
		}
		return null;
	}
}
