package com.aiknights.com.rapidviewmain.controller;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aiknights.com.rapidviewmain.DAOImpl.ClaimNotificationDAOImpl;
import com.aiknights.com.rapidviewmain.models.ClaimNotification;
import com.aiknights.com.rapidviewmain.models.PaypalPaymentModel;
import com.aiknights.com.rapidviewmain.models.UserDetails;
import com.aiknights.com.rapidviewmain.models.WeatherAssessmentDataModel;
import com.aiknights.com.rapidviewmain.models.WeatherPaymentRequestModel;
import com.aiknights.com.rapidviewmain.service.RoleService;
import com.aiknights.com.rapidviewmain.service.UploadWeatherAssessmentDetailstoDBService;
import com.aiknights.com.rapidviewmain.service.UserDetailsService;

@RequestMapping("/api")
@Controller
public class WeatherReportController {
	@Autowired
	private UploadWeatherAssessmentDetailstoDBService uploadWeatherDataService;

	@Autowired
	private WeatherDBController weatherDBController;

	@Autowired
	private RoleService roleService;

	@Autowired
	private UserDetailsService userdetailsservice;

	String Timezone = "GMT";
	private static final Logger LOGGER = LoggerFactory.getLogger(ClaimNotificationDAOImpl.class);

	@RequestMapping(value = "/generateReport", method = RequestMethod.POST)
	public ResponseEntity<Map<String, String>> saveweatherassessmentdetails(
			@RequestBody final WeatherPaymentRequestModel weatherassessmentdetails) {

		final Map<String, String> responseMap = new HashMap<>();
		WeatherAssessmentDataModel weatherAssessmentDataModel = new WeatherAssessmentDataModel();
		weatherAssessmentDataModel = weatherassessmentdetails.getWeatherInfo();
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Timezone"));
		String Report_Received_Date = simpleDateFormat.format(new Date());
		System.out.println(Report_Received_Date);
		weatherAssessmentDataModel.setReport_Received_Date(Report_Received_Date);
		int uploadWeatherDetailsStatus = 0;
		weatherAssessmentDataModel.setStagingstatus("false");
		boolean IsReportIdExist = uploadWeatherDataService
				.getWeatherAssessmentDetailsByReportId(weatherAssessmentDataModel.getReportId());
		PaypalPaymentModel payPalPayment = weatherassessmentdetails.getPaymentInfo();

		List<PaypalPaymentModel> payPalPaymentList = new ArrayList<>();
		payPalPaymentList.add(payPalPayment);

		if (!(payPalPayment.getCreate_time() == null)) {
			try {
				byte[] paypalPaymentObject = getByteArrayObject(payPalPayment);
				uploadWeatherDataService.uploadpayPalPaymentInfo(payPalPayment, weatherAssessmentDataModel.getUserid(),
						paypalPaymentObject);
			} catch (Exception e) {

				LOGGER.error(e.getMessage());
			}

		}

		if (IsReportIdExist) {
			uploadWeatherDetailsStatus = 1;
			LOGGER.info("Weather Assessment Data exists for Report : " + IsReportIdExist);
		} else {
			uploadWeatherDetailsStatus = uploadWeatherDataService
					.UploadWeatherAssessmentDetails(weatherAssessmentDataModel);
		}
		if (uploadWeatherDetailsStatus == 1) {
			LOGGER.info("Weather Assessment data inserted into DB");
			boolean stageOutput = weatherDBController
					.stageWeatherDataByReportID(weatherAssessmentDataModel.getReportId());
			if (stageOutput == true) {
				uploadWeatherDataService.MarkWeatherStagingCompleted(weatherAssessmentDataModel.getReportId());
			}
			responseMap.put("WeatherStageStatus", Boolean.toString(stageOutput));
			responseMap.put("UploadStatus", "Success");
			return new ResponseEntity<>(responseMap, HttpStatus.OK);
		} else {
			responseMap.put("UploadStatus", "Failed");
			return new ResponseEntity<>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping("/getReportForUser/{userId}")
	public ResponseEntity<Map<String, List<WeatherAssessmentDataModel>>> getClaimsAssignedForUser(
			@PathVariable final int userId) {
		final Map<String, List<WeatherAssessmentDataModel>> responseMap = new HashMap<>();
		final List<WeatherAssessmentDataModel> weatherassessmentdetailsList = uploadWeatherDataService
				.getAssigedWeatherassessmentForUser(userId);
		responseMap.put("weatherassessmentdetailsList", weatherassessmentdetailsList);
		return new ResponseEntity<>(responseMap, HttpStatus.OK);
	}

	@GetMapping("/manageWeatherReports/{adminId}")
	public ResponseEntity<Map<String, List<WeatherAssessmentDataModel>>> manageWeatherReportsAsAdmin(
			@PathVariable final String adminId) {
		final Map<String, List<WeatherAssessmentDataModel>> responseMap = new HashMap<>();
		UserDetails data = userdetailsservice.getUserDetailsByUserID(adminId);
		String companyName = data.getcompanyName();
		String userName = data.getUserName();
		List<String> role = roleService.getRolesByUser(userName);
		if (role.get(0).equals("ADMIN")) {
			final List<WeatherAssessmentDataModel> weatherassessmentdetailsList = uploadWeatherDataService
					.manageWeatherReportsAsAdmin(companyName);
			responseMap.put("WeatherReports", weatherassessmentdetailsList);
			return new ResponseEntity<>(responseMap, HttpStatus.OK);
		} else {
			responseMap.put("You should be an Admin to view this Data", null);
			return new ResponseEntity<>(responseMap, HttpStatus.OK);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/getReportByReportID/{reportId}")
	public ResponseEntity<Map<String, ClaimNotification>> getClaimNotificationByClaimId(
			@PathVariable final String reportId) {
		final Map<String, WeatherAssessmentDataModel> weatherAssessmentDataMap = new HashMap<>();
		final WeatherAssessmentDataModel weatherAssessmentData = uploadWeatherDataService
				.getWeatherReportById(reportId);
		if (weatherAssessmentData == null) {
			weatherAssessmentDataMap.put("weatherReportData", null);
			return new ResponseEntity(weatherAssessmentDataMap, HttpStatus.NOT_FOUND);
		} else {
			weatherAssessmentDataMap.put("weatherReportData", weatherAssessmentData);
			return new ResponseEntity(weatherAssessmentDataMap, HttpStatus.OK);
		}
	}

	public byte[] getByteArrayObject(PaypalPaymentModel simpleExample) {

		byte[] byteArrayObject = null;
		try {

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(simpleExample);

			oos.close();
			bos.close();
			byteArrayObject = bos.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
			return byteArrayObject;
		}
		return byteArrayObject;
	}
}