package com.aiknights.com.rapidviewmain.dto;

/**
 * @author ARUN
 *
 */
public class RoleResultsDTO {

	private int roleid_pk;

	private boolean insertion_status;

	public int getRoleid_pk() {
		return roleid_pk;
	}

	public void setRoleid_pk(int roleid_pk) {
		this.roleid_pk = roleid_pk;
	}

	public boolean isInsertion_status() {
		return insertion_status;
	}

	public void setInsertion_status(boolean insertion_status) {
		this.insertion_status = insertion_status;
	}

	public boolean getInsertion_status() {
		return insertion_status;
	}

	@Override
	public String toString() {
		return "RoleResultsDTO [roleid_pk=" + roleid_pk + ", insertion_status=" + insertion_status + ", getRoleid_pk()="
				+ getRoleid_pk() + ", isInsertion_status()=" + isInsertion_status() + "]";
	}

}
