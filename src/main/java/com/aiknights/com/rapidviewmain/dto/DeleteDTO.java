package com.aiknights.com.rapidviewmain.dto;

public class DeleteDTO {

	private String claim_ID;
	private boolean Status;
	public String getClaim_ID() {
		return claim_ID;
	}
	public void setClaim_ID(String claim_ID) {
		this.claim_ID = claim_ID;
	}
	public boolean isStatus() {
		return Status;
	}
	public void setStatus(boolean status) {
		Status = status;
	}
	
	

}
