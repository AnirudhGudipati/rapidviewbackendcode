package com.aiknights.com.rapidviewmain.dto;

public class PolicyResultDTO {

private int policy_id_pk;
	
	private boolean insertion_status;

	public int getPolicy_id_pk() {
		return policy_id_pk;
	}

	public void setPolicy_id_pk(int policy_id_pk) {
		this.policy_id_pk = policy_id_pk;
	}

	public boolean isInsertion_status() {
		return insertion_status;
	}

	public void setInsertion_status(boolean insertion_status) {
		this.insertion_status = insertion_status;
	}
	

	public boolean getinsertion_status() {
		return insertion_status;
	}
	@Override
	public String toString() {
		return "PolicyResultDTO [policy_id_pk=" + policy_id_pk + ", insertion_status=" + insertion_status + "]";
	}

	
}
