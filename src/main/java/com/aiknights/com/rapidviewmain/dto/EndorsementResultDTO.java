package com.aiknights.com.rapidviewmain.dto;

public class EndorsementResultDTO {
	
	private int endorsement_id_pk;
	
	private boolean insertion_status;

	public int getEndorsement_id_pk() {
		return endorsement_id_pk;
	}

	public void setEndorsement_id_pk(int endorsement_id_pk) {
		this.endorsement_id_pk = endorsement_id_pk;
	}

	public boolean isInsertion_status() {
		return insertion_status;
	}

	public void setInsertion_status(boolean insertion_status) {
		this.insertion_status = insertion_status;
	}
	
	public boolean getinsertion_status() {
		return insertion_status;
	}

	@Override
	public String toString() {
		return "EndorsementResultDTO [endorsement_id_pk=" + endorsement_id_pk + ", insertion_status=" + insertion_status
				+ "]";
	}
	
	

}
