package com.aiknights.com.rapidviewmain.dto;

import java.util.Date;

public class ReportGenerationDTO {
	
	private String claimantName;
	
	private String policyNumber;

	private String address;

	private String claimNumber;

	private String serviceYears;

	private String typeOfClaim;

	private Date incidentDate;

	private Date incidentTime;

	private String policyCoverage;

	private Integer dudictable;

	private String claimAmount;

	private String payoutAmount;

	private String dateReceived;

	private String claimantContacted;

	private String dateInspected;

	private String dateRecorded;

	private String dateClosed;

	private String policyAssesment;

	private String structuralAssesment;

	private String weatherAssesment;

	private String incidentEvidenceAnalysis;

	private String contractorAssesment;

	private String overallRisk;
	
	private String insuranceCompany;
	
	

	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public String getClaimantName() {
		return claimantName;
	}

	public void setClaimantName(String claimantName) {
		this.claimantName = claimantName;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getClaimNumber() {
		return claimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}

	public String getServiceYears() {
		return serviceYears;
	}

	public void setServiceYears(String serviceYears) {
		this.serviceYears = serviceYears;
	}

	public String getTypeOfClaim() {
		return typeOfClaim;
	}

	public void setTypeOfClaim(String typeOfClaim) {
		this.typeOfClaim = typeOfClaim;
	}

	public Date getIncidentDate() {
		return incidentDate;
	}

	public void setIncidentDate(Date incidentDate) {
		this.incidentDate = incidentDate;
	}

	public Date getIncidentTime() {
		return incidentTime;
	}

	public void setIncidentTime(Date incidentTime) {
		this.incidentTime = incidentTime;
	}

	public String getPolicyCoverage() {
		return policyCoverage;
	}

	public void setPolicyCoverage(String policyCoverage) {
		this.policyCoverage = policyCoverage;
	}

	public Integer getDudictable() {
		return dudictable;
	}

	public void setDudictable(Integer dudictable) {
		this.dudictable = dudictable;
	}

	public String getClaimAmount() {
		return claimAmount;
	}

	public void setClaimAmount(String claimAmount) {
		this.claimAmount = claimAmount;
	}

	public String getPayoutAmount() {
		return payoutAmount;
	}

	public void setPayoutAmount(String payoutAmount) {
		this.payoutAmount = payoutAmount;
	}

	public String getDateReceived() {
		return dateReceived;
	}

	public void setDateReceived(String dateReceived) {
		this.dateReceived = dateReceived;
	}

	public String getClaimantContacted() {
		return claimantContacted;
	}

	public void setClaimantContacted(String claimantContacted) {
		this.claimantContacted = claimantContacted;
	}

	public String getDateInspected() {
		return dateInspected;
	}

	public void setDateInspected(String dateInspected) {
		this.dateInspected = dateInspected;
	}

	public String getDateRecorded() {
		return dateRecorded;
	}

	public void setDateRecorded(String dateRecorded) {
		this.dateRecorded = dateRecorded;
	}

	public String getDateClosed() {
		return dateClosed;
	}

	public void setDateClosed(String dateClosed) {
		this.dateClosed = dateClosed;
	}

	public String getPolicyAssesment() {
		return policyAssesment;
	}

	public void setPolicyAssesment(String policyAssesment) {
		this.policyAssesment = policyAssesment;
	}

	public String getStructuralAssesment() {
		return structuralAssesment;
	}

	public void setStructuralAssesment(String structuralAssesment) {
		this.structuralAssesment = structuralAssesment;
	}

	public String getWeatherAssesment() {
		return weatherAssesment;
	}

	public void setWeatherAssesment(String weatherAssesment) {
		this.weatherAssesment = weatherAssesment;
	}

	public String getIncidentEvidenceAnalysis() {
		return incidentEvidenceAnalysis;
	}

	public void setIncidentEvidenceAnalysis(String incidentEvidenceAnalysis) {
		this.incidentEvidenceAnalysis = incidentEvidenceAnalysis;
	}

	public String getContractorAssesment() {
		return contractorAssesment;
	}

	public void setContractorAssesment(String contractorAssesment) {
		this.contractorAssesment = contractorAssesment;
	}

	public String getOverallRisk() {
		return overallRisk;
	}

	public void setOverallRisk(String overallRisk) {
		this.overallRisk = overallRisk;
	}
	
	

	
	
	
	
	

}
