package com.aiknights.com.rapidviewmain.dto;

public class UserDetailsResultsDTO {

	private int userid_pk;

	private boolean insertion_status;

	public int getUserid_pk() {
		return userid_pk;
	}

	public void setUserid_pk(int userid_pk) {
		this.userid_pk = userid_pk;
	}

	public boolean isInsertion_status() {
		return insertion_status;
	}

	public void setInsertion_status(boolean insertion_status) {
		this.insertion_status = insertion_status;
	}

	public boolean getInsertion_status() {
		return insertion_status;
	}

	@Override
	public String toString() {
		return "UserDetailsResultsDTO [userid_pk=" + userid_pk + ", insertion_status=" + insertion_status
				+ ", getUserid_pk()=" + getUserid_pk() + ", isInsertion_status()=" + isInsertion_status() + "]";
	}

}
