package com.aiknights.com.rapidviewmain.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.server.RequestUpgradeStrategy;
import org.springframework.web.socket.server.standard.TomcatRequestUpgradeStrategy;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfiguration implements WebSocketMessageBrokerConfigurer {

	@Value("${rapid.security.origin}")
	String origin;

	@Override
	public void registerStompEndpoints(StompEndpointRegistry
			registry) {
		RequestUpgradeStrategy upgradeStrategy = new TomcatRequestUpgradeStrategy();
		registry.addEndpoint("/mywebsockets")
		.setHandshakeHandler(new DefaultHandshakeHandler(upgradeStrategy))
		.setAllowedOrigins(origin).withSockJS();
	}

	@Override
	public void configureMessageBroker(MessageBrokerRegistry config){ 
		config.enableSimpleBroker("/topic/");
		config.setApplicationDestinationPrefixes("/app");
	}
}